.. _developer:

***************
Developer Guide
***************

.. topic:: Preface

   This part of the documentation is intended for developers who want
   to contribute to the project or just look at the code base.



.. _itm_package:

The ITM CLI Package
===================
The source code for the ITM Command Line interpreter is located in the
control software machine (10.10.18.19), under the
*katyusha/src/python/itm_cli* directory::

   $ ls ~/katyusha/src/python/itm_cli
   __init__.py  itm_cli  setup.py

The *setup.py* file is the one you have to edit in case you want
to customize the package installation.  It defines a class
``ITMInstall`` and its method ``ITMInstall.run()``, used
to change some file permissions and to copy the configuration files
to the desired directory.

The package has the following structure::

   $ ls itm_cli/
   catalog  cli.py  commands  config  __init__.py  utils.py

The *catalog* directory contains all catalogs, the *commands* directory
all commands, and the *config* directory the configuration files.
Let's see how to write a command.



.. _observation_command:

How to write an observation command
-----------------------------------
In this section we will see each step required in order to write
a new command.  For the sake of simplicity, let's write an easy
command, for instance ``stop``.  The procedure is always the same,
whether the command is easy or very long and complicated.
You have to accomplish only two steps:

#. write a method ``do_stop()`` in the *cli.py* file. Here you have to
   write the command documentation and the arguments it takes
#. create a new file *commands/stop.py* that implements the logic of
   the command.

The method ``do_stop()`` is the following:

.. code-block:: python

   def do_stop(self, line):
       """Stop the mount.

       > stop
       """
       try:
           self.call_script('stop')
       except Exception, ex:
           print(ex)


As you can see there is a docstring that documents the command, and after
there is the call to ``self.call_script('stop')``, which executes the script
*commands/stop.py*.  In case the command is called ``foo``, than you
have to create a file *command/foo.py* and a method ``do_foo()`` inside
the file *cli.py*.  The ``do_foo()`` method has to call ``self.call_script('foo')``.

In this case the docstring is really easy.  Let's see one more elaborated, for
instance the one of the ``goto`` command:

.. code-block:: python

   def do_goto(self, line):
       """Take the source name as argument and go to the source.

       Example:

       > goto source=canopus

       You can also specify the altitude offset and the azimuth offset.
       For instance, in the following case we add to the Canopus coordinates
       a 0.02 altitude offset and a 0.03 azimuth offset:

       > goto source=canopus alt_offset=0.02 az_offset=0.03
       """
       command_name = 'goto'
       allowed_args = {
           'source': ArgumentType(str, required=True),
           'alt_offset': ArgumentType(float, required=False, default=0),
           'az_offset': ArgumentType(float, required=False, default=0),
       }
       try:
           args = self.check_args(command_name, line, allowed_args)
           self.save_args(command_name, args)
           self.call_script(command_name)
       except Exception, ex:
           print(ex)


Once we wrote the ``do_stop()`` method, we write the following
*command/stop.py* script:

.. code-block:: python

   #!/usr/bin/python
   import time
   from itm_cli.utils import RTS2Wrapper


   class Stop(RTS2Wrapper):

       def run(self):
           self.setValue('Mount', 'Stand-by', 'T0')
           self.setValue('Mount', 'STOP', 'T0')
           self.setValue('point', 'false', 'T0')


   if __name__ == '__main__':
       script = Stop()
       script.run()

As you can see it is really easy.  You have to inherit from the class
``RTS2Wrapper`` and define the method ``run()``.  The call to
``RTS2Wrapper.setValue()`` interacts with RTS2, the telescope control
framework, and sets a value.

..
  .. _itm_cli_api:
  
  ITM Command Line APIs
  =====================
  This part of the documentation covers the ITM CLI APIs.  For
  parts where ITM CLI depends on external libraries, we document the most
  important right here and provide links to the canonical documentation.
  
  
  ``ITMShell`` class of the ``cli`` module
  ----------------------------------------
  
  .. module:: itm_cli.cli
  
  .. autoclass:: ITMShell
     :members:
     :inherited-members:
