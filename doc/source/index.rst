#################
ITM Documentation
#################

.. topic:: Preface

   The International Telescope Maffei (ITM), also known as
   International Robotic Antarctic Infrared Telescope (IRAIT), is an
   altazimuth mount Nasmyth telescope, with 800mm of aperture and about
   17m of focal length (f/21.165).
   It currently operates only on the multi-band optical spectrum, performing
   AGB (Asymptotic Giant Branch Stars), exoplanets, AGN (Active Galactic
   Nucleus), and on-alert GRB observations. ITM is financed by the Clément
   Fillietroz ONLUS Foundation, and managed by the Osservatorio Astronomico
   della Regione Autonoma Valle d'Aosta.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   user.rst
   maintenance.rst
   developer.rst
   team.rst
