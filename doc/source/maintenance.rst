.. include:: roles.txt

.. _maintenance:

*****************
Maintenance Guide
*****************

.. sectionauthor:: :ref:`abau`, :ref:`mbuttu`


.. topic:: Preface

   This chapter describes in details all electrical boxes,
   and the connections between them.  Each box has its section
   with a picture and a list of all devices hosted in the box.
   There are also devices outside the boxes, like an IP camera
   that visualizes the dome, the engines, and the telescope
   guide camera.  A the end of the chapter there is a network schema.



.. _box0:

Box n.0
=======
This box is located in the shelter, in the room on the right of the
main entrance (just beside the mechanical workshop).
The box is at the end of the room, on the left side.
It is not termal insulated, because the room is at about 5 degrees.
:numref:`box0_fig` is a picture of the box.

.. _box0_fig:

.. figure:: images/boxes/box0_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.0. :download:`Click here <images/boxes/box0.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Power supply 24V DC

#. :ref:`abb_plc_master`

#. :ref:`moxa_box0`

#. Differential gear switch

#. Serial lines from the :ref:`moxa_box0` to the :ref:`abb_plc_master`

#. Cables CS-321 (ABB proprietary), that allow the :ref:`abb_plc_master` to
   communicate with the :ref:`abb_plc_slaves` (via modbus protocol)

#. Power input AC 230V

#. Cooling fan

#. Ethernet cable that connects the :ref:`moxa_box0` and the :ref:`switch_shelter`.


This box is connected to the :ref:`abb_plc_slaves` of :ref:`box2`
(CS-321 cables, modbus protocol) and via ethernet to the :ref:`switch_shelter`.


.. _box1:

Box n.1
=======
In the gound floor af the telescope there are two boxes close to the azimuth
derotator: the bigger one is the box n.2-3, and the other one is the n.1,
which has a big heat sink. :numref:`box1_fig` is a picture of the box.
This box is thermal insulated, and thermal regulated as all boxes in the
telescope: there is a cooling fan in the cover of the box (not shown in
:numref:`box1_fig`) and three heaters.

.. _box1_fig:

.. figure:: images/boxes/box1_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.1. :download:`Click here <images/boxes/box1.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. :ref:`phytron` module, which drives the power of azimuth and altitude engines

#. :ref:`galil`, for the :ref:`phytron` control software, connected via
   ethernet to the :ref:`switch_box2`.

#. :ref:`io_galil_card`

#. Sensor temperature n.1 (PT-100)

#. Sensor temperature n.2 (PT-100)

#. Sensor temperature n.3 (PT-100)

#. Heaters

About the connections with the other boxes: the power comes from the
:ref:`box3`, and the :ref:`galil` is connected via ethernet to the
:ref:`switch_box2`.

.. _box2:

Box n.2
=======
In the gound floor af the telescope there are two boxes close to the azimuth
derotator: the bigger one is the box n.2-3 (two separate boxes
with the same cover), and the box n.2 is the one on the left.
:numref:`box2_fig` is a picture of the box.
The box is thermal insulated and thermal regulated:
there is a cooling fan in the cover of the box (not shown in
:numref:`box2_fig`) and two heaters, but only the one in the top is active.

.. _box2_fig:

.. figure:: images/boxes/box2_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.2. :download:`Click here <images/boxes/box2.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. :ref:`abb_plc_slaves`

#. :ref:`moxa_box2` used to communicate via ethernet to the :ref:`multimeter`

#. :ref:`multimeter`

#. Power supply 12V for :ref:`moxa_box2` end ethernet switch

#. Power supply 24V for the :ref:`abb_plc_slaves`

#. :ref:`switch_box2`

#. Sensor temperature (PT-100) n.1

#. Sensor temperature (PT-100) n.3

#. Thermostat that controls the heater

#. Heater

#. Glicole circuit, disabled.

#. Power supply for all :ref:`abb_plc_slaves`

#. Tele-switch (teleruttore) C9-17: activates the :ref:`slipring` heater

#. Tele-switch (teleruttore) C16-03: activates the heater of azimuth and altitude engines

The power comes from the :ref:`box3`.


.. _box3:

Box n.3
=======
In the gound floor af the telescope there are two boxes close to the azimuth
derotator: the bigger one is the box n.2-3 (two separate boxes
with the same cover), and the box n.3 is the one on the right.
:numref:`box3_fig` is a picture of the box.
The box is thermal insulated and thermal regulated:
there is a cooling fan in the cover of the box (not shown in
:numref:`box3_fig`) and three heaters.

.. _box3_fig:

.. figure:: images/boxes/box3_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.3. :download:`Click here <images/boxes/box3.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Differential gear switch M20-1: activates heaters and fans of :ref:`box1`, :ref:`box2`, :ref:`box3`

#. Differential gear switch M20-2: activates power for :ref:`box4`

#. Differential gear switch M20-3: activates power for :ref:`abb_plc_slaves`,
   :ref:`moxa_box2`, and :ref:`io_galil_card` (M20-3)

#. Differential gear switch M20-4: activates :ref:`tiltimeter`

#. Differential gear switch M40-1: activates :ref:`galil` and :ref:`phytron`

#. Differential gear switch M63-1: activates the power of this box

#. Differential gear switch M63-2: you can not see it because it is hidden; it
   was used in the past for the power of AMICA, and it is currently disabled

#. Tele-switch C9-01: enables heater 1 of :ref:`box1`

#. Tele-switch C9-02: enables heater 1 of :ref:`box2`

#. Tele-switch C9-03: enables heater 1 of :ref:`box3`

#. Tele-switch C9-04: enables heater 2 of :ref:`box1`

#. Tele-switch C9-05: enables heater 2 of :ref:`box2`

#. Tele-switch C9-06: enables heater 2 of :ref:`box3`

#. Tele-switch C9-07: enables the fans of :ref:`box1`, :ref:`box2` and :ref:`box3`

#. Tele-switch C9-14: inactive

#. Tele-switch C9-15: inactive

#. Tele-switch C9-16: inactive

#. Tele-switch C16-02: enables the power of :ref:`box4`, in series with M20-2

#. Tele-switch C40-01: inactive (it was used for AMICA)

#. Tele-switch C9-11: enables the heater for the encoder azimuth

#. Tele-switch C16-01: enables the :ref:`phytron`

#. Tele-switch C9-10: enables the :ref:`galil`

#. Temperature sensor n.1 (PT-100)

#. Temperature sensor n.2 (PT-100)

#. Temperature sensor n.3 (PT-100)

#. Tele-switch C9-08: enables the :ref:`tiltimeter` (you can not see it because it is hidden)

#. Tele-switch C9-09: enables the :ref:`multimeter` (you can not see it because it is hidden)

The power comes from the :ref:`boxBT`.

.. _box4:

Box n.4
=======
The box n.4 is located in the first floor of the telescope house.
:numref:`box4_fig` is a picture of the box.

.. _box4_fig:

.. figure:: images/boxes/box4_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.4. :download:`Click here <images/boxes/box4.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Differential gear switch

#. :ref:`evco_plc` that controls the temperature of this box

#. Power supply for the :ref:`telescope_control_software_computer`

#. Power supply for the :ref:`evco_plc`

#. Power supply for the :ref:`m2_driver_engine`

#. UPS for everything but the :ref:`m2_driver_engine`

#. Heater with fan

#. Fan

#. :ref:`moravian`

#. :ref:`telescope_control_software_computer`

#. :ref:`m2_driver_engine`

#. Switch ethernet

#. Temperature sensor

#. Switch to turn on the heater that warms the M2 actuator.

.. todo:: In February you have to turn on the heater of this box (there
   is a switch) and add the insulation too, because we removed it during
   the summer campaign.

The power comes from the :ref:`box3` (switch M20-2).


.. _box5:

Box n.5
=======
The box n.5 is located in the ground flor of the telescope house, just in
front of the right entrance door.  :numref:`box5_fig` is a picture of the box.

.. _box5_fig:

.. figure:: images/boxes/box5_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.5. :download:`Click here <images/boxes/box5.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Transformer 230-24V AC.

#. :ref:`evco_plc` that controls the dome final-limits, the sensor temperature of
   the box and of the dome engines

#. Differential gear switch for: engines, Tele-switches (teleruttori), heaters, PLC,
   camera IP dome

#. Tele-switch (teleruttore) to open and close the dome

#. Save-engines that turn off the engines in case of over current

#. Fan

#. Heater


:download:`Here <images/boxes/box5_details.jpg>` is a picture of the cover of
the box, where you can find some details about the connections.

The power comes from :ref:`boxBT`.


.. _box6:

Box n.6
=======
The box n.6 is located in the first floor of the telescope house.
:numref:`box6_fig` is a picture of the box.

.. _box6_fig:

.. figure:: images/boxes/box6_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box n.6. :download:`Click here <images/boxes/box6.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. :ref:`ups_box6`

#. Switch ethernet

#. :ref:`modem_wifi_box6`

#. Power supply for the :ref:`evco_plc`

#. :ref:`evco_plc` that controls the temperature of this box, of the :ref:`boxCCD`, of
   the dome, the CCD on-off, the :ref:`encoder_reader` on-off, and the :ref:`modem_wifi_box6`
   on-off

#. Heater

#. Fan

#. :ref:`moxa_box6`

#. Power supply for :ref:`moxa_box6`, :ref:`modem_wifi_box6` and switch ethernet

#. :ref:`encoder_reader`

#. Cooling fan

#. Temperature sensor

The power comes from :ref:`box7`.


.. _box7:

Box n.7
=======
The box n.7 is located in the ground floor of the telescope house.
:numref:`box7_fig` is a picture of the box.

.. _box7_fig:

.. figure:: images/boxes/box7_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box 7. :download:`Click here <images/boxes/box7.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Differential gear switch that provides the power to the :ref:`box6`.

The power comes from :ref:`box3`.


.. _boxCCD:

Box CCD
=======
The box n.6 is located in the first floor of the telescope house.
:numref:`boxCCD_fig` is a picture of the box.

.. _boxCCD_fig:

.. figure:: images/boxes/boxCCD_numbered.jpg
   :figwidth: 100%
   :align: center

   Picture of box CCD. :download:`Click here <images/boxes/boxCCD.jpg>` to get an
   high resolution picture of the box.

The box hostes the following devices:

#. Differential gear switch for the Nashmit focus heater

#. Heater with fan

#. Heating plate

#. Fan

#. USB cable that arrives from the :ref:`moravian`

#. Temperature sensor

#. Power supply for the CCD

The power comes from :ref:`box7`.

.. _boxBT:

Box BT
=======
This box is located in the ground floor of the telescope house, just in front
of the left entrance door.  :numref:`boxBT_fig` is a picture of the box.

.. _boxBT_fig:

.. figure:: images/boxes/boxBT_resized.jpg
   :figwidth: 100%
   :align: center

   Picture of box BT. :download:`Click here <images/boxes/boxBT.jpg>` to get an
   high resolution picture of the box.

This box is in charge to the BT.  The switch Q6 (or Q7) is for the input power
of :ref:`box3`

.. _temperatures:

Temperatures of the boxes
=========================
To see the temperatures of the boxes, :ref:`start_up_webapp` and open the
**CLIMA** section.  You will see the temperature information of all boxes,
as illustrated in :numref:`webapp_temperatures_fig`:

.. _webapp_temperatures_fig:

.. figure:: images/webapp_temperatures.png
   :figwidth: 100%
   :align: center

   The **CLIMA** page in the web application.

Sometimes the date related to the temperatures is not updated.  In that case
restart the :ref:`shelter_computer`:

#. :ref:`login_shelter_machine`
#. Execute the command: ``sudo shutdown -r now``

.. todo:: Currently to solve this problem we are restarting the
   :ref:`shelter_computer`.  We need to look for the source of the problem, in order to
   find an easier solution (maybe we should just restart ``graphite``).

If you click on a box, you will see a plot of the temperature of the box over
the time.  For instance, :numref:`webapp_plot_temperatures_fig` shows the plot for
the :ref:`box2`.

.. _webapp_plot_temperatures_fig:

.. figure:: images/webapp_plot_temperatures.png
   :figwidth: 100%
   :align: center

   In the web application you can see the temperature of a box over the time.

In case the plot is frozen and does not update any temperature, restart the
:ref:`shelter_computer` as explained just a few lines above.

.. _cloning:

Control system hard disk cloning
================================
To clone the hard disk of the control system machine (10.10.18.19),
take the FitPC disk from the drawer on the right of the IRAIT desktop
(on the shelter) and the USBtoSATA converter located inside the drawer of the
ASTEP desktop (on the left of the IRAIT desktop).  Attach the disk to the
FitPC on :ref:`box4` and from a remote computer
:ref:`login_cs_machine` and execute the following command::

    $ sudo dd if=/dev/sda of=/dev/sdb bs=4096 conv=notrunc,noerror

Wait until it completes the operation and after you can unmount the disk
and put it again on the shelter.


.. _interact_database:

How to interact with the database
=================================
The web application uses the database to get the telescope control software data.
As reported in :ref:`start_up_webapp`, the script
:filename:`telescope_check.py` (executed in the
`telescope_control_software_computer`) gets the data from the control software,
and saves them in the database.  The web application reads the data from the
database.  Sometimes you want to see the data directly from the database, maybe
because you have one software problem and it helps you to better understand
what is going wrong.

To login into the database:

.. code-block:: bash

   $ mysql -u root -p  # Password is 'irait'

You can see all available databases with the command ``show databases``:

.. code-block:: sql

   > show databases;
   +--------------------+
   | Database           |
   +--------------------+
   | information_schema |
   | irait              |
   | irait_control      |
   | mysql              |
   | performance_schema |
   | redmine_default    |
   +--------------------+
   6 rows in set (0.00 sec)


The database used by the web application is ``irait_control``:

.. code-block:: sql

   > use irait_control;
   Reading table information for completion of table and column names
   You can turn off this feature to get a quicker startup with -A

   Database changed

Now that we have selected the ``irait_control`` database, we can see
its tables:

.. code-block:: sql

   > show tables;
   +-------------------------+
   | Tables_in_irait_control |
   +-------------------------+
   | active_element          |
   | active_element_box      |
   | active_element_element  |
   | active_element_mtm      |
   | authorities             |
   | box                     |
   | commands                |
   | element                 |
   | element_box             |
   | events                  |
   | maintenance             |
   | mtm                     |
   | plc                     |
   | plc_box                 |
   | tel_el_write            |
   | tel_element             |
   | users                   |
   | warehouse               |
   +-------------------------+
   18 rows in set (0.00 sec)

You can see the database schema using the ``describe`` command:

.. code-block:: sql

   > describe box;
   +------------------+---------------+------+-----+---------+----------------+
   | Field            | Type          | Null | Key | Default | Extra          |
   +------------------+---------------+------+-----+---------+----------------+
   | ID               | int(11)       | NO   | PRI | NULL    | auto_increment |
   | name             | varchar(20)   | NO   |     | NULL    |                |
   | image            | varchar(255)  | YES  |     | NULL    |                |
   | status           | int(5)        | NO   |     | NULL    |                |
   | last_update      | datetime      | NO   |     | NULL    |                |
   | description      | varchar(255)  | YES  |     | NULL    |                |
   | temperature      | decimal(10,1) | YES  |     | NULL    |                |
   | enabled          | tinyint(1)    | NO   |     | NULL    |                |
   | associated_graph | varchar(255)  | YES  |     | NULL    |                |
   +------------------+---------------+------+-----+---------+----------------+
   9 rows in set (0.00 sec)

If you want to see all data from the ``box`` table:

.. code-block:: sql

   > select * from box;

If you want to delete one entry, for instance the one with ``id=50``:

.. code-block:: sql

   > delete from box where id=50;


Database backup
---------------
To save a dump of the database, use the ``mysqldump`` command.  For instance,
in the following case we save a dump file of the database ``irait_control``:

.. code-block:: bash

   $ mysqldump -u root -p irait_control > ~/bkp/sql/irait_control_20180120.sql

To load a dump file:

.. code-block:: sql

   > use irait_control;
   > irait_control < file_dump_name


.. _devices:

Devices
=======
In this section you will find all telescope devices, either included in a
box or not.


.. _telescope_control_software_computer:

Telescope control software computer
-----------------------------------
The telescope control software computer (aka *fitPC*) is located in
:ref:`box4`, and has IP address ``10.10.18.19``.  It has installed
the ``rts2`` control software and all python script required for observing
with ITM.  There is also the *modbus* server to deal with the
:ref:`abb_plc_master`, and the TCP2Serial server to driver the M2 actuator.
User and password for this machine are: *irait* and *irait*.



.. _shelter_computer:

Shelter computer
----------------
This computer is located in the shelter, has IP
address ``10.10.18.16`` and alias ``irait.concordiastation.aq``.
This machine provides a dual boot with Windows 7 and Ubuntu 14.10.
In the Ubuntu OS are installed:

* ITM web application working on Tomcat8 and MySQL
* database for web application
* deamons for the web application support (cummunication between DB and PLCs)
* a Windosw XP VM with the software for configuring the PLCs (:ref:`abb_plc_master` and :ref:`evco_plc`),
  the ``SIPS`` program to interact with the :ref:`moravian` CCD cameras.
* a full repository of Ubuntu 14.10.
* server Git hosting the repositories of ``rts2`` (IRAIT customization) and
  ``katyusha`` (python scripts)
* Icinga (hw checks) and Graphite (temperature charts)
* xEphem
* Mediawiki
* ``upsmon`` to check :ref:`ups_box6` state. Its data are displayed by
  the web application.

User and password for this machine are: *irait* and *irait*.

.. note:: When used in *dual monitor* configuration sometimes the user
   interface blocks and needs to be restarted.  This machine has to be always
   running in order to have the web application on-line.


.. _abb_plc_master:

ABB PLC master
--------------
The ABB PLC master, illustrated in :numref:`abb_plc_master_fig`,
is hosted in the :ref:`box0`.

.. _abb_plc_master_fig:

.. figure:: images/boxes/abb_plc_master_numbered.jpg
   :figwidth: 100%
   :align: center

   The ABB PLC master hosted in :ref:`box0`.

It has two RS-485 ports, connected to the :ref:`moxa_box0`: the port
**1** is used to program the PLC, and the **2**
for the modbus communication.  This PLC master has some :ref:`abb_plc_slaves`, all
hosted in :ref:`box2`.  The :ref:`telescope_control_software_computer`
asks the PLC master the temperatures, via modbus, and the master forwards the
request to the slaves.  The communication between master and slaves
is done via three proprietary CS31 ABB cables, reported in :numref:`abb_plc_master_fig`
as number **3**.  If you want to know which temperatures the master is reading, see
section :ref:`abb_plc_slaves`.


How to program the ABB PLC master
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The ABB PLC has a software to program it, called *AC1131*.  This software is installed
in a VM hosted in the :ref:`shelter_computer`.  That software is already
configured to communicate to the ABB PLC master.  There is also a version of
this software installed in the notebook located in the shelter warehouse. 

You can access to the program via the Desktop icon called :filename:`ABB PLC Program`.
In the Desktop there is also a directory called :filename:`PRJ_ABB_ac1131`.
All projects are inside that directory, named in chronological order.
The one currently installed is :filename:`DomeC_2017_01_09`.  There is also
a newer project (:filename:`DomeC_2017_01_09`), created by :ref:`ydepra` but not yet tested.

Once you open the project, if you select the last function, ``PLC_PRG (PRG)``, the
PLC schema will appear on the right.  Each one of the other functions represent a
single PLC block.  The ``QX`` are digital outputs that activate some counters
(relès).  The ``IX`` are digital inputs used to read the counters status.
The ``IW`` are analog inputs from the PT-100 temperature sensors (3 wires).
``MX`` are the internal ABB variables: you can read them via modbus.
The blocks that start with the capital **D** want to *disable* something, while
the blocks that start with **FF** want to *force* something.

Using ``rts2-mon``, with ``S2-Control`` you can disable some relès.  In that case,
if you want to go back to the original configuration just restart the PLC
master (putting down and up again the differential gear switch).

The :ref:`abb_plc_slaves` do not need any programming: you just have to set the
*dipswitch*, opening the PLC side window and giving the dipswitch
the same address of the one you are dismissing.


.. _abb_plc_slaves:

ABB PLC slaves
--------------
The ABB PLC slaves, illustrated in :numref:`abb_plc_slaves_fig`,
are hosted in the :ref:`box2`.

.. _abb_plc_slaves_fig:

.. figure:: images/boxes/abb_plc_slaves_numbered.jpg
   :figwidth: 100%
   :align: center

   The ABB PLC slaves, hosted in :ref:`box2`.

The three PLC slaves on the top, indicated with the number **1**,
have analog I/O and control the temperature of boxes 1, 2 and 3.
They also control the temperature of the :ref:`tiltimeter`, of the
:ref:`slipring`, and eventually of the azimuth and altitude engines.

The two PLC slaves on the bottom, indicated with the number **2**,
have digital I/O and control the Tele-switches of :ref:`box2` and
:ref:`box3`.

As we told before, remember  the ABB PLC slaves do not need any programming:
you just have to set the *dipswitch*, opening the PLC side window and giving
the dipswitch the same address of the one you are dismissing.


.. _moxa_box0:

Moxa box n.0
------------
The moxa hosted in the :ref:`box0` has IP address ``10.10.18.40``.  It allows
us to communicate, via ethernet, to the :ref:`abb_plc_master` which does not
have ethernet ports but serial RS-485.
The Moxa takes in input the ethernet (coming from the switch in the shelter),
and has two outputs which are the PLC serial inputs: one is used to get the data
from the :ref:abb_plc_master`, the other one is used for programming the PLC.
The telescope control software sees the two serial ports as Virtual COM.

.. _moxa_box2:

Moxa box n.2
------------
The moxa hosted in the :ref:`box2` has IP address ``10.10.18.42``.  It allows
us to communicate, via ethernet, to the :ref:`multimeter` which does not
have ethernet ports but serial RS-485.
The Moxa takes in input the ethernet coming from the switch in the :ref:`box2`.

.. _moxa_box6:

Moxa box n.6
------------
The moxa hosted in the :ref:`box6` has IP address ``10.10.18.41``.  It allows
us to communicate, via ethernet, to the UPS and :ref:`evco_plc` of this box.

.. _restart_moxa_box6:

Restart moxa :ref:`box6`
~~~~~~~~~~~~~~~~~~~~~~~~
To restart the moxa in :ref:`box6`, open a browser and go to the address
``10.10.18.41``.  You will see the page illustrated in :numref:`moxa_box_6_save_restart_fig`.

.. _moxa_box_6_save_restart_fig:

.. figure:: images/moxa_box_6_save_restart.png
   :figwidth: 100%
   :align: center

   The control panel of the moxa hosted in :ref:`box6`

Now open the section **save and restart** in order to restart the moxa.



.. _encoder_reader:

Encoder reader of altitude and azimuth engines
----------------------------------------------
This (Heidenhain) encoder reader is hosted in :ref:`box6`, and has IP address
``10.10.18.105``.


.. _tiltimeter:

Tiltimeter
----------
The titltimeter is a device that measures the tilt of the azimuth plane.


.. _multimeter:

Multimeter
----------
The :ref:`multimeter` (aka MTME) is a device hosted in :ref:`box2` that reads
the 3-phase voltage and current values.  It has a RS-485 serial port in input,
so to we use the :ref:`moxa_box2` to communicate remotely with it.


.. _slipring:

Slipring
--------
The slipring is the rotator azimuth cilinder.


.. _phytron:

Phytron module
--------------
The Phytron module, located in :ref:`box1`, drives the power of azimuth
and altitude engines.


.. _galil:

Galil
-----
The Galil is the computer with the Phytron control software, and has
IP address ``10.10.18.50``.  It is located in :ref:`box1` and connected
via ethernet to the switch in :ref:`box2`.

.. _io_galil_card:

I/O Galil card
--------------
This card takes in input the altitude and azimuth final-limits, the motor torques, ecc.
It is connected via ethernet to the switch of :ref:`box2`.

.. _evco_plc:

EVCO PLC
--------

How to program the EVCO PLC
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The EVCO PLC has a software to program it, called *UNI PRO 3*.
This software is installed in a VM hosted in the :ref:`shelter_computer`.
That software is already configured to communicate to the ABB PLC master.
There is also a version of this software installed in the notebook located
in the shelter warehouse.

In the Desktop there is a directory called :filename:`PRJ_evco_unipro`.
Once you modify the program, compile it, connect to the PLC using the USB
cable and click on the blue arrow.  You do not have to connect the PLC to
power, because it is provided by the USB.


.. _m2_driver_engine:

M2 driver engine
----------------
This is the device that drives the second mirror engine.  The engine moves
an actuator joined with the second mirror (M2).


.. _moravian:

Moravian
--------
The Moravian is an industrial computer that controls the Moravian CCD cameras.
It has IP address ``10.10.18.21``, and you can access it with:

.. code-block:: bash

   $ ssh -p 11011 root@10.10.18.21  # password is culo

The only important thing of this machine is the deamon that controls the communication
to the camiera via USB.  If it is not alive, :ref:`restart_moravian`.


.. _modem_wifi_box6:

Modem wifi :ref:`box6`
----------------------
This modem, hosted in :ref:`box6`, has IP address ``10.10.18.61``, and is
connected via ethernet to the :ref:`switch_box6`, and via wifi to the
:ref:`switch_shelter`.


.. _switch_shelter:

Switch in the shelter
---------------------
The ethernet switch in the shelter connected to the :ref:`shelter_computer`,
to the :ref:`wifi_shelter`, to the :ref:`moxa_box0`, to the :ref:`evco_plc` in
:ref:`box5`, and to the :ref:`camera_dome`.

.. _wifi_shelter:

WiFi in the shelter
-------------------
In the shelter there is a WiFi modem with IP address ``10.10.18.60``.  It is
connected via ethernet to the :ref:`switch_shelter` and via wifi to the
:ref:`wifi_box6`.

.. _wifi_box6:

WiFi in :ref:`box6`
-------------------
The wifi in :ref:`box6` is connected via wifi to the :ref:`switch_shelter` and
via ethernet to the :ref:`switch_box6`.

.. _switch_box2:

Switch in :ref:`box2`
---------------------
The ethernet switch located in :ref:`box2` is connected to the :ref:`galil`,
to the :ref:`moxa_box2`, to the :ref:`switch_box4` and to the
:ref:`switch_box6`.


.. _switch_box4:

Switch in :ref:`box4`
---------------------
The ethernet switch located in :ref:`box4` is connected to the
:ref:`switch_box2`, to the :ref:`telescope_control_software_computer`, to
the :ref:`moravian`, and to the :ref:`evco_plc` in :ref:`box4`.


.. _switch_box6:

Switch in :ref:`box6`
---------------------
The ethernet switch located in :ref:`box6` is connected to the
:ref:`switch_box2`, to the :ref:`wifi_box6`, to the :ref:`encoder_reader`, and
to the :ref:`moxa_box6`.

.. _ups_box6:

UPS in the :ref:`box6`
----------------------
The UPS located in the :ref:`box6` sometimes becames unavailable, and in the
web application, section **ENERGY**, you will see the error **Communication lost**.
In that case:

#. :ref:`login_shelter_machine`
#. Execute the program ``/opt/upsmon/upsview`` program, you will see the screen page
   illustrated in :numref:`opt_upsmon_upsview_fig`

   .. _opt_upsmon_upsview_fig:

   .. figure:: images/opt_upsmon_upsview.png
      :figwidth: 100%
      :align: center

      The program ``upsview`` shows the :ref:`ups_box6` status and configuration.

#. In **1** select all default values
#. Select the point **2**, and if you see that the communication is lost, as in
   :numref:`opt_upsmon_upsview_communication_lost_fig`, than :ref:`restart_moxa_box6`

   .. _opt_upsmon_upsview_communication_lost_fig:

   .. figure:: images/opt_upsmon_upsview_communication_lost.png
      :figwidth: 100%
      :align: center

      The program ``upsview`` shows the communication lost message

#. :ref:`login_shelter_machine` and start the ``upsmon`` service:

   .. code-block:: bash

      $ sudo service upsmon stop
      $ sudo service upsmon start

Now wait a couple of minutes, and the **Communication lost** error message
(in the web application) should disappear.


.. _camera_dome:

Camera of the dome
------------------
In the first floor of the telescope house there is a camera that visualizes the
dome. It has IP address ``10.10.18.195``, and its image is visualized by the web
application, section **DOME**.  In case the image is not visualized, point to
the camera IP address with a browser.  You will see the page illustrated in
:numref:`camera_dome_page_fig`.

.. _camera_dome_page_fig:

.. figure:: images/camera_dome_page.png
   :figwidth: 100%
   :align: center

On the left of the page open the section **Administration Tools**, then
the section **Support** and eventually **Restart/Reset**.  Here click on
**Restart** and wait until the dome image appears on the web application.


.. _networking:

Network schema
==============
:numref:`networking_fig` illustrates a schema of all devices in the
ITM network.

.. _networking_fig:

.. figure:: images/networking.jpg
   :figwidth: 100%
   :align: center

   Schema of all devices in the ITM network.

You can also download a :download:`pdf format <images/networking.pdf>` of the
schema.
