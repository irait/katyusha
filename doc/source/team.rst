
.. _team:

********
ITM team
********


People involved in the project
==============================

.. _abau:

Alessandro Baù
--------------
`Alessandro <mailto:alessandro.baux@gmail.com>`_ works for the
University of Milano Bicocca, Dip. of Physics "G.Occhialini", Milano.
He is formally involved in the ITM project since the Concordia
summer campaign 2017.


.. _mbusso:

Maurizio Busso
--------------
`Maurizio Busso <mailto:mauriziombusso@gmail.com>`_.


.. _mbuttu:

Marco Buttu
-----------
`Marco Buttu <mailto:mbuttu@oa-cagliari.inaf.it>`_ works
for the `Italian National Institute for Astrophysics
<http://www.inaf.it/>`_, and he is involved in the ITM project since 2017,
when he started is winter over.


.. _jmchristille:

Jean Marc Christille
--------------------
`Jean Marc <mailto:jeanmarc.christille@gmail.com>`_ is the
director of the Astronomical Observatory of the Autonomous Region of the Aosta
Valley (OAVdA), and ITM Principal Investigator.


.. _ydepra:

Yuri De Pra
-----------
`Yuri De Pra <yuridepra@gmail.com>`_ is a ITM 2016/2017 winter over.


.. _ssartor:

Stefano Sartor
--------------
`Stefano Sartor <sartor.stefano@gmail.com>`_


.. _dtavagnacco:

Daniele Tavagnacco
------------------
`Daniele Tavagnacco <tavagnacco@oats.inaf.it>`_
