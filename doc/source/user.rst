.. include:: roles.txt

.. _user:

**********
User Guide
**********

.. sectionauthor:: :ref:`mbuttu`

.. topic:: Preface

   This chapter is intended for astronomers who want to operate the
   telescope.  If you are looking for some maintenance procedures,
   in order to solve hardware, mechanical or electrical failures,
   read the :ref:`maintenance`.  If you want to contribute to the
   software development, look at the :ref:`developer`.



Prerequisites
=============
Cleaning the mirrors every day is the best way to get good images.
That said, before executing the observation:

#. :ref:`start_up_control_system`

#. :ref:`start_up_control_system_monitor` in order to monitor the telescope

#. :ref:`start_up_webapp` in order to open the dome and check the temperatures

#. Verity that the telescope is not pointing to the :ref:`sun_position` (take at least
   some degrees of difference to the Sun).  If it is pointing close to the
   Sun, :ref:`move_telescope`, away from the Sun

#. :ref:`open_dome`

#. :ref:`set_ccd_temp` to -30 degrees

#. Wait at least 30 minutes from the time you opened the dome, and then
   you are ready to run the ITM Command Line interpreter, in order to
   perform the observations.

.. important:: Do not observe in case the wind is more than 6 m/s of wind.

The telescope is currently operated (mainly) by using the :ref:`itm_cli`,
and this documentation describes all you need.  There is also a small guide
written by :ref:`ydepra`, that almost reports what it is written here, with
some complementary notions. You can dowload it from :download:`here <images/yuri_guide.pdf>`.


.. _itm_cli:

ITM Command Line Interpreter
============================
To interact with the telescope :ref:`login_cs_machine` and run the
ITM Command Line Interface:

.. code-block:: shell

   $ itm_cli
   Welcome to the ITM command line interface.

   ITM >

As you can see, there is a new prompt and you can start executing commands.
Let's execute the command ``help``:

.. code-block:: shell

   ITM > help

   Documented commands (type help <topic>):
   ========================================
   EOF        goto  observe  pmel       q       set    show
   autofocus  help  park     pmoffsets  quit    setup  stop
   cleanfits  map   pmaz     pmscan     rotate  shoot  track


As you can see there are about 15 commands available.  To see the documentation
of a command, type ``help command_name``::

    ITM > help goto
    Take the source name as arguments, and go to the source.

        Example:

        > goto source=canopus

        The source argument can also take equatorial coordinates (RA-DEC):

        > goto source=(6:23:57.2,-52:41:43.9)

        You can also specify the altitude offset and the azimuth offset.
        For instance, in the following case we add to the Canopus coordinates
        a 0.02 altitude offset and a 0.03 azimuth offset:

        > goto source=canopus alt_offset=0.02 az_offset=0.03

Let's see how the commands work, starting form ``observe``,
the one you will use to perform the observations.

.. _observe_command:

Command ``observe``
-------------------
The command ``observe`` allows you to run the observations.  It takes
one mandatory argument: the directory where to save the data.  A second
argument, ``catalog``, has ``agb`` as a default value.
In the following case we run the AGB observations and save the
data to the directory *~/fits/agb/2018_obs02*::

   ITM > observe out_dir=2018_obs02

The previous command is equivalent to this one::

   ITM > observe catalog=agb out_dir=2018_obs02

The program reads the catalog from the *itm_cli/catalog/*
directory.

.. note:: The ITM CLI source code is hosted in the katyusha
   repository, under the directory *katyusha/src/python*.

If you specify ``catalog=agb`` then the program will read the
file *itm_cli/catalog/agb_catalog.py*. Let's have a look at that file:

.. literalinclude:: ../../src/python/itm_cli/itm_cli/catalog/agb_catalog.py
   :language: python
   :linenos:

All information about the sources to be observed and about the calibrator
is here. The program performs the following actions:

#. it moves the telescope in order to point to the calibrator; in that case
   the calibrator is Kaus Australis
#. it sets the filter and the related exposition, for instance the filter ``C``
   and an exposition of ``0.03`` seconds
#. it executes the autofocus for the filter ``C``, exposing for ``0.03``
   seconds
#. it moves to the first target, in that case ``V401``, and takes some pictures
   with that filter; the number of pictures to be taken for the agb catalog,
   and the exposition of each image are indicated in the file *itm_cli/commands/agb.py*,
   as reported in the section :ref:`observation_command` of the :ref:`developer`
#. once the program shoots all pictures for that filter it moves the
   telescope to the calibrator and the cycle continues for the second filter

The images are saved inside the specified directory, in a subdirectory
with the name of the star and finally in the subdirectory with the name
of the filter:

.. code-block:: shell

    $ ls ~/fits/agb/2018_obs02/
    FY  GP  GQ  GR  V401  V405  V412  V418  V421  V422  V424  V425
    $ ls ~/fits/agb/2018_obs02/V401/
    B  C  I  R  V
    $ ls ~/fits/agb/2018_obs02/V401/C/
    G4_C_00.fits  G4_C_02.fits  ...

The following is a convenient way to choose the directory names:

.. code-block:: shell

    $ ls ~/fits/agb/
    2018_obs01  2018_obs02  2018_obs03  2018_obs04  2018_obs05  2018_obs06

Each directory contains all targets indicated in the catalog.  You can run
the observation many times over the same directory:
if all targets are already observed, the program will skip everything, otherwise it
will continue from the last star and filter observed.  For instance, let's suppose
we run::

   ITM > observe catalog=agb out_dir=2018_obs02

If the directory *2018_obs02* does not exists, then the program
creates it and starts observing the first target,
``V401``.  Let's suppose we stop the program (with ``CTR-C``)
after observing all images of filters ``B`` and ``C`` of ``V401``.
If we run the previous command again, then the program will
continue the observation, but skipping the filters ``B`` and ``C`` of ``V401``.

The command ``observe`` takes some optional arguments.  One of them is
``focus_position``, and its value is the central focus position used
by the autofocus procedure.  For instance, in the following case the
central position for the autofocus will be ``3.0`` mm::

    ITM > observe out_dir=2018_obs02 focus_position=3.0

You can modify the autofocus exposition by using the
``focus_exp_factor`` argument.  For instance, let's suppost the autofocus
exposition indicated in the catalog, for the filter ``C``, is ``0.03``.
Then in the following case each autofocus exposition value will be multiplied
by ``1.5``, and that means the autofocus for the filter ``C`` will have
an exposition of ``0.045`` seconds::

    ITM > observe out_dir=2018_obs02 focus_exp_factor=1.5

The arguments ``start_from`` and ``stop_at`` allow you to choose the star
where to start from and the one where to stop to.  For insance, in the
following case the AGB observation will start from the star V418 and will
stop after observing the star V425::

    ITM > observe out_dir=2018_obs02 start_from=V418 stop_at=V425

When the observation terminates, the telescope will be automatically
parked and the CCD camera will be warmed up to 0 degrees.  If you want to
manually stop the observations, then type ``CTR-C`` and execute the
:ref:`itm_stop`.

.. important:: At the end of the observation you have to remember to :ref:`close_dome`.


.. _itm_stop:

Command ``stop``
----------------
This command stops the telescope movement:

.. code-block:: shell

   ITM > stop


Command ``park``
----------------
This command puts the telescope in a park position, then stops the movement
and warm the CCD up:

.. code-block:: shell

   ITM > park


Command ``goto``
----------------
This command takes a source name and moves the telescope to the source,
following it.  For instance, in this case the telescope will follow Canopus:

.. code-block:: shell

   ITM > goto source=canopus

The source argument can also take equatorial coordinates:

.. code-block:: shell

   ITM > goto source=(6:23:57.2,-52:41:43.9)

You can also specify the altitude and azimuth offsets.
For instance, in the following case we add to the Canopus coordinates
a 0.02 altitude offset and a 0.03 azimuth offset:

.. code-block:: shell

   ITM > goto source=canopus alt_offset=0.02 az_offset=0.03

.. todo:: Extend the command in order to give it alt/az or equatorial
   coordinates.

You can also follow the planets, the Moon and the Sun, all listed in the
catalog *itm_cli/catalog/ephem_catalog.py*:

.. code-block:: shell

   ITM > goto source=jupiter
   ITM > goto source=mars
   ITM > goto source=mercury
   ITM > goto source=moon
   ITM > goto source=neptune
   ITM > goto source=saturn
   ITM > goto source=sun
   ITM > goto source=uranus
   ITM > goto source=venus


Command ``track``
-----------------
If you are followning a source, and then you stop the telescope with the ``stop``
command, you can use ``track`` to follow again the source:

.. code-block:: shell

   ITM > track

It is equivalent to:

.. code-block:: shell

   ITM > stop
   ITM > goto source=sourcename


Command ``shoot``
-----------------
This command shoots an image. You have to specity at least the ``exposition``,
in seconds:

.. code-block:: shell

   ITM > shoot exposition=2

You can also specify the ``filter`` (default is ``R``, for red), the number
``n`` of shoots (default is ``1``) and the directory ``dir`` where to save the image
(default is ``$HOME/images``):

.. code-block:: shell

   ITM > shoot exposition=2 filter=U n=3 dir=/home/irait/foo

The allowed values for ``filter`` are: ``C``, ``I``, ``R``, ``V``, ``B``,
``U``, ``H``.  The filter value ``C`` means *Clear* (no filter, all band).
There is also a ``distance`` parameter, and it is the distance in seconds
between the shoots.  For instance, in the following case it will take 10 images,
waiting 10 seconds from one image to the next:

.. code-block:: shell

   ITM > shoot exposition=2 n=10 distance=60


Command ``set``
---------------
The command ``set`` allows you to set a value.  Currently you can
only set the filter, but the idea is to be extended in order to set
everything you want (the temperature, the focus position, etc.):

.. code-block:: shell

   ITM > set filter=R


Command ``autofocus``
---------------------
This command takes some pictures in order to set the best focus (optimal M2
position).  You have to specify at least the ``exposition`` (in seconds) of the shoots:

.. code-block:: shell

   > autofocus exposition=0.25

You can also specify some optional arguments:

.. code-block:: shell

   > autofocus exposition=0.25 step=0.2 central_position=107 distance=4

The meaning of these  optional arguments is the following:

* ``step``: the command moves M2 one step at a time, and shoots one picture at
  every step.  The default value for the step is 0.2mm

* ``central_position``: the central position of M2 during the autofocus.
  If the central position is 2mm and you choose a distance of 3mm to be covered,
  then the autofocus algorithm will take the first shoot
  at position 0.5mm, the second one at 0.7mm, and so on until the last position,
  3.5mm

* ``distance``: M2 will covered ``distance/2`` on the left and on the right of
  the ``central_position``, taking one shoot every ``step``.  The default value
  is 3mm

* ``shoots``: the number of shoot for every step. The default value is 1

* ``filter``: the default value is ``C``. Allowed values: ``C``, ``I``, ``R``,
  ``V``, ``B``, ``U``, ``H``.  The filter value ``C`` means *Clear* (no filter, all band).


Command ``map``
---------------
This command makes a map around a source.
For example, in the following case the telescope will
follow Canopus and create a 3x3 matrix (because `dimension=3`)
centered in the source:

.. code-block:: shell

   ITM > map exposition=10 source=canopus dimension=3

.. _map:

.. figure:: images/map.png
   :figwidth: 15%
   :align: right

   Representation of a map with ``dimension=3``.

It will shoot 9 images with a little bit of overlap
between them. One image will be centered in the source,
one on the left of the source, one on the right,
one line above the source and one line below, as
illustrated in :numref:`map`.
You can also choose the filter:

.. code-block:: shell

   ITM > map exposition=10 source=canopus dimension=3 filter=I

This command, as we will see later in the section :ref:`pointing_model`,
is really useful when you are performing the pointing model procedure.


Command ``cleanfits``
---------------------
Before executing this command, have a look at all FITS files you
want to clean, and in case some of them are not good, than you
have to rename (not to remove) them, changing the extension from
*fits* to another one. For instance, suppose the file *G4_C_02.fits*
is a bad image, than::

   $ mv G4_C_02.fits G4_C_02.bad

The command ``cleanfits`` cleans the FITS files applying the dark and
flat images. Here is an example:

.. code-block:: shell

   ITM > cleanfits catalog=agb observation_dir=2018_obs02

The meaning of the arguments ``catalog`` and `` ``observation_dir``
is the same of the :ref:`observe_command`.  The previous command
creates one new image for every FITS file. For instance, when it goes
in the directory *V401/C* it applies the flat and dark files to the image *G4_C_00.fits*
and in the same directory it creates the cleaned file, called
*elab_G4_C_00.fits*.  It creates an *elab* image for every file RAW.


Command ``rotate``
------------------
This command rotates a group of FITS files in order to get a single image::

.. code-block:: shell

   ITM > rotate catalog=agb observation_dir=2018_obs02

It has to be executed only over the *elaborated* files created by the
command ``cleanfits``.
The ``catalog`` is an optional argument with ``agb`` as a default value.
The command ``rotate`` will look for the filters directories
of each star and will rotate all elaborated images (the ones created
by the ``cleanfits`` command).
It is also possible to specity the stars to rotate and the filters:

.. code-block:: shell

   ITM > rotate observation_dir=2018_obs02 stars=V418,GP filters=C,B

To default it rotates all stars and filters.  This command executes the
``sextractor`` program that reads its configuration from the file
*katyusha/src/python/fieldRotation/derot_alipy/sex.cfg*.  This file has
many parameters and one of them is ``BACK_VALUE``, the background value.
Each image has its proper background, so it is not possible to have a
fixed value for ``BACK_VALUE``. That is why the ``rotate`` command changes
it dynamically. It reads the mean from the image, subtracts an offset
and writes the final value to the configuration file, and finally
executes ``sextractor``.

The command is not able to rotate all images, specially the filters ``B``,
``V`` and ``R``, so in that cases you have to tune the configuration file
manually. After executing ``rotate`` you can run the command ``show`` in
order to see what has actually been rotated.


Command ``show``
----------------
To see which images have already been ``cleaned`` or ``rotated`` you
have to execute the command ``show``:

.. code-block:: shell

   ITM > show what=rotated catalog=agb observation_dir=2018_obs06

     GP -> C
     GP -> I
     GP -> R
     GR -> C
     GR -> R
     V401 -> C
     V401 -> I
     V401 -> R
     V412 -> C
     V418 -> C

In this example it looks inside the directory ``2018_obs06``
in order to see which filters have been rotated, and prints
the result.  The ``catalog`` argument is optional, and its
default value is ``agb``.  You can also see what has not been rotated,
using the ``verbose`` argument:

.. code-block:: shell

   ITM > show what=rotated observation_dir=2018_obs06 verbose=yes

     GP -> C
     GP -> I
     GP -> R
     GR -> C
     GR -> R
     V401 -> C
     V401 -> I
     V401 -> R
     V412 -> C
     V418 -> C

   Not rotated:
     GP -> B
     GP -> V
     GQ -> B
     GR -> B
     GR -> I
     GR -> V
     V401 -> B
     V401 -> V
     V412 -> B
     V412 -> I
     V412 -> R
     V412 -> V
     V418 -> I
     V418 -> R
     V418 -> V

The argument ``what`` can take also the ``cleaned`` value, and in that
case the command will show the ``cleaned`` images:

.. code-block:: shell

   ITM > show what=cleaned catalog=agb observation_dir=2018_obs06

     GP -> B
     GP -> C
     GP -> I
     GP -> R
     GP -> V
     GQ -> B
     GR -> B
     GR -> C
     GR -> I
     GR -> R
     GR -> V
     V401 -> B
     V401 -> C
     V401 -> I
     V401 -> R
     V401 -> V
     V412 -> B
     V412 -> C
     V412 -> I
     V412 -> R
     V412 -> V
     V418 -> C
     V418 -> I
     V418 -> R
     V418 -> V


Command ``quit``
----------------
Use this command to exit from ITM CLI:

.. code-block:: shell

   ITM > quit
   $

If you want to look at the ITM CLI code, see the :ref:`developer` chapter.
Finally there are the commands ``pmscan``, ``pmoffsets``, ``pmaz`` and ``pmel``,
that we will discuss in the section :ref:`pointing_model`.



.. _flat_dark_files:

Flat and Dark files
===================

.. sectionauthor:: :ref:`ydepra`

Before observing we should create some *flat* and *dark* files.
These kind of files should be created every 2 months.

.. _flat_files:

How to make a flat file
-----------------------
The flat files are required in order to make uniform the light in the image.
For instance, let's think to the vignetting effect of a wide angle lens. If we
make a flat file that represents that effects, we can eliminate it from the
pictures by subtracting the flat file.
In other words, the flat files allow us to eliminate some effects caused by the
mirrors. It is better to make the flat files when there is enough light.

.. _flat_files_enough_light:

When there is enough light
~~~~~~~~~~~~~~~~~~~~~~~~~~
Right after the sunset, point the telescope at about 180 degrees in respect to
the :ref:`sun_position`, at 30 degrees of altitude (elevation). The sky must be uniform,
without any gradient. Start the acquisition by using the darkest filters (high
frequencies), because at the beginning of the sunset there is more light then at the end,
and the current camera is more sensible to the low frequencies than to the
highest ones.

.. note:: The filters are band-pass, it means an IR filter makes the IR
   radiation to pass and blocks the others. We have 6 filters:
   **I** (infra-red), **R** (red), **V** (green, the Italian Verde), **B** (blue),
   **U** (ultra-violet), and **H**.  The **C** code indicates no filter (full band).

For each flat file you should have an average of about 30-35k counts. The center
of the image is more sensible, so here the counts are usually over the average.
Pay attention that the max value for the counts (usually in the center of the image)
does not overtake 40k.

.. _flat_files_polar_night:

During the polar night
~~~~~~~~~~~~~~~~~~~~~~
Follow this procedure:

#. :ref:`close_dome`
#. turn on the light in the dome
#. hang a sheet up to the dome, in front of the telescope, in order for the
   telescope to see it
#. the light should be uniform in the sheet; if it is not, take the light
   (faretto) from the first floor and point it to the sheet, in order to make
   the light uniform.
#. run the script that creates the flat files (see :ref:`create_flat_files`
   section).

.. _create_flat_files:

Create the flat files
~~~~~~~~~~~~~~~~~~~~~
Follow these steps:

#. :ref:`start_up_control_system`

#. :ref:`start_up_control_system_monitor`

#. In case there is enough light, :ref:`move_telescope` to 30 degrees of altitude, and
   in azimuth to 180 degrees from the :ref:`sun_position`, as reported in
   :ref:`flat_files_enough_light`

#. In case there is not enough light, follow the instructions of section
   :ref:`flat_files_polar_night`

#. :ref:`stop_control_system`, otherwise in the next step you will not be able to run the script

#. :ref:`login_cs_machine` and open the script :filename:`~/flats/flat_G4.py`. This script
   sets the filter and the G4 camera temperature, than waits until the desired
   temperature (``ccd_temp_set``) is reached. We need to create several expositions for
   each filter, so edit the script in order to change the filter according to the
   flat file you want to create, and the exposition (``exp``) too (in seconds).
   When you run the script (wait to run it until you read all this step),
   it makes a series of exposition and prints on the screen
   the average counts of each exposition.  The number of counts has to be higher than
   30k counts, so as soon as you see printed a value lower than 30k, stop the script.

   .. important:: Stop the script right after it prints the results. In fact, after
      printing it sleeps for 2 seconds, and that is the time to stop it.  If you try to
      stop the script when it is not sleeping, some problems may occur.

   At this point open the script and set the index ``i`` to the number of expositions
   already taken (for instance, if you made 10 expositions, then set ``i = 10``),
   and increase the exposition ``exp``.
   Then run again the script until you have 30 images for this filter.  When you are
   done with this filter, edit the file changing the filter and exposition, and make
   30 expositions for this second filter too, in the same way you did for the previous filter.
   We are almost ready to run the script. Now edit again the file and set the current date::

      hdulist.writeto('/home/irait/flats/20170829/filter_%s/g4_flat ...)

   As you can see the date is ``20170829``. Change it according to the current year, month
   and day, and then create the corresponding directories in ``~/flats``.  For instance,
   if today is 20180629, write this date in the file and create the following directories:

   .. code-block:: bash

      $ mkdir ~/flats/20180629/
      $ mkdir ~/flats/20180629/filter_1
      $ mkdir ~/flats/20180629/filter_2
      $ mkdir ~/flats/20180629/filter_3
      $ mkdir ~/flats/20180629/filter_4
      $ mkdir ~/flats/20180629/filter_5

   Now you are ready to run the script:

   .. code-block:: bash

      $ cd ~/flats
      $ python flat_G4.py

   .. important:: The script cools down the camera sensor, so once you terminate you have to
      :ref:`set_ccd_temp` to 5 degrees.

#. Once all images are created, you have to run the :filename:`~/flats/mean50frame.py` script.
   Open it and set the correct filter, the data etc.  Verify that
   ``Info['extname']= "FLAT"``. The scripts has to be run
   once for each filter.  It takes the directory containing the images (for
   that filter) as argument. For instance, if you want to make the mean of the
   images taken with the filter number 2:

   .. code-block:: bash

      $ python ~/flats/mean50frame.py /home/irait/flats/20171204/filter_2

   The script creates the file :filename:`~/flats/mean30frame_FLAT_filter_B.fits`.

#. Once you create one flat file for each filter, copy them to the directory
   :filename:`~/katyusha/src/python/proc_raw_fits`:

   .. code-block:: bash

      $ cp  ~/flats/*FLAT*.fits ~/katyusha/src/python/proc_raw_fits


.. _dark_files:

How to make a dark file
-----------------------
The best time to make dark files is during the winter, when there is no light.
You should make about 50 images for each exposition.  The usual expositions
are (in seconds): 1, 3, 5, 10, 30, 60, 90, 120. The workflow is the following:

#. :ref:`start_up_control_system`

#. :ref:`start_up_control_system_monitor`

#. :ref:`set_ccd_temp` to -30 degrees and wait until it is reached

#. :ref:`login_cs_machine` and edit the script :filename:`~/dark/dark_G4.py`,
   changing the exposure (``exp``) and the destination directory given as argument to
   ``hdulist.writeto`` (according to the current date). Create the destination directories
   (today is the 8th of December, 2017):

   .. code-block:: bash

      $ mkdir /home/irait/dark/20171208
      $ mkdir /home/irait/dark/20171208/exp1sec
      $ mkdir /home/irait/dark/20171208/exp3sec
      $ mkdir /home/irait/dark/20171208/exp5sec
      $ mkdir /home/irait/dark/20171208/exp10sec
      $ mkdir /home/irait/dark/20171208/exp30sec
      ...

   Run the script once for each exposure:

   .. code-block:: bash

      $ python ~/dark/dark_G4.py

   There is nothing to control, just let it run until the end, and it will create 50 images
   inside the directory of the exposure we set. For instance, if in
   :filename:`~/dark/dark_G4.py` we set ``exp = 1``, the files will be saved in
   :filename:`/home/irait/dark/20171208/exp1sec`.
   Once the script terminates, change the exposure and run the script again. Once you have 50
   images for each exposure, go to the next step.

   .. important:: The script cools down the camera sensor, so once you terminate you have to
      :ref:`set_ccd_temp` to 5 degrees.

   .. note:: In case the script causes some problems to the moravian driver,
      :ref:`restart_moravian`.

#. Open :filename:`~/flats/mean50frame.py` and set the exposure
   (``Info['exp']``) to the correct value (related to the image you want to process). Also verify that
   ``Info['extname']= "DARK"`` and set ``Info['Filter']`` to ``None``. For
   instance, in the following case we are executing the script on the images
   taken with 1 second of exposure:

   .. code-block:: bash

      $ python ~/flats/mean50frame.py /home/irait/dark/20171204/exp1sec/

   It creates the file :filename:`~/dark/mean30frame_DARK_filter_None.fits`.
   Rename the file:

   .. code-block:: bash

      $ mv mean30frame_DARK_filter_None.fits mean30frame_DARK_exp_1.fits

   Once you have one :filename:`*_DARK_exp_*.fits` for each exposure, go to
   the next step.

#. Copy all dark files to :filename:`~/katyusha/src/python/proc_raw_fits`:

   .. code-block:: bash

      $ cp ~/dark/*DARK*.fits ~/katyusha/src/python/proc_raw_fits


.. _pointing_model:

How to update the pointing model
================================
In case the telescope is not pointing properly the targets
located at low and high elevations, than you should update the
pointing model.  The procedure is the following:

#. rename the directory containing the old FITS files::

     $ cd ~/pmodel/
     $ mv shoots/ shoots_old

#. execute the :ref:`pmscan`: the telescope will point all
   stars listed in the catalog *itm_cli/catalog/itmctg.dat*,
   shoot an image for each one and write the FITS file in the
   directory *~/pmodel/shoots/*

#. execute the :ref:`pmoffsets`: it will read all images
   in the *~/pmodel/shoots/* directory, compute the offsets
   of the star from the center of the image and write, in the
   same directory, a file called *offsets.txt*. Every line
   of this file contains the identification of the star,
   the azimuth, the elevation and the related offsets

#. execute the :ref:`pmaz`: it will print to the screen the
   azimuth pointing model parameters, to be copied and written
   in a CPP file

#. execute the :ref:`pmel`: it will print to the screen the
   elevation pointing model parameters, to be copied and written
   in a CPP file


.. _pmscan:

``pmscan`` command
------------------
Take the exposition of the camera (in seconds) as argument and make a scan of the sky,
in order to save the FITS files required to compute the pointing model offsets.
Example::

    ITM > pmscan exposition=0.1

The optional argument ``catalog`` indicates a file of sources.  The default
catalog is *itmctg.dat*.  The optional argument ``starting_star`` indicates
the star in the catalog from which pmscan will start the observation, and the
argument ``ending_star`` the one to stop.
You can also specify the camera filter.  The default value is ``R`` (red)::

    ITM > pmscan catalog=FOO.DAT exposition=0.1 starting_star=10
    ITM > pmscan exposition=0.1 filter=B starting_star=10 ending_star=20

The allowed values for ``filter`` are: C, I, R, V, B, U, H.
The filter value C means Clear (no filter, all band).
You can stop the execution of ``pmscan`` whenever you want, because if
you run it again it will continue from the last star observed. In fact
for every source in the catalog *itmctg.dat*, it looks for the related
FITS file in *~/pmodel/shoots* and observes the source only if the FITS
file does not exists.

You can also do a map around the star, using the ``star_map`` optional argument::

    ITM > pmscan exposition=1 star_map=3

It will do a matrix 3x3 centered on the star.  This is useful in case you do
not see the star, tipically at low elevation.  The suggested ``pmscan`` procedure
is the following:

#. execute ``pmscan`` withoud ``star_map`` and at the end you will see
   that some FITS files are without the star, maybe because the star
   is a little bit out of the image

#. remove the empty FITS files (otherwise if ``pmscan`` finds the file then it will
   skip the observation for it) and execute ``pmscan`` with ``star_map=3``

#. once you get the map of these remaining stars, take only the FITS where there
   is the source, and rename the files giving them the identification of the source.
   Now you are ready to execute the :ref:`pmoffsets`.


.. _pmoffsets:

``pmoffsets`` command
---------------------
The ``pmoffsets`` command will read all images in the
*~/pmodel/shoots/* directory and compute the offsets using the
formulas described in :download:`this document <images/pointing_model.pdf>`.
The offsets will be saved in the same directory of the FITS
images, in a file called *offsets.txt*. Every line
of this file contains the identification of the star,
the azimuth, the elevation and the related offsets.
The *offset.txt* file will be read from the :ref:`pmaz` and
:ref:`pmel` in order to compute the azimuth and elevation
parameters of the pointing model.

.. important:: If you rotate the CCD then you need to compute again the
   parameter ``CCD_ANGLE`` defined in the *itm_cli/config.py* file.
   To calculate this parameter use the same procedure described in the
   :download:`document <images/pointing_model.pdf>`.


.. _pmaz:

``pmaz`` command
----------------
This command computes the azimuth parameters of the pointing model::

    ITM > pmaz
    long double aa0 = -0.979736237408;
    long double aa1 = -0.00575685573437;
    long double aa3 = 2.08278756432e-06;
    long double aa4 = -1.43171360762e-08;
    long double aa5 = 3.36210872206e-11;
    long double aa6 = -2.65907339547e-14;
    long double acec = 0.00725602001729;
    long double ae5 = 1.11471813957e-10;
    long double aes = -0.865971596275;
    long double aes_delta_azr = -14.7659261475;
    long double aes_delta_elr = 1.39012652429;

If you want to change the initial point for the ``pmaz`` fit, then edit
the file *~/pmodel/config/az_init.txt* and install ``itm_cli`` again.

Once you get these lines :ref:`login_cs_machine` and go to this
directory::

    $ cd ~/rts2_sartor/src/teld/

Open the file *irait.cpp* and move to the function ``applyGpoint()``:

.. code-block:: cpp

   int Irait::applyGpoint( ... )
   {
       double az = altAz_pos->az;
       float azr = az * M_PI/180.;  // radians
       double el = altAz_pos->alt;
       float elr = el * M_PI/180.;  // radians

       long double aa0 = -0.979736237408;
       long double aa1 = -0.00575685573437;
       long double aa3 = 2.08278756432e-06;
       long double aa4 = -1.43171360762e-08;
       long double aa5 = 3.36210872206e-11;
       long double aa6 = -2.65907339547e-14;
       long double acec = 0.00725602001729;
       long double ae5 = 1.11471813957e-10;
       long double aes = -0.865971596275;
       long double aes_delta_azr = -14.7659261475;
       long double aes_delta_elr = 2.39012652429;
       double delta_az =  aa6*pow(az, 6) + ...

Here you have to replace the lines located above the definition of ``delta_az`` with the
ones printed by the :ref:`pmaz`.


.. _pmel:

``pmel`` command
----------------
This command computes the elevation parameters of the pointing model::

    ITM > pmel
    long double ea0 = -0.366814429496;
    long double ea1 = 0.00447757559274;
    long double ea3 = -1.38205868572e-07;
    long double ea5 = 1.67578749094e-12;
    long double ea6 = -2.40541435372e-15;

If you want to change the initial point for the ``pmel`` fit, then edit
the file *~/pmodel/config/el_init.txt* and install again ``itm_cli``.
Once you get these lines :ref:`login_cs_machine` and go to this
directory::

    $ cd ~/rts2_sartor/src/teld/

Open the file *irait.cpp*, and move to the function ``applyGpoint()``:

.. code-block:: cpp

   int Irait::applyGpoint( ... )
   {
        // ...
        long double ea0 = -0.366814429496;
        long double ea1 = 0.00447757559274;
        long double ea3 = -1.38205868572e-07;
        long double ea5 = 1.67578749094e-12;
        long double ea6 = -2.40541435372e-15;
        double delta_el =  ea6*pow(az, 6) + ...

Here you have to replace the lines located above the definition of ``delta_el`` with the
ones printed by the :ref:`pmel`. Once you substitute these lines and the lines you got from
:ref:`pmaz`, then compile and install again RTST::

    $ cd ..
    $ make
    $ sudo make install

:ref:`stop_control_system` and :ref:`start_up_control_system` again.

Offsets
-------
At the end of the 2018 winterover, the pointing model offsets where the ones
reported in :numref:`az_offsets` and :numref:`el_offsets`.

.. _az_offsets:

.. figure:: images/az_offsets.png
   :figwidth: 100%
   :align: center

   The azimuth offset when the elevation changed between 7 and 75 degrees.


.. _el_offsets:

.. figure:: images/el_offsets.png
   :figwidth: 100%
   :align: center

   The elevation offset for a full azimuth rotation.


RTS2 Interface
==============
The telescope is controlled by some modules implemented inside
the RTS2 framework.  A RTS2 user interface has been written
in order to communicate with the telescope. In order to
start the user interface follow the instuctions
reported in the section :ref:`start_up_control_system_monitor`.

The main module are **C0**, **F0** and **T0**.  The first one
controls the CCD camera, and usually you will use it to
:ref:`set_ccd_temp`.   The second one controls the focus, as
you can see in the section :ref:`set_focus`, and the tirdh one
manages the movement, as you can see for in instance in the
section :ref:`move_telescope`.

You can also run a Python script that will be executed by RTS2.
For instance, in the following case you are executing the script
called *myscript.py*::

   $ rts2 script-exec -d C0 -s "exe myscript.py"

The commands in the :ref:`itm_cli` are executed by calling
the scripts exactly in that way.  For instance, let's suppose you execute the
command::

   ITM > goto source=canopus

In this case the ITM CLI makes a call to *goto.py*, by executing
the command::

   $ rts2 script-exec -d C0 -s "exe path_to/goto.py"

The scripts you want to execute under RTS2 have to be permission
755.  Inside the script you can not execute a ``print``.  If you
want to show something on the stardard output then make a call to
``self.log()``, like the following:

.. code-block:: python

   self.log('I', 'Info message')
   self.log('W', 'Warning message')
   self.log('E', 'Error message')

To see how to write a script look at the section
:ref:`observation_command` of the :ref:`developer` chapter.  The
*stop.py* script is an example.  As you see you have to inherit from
a ``RTS2Wrapper`` class.


How to transfer big files to Europe
===================================
If you want to transfer some big files to Europe, use the
*hermes* server.  Connect to it via SFTP:

.. code-block:: bash

   $ sftp irait@hermes2.concordiastation.aq  # pass: irait_DC11

As you can see there is a directory *from_dmc*:

.. code-block:: bash

   $ sftp> ls
   from_dmc

Put inside the directory *from_dmc* the files you want to transfer.



Procedures
==========

.. _login_cs_machine:

Login into the control system machine
-------------------------------------
To login into the :ref:`telescope_control_software_computer`:

.. code-block:: bash

   $ ssh -Y irait@10.10.18.19  # password is irait


.. _login_shelter_machine:

Login into the computer in the shelter
--------------------------------------
To login into the :ref:`shelter_computer`:

.. code-block:: bash

   $ ssh -Y irait@10.10.18.16  # password is irait


.. _start_up_control_system:

Start up the control system
---------------------------
:ref:`login_cs_machine` and start up RTS2 using the following command:

.. code-block:: bash

   $ sudo /etc/init.d/rts2 start


.. _stop_control_system:

Stop the control system
-----------------------
:ref:`login_cs_machine`  and stop the control system *twice*,
as reported below:

.. code-block:: bash

   $ sudo /etc/init.d/rts2 stop
    * Stopping RTS2
    * Stoping RTS2 EXEC
            ...
   $ sudo /etc/init.d/rts2 stop
    * Stopping RTS2
    * RTS2 EXEC already stopped
            ...

.. _start_up_control_system_monitor:

Start up the control system monitor
-----------------------------------
:ref:`login_cs_machine` and execute the following command:

.. code-block:: bash

   $ rts2-mon

You should see a textual interface like the one in :numref:`rts2_mon_fig`.

.. _rts2_mon_fig:

.. figure:: images/rts2_mon.png
   :figwidth: 100%
   :align: center

   A screenshot of the ``rts2-mon`` program.

If you do not see the textual interface, but the message
``Trying to contact centrald``, then you need to :ref:`start_up_control_system`
and then execute ``rts2-mon`` again.


.. _stop_control_system_monitor:

Stop the control system monitor
-------------------------------
To stop ``rts2-mon``, just type **CTR-C** in the **rts2-mon** textual interface.


.. _start_up_webapp:

Start up the web application
----------------------------
To start up the web application, open a web browser and go to the address
`<http://irait.concordiastation.aq:8080/irait>`_. Login with user ``irait`` and
password ``irait_itm``. You will see the web application home
page, as illustrated in :numref:`webapp_home_fig`.

.. _webapp_home_fig:

.. figure:: images/webapp_home.png
   :figwidth: 100%
   :align: center

   The web application home page

At this point you need to verify that the web application is synchronized with
the control system. Open the **TELESCOPE** page and look at the date and time
on top of the page. If it is written in red, like in
:numref:`webapp_not_connected`, then the web application is not synchronized
and you can not control the telescope neither read the current status.

.. _webapp_not_connected:

.. figure:: images/webapp_not_connected.png
   :figwidth: 100%
   :align: center

   The date is written in red, ant this means that the web application is not synchronized
   with the control system.

To synchronize the web application :ref:`login_cs_machine` and execute the :filename:`telescope_check.py`
script using ``rts2-scriptexec``:

   .. code-block:: bash

      $ cd ~/katyusha/src/python/irait_control_webapp
      $ rts2-scriptexec -d C0 -s " exe telescope_check.py"

Wait for about one minute (that is the refresh time of the web application), and the date will
become green, as in :numref:`webapp_synchronized`.

.. _webapp_synchronized:

.. figure:: images/webapp_synchronized.png
   :figwidth: 100%
   :align: center

If the date does not become green, the problem could be the date on your
machine. In that case please set the date and time to Perth, Australia.

.. note:: The script :filename:`telescope_check.py` gets the values from the
   control system and writes them to the database. The web application
   reads from the database and publishes the current values.  When the
   web applications wants to communicate to the control system, it writes
   the commands to the database, and the script :filename:`telescope_check.py`
   reads the commands from the database and sends them to the control system.


.. _open_dome:

Open the dome
-------------

#. Be sure nothing can interfere with the moving parts

#. Be sure the telescope is not poiting to the :ref:`sun_position`

#. :ref:`start_up_webapp`

#. Open the **DOME** page and in the **Command** section, lable **OPEN**, click **ON**

#. Whait until the **COMPLETE OPEN**  light becomes yellow, as illustrated in
   :numref:`webapp_dome_complete_open_fig`.

   .. _webapp_dome_complete_open_fig:

   .. figure:: images/webapp_dome_complete_open.png
      :figwidth: 100%
      :align: center

      The dome is opened when the **COMPLETE OPEN** light becomes yellow.

#. When the dome is opened, in the **OPEN** lable of **Command** click **OFF**.


.. _close_dome:

Close the dome
--------------

#. be sure nothing can interfere with the moving parts

#. :ref:`start_up_webapp`

#. open the **DOME** page and in the **Command** section, lable **CLOSE**, click **ON**

#. Whait until the **COMPLETE CLOSE**  light becomes yellow, as illustrated in
   :numref:`webapp_dome_complete_close_fig`.

   .. _webapp_dome_complete_close_fig:

   .. figure:: images/webapp_dome_complete_close.png
      :figwidth: 100%
      :align: center

      The dome is closed when the **COMPLETE CLOSE** light becomes yellow.

#. When the dome is closed, in the **CLOSE** lable of **Command** click **OFF**.


.. _disable_pointing:

Disable the pointing
--------------------
When the telescope is in *pointing* mode, the control system updates the position
in order to follow the sky.  You do not want to have this behavior when you
point the telescope to a fixed position.  In that case, you need to disable the
pointing:

#. :ref:`start_up_control_system_monitor`
#. In the first window (left side) move to ``T0``
#. Type **TAB** to move the cursor to the second window and look for the **point** label
#. type **ENTER** over the **point** lable and then choose **false**, as shown in
   :numref:`rts2_mon_T0_point_false_fig`:

   .. _rts2_mon_T0_point_false_fig:

   .. figure:: images/rts2_mon_T0_point_false.png
      :figwidth: 100%
      :align: center

      Use ``rts2-mon`` to disable the telescope tracking. In this way the
      telescope will stay in a fixed position without trying to follow the sky.


.. _move_telescope:

Move the telescope to a fixed position
--------------------------------------
:ref:`disable_pointing`, stay on **T0**, and then on the right window look for **TEL_**,
type **ENTER** and set the position. The first field is the *altitude* (elevation), the second one
is the *azimuth*.  :numref:`rts2_mon_T0_TEL__fig` illustrates this operation.

.. _rts2_mon_T0_TEL__fig:

.. figure:: images/rts2_mon_T0_TEL_.png
   :figwidth: 100%
   :align: center

   Set the telescope position. The first field is the altitude, the second
   one is the azimuth.

The control software does not allow the user to move the telescope above
``84°`` and below ``5°``.  If for some reasons these software limits do not work, the
movement is stopped by some *final-limits*.  The lower final-limit is ``4°
30'``, the upper final-limit is ``84° 28'``.  The mechanical limits, where the elevation
wheel will beat, are ``0°`` and ``85° 15'``.


.. _stop_telescope:

Stop the telescope
------------------
In case you want to stop the telescope,
for instance for emergency reasons, :ref:`start_up_control_system_monitor`
and select **T0** in the first window, then look for **Mount** on
the second window, type **ENTER** and select **STOP**, as reported
in :numref:`rts2_mon_T0_Mount_STOP_fig`.

.. _rts2_mon_T0_Mount_STOP_fig:

.. figure:: images/rts2_mon_T0_Mount_STOP.png
   :figwidth: 100%
   :align: center

   Stop the telescope with ``rts2-mon``.

It is also possible to stop the movement from the web application.
In that case :ref:`start_up_webapp`, open the **TELESCOPE** page e press the
red and yellow emergency stop button, on the left of the page, as illustrated in
:numref:`webapp_stop_mount_fig`.

.. _webapp_stop_mount_fig:

.. figure:: images/webapp_stop_mount.png
   :figwidth: 100%
   :align: center

   The emergency stop buttom in the TELESCOPE page of the web application.


.. _sun_position:

Sun position
------------
To know the Sun position, :ref:`start_up_control_system_monitor` and select
**centrald** from the first window. On the second window look for
**sun_alt** and **sun_az**, as illustrated in :numref:`sun_position_fig`.

.. _sun_position_fig:

.. figure:: images/sun_position.png
   :figwidth: 100%
   :align: center

   The Sun position reported by ``rts2-mon``.  Pay attention because the
   azimuth differs from the real one by 180 degrees.

The azimuth position differs from the real one by 180 degrees, so if you read
a Sun azimuth of 190, it is actually 10 degrees.


.. _set_focus:

Move the second mirror
----------------------
If you want to move the second mirror, for instance in order to change the
focus, then:

#. :ref:`start_up_control_system_monitor`
#. In the first window (left side) move to ``F0``
#. Type **TAB** to move the cursor to the second window and look for the **FOC_TAR** label
#. type **ENTER** over the **FOC_TAR** lable and then set the target position, as shown in
   :numref:`rts2_mon_F0`:

   .. _rts2_mon_F0:

   .. figure:: images/rts2_mon_F0.png
      :figwidth: 100%
      :align: center

      Use ``rts2-mon`` to manually set the M2 position.


.. _set_ccd_temp:

Set the CCD temperature
-----------------------
To set the CCD temperature, :ref:`start_up_control_system` and
:ref:`start_up_control_system_monitor`.  In the first window of ``rts2-mon``
select **C0**, and in the second window select **CCD_SET**, press
**ENTER** and set the temperature, as illustrated in
:numref:`rts2_mon_C0_CCD_SET_fig`.

.. _rts2_mon_C0_CCD_SET_fig:

.. figure:: images/rts2_mon_C0_CCD_SET.png
   :figwidth: 100%
   :align: center

   Set the CCD temperature with ``rts2-mon``: select **C0**, then **CCD_SET**.

.. note:: The label **CCD_TEMP** right above **CCD_SET** is supposed to be the current
   CCD temperature, but it is not updated automatically.  If you want to see the
   current value :ref:`stop_control_system_monitor` and
   :ref:`start_up_control_system_monitor` again.

Sometimes ``rts2-mon`` does not show the **C0** field in its first window.
In that case you have to:

#. :ref:`stop_control_system_monitor`

#. :ref:`stop_control_system`

#. :ref:`restart_moravian`

#. :ref:`start_up_control_system`

#. :ref:`start_up_control_system_monitor`


.. _restart_moravian:

Restart the moravian machine and service
----------------------------------------
The moravian is the machine that controls the camera (aka G4 camera). It is
hosted in box n.6, and has IP address 10.10.18.21. To restart the moravian:

#. :ref:`start_up_webapp`

#. Open the **DEVICE** page of the web application and scroll down the page until you
   see the **BOX 6** section, as illustrated in :numref:`webapp_device_box6_fig`.

   .. _webapp_device_box6_fig:

   .. figure:: images/webapp_device_box6.png
      :figwidth: 100%
      :align: center

      The **OFF** buttom in the **CCD power** window shuts down the moravian machine.

#. Press the **OFF** buttom in the **CCD power** window and wait for 50 seconds
   (the web application refresh time) until you see the status **OFF**, as
   illustrated in :numref:`webapp_device_ccd_power_off_fig`.

   .. _webapp_device_ccd_power_off_fig:

   .. figure:: images/webapp_device_ccd_power_off.png
      :figwidth: 100%
      :align: center

      The status **OFF**, in red color, indicates that the moravian machine is
      stopped.

#. Restart the moravian machine by pressing **ON** and wait until the status
   is **ON** (in green color).

#. Login into the moravian machine and restart the service:

   .. code-block:: bash

      $ ssh -p 11011 root@10.10.18.21  # pass is "culo"
      $ systemctl stop micamd.service
      $ systemctl start micamd.service
      $ systemctl status micamd.service


.. _reestablish_encoder_communication:

Reestablish the communication with the encoder
----------------------------------------------
Sometimes the label **T0** does not appear in ``rts2-mon``.  In that case you
can not get the altitude and azimuth positions.  The web application has the
same problem: you can not read the telescope position because you can not run
the script :filename:`telescope_check.py`, as explained in :ref:`start_up_webapp`.
In fact after a while (about 15 seconds) the script will terminate printing the
error message: ``ERROR: try to restart jmodbus, the encoder and rts2``.

The root problem is that the control software is not able to communicate with the
:ref:`encoder_reader`.  To fix the problem:

#. :ref:`restart_jmodbus`

#. Restart the encoder

#. Restart rts2

.. _restart_jmodbus:

Restart jmodbus
---------------
:ref:`login_cs_machine` and stop the service:

.. code-block:: bash

   $ sudo /etc/init.d/jmodbus stop

Now look for the ``jmodbusd`` active processes in order to get its IDs,
because you want to kill them:

.. code-block:: bash

   $ ps aux | grep jmodbusd
   root     13404  0.1  0.1  45572  ...  /usr/local/bin/rts2-jmodbusd ...
   root     13406  0.0  0.1  45600  ...  /usr/local/bin/rts2-jmodbusd ...
   root     13408  0.0  0.1  45600  ...  /usr/local/bin/rts2-jmodbusd ...
   root     13410  0.0  0.1  45600  ...  /usr/local/bin/rts2-jmodbusd ...
   root     13412  0.0  0.1  45600  ...  /usr/local/bin/rts2-jmodbusd ...
   irait    13546  0.0  0.0  13648  ...  grep --color=auto jmodbusd

Kill all ``root`` processes:

.. code-block:: bash

   $ sudo kill -9 13404
   $ sudo kill -9 13406
   $ sudo kill -9 13408
   $ sudo kill -9 13410
   $ sudo kill -9 13412


Now start the service:

.. code-block:: bash

   $ sudo /etc/init.d/jmodbus start

If you look at the ``jmodbus`` status, all services should be running:

.. code-block:: bash

   $ sudo /etc/init.d/jmodbus status
   * J0_ABB RUNNING
   * J1_BOX6 RUNNING
   * J2_PWR RUNNING
   * J3_BOX5 RUNNING
   * J4_BOX4 RUNNING

