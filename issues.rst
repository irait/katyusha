1) Write a CL interface for the telescope
=========================================
Currently the telescope is not user friendly.  To operate it you
need to manually edit some scripts, you need to know where this
scripts are and how they work.  The interaction to the telescope is
hard to explain and the user looses lot of time doing repetitive tasks.
Another problem of this machinery is that you always have to edit scripts
that are under revision control.  To solve these problems we should have
an interactive interface that accepts some commands and execute a script
for every command.  Example of commands:

  > goto canopus  # The telescope will follow Canopus
  > autofocus  # This command will perform the focus procecure
  > shot camera, exposure, filter  # Get the image

The commands autofocus and shot will return an error message in case
the telescope is not ready (for instance because it has not yet reached
the source):

  > autofocus
  ERROR: The telescope has not yet reached the source

Every command is associated to a script, and the script will be executed
by `rts2-scriptexec`.  It is not possible to give the script command line
arguments.  A solution could be to have a pre-step where we save the parameters,
and then the script will read them.  For instance, let's have the following
command:

  > goto canopus

In this case the command line interpreter:

a) pickles the argument `canopus` to a file
b) executes the script `goto` by means of `rts2-scriptexec`
c) the script will read the argument from its pickled file
d) the script will looks for the canopus coordinates from a catalog
e) the script will move the telescope in order to follow Canopus


2) Pass keyword only arguments to the CL
========================================
The commands have to take only keywords arguments:

  > goto source=canopus az_offset=3 az_altitude=2

The order of the arguments does not matter, and some arguments
are optional.  In this way is more easier the management of the
CL arguments.


3) Implement the autofocus command
==================================
The ``autofocus`` command has to perform the autofocus setting
the M2 position.

Example:

  > autofocus exposure=0.25

You can also specify some optional arguments:

  > autofocus exposure=0.25 step=0.2 central_position=107 distance=4

The optional arguments are:

* step: default=0.5 mm
* central_position: default=108 mm
* distance: default=3
* shoots: default=2
* filter: default='R'. Allowed values:
          'C', 'I', 'R', 'V', 'B', 'U', 'H'

The filter value 'C' means Clear (no filter, all band).


4) Implement the stop command
=============================
The ``stop`` command stops the mount by setting ``Mount->STOP`` and
``point->false``.

Example:

  > stop


5) Implement the track command
==============================
The ``track`` command tracks the source previously set by ``goto``,
by setting ``Mount->Stand-by`` and ``point->true``.  The ``goto``
actually already sets these attributes, but it is possible that
the mount has been stopped by ``stop``.  In that case ``track`` avoid
to execute again the ``goto`` command.

Example:

  > track


6) Implement the park command
=============================
The ``park`` command performs all actions required at the end of
the observation:

* stop the Mount
* warm the CDD up
* close the dome

Example:

  > park


7) The goto command has to take care of the altitude bug
========================================================
There is a bug in RTS2 or in the Galil firmware, and if you
reach the azimuth before the altitude,
then it is possible the movement does not stop in altitude and the
telescope stops only when it reaches the final-limit.  In that
case the Galil will have some problems and you will lose lot
of time trying to make things work properly as before, as we
experienced this summer.

The ``goto`` command has to take care of this, aviding the telescope
to reach the azimuth position before the altitude one.


8) Implement a command pmscan that creates an input file for TPoint
===================================================================
To create a pointing model for the telescope we will use the TPoint
program.  We have to write a command, called ``pmscan``, that makes a scan
of the sky, in order to create a file of input coordinates for TPoint.
The ``pmscan`` command performs the following operations:

1. If the star is already in the INDATA file, it jumps to the next star
2. Gets the updated coordinates, taking in account the motion per year
3. Goes to the star
4. Shoots the image
5. Stops the mount
6. Opens the FITS file and verify there is an object. If there is:
    6a. Computes the alt/az offsets from the center
    6b. Gets the telescope coordinates from the FITS
    6c. Computes the coordinates: telescope coordinates + offsets_from_center
    6d. Reads the time of the shoot from the FITS file
    6e. Saves the star id, coordinates, and time to the INDATA file

9) Implement the step 1 of issue #8
===================================
The ``pmscan`` command has to create the directory containing the
INDATA file (if it does not exist), and for each star has to check
if it has already been observed. In that case it will skip the observation
and move to the next star listed in the TPoint catalog.

10) Implement the steps 2 and 3 of issue #8
===========================================
The ``pmscan`` command has to compute the coordinate of the source
and correct them taking in account the motion per year.  Then it has to
go to the source.  The ``goto`` command has to be modified in order to
take an instance of ``sources.Source`` as argument.


11) Implement the steps 4 and 5 of issue #8
===========================================
After the ``pmscan`` tracks the source, it has to shoot the picture.
Use the `utils.Camera.shoot()` method, extending it in order to choose
if we want a date subdir directory, and extending the ``Camera`` interface
adding two arguments: the ``exposition`` and the ``filter``. Modify the
``cli.py`` and ``autofocus.py`` accordingly.  Once the picture is taken,
stop the mount.

12) Implement the step 6 of issue #8
====================================
The ``pmscan`` has to open the FITS file and verify that there is the star.
If the star is there, then it:

    * Computes the alt/az distance from the star to the center of the image
    * Gets the telescope coordinates from the FITS
    * Computes the coordinates: telescope coordinates + alt/az distance
    * Reads the time of the shoot from the FITS file
    * Saves the star id, coordinates, and time to the INDATA file

13) Refactoring of pmscan removing duplicate of observer
========================================================
The ``pmscan`` currently reads some information from the FITS file.  All
the information related to the observer should be available in an ``ITMObserver``
class. We also have to add temperature value to this class. The ``Source``
class has to instantiate a ``ITMObserver`` and define a method ``radec()``
that returns the radec coordinates for a given alt/az and time.

14) Set the current sign of the offset in pmscan
================================================
Currently the sign of the altitude and azimuth offsets, retrived by ``pmscan``
from the image is positive. We should make a test to see the real sign,
setting an offset and observing where the source goes in the image. At this
point we can set the proper sign of the offset in ``pmscan``.

15) Catch KeyboardInterrupt and use quit to exit from itm_cli
=============================================================
Some commands are blocking, so to stop them you have to use
a KeyboardInterrupt.  To keep the itm_cli alive, catch
the exception and use the ``quit`` command to exit the program.

16) Make a wrapper of rts2.scriptcomm.Rts2Comm in order to log to file
======================================================================
Currently the ``rts2.scriptcomm.Rts2Comm.log()`` method prints the
output to the screen.  In order to log to a file and keep the current
behavior, we need to create a wrapper (inherithing from ``Rts2Comm``,
calling the super class method and then using the Python ``logging``
module to log to a file.

17) Implement the command shoot
===============================
The command ``shoot`` takes the exposition (in seconds) and shoots an image:

  > shoot exposition=2

You can also specify the filter (default is R (red), the number
of shoots (default is 1), the file_name (dafault is 'image', and the extension
'.fits' will be appended automatically), and the directory where to save the
images (default is $HOME/images): 

  > shoot exposition=2 filter=U n=3 file_name=foo dir=/home/irait/foo

The allowed values for `filter` are: 'C', 'I', 'R', 'V', 'B', 'U', 'H'.
The filter value 'C' means Clear (no filter, all band).

18) Create an ITM stars catalog for TPoint
==========================================
From the stars list used in the previous ITM pointing model, create
a catalog in the TPoint format.

19a) Add the itm_guide to the repository
========================================
The user and development guide should be added to the repository,
in order to be easy to point to the code and generate automatically
the APIs documentation.

19b) Create a command that analizes a directory of fits files
=============================================================
We have to split the command ``pmscan`` in two commands. One
will read the catalog, point the sources and write the FITS
files, the second one (``pminput``) will read the FITS and
write the input data file (INDATA) for tpoint.  In this
way ``pmscan`` will call ``pminput`` at the end of the sky
scan, and ``pminput`` can also be called alone in order to
read a directory of FITS files and write the INDATA file.

20) Modify the dimension of the beam
====================================
Currently the focal reducer is not mounted, so we need to modify
the beam to 7arcmin x 7 arcmin

21) Add the elevation contribution to the azi offset
====================================================
The azimuth offset has to take in account the cos(el):

delta_az = delta_x * cos(el)

Where delta_az is the delta of the azimuth position,
and delta_x is the distance in pixel of the star,
from the center of the image.

22) Implement a setup command
=============================
The setup command has to set the telescope, cooling down
the CCD and setting a default focus position.

23) Crete indata file for observations in different dates
=========================================================
One of the INDATA file metadata is the observation date. We should
create one indata file for every observation data, and look at every
indata file in order to see if a star has already been observed.
In the metadata of the INDATA file has the date of the observation

24) pminput has to close the INDATA file at each writing
========================================================
Each time we write the star coordinates to the INDATA file,
we have to close the file (writing END).

25) The telescope does not follow the start when shooting in pmscan
===================================================================
When the command pmscan is taking the image, the telescope is stopped
istead of follow the source.

26) Improve the pminput algorithm for finding stars
===================================================
The pminput algorithm does not find the star in case there is
a small difference between the maximum count value and the mean.

27) pminput offsets taking care of the CCD rotation
===================================================
To compute the offsets of the star from the center we need to
know the position of the star, considering all corrections.
We have to save these coordinates to the FITS file, and compute
the offsets taking care of the CCD rotation.

28) The catalog is not properly formatted for TPoint
====================================================
TPoint does not accept comments between stars in the catalog.
The catalog also needs the END keyword at the end of the file,
and the newlines has to be the Microsoft Windows newline (\r\n).
The file name itm_catalog.dat is too long for TPoint, we have to
shorten it.

29) The INDATA file is not properly formatted for TPoint
========================================================
The INDATA file has to be writtin with the Microsoft
Windows newlines (\r\n), and the temperature has to be
-50 (is the minimum temperature allowed).

30) Extended the source catalog to 171 stars
============================================
The source catalog has been extended to 171 stars,
adding new stars at low altitude.

31) pminput should create a file with the coordinated of the stars
==================================================================
When pminput analyzes the stars, it should save to a file the
coordinates, so we can always check if the coordinates have been
computed correctly.

32) Add the parameter starting_star to pmscan
=============================================
The command pmscan has to have a new argument that indicates the
star of the catalog from which it has to start the observation.

33) Implement the command set
=============================
The itm_cli should have a command set in order to set a value.
For instance:

> set filter=R

34) Implement the command map
=============================
The itm_cli should have a command ``map`` that makes a map
arount a source.

35) Change the pminput command name to pmoffsets
================================================
We are not using anymore TPoint, but we are generating
a file of offsets to be read from custom script, in order to
produce the pointing model parameters. We have to rename
the pminput command to pmoffsets.

36) Improve the autofocus algorithm
===================================
Improved the autofocus algorithm and added a new command line argument,
to be used in order to choose whether to apply the best focus position.

37) Add some fileds to the AGB fits
===================================
The FITS files require some extra fields.

38) Observations automatization
===============================
The AGB observation can be executed by the ITM command line interpreter.
There is no need to edit any file manually, because giving just one command
everything will be performed automatically.  For instance, giving the following command:

> observe catalog=agb out_dir=2018_obs01

the interpreter will read the agb catalog (agb_catalog.py) and perform the operations indicated
in the agb schedule (agb.py): go to the calibrator, perform the autofocus for the first filter,
go to the first star and take N shoots. Everything is parametrized with default values, but
if something has to be changed, some command line arguments are provided and nothing has to be
done manually.  Tha autofocus algorithm is a custom one, fast and well tested.
and perform the 

39) Options to choose from which star to start and from which to stop
=====================================================================
We want to add two new options in order to indicate where to start
and stop the observation.

> observe catalog=agb out_dir=2018_obs01 start_from=V401 stop_at=V418

40) Add command cleanfits
=========================
The command `cleanfits` hast to clean the FITS files applying the dark and flat images.

Example of usage:

> cleanfits catalog=agb observation_dir=2018_obs02

The catalog is an optional argument with 'agb' as a default value.

41) The cleanfits command stops if it does not find a dark file
===============================================================
Currently the cleanfits command stops if it does not find a dark.
Now it takes the dark file with the closest exposure.

42) The cleanfits command skips the filters already elaborated
==============================================================
The cleanfits command adds some information to the header. In order
to skip this step, the command save to a text file the name of
the files alreay updated, in order to know if it has to updatade
a file or not.

43) Add command rotate
======================
The command `rotate` has to rotate a group of images in order
to get a single image.

Example of usage:

> rotate catalog=agb observation_dir=2018_obs02

The catalog is an optional argument with 'agb' as a default value.

44) Follow the planets, the Sun and the Moon
============================================
The AntartiCOR project that will be operative in Concordia next
summer, is going to use the ITM telescope in order to point the Sun.
The `goto` command has to manage this and follow the Sun, the Moon
and the planets, in the same way as for the stars:

> goto source=sun

45) Extend the shoot command adding a distance parameter
========================================================
The `shoot` command has to take an additional parameter,
called `distance`, that represents the number of seconds
between each shoot:

> shoot exposition=2 distance=60

46) Extend the rotate command in order to choose the stars and filters
======================================================================
The `rotate` command should take two new arguments, `stars` and `filters`,
in order to choose which stars and filters rotate.  For instance, in
the following case only the images taken with the filters C and B
of stars GP and GR will be rotated:

> rotate observation_dir=2018_obs02 stars=GP,GR filters=C,B


47) The rotate command has to change the source extractor configuration
=======================================================================
The background value of the pictures depends of the filters.  Source
extractor, the program that rotates the images has a configuration file
containing the value to use as a background.  The rotate command
has to change that value before executing Source Extractor, using
as a value the mean of the image reduced by some counts.


48) Change the user documentation to describe ITM CLI
=====================================================
Currently the telescope should be operated with only
the ITM CLI, with just a few exeptions (open and close
the dome, temperature monitoring, device management).
The user documentation has to be updated accordingly.


49) Implement the ``show`` command
==================================
This command shows the images that have been
rotated and elaborated.


50) Implement the ``pmaz`` command
==================================
This command has to read the offset.txt file created
by the ``pmoffset`` command and print the azimuth
parameters for the pointing model.


51) The az_init and el_init files have to be installed
======================================================
The ITM_CLI program has to install the pointing model
az_init.txt and el_init.txt configuration files.
The commands pmaz and pmel read these files in order
to set an initialization point for the data fit.


52) Implement the ``pmel`` command
==================================
This command has to read the offset.txt file created
by the ``pmoffset`` command and print the azimuth
parameters for the pointing model.


53) Add the doc for the pointing model procedure
================================================
The pointing model procedure has to be documented.


54) Doc update: rename the bad FITS files
=========================================
The bad fits files should be renamed, not removed.


55) Add the plots of the pointing model offsets
===============================================


56) Write the developer documentation
=====================================

57) How to clone the control system HD
======================================
Put in the documentation a procedure that explains how
to clone the control system HD.

58) Reduce the number of messages of meteo update in focusServer.py
===================================================================
The script focusServer.py is executed as a daemon in the control
software machine (10.10.18.19).  This script makes a call to the
Intranet meteo page, in order to get some atmospheric values. The
Intranet is currently not available, so the focusServer.py writes
one error message per second to the log file. I reduced the
frequency to one of these messages every 5 minutes.

59) Refactoring of the ITM CLI docstrings
=========================================

60) Describe how to manually set the focus
==========================================

61) Describe RTS2 and the RTS2 interface
========================================

62) Put a link to the Yuri's PDF doc
====================================

63) Fix some typos in the team doc page
=======================================

64) Extend the goto command
===========================
The goto command has to take the equatorial coordinates:

> goto source=(6:23:57.2,-52:41:43.9)
