import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import time as tm
import pyfits
import ephem as ep


exp = 80.
ccd_temp_set = -30
filter_num = 1

Info={}
Info['Valid']='2017apr3'
Info['Expire']='undef'
Info['Creator']='flat_G4.py'
Info['Telescope']='IRAIT'
Info['Instrument']='G4-9000 Moravian'
Info['Filter']='U'
Info['Version']='1.0'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

gx.configure_eth('10.10.18.21',gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(6103)
cam.set_filter(filter_num)
cam.set_temperature_ramp(3.)
cam.set_temperature(ccd_temp_set)

chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)

curr_ccd_temp = cam.get_value(gx.GV_CHIP_TEMPERATURE)

while (curr_ccd_temp > (ccd_temp_set +1)):
    print "ccd not ready, ccd temp is: %s" % (str(curr_ccd_temp))
    tm.sleep(30.)
    curr_ccd_temp = cam.get_value(gx.GV_CHIP_TEMPERATURE)


i = 47


while i < 50:
    cam.start_exposure(exp,True,0,0,chip_w,chip_d)

    tm.sleep(exp+0.5)

    img = cam.read_image(chip_d,chip_w)

    curr_ccd_temp = cam.get_value(gx.GV_CHIP_TEMPERATURE)
    curr_camera_temp = cam.get_value(gx.GV_CAMERA_TEMPERATURE)


    hdulist=pyfits.HDUList()
    hdulist.append(pyfits.CompImageHDU(data=img))
    ihdu= len(hdulist)-1
    timeExp = dt.datetime.now()
    utcNow =timeExp - dt.timedelta(hours=8)
    jd = ep.julian_date(utcNow)

    hdulist[ihdu].header.set('extname',"FLAT")
    hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
    hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
    hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
    hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
    hdulist[ihdu].header.set('Telescop',Info['Telescope'])
    hdulist[ihdu].header.set('Instrum',Info['Instrument'])
    hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
    hdulist[ihdu].header.set('RefDoc','None','documentation reference')
    hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
    hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
    hdulist[ihdu].header.set('Chip_t',curr_ccd_temp,'Chip CCD temperature (Celsius) ')
    hdulist[ihdu].header.set('Camera_t',curr_camera_temp,'Camera temperature (Celsius) ')
    hdulist[ihdu].header.set('Expos',exp,'Exposition time (sec)')

    hdulist.writeto('/home/irait/flats/filter_1/g4_flat_%s.fits' % (i),clobber=True)

    #img = np.rot90(img,2)

    #print img

    print "done exp n. %d" % i
    print img.mean() 
    print img.max()
    print img.min()
    i += 1
    tm.sleep(3)
#imgpolt = plt.imshow(img,cmap='gray')
#plt.show()

