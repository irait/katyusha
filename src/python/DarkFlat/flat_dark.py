__DESCRIPTION__=""" Example of script for dark and flat"""

def fftfilt(data,fftFilt,x=None,forceToZero=True,renorm=False) :
   """returns the fft of the bandpass, fftFilt=None, no filtering, fftFilt<0 highpass filter, fftFilt>0, lowpass filter
      negative samples are forced to zero
   """
   import numpy.fft as FFT
   import numpy as np
   if x == None :
      step=1.
   else :
      step=(x[1:]-x[0:-1]).mean()
   ndata=len(data)
   fbaseline=FFT.fftfreq(ndata)/step
   mT=data.mean()
   ft=FFT.fft(data-mT)
   if fftFilt == None :
      return {'ft-zero':mT,'ft-step':step,'ft-Base':fbaseline/step,'ft-data':ft,'ft-data-Filtered':None,'data-Filtered':None,'ft-Filter-Threshold':None}
   else :
      if fftFilt >= 0 :
         idx=np.where(abs(fbaseline)>abs(fftFilt))[0]
      else :
         idx=np.where(abs(fbaseline)<abs(fftFilt))[0]
      print "Applied FFT Filter ",fftFilt,' over ',len(idx),' elements of fft'
      ff=ft*1
      if len(idx) > 0 : ff[idx]=0.
      TFilter=np.real(FFT.ifft(ff))+mT
      if forceToZero :
         idx=np.where(TFilter < 0)[0]
         if len(idx)>0 : 
            print "Forced to 0 negative values in ",len(idx)," samples "
            TFilter[idx]=0
      if renorm :
         print "Renormalization constant ",TFilter.sum()*step
         TFilter*=1/(TFilter.sum()*step)
      else :
         return {'ft-zero':mT,'ft-step':step,'ft-Base':fbaseline/step,'ft-data':ft,'ft-data-Filtered':ff,'data-Filtered':TFilter,'ft-Filter-Threshold':fftFilt}

import sys
import numpy as np
import pyfits
import time

outfileName='dark_flat_2017jan10.fits'

Info={}
Info['Valid']='2017jan10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Instrument']='G1-2000 Moravian'
Info['Filter']='NONE'
Info['Version']='1.'




#hdu containing the image
hduimage=1

# number of darks (0,1,..., nflat-1)
ndark=2

#list of row dark
rawdark=np.array([pyfits.open('fits/dark%d.fits'%k)[0].data for k in range(ndark)],dtype=float)

# median dark
median_dark=np.median(rawdark,axis=0)

# number of flats (0,1,..., nflat-1)
nflat=3

#list of row flats
rawflat=np.array([pyfits.open('fits/flat%d.fits'%k)[hduimage].data for k in range(nflat)],dtype=float)

# median flat
median_flat=np.median(rawflat,axis=0)

# frame is indexed [row,col]
nrow,ncol=median_flat.shape

# cutting limits are :
# all columns, columns = 0 - ncol-1
# rows in [5,1050]

good_row_min=5
good_row_max=1050
good_row=np.arange(good_row_min,good_row_max+1)

good_col_min=1
good_col_max=ncol-2
good_col=np.arange(good_col_min,good_col_max+1)


# median_dark is relativelly constant along columns 
# dark frame is median of columns 
#cmedian_dark=np.median(median_dark[:,good_row],axis=0)
cmedian_dark=np.array([np.median(median_dark[good_row,k]) for k in range(ncol)])

# removes too fast variations
filtered_cmedian_dark=fftfilt(cmedian_dark,0.03)['data-Filtered']

# final dark is repetition of filtered dark over rows
final_dark=np.array([filtered_cmedian_dark for k in range(nrow)])

# now looks for final flat
# removes final dark
flat_step1=median_flat-final_dark

# masks bad parts of frame
flat_step1[:,0:good_col_min]=np.nan
flat_step1[:,good_col_max:]=np.nan
flat_step1[0:good_row_min,:]=np.nan
flat_step1[(good_row_max+1):,:]=np.nan

# normalizes to have maximum value in the good region to be 1
final_flat=flat_step1/np.nanmax(flat_step1)


# saves in a fits file
hdulist=pyfits.HDUList()

# creates primary hdu 
hdulist.append(pyfits.PrimaryHDU())
ihdu=len(hdulist)-1
hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
hdulist[ihdu].header.set('Content','dark, flat','Content of file')
hdulist[ihdu].header.set('Valid',Info['Valid'],'Firts validity epoch')
hdulist[ihdu].header.set('Expire',Info['Expire'],'Last validity epoch')
hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
hdulist[ihdu].header.set('Telescope',Info['Telescope'])
hdulist[ihdu].header.set('Instrument',Info['Instrument'])
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
hdulist[ihdu].header.set('ElaRaw','Elaborated','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')
hdulist[ihdu].header.add_comment('File of dark and flat')

# creates image hduimage
hdulist.append(pyfits.ImageHDU(data=final_dark))
ihdu=len(hdulist)-1
hdulist[ihdu].header.set('extname','dark')
hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
hdulist[ihdu].header.set('Content','dark','Content of hdu')
hdulist[ihdu].header.set('Valid',Info['Valid'],'Firts validity epoch')
hdulist[ihdu].header.set('Expire',Info['Expire'],'Last validity epoch')
hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
hdulist[ihdu].header.set('Telescope',Info['Telescope'])
hdulist[ihdu].header.set('Instrument',Info['Instrument'])
hdulist[ihdu].header.set('ElaRaw','Elaborated','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')
hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
hdulist[ihdu].header.add_comment('dark frame')

# creates image hduimage
hdulist.append(pyfits.ImageHDU(data=final_flat))
ihdu=len(hdulist)-1
hdulist[ihdu].header.set('extname','flat')
hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
hdulist[ihdu].header.set('Content','flat','Content of hdu')
hdulist[ihdu].header.set('Valid',Info['Valid'],'Firts validity epoch')
hdulist[ihdu].header.set('Expire',Info['Expire'],'Last validity epoch')
hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
hdulist[ihdu].header.set('Telescope',Info['Telescope'])
hdulist[ihdu].header.set('Instrument',Info['Instrument'])
hdulist[ihdu].header.set('ElaRaw','Elaborated','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')
hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
hdulist[ihdu].header.set('HIERARCH good_row_min',good_row_min)
hdulist[ihdu].header.set('HIERARCH good_row_max',good_row_max)
hdulist[ihdu].header.set('HIERARCH good_col_min',good_col_min)
hdulist[ihdu].header.set('HIERARCH good_col_max',good_col_max)
hdulist[ihdu].header.add_comment('flat frame')
hdulist[ihdu].header.add_comment('Masked pixels of frame are marked as NaN')
hdulist[ihdu].header.add_comment('good_row_min, good_row_max, good_col_min, good_col_max frame the region where pixels can be used')

# 
hdulist.writeto(outfileName,clobber=True)
