import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import time as tm
import pyfits
import ephem as ep


ccd_temp_set = 10
filter_num = 6


gx.configure_eth('10.10.18.21',gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(6103)
cam.set_filter(filter_num)
cam.set_temperature_ramp(2.)
cam.set_temperature(ccd_temp_set)

curr_ccd_temp = cam.get_value(gx.GV_CHIP_TEMPERATURE)

while (curr_ccd_temp < (ccd_temp_set -1)):
    print "ccd not ready, ccd temp is: %s" % (str(curr_ccd_temp))
    tm.sleep(30.)
    curr_ccd_temp = cam.get_value(gx.GV_CHIP_TEMPERATURE)


print "now you can go sleep yuri"

