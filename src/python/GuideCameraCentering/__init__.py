__DESCRIPTION__=""" classes to center guide gamera on a single, brigher object"""

__VERSION__ = """0.0 - 2016 dec 13 - By M.Maris, Y.De Pra, S. Sartor"""

class GaussianCentering :
   def __init__(self,GCF_DarkFlatCleaned,FrameName,hwidth,gsigma,rconfidence) :
      import numpy as np
      from matplotlib.cbook import flatten
      self.hwidth=hwidth
      self.gsigma=gsigma
      self.rconfidence=rconfidence
      self.FrameName='reduced' if FrameName == None or FrameName=='' else FrameName
      #
      rc,a,Peak,c = GCF_DarkFlatCleaned.findPeakRaw(self.FrameName)
      #
      self.subframe=GCF_DarkFlatCleaned.submap(rc[0]-hwidth,rc[0]+hwidth,rc[1]-hwidth,rc[1]+hwidth)
      X=self.subframe['_row_values']
      Y=self.subframe['_col_values']
      D=self.subframe[self.FrameName]
      #
      self.shape2d=X.shape
      self.shape1d=self.shape2d[0]*self.shape2d[1]
      #
      XX=np.array(list(flatten(X)))
      YY=np.array(list(flatten(Y)))
      DD=np.array(list(flatten(D)))
      MM=DD*0
      #
      self.idxGood=np.where(np.isnan(DD)==False)[0]
      XX=XX[self.idxGood]
      YY=YY[self.idxGood]
      DD=DD[self.idxGood]
      #
      # first run is just used to give an estimate of baseline
      MDL1=Gaussian2D(XX,YY,DD,Peak,XX.mean(),YY.mean(),gsigma**2,0.,gsigma**2,0.)
      idx=np.where(MDL1.distance_from_center()>rconfidence)[0]
      if len(idx) > 10 :
         MDL1=Gaussian2D(XX,YY,DD,Peak,XX.mean(),YY.mean(),gsigma**2,0.,gsigma**2,DD[idx].mean())
      self.Mbest=MDL1.run_curve_fit(return_model=True)
   def result(self,asDict=True) :
      if asDict : return self.Mbest.parameters2dict()
      
class Gaussian2D :
   def __init__(self,X,Y,DATA,A,X0,Y0,SigmaXX,SigmaXY,SigmaYY,b) :
      """
Gaussian2D(X,Y,DATA,A,X0,Y0,SigmaXX,SigmaXY,SigmaYY,b) 
A class to handle a 2D gaussian and its fitting with scipy.oprtimize.curve_fit

It takes input X,Y,DATA which are vectors and a list of guess values for the parameters.
      """
      self.X=X.copy()
      self.Y=Y.copy()
      self.DATA=DATA.copy()
      self.A=A
      self.X0=X0
      self.Y0=Y0
      self.SigmaXX=SigmaXX
      self.SigmaXY=SigmaXY
      self.SigmaYY=SigmaYY
      self.b=b
      self.fit_cov_matrix=None
      self.delta = self.SigmaXX*self.SigmaYY-self.SigmaXY**2
      if self.delta==0 : 
         #print "Warning cov matrix with null determinant"
         pass
   def __getitem__(self,this) :
      return self.__dict__[this]
   def copy(self) :
      import copy
      return copy.deepcopy(self)
   def len(self) :
      return len(self.X)
   def keys(self) :
      return ['A','X0','Y0','SigmaXX','SigmaXY','SigmaYY','b']
   def gaussCovar(self) :
      "returns gaussian covariance matrix"
      import numpy as np
      return np.array([[self.SigmaXX,self.SigmaXY],[self.SigmaXY,self.SigmaYY]])
   def gaussInvCovar(self) :
      "returns inverse of gaussian covariance matrix"
      import numpy as np
      return np.array([[self.SigmaYY,-self.SigmaXY],[-self.SigmaXY,self.SigmaXX]])/self.delta
   def paridx(self,par) :
      "index of gaussian parameters"
      try :
         return self.keys().index(par)
      except :
         return -1
   def parameters2dict(self) :
      "return fit parameters as dictionary"
      from collections import OrderedDict
      out=OrderedDict()
      for k in self.keys() : out[k]=self[k]
      return out
   def parameters2array(self) :
      "return gaussian parameters as an array"
      import numpy as np
      return np.array([self.A,self.X0,self.Y0,self.SigmaXX,self.SigmaXY,self.SigmaYY,self.b])
   def fit_error(self,par) :
      "returns fit error for a given parameter"
      if self.fit_cov_matrix == None : return None
      i1=self.paridx(par)
      return self.fit_cov_matrix[i1,i1]**0.5
   def banner(self) :
      for k in self.keys() :
         #print "%-10s"%k,"%-20E"%self[k],
         if self.fit_cov_matrix != None :
            #print '+-',"%-20e"%self.fit_error(k),
            pass
         #print
   def __call__(self,XY,A,X0,Y0,SigmaXX,SigmaXY,SigmaYY,b) :
      """computes for XY=np.array([X,Y]) and gaussian parameters the gaussian shape, 
         the interface is designed to be used by scipy.optimize.curve_fit.
      """
      import numpy as np
      delta=SigmaXX*SigmaYY-SigmaXY**2
      Cxx=SigmaYY/delta
      Cxy=-SigmaXY/delta
      Cyy=SigmaXX/delta
      dx=(XY[0]-X0)
      dy=(XY[1]-Y0)
      sx=Cxx*dx+Cxy*dy
      sy=Cxy*dx+Cyy*dy
      out=dx*sx+dy*sy
      out=-0.5*out
      out=A*np.exp(out)+b
      return out
   def calc(self,X=None,Y=None,normalized=False) :
      "computes for the input data the given parameter the "
      import numpy as np
      out = self(np.array([self.X if X==None else X,self.Y if Y ==None else Y]),self.A,self.X0,self.Y0,self.SigmaXX,self.SigmaXY,self.SigmaYY,self.b)
      if normalized : return (out-self.b)/self.A
      return out
   def return_XY(self) :
      "return data in the right format to be used with curve_fit"
      import numpy as np
      return np.array([self.X,self.Y])
   def run_curve_fit(self,return_model=False,verbose=False) :
      import numpy as np
      from scipy.optimize import curve_fit
      try :
         pars,covm=curve_fit(self,self.return_XY(),self.DATA,p0=self.parameters2array())
      except :
         if verbose :
            #print "Fitting failled"
            pass
         nn=len(self.parameters2array())
         pars=np.zeros(nn)+np.nan
         covm=np.zeros([nn,nn])+np.nan
      if return_model :
         out=self.copy()
         out.A=pars[0]
         out.X0=pars[1]
         out.Y0=pars[2]
         out.SigmaXX=pars[3]
         out.SigmaXY=pars[4]
         out.SigmaYY=pars[5]
         out.b=pars[6]
         out.fit_cov_matrix=covm
         return out
      return pars,cov
   def distance_from_center(self) :
      return ((self.X-self.X0)**2+(self.Y-self.Y0)**2)**0.5
   def estimateNoise(self,threshold=0.01) :
      import numpy as np
      o=self.calc(normalized=True)
      idx=np.where(o<threshold)[0]
      return self.DATA[idx].std()
   def signal2noise(self,threshold) :
      return self.A/self.estimateNoise(threshold=threshold)
   def residual(self) :
      return self.DATA-self.calc()
                
if __name__ == '__main__' :
   import sys
   
   if len(sys.argv) > 1 :
      ifile=sys.argv[1]
   else : 
      ifile='fits_test/PRE_2016-12-13T18:30:05.601852_exp_0.001.fits'
   
   hwidth=60
   gsigma=hwidth/10.
   rconfidence=5*gsigma

   df=GuideCameraDarkFlat('fits_test/dark_flat_2016dec12.fits')
   
   f1=GuideCameraFrame(ifile)
   f1.darkFlat(df)
   
   GC=GaussianCentering(f1,'reduced',hwidth,gsigma,rconfidence)
   print GC.result()
   
   

   #f2=GuideCameraFrame('fits_test/FIX_2016-12-13T22:25:27.899676_exp_0.001.fits')
   
