import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt

gx.configure_eth('10.10.18.21',gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(3247)

chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)
cam.start_exposure(0.005,True,0,0,chip_w,chip_d)

img = cam.read_image(chip_d,chip_w)
#img = np.rot90(img,2)

print "max: "+str(img.max())
print "min: "+str(img.min())
print "mean: "+str(img.mean())

imgpolt = plt.imshow(img)
plt.show()

