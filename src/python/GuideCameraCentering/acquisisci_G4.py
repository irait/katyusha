import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt
import time as tm
import pyfits

exp = 0.6

gx.configure_eth('10.10.18.21',gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(6103)
cam.set_filter(0)
#cam.set_temperature(-30)

chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)

cam.start_exposure(exp,True,0,0,chip_w,chip_d)

tm.sleep(5)

img = cam.read_image(chip_d,chip_w)

print img.mean() 
print img.max()
print img.min()
imgpolt = plt.imshow(img,cmap='gray')
plt.show()

