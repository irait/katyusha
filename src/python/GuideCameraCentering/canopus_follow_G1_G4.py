#!/usr/bin/python

from find_center_object import centrami

import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo
import cv2
import pylab as plt

__BASE_PATH__ = "/home/irait/fits/canopus_20170324/"


fixedStar=ephem.star('Canopus')


exp = 0.003
exp_g4 = 0.3
filterDay = True


def getAltAzi(alt_d=0.,azi_d=0.):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt)+alt_d,(np.rad2deg(fixedStar.az)+180.0 + azi_d)%360.)

def getRADec():
    jd = ep.julian_date(dt.datetime.utcnow())
    return (np.interp(jd,HTI.UT_JD,HTI.Astro_RA),np.interp(jd,HTI.UT_JD,HTI.Astro_Dec))  


class FrameToFits:
	def __init__(self,image, filename, alt, azi, exp_time_loc, chip_temp, exp):
		self._image = image
        	self._filename =  filename
		self._alt = alt
		self._azi = azi
		self._altCorr = None
		self._aziCorr = None
		self._tiltX = None
		self._tiltY = None
		self._fitsRef = None
		self._exp_time_loc = exp_time_loc
		self._chip_temp = chip_temp
		self._exp = exp 
		self._teoricAlt = None
		self._teoricAz = None

	def setCorrection(self, altCorr, aziCorr):
		self._altCorr = altCorr
		self._aziCorr = aziCorr

	def setTilt(self, tiltX, tiltY):
		self._tiltX = tiltX
		self._tiltY = tiltY

	def setReference(self, filename):
		self._fitsRef = filename
	
	def setTeoricCoord(self,altTeoric, azTeoric):
		self._teoricAlt = altTeoric
		self._teoricAz = azTeoric

class Tilt:
    BUFFER_SIZE = 1024

    def __init__(self,hostname,port):
        self.__s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.connect((hostname,port))

    def __del__(self):
        self.__s.close()

    def __get_tilt(self,i):
        if i > 32767:
            return -(65536 - i)
        else:
            return i

    def getTilt(self):
        MESSAGE = '{"function":"R_REGISTERS","address":"0x0008","count":2}'
        self.__s.send(MESSAGE)
        data = self.__s.recv(Tilt.BUFFER_SIZE)

        j = json.JSONDecoder()
        try:
            jobj = j.decode(data[0:-1])
        except:
            return None
        if jobj.has_key('error'):
            return None

        return(self.__get_tilt(jobj['data'][0]), self.__get_tilt(jobj['data'][1]))

import random
import time
import rts2.scriptcomm
import sys

from datetime import datetime as dtd


from threading import Thread,Lock
import time as tm

class ObjLock:
    def __init__(self,lock,block=True):
        self._lock = lock
        #self.is_lock = self._lock.acquire(block) ##very ugly
        self.is_lock = True ## super ugly

    def __del__(self):
        #self._lock.release() ##very ugly
        pass

class G4_thread:
    def __init__(self):
        self._do_read = 0
        self._cam = gx.GxCCD(6103)
        if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
            print "bad camera, exiting"
            exit(1)
        self._d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
        self._w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
        if(self._d == -1 or self._w == -1):
            print "bad camera, exiting"
            exit(1)
        self._cam.set_filter(0)

    def restartG4Connection(self):
	self._cam = None
	self._cam = gx.GxCCD(6103)
	if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
            print "bad camera, exiting"
            exit(1)
	

    def setNight(self):
	global exp_g4
	global filterDay
	self._cam.set_filter(6)
	exp_g4 = 2.
	filterDay = False

    def setDay(self):
	global exp_g4
	global filterDay
	self._cam.set_filter(0)
	exp_g4 = 0.2
	filterDay = True

    def is_ready(self):
        if tm.time()> self._do_read and self._do_read != 0:
            return True
        else:
            return False
        
    def start_exposure(self,exp_time):
        if self.is_ready() or self._do_read == 0:
            e = self._cam.start_exposure(exp_time,True,0,0,self._w,self._d)
            if e == 0:
                tm.sleep(exp_time+0.1)
                self._do_read = tm.time()+20
            return e == 0

    def get_image(self):
        if self.is_ready():
            image = self._cam.read_image(self._d,self._w)
	    chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
            self._do_read = 0
            return image, chip_temp
        else:
            return None
                


Info={}
Info['Valid']='2016dec10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Guide']='G1-2000 Moravian'
Info['Instrument']='G4-9000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='None'
Info['Version']='1.2'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
        exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
    if(self._chip_d == -1 or self._chip_w == -1):
        exit(1)


    self.g4_th = G4_thread()
        
  def saveFits(self,frame, frame1):
	

	temperature = None
        pressure = None
        humidity = None
        w_dir = None
        w_speed = None
        try:
            mt=Meteo.Meteo(lg=None)
            temperature,pressure,humidity, w_dir, w_speed =mt.retrieve()
        except:
            self.log('W', 'no meteo data available')

	# saves in a fits file
	hdulist=pyfits.HDUList()


	# creates image hduimage (science)
	hdulist.append(pyfits.CompImageHDU(data=frame1._image))
	ihdu=len(hdulist)-1
	
	timeExp = frame1._exp_time_loc
	utcNow =timeExp - dt.timedelta(hours=8)
	jd = ep.julian_date(utcNow)

	hdulist[ihdu].header.set('extname',"LIGHT")
	hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
	hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Tilt_str',Info['Tiltmeter'])
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')
	hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('CoordAlt',frame1._alt,'Alt coordinates telescope')
	hdulist[ihdu].header.set('CoordAzi',frame1._azi,'Azi coordinates telescope')
	hdulist[ihdu].header.set('CoAlt_Mo',frame1._teoricAlt,'Alt coordinates in model (calculated)')
	hdulist[ihdu].header.set('CoAz_Mo',frame1._teoricAz,'Az coordinates in model (calculated)')
	hdulist[ihdu].header.set('TiltX',frame1._tiltX,'Tilt value measured in x axis (ADU)')
	hdulist[ihdu].header.set('TiltY',frame1._tiltY,'Tilt value measured in y axis (ADU)')
	hdulist[ihdu].header.set('DeltaAlt',frame1._altCorr,'Alt correction from  center (deg)')
	hdulist[ihdu].header.set('DeltaAzi',frame1._aziCorr,'Azi correcction from center (deg)')
	hdulist[ihdu].header.set('Chip_t',frame1._chip_temp,'Chip temperature measured (Celsius) ')
	hdulist[ihdu].header.set('Expos',frame1._exp,'Exposition time (sec)')
	if (temperature is not None):
            hdulist[ihdu].header.set('Temp', temperature, 'Temperature (Celsius)')
            hdulist[ihdu].header.set('Press', pressure, 'Pressure (hPa)')
            hdulist[ihdu].header.set('Hum', humidity, 'Humidity (%)')
            hdulist[ihdu].header.set('W_dir', w_dir, 'Wind_dir  (degrees)')
            hdulist[ihdu].header.set('W_speed', w_speed, 'Wind_speed (m/s)')

	
	# creates  (guide) hdu
	timeExp = frame._exp_time_loc
	utcNow =timeExp - dt.timedelta(hours=8)
	jd = ep.julian_date(utcNow)

	hdulist.append(pyfits.CompImageHDU(data=frame._image)) #CompImageHDU
	ihdu=len(hdulist)-1
	hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(dt.datetime.utcnow()),'Date of creation UTC')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Guide'])
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')
	hdulist[ihdu].header.set('CoordAlt',frame._alt,'Alt coordinates telescope')
	hdulist[ihdu].header.set('CoordAzi',frame._azi,'Azi coordinates telescope')
	hdulist[ihdu].header.set('DeltaAlt',frame._altCorr,'Alt correction from center (deg)')
	hdulist[ihdu].header.set('DeltaAzi',frame._aziCorr,'Azi correcction from center (deg)')	
	hdulist[ihdu].header.set('TiltX',frame._tiltX,'Tilt value measured in x axis (ADU)')
	hdulist[ihdu].header.set('TiltY',frame._tiltY,'Tilt value measured in y axis (ADU)')
	hdulist[ihdu].header.set('Chip_t',frame._chip_temp,'Chip temperature measured (Celsius) ')
	hdulist[ihdu].header.set('Expos',frame._exp,'Exposition time (sec)')
	

	hdulist.writeto(frame._filename,clobber=True)



  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp

  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def waitAndRepoint(self):

      self.log('W', "No target found, probably cloudly, sleep 5 minutes")
      time.sleep(60*5)

      #reset counter for new acquisiion                                                                                      
      counter = 0
      #reset previous spiral search correction                                                                               
      alt_d = 0.
      azi_d = 0.
      #get coordinates for new time                                                                                          
      alt,az = getAltAzi(alt_d,azi_d)
      self.log('I', 'Recalculated coord after wait -- Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
      self.setValue('TEL_','{} {}'.format(alt,az),'T0')
      time.sleep(5.)

      while 'STOPPED' != self.getValue('status','T0'):
          time.sleep(0.1)



  def run(self):

    myMinute = dtd.now().minute 
    global exp
    global exp_g4
    alt_d = -0.
    azi_d = -0.
    delta_x_G1 = 0.055 #0.055
    delta_y_G1 = -0.10  #-0.085
    mylog = open("%snew_dati_%s.csv"%(__BASE_PATH__,now_str()),"wt+")
 
    mylog_g4 = open("%sg4_data_%s.csv"%(__BASE_PATH__,now_str()),"wt+")

    self.setValue('point','false','T0')

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    #set position of the star
    alt,az = getAltAzi(alt_d,azi_d)
    self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
    self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
    time.sleep(2.)
    while 'STOPPED' != self.getValue('status','T0'):
        time.sleep(0.1)
        
#    self.setValue('point','false','T0')


    targetNotFoundCnt = 0
    maxTryToRecognize = 20
    do_repeat = True
    counter = 0
    while do_repeat:
        # first part: find obj in field and calculate first correction
           
        image, e_time, chip_temp = self.gx_exposure(exp,5)
        filename = "%sGUIDE_CAMERA_FIRST_HIT_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp)

        alt_t, az_t = self.getPosTelAdj()
        self.log('I', 'acquired %s' % filename)
	#plt.imshow(image)
	#plt.show()
        deltax, deltay, newExp, counter= centrami(image, exp, counter)
        if (newExp is not None):
            exp = newExp

        if (deltax is not None) and (deltay is not None) :
            do_repeat = False
            targetNotFoundCnt = 0
            self.log('I', 'deltax %.4f deltay %.4f of cur_pos alt %.4f alt %.4f ' % (deltax, deltay, alt_t, az_t))
            mylog.write("%s,%.4f,%.4f,%.4f,%.4f,"%(filename,alt_t,az_t,deltay,deltax))
		
            alt_t, az_t = self.getPosTelAdj()
            alt_t = alt
            az_t = az
            fr = FrameToFits(image, filename, alt_t, az_t, e_time, chip_temp, exp) 
            self.saveFits(fr,fr)
            alt_t -= deltay
            az_t  -= deltax
            az_t %= 360.	     
        else:
            targetNotFoundCnt += 1
            self.log('W', "TARGET NOT RECOGNIZE...")

            if ((newExp is not None) and (counter >= 0)): #bad exposition or bad frame
                self.log('I', "TRY TO re-EXPOSE")
                do_repeat = True
            elif ((newExp is not None) and (counter < 0)): #target not present in field
                self.log('I', "No target found, spiral search move {0:4.6f}".format(counter))
                if (counter > -9):
                    myMove = 0.30
                    if (counter == -1):
                        alt_d = 0.
                        azi_d = 0.
                    alt_t, az_t = self.getPosTelAdj()
                    self.log('I', 'Actual coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_t,az_t))
                    mycnt = abs(counter)
                    if (mycnt == 1):
                        alt_d -= myMove
                    elif (mycnt == 2):
                        azi_d += myMove
                    elif (mycnt == 3):
                        alt_d += myMove
                    elif (mycnt == 4):
                        alt_d += myMove
                    elif (mycnt == 5):
                        azi_d -= myMove
                    elif (mycnt == 6):
                        azi_d -= myMove
                    elif (mycnt == 7):
                        alt_d -= myMove
                    elif (mycnt == 8):
                        alt_d -= myMove

                    alt,az = getAltAzi(alt_d,azi_d)
                    self.log('I', 'Add delta coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_d,azi_d))
                    self.log('I', 'spiral search coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt,az))
                    
                    self.setValue('TEL_','{} {}'.format(alt,az),'T0')
                    time.sleep(2.)
                    while 'STOPPED' != self.getValue('status','T0'):
                        time.sleep(0.1)
                else:
                    #spiral search end with no result
                    self.waitAndRepoint()
		
            else: #bad weather
                self.waitAndRepoint()

        if (targetNotFoundCnt > maxTryToRecognize):
            exit(1)
            
    #end first part (found object in the field and appropriate correction to move in the right position)	
    az_t += delta_x_G1
    alt_t += delta_y_G1
    self.log('I', 'NEW Alt: {0:4.6f}, Az: {1:4.6f} with guide camera correction deltax: {2:4.6f} and deltay: {3:4.6f}'.format(alt_t,az_t, delta_x_G1, delta_y_G1))
    self.setValue('TEL_','{} {}'.format(alt_t,az_t),'T0')
    time.sleep(3.)
    while 'STOPPED' != self.getValue('status','T0'):
        time.sleep(0.1)

    targetNotFoundCnt = 0
    firstTime  = True
    G1FrameToSave = None
    G4Alt= None
    G4Az=  None
    G4Time = None
    G4Tilt = None
    G4AltTeoric = None
    G4AzTeoric = None
    imgRead = True
 

    G4pixel2deg = 0.13483871 / 3600.0
    surf = cv2.SURF(20)

    self.setValue('point','true','T0')

    while True: #maintain star in the same point, store correction and save a fits every hour
            
        image, e_time, chip_temp = self.gx_exposure(exp,5)
        #plt.imshow(image)
        #plt.show()
        filename = "%sG1G4_CANOPUS_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp_g4)

        alt_t, az_t = self.getPosTelAdj()
        fr = FrameToFits(image, filename, alt_t, az_t, e_time, chip_temp, exp)

        self.log('I', 'acquired %s' % filename)
                    
        deltax, deltay, newExp, counter = centrami(image, exp,counter)
        if (newExp is not None):
            exp = newExp
        if (deltax is not None) and (deltay is not None) :
            targetNotFoundCnt -= 1 #instead of (=0) to deny false positive
            tilt = Tilt('10.10.18.19',8000)
            tl = tilt.getTilt()
            if tl is not None:
                mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%s,%s\n"%(filename,alt_t,az_t, deltax, deltay, tl[0], tl[1]))
                fr.setTilt(tl[0], tl[1])
            else:
                mylog.write("%s,%.4f,%.4f,%.4f,%.4f\n"%(filename,alt_t,az_t, deltax, deltay))
                
            mylog.flush()
	    fr.setCorrection(deltay, deltax)
			
	    

	    alt_t -= deltay
            alt_t += delta_y_G1

            az_t -= deltax
            az_t += delta_x_G1
            az_t %= 360.
        
            #self.setValue('point','true','T0')
            


	    time.sleep(25)

	    if self.g4_th.is_ready():
		    imgRead = True
                    self.log('I', "G4 image ready")
                    image, chip_temp = self.g4_th.get_image()        
			
                    fr_g4 = FrameToFits(image, G1FrameToSave._filename, G4Alt, G4Az, G4Time,chip_temp, exp_g4)
		    fr_g4.setTilt(G4Tilt[0],G4Tilt[1])
		    
		    fr_g4.setTeoricCoord(G4AltTeoric, G4AzTeoric)

		    imgray = np.uint8(image/2**8)
		    ret, img = cv2.threshold(imgray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
		    kp, des = surf.detectAndCompute(img,None)

		    maxkval = 0
		    maxk = None
		    delta_x = None
		    delta_y = None

		    for k in kp:
			if k.size > maxkval:
				maxkval = k.size
				maxk = k

		    if maxk is not None:
			mypoint = maxk
			x = round(mypoint.pt[1])
			y = round(mypoint.pt[0])
		        delta_x = (1528 - x) * G4pixel2deg
		        delta_y = (1528 - y) * G4pixel2deg
			
		    

	
		    if (delta_x is not None) and (delta_y is not None) :
			self.log('I', 'delta_x_g4:  %0.6f, delat_y_g4: %0.6f ' % (delta_x, delta_y))
		    	fr_g4.setCorrection( delta_y, delta_x)
		    	mylog_g4.write("%s,%.4f,%.4f,%.4f,%.4f,%s,%s,%.4f,%.4f,%s\n"%("G4Frame_"+now_str(G4Time),G4Alt, G4Az, delta_x, delta_y,G4Tilt[0],G4Tilt[1], G4AltTeoric, G4AzTeoric,now_str(G4Time) ))
			mylog_g4.flush()

			if (abs(delta_x) > 0.05 or abs(delta_y) > 0.05):
				exit(1)
				#self.setValue('TEL_','{} {}'.format(alt_t,az_t),'T0')
				#while 'POINTING' != self.getValue('status','T0'):
				#    time.sleep(0.1)

                    self.saveFits(G1FrameToSave,fr_g4)

		    self.log('I', "exposure saved") 

		    G1FrameToSave = None
		    G4Alt= None
		    G4Az = None
		    G4Time = None
		    G4Tilt = None
		    G4AltTeoric = None
		    G4AzTeoric = None

		    #remove defected pixels
		    image[image > 65000] = 0
		    #adapt exposure time
		    g4max = image.max() 
		    if g4max < 30000:
                       exp_g4 *= 2.
		    elif g4max > 60000:
		    	exp_g4 /= 2.

		    if exp_g4 > 10. and filterDay:
		    	self.setNight()	
		    if exp_g4 < 0.4 and not filterDay:
		    	self.setDay()

		    self.g4_th.restartG4Connection()
		      	
	    
	    elif self.g4_th.start_exposure(exp_g4) and imgRead:
                     			
  		    imgRead = False
                    self.log('I', "G4 exposure")  
		    alt_t, az_t = self.getPosTelAdj()
		    alt_teoric,az_teoric = getAltAzi()
		    G4Alt= alt_t
		    G4Az = az_t
		    G4Time = dt.datetime.now()
		    G4AltTeoric = alt_teoric
		    G4AzTeoric = az_teoric

		    tilt = Tilt('10.10.18.19',8000)
		    tl = tilt.getTilt()
		    if tl is not None:
		        G4Tilt =tl

		    G1FrameToSave = fr 
		    time.sleep(0.1)
		    

        else:
            if (newExp is not None):
                self.log('I', "TRY TO REDO EXPOSE")
                targetNotFoundCnt += 1
            else:
                self.log('I', 'TARGET LOST: bad correction!!!')
                exit(1)

        if (targetNotFoundCnt > maxTryToRecognize):
            exit(1)

    time.sleep(5)

a = Script()
a.run()

