#!/usr/bin/python

from find_center_object import centrami

import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo

__BASE_PATH__ = "/home/irait/fits/canopus_stop_20170208/"

fixedStar=ephem.star('Canopus')



exp = 0.001

def getAltAzi(alt_d=0.,azi_d=0.):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt)+alt_d,(np.rad2deg(fixedStar.az)+180.+azi_d)%360.)

def getRADec():
    jd = ep.julian_date(dt.datetime.utcnow())
    return (np.interp(jd,HTI.UT_JD,HTI.Astro_RA),np.interp(jd,HTI.UT_JD,HTI.Astro_Dec))  


class FrameToFits:
	def __init__(self,image, filename, typeFits, alt, azi, exp_time_loc, chip_temp, exp):
		self._image = image
        	self._filename =  filename
		self._type = typeFits
		self._alt = alt
		self._azi = azi
		self._altCorr = None
		self._aziCorr = None
		self._tiltX = None
		self._tiltY = None
		self._fitsRef = None
		self._exp_time_loc = exp_time_loc
		self._chip_temp = chip_temp
		self._exp = exp 

	def setCorrection(self, altCorr, aziCorr):
		self._altCorr = altCorr
		self._aziCorr = aziCorr

	def setTilt(self, tiltX, tiltY):
		self._tiltX = tiltX
		self._tiltY = tiltY

	def setReference(self, filename):
		self._fitsRef = filename

class Tilt:
    BUFFER_SIZE = 1024

    def __init__(self,hostname,port):
        self.__s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.connect((hostname,port))

    def __del__(self):
        self.__s.close()

    def __get_tilt(self,i):
        if i > 32767:
            return -(65536 - i)
        else:
            return i

    def getTilt(self):
        MESSAGE = '{"function":"R_REGISTERS","address":"0x0008","count":2}'
        self.__s.send(MESSAGE)
        data = self.__s.recv(Tilt.BUFFER_SIZE)

        j = json.JSONDecoder()
        try:
            jobj = j.decode(data[0:-1])
        except:
            return None
        if jobj.has_key('error'):
            return None

        return(self.__get_tilt(jobj['data'][0]), self.__get_tilt(jobj['data'][1]))

import random
import time
import rts2.scriptcomm
import sys


Info={}
Info['Valid']='2017dec10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Instrument']='G1-2000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='NONE'
Info['Version']='1.1b'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):

  
  halfCCDdeg = 0
  halfCCDdeg_heigh = 0

  def __init__(self):
	
    global halfCCDdeg
    global halfCCDdeg_heigh

    pix2deg = 0.930205229 / 3600.0

    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
        self.log('E','cannot connect to camera, exiting...')
        exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)

    halfCCDdeg = ((self._chip_w / 2.)) * pix2deg
    halfCCDdeg_heigh = ((self._chip_d / 2.)) * pix2deg

    #self.log('I', 'half cd heeight %.4f' % halfCCDdeg_heigh)

    if(self._chip_d == -1 or self._chip_w == -1):
        self.log('E', 'got wrong parameters from camera, exiting')
        exit(1)
        
  def saveFits(self,frame):
	timeExp = frame._exp_time_loc
	utcNow =timeExp - dt.timedelta(hours=8)
	jd = ep.julian_date(utcNow)
	# saves in a fits file

        temperature = None
        pressure = None
        humidity = None
        w_dir = None
        w_speed = None
        try:
            mt=Meteo.Meteo(lg=None)
            temperature,pressure,humidity, w_dir, w_speed =mt.retrieve()
        except:
            self.log('W', 'no meteo data available')
	hdulist=pyfits.HDUList()
	
	# creates primary hdu 
	hdulist.append(pyfits.PrimaryHDU())
	ihdu=len(hdulist)-1
	hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(dt.datetime.utcnow()),'Date of creation UTC')
	hdulist[ihdu].header.set('Content',frame._type,'Content of file')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')	
	# creates image hduimage
	hdulist.append(pyfits.ImageHDU(data=frame._image))
	ihdu=len(hdulist)-1
	hdulist[ihdu].header.set('extname',"LIGHT")
	hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
	hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
	hdulist[ihdu].header.set('Content','Image %s with center correction data ' % (frame._type),'Content of hdu')
        hdulist[ihdu].header.set('Sequence',frame._type,'Sequnce of hdu with tel stopped')

	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Tilt_str',Info['Tiltmeter'])
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')
	hdulist[ihdu].header.set('FitsRef',frame._fitsRef,'fits file reference (dependance)')
	hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('CoordAlt',frame._alt,'Alt coordinates telescope')
	hdulist[ihdu].header.set('CoordAzi',frame._azi,'Azi coordinates telescope')
	#hdulist[ihdu].header.set('DeltaAlt',frame._altCorr,'Alt correction from center (deg)')
	#hdulist[ihdu].header.set('DeltaAzi',frame._aziCorr,'Azi correcction from center (deg)')
	hdulist[ihdu].header.set('TiltX',frame._tiltX,'Tilt value measured in x axis (ADU)')
	hdulist[ihdu].header.set('TiltY',frame._tiltY,'Tilt value measured in y axis (ADU)')
	hdulist[ihdu].header.set('Chip_t',frame._chip_temp,'Chip temperature measured (Celsius) ')
	hdulist[ihdu].header.set('Expos',frame._exp,'Exposition time (sec)')
        if (temperature is not None):
            hdulist[ihdu].header.set('Temp', temperature, 'Temperature (Celsius)')
            hdulist[ihdu].header.set('Press', pressure, 'Pressure (hPa)')
            hdulist[ihdu].header.set('Hum', humidity, 'Humidity (%)')
            hdulist[ihdu].header.set('W_dir', w_dir, 'Wind_dir  (degrees)')
            hdulist[ihdu].header.set('W_speed', w_speed, 'Wind_speed (m/s)')

	hdulist.writeto(frame._filename,clobber=True)



  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
          image = np.rot90(image,2)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp


  def run(self):
    
    mylog = open("%sdati_%s.csv"%(__BASE_PATH__,now_str()),"wt+")
	

    global halfCCDdeg
    global exp
    alt_d = 0.
    azi_d = 0.
 
    self.setValue('point','false','T0')    
    alt_st,az_st=self.getValue('position.AA','T0').split()
    alt_t = float(alt_st)
    az_t= float(az_st)

    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    alt,az = getAltAzi(alt_d,azi_d)
    self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
    self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
    time.sleep(5.)

    while 'STOPPED' != self.getValue('status','T0'):
        time.sleep(0.1)
    alt_st,az_st=self.getValue('position.AA','T0').split()

    targetNotFoundCnt = 0
    maxTryToRecognize = 10 # DEBUG 20

    while True:
	
        tilt = Tilt('localhost',8000)
        tl = tilt.getTilt()
        if tl is not None:
            mylog.write("%s,%s,"%tl)
            mylog.flush()

        do_repeat = True
	counter = 0
        while do_repeat:
            image, e_time, chip_temp = self.gx_exposure(exp,5)
            filename = "%sGUIDE_CAMERA_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp)

            alt_st,az_st=self.getValue('position.AA','T0').split()
            alt_t = float(alt_st)
            az_t= float(az_st)
            az_t += 180.
            az_t %= 360.

            self.log('I', 'acquired %s' % filename)
        
            
	    deltax, deltay, newExp, counter= centrami(image, exp, counter)
	    if (newExp is not None):
	    	exp = newExp

            if (deltax is not None) and (deltay is not None) :
                do_repeat = False
		targetNotFoundCnt = 0

                self.log('I', 'pre deltax %.4f' % (deltax))

		deltax -= (halfCCDdeg+0.10)
 		deltay += 0.08

		self.log('I', 'deltax %.4f deltay %.4f ' % (deltax, deltay))
		
                mylog.write("%s,%.4f,%.4f,%.4f,%.4f,"%(filename,alt_t,az_t,deltay,deltax))
		
		
		alt_st,az_st=self.getValue('position.AA','T0').split()
		alt_t = float(alt_st)
		az_t= float(az_st)
		az_t += 180.

		
		alt_t += deltay
		az_t  += deltax
		    
		az_t %= 360.

		fr = FrameToFits(image, filename, -1, alt_st, az_st, e_time, chip_temp, exp) 
	    	fr.setTilt(tl[0], tl[1])
		fr.setCorrection(deltay, deltax)
		lastPreFitsFile = fr._filename
		self.saveFits(fr)

	     
            else:
		targetNotFoundCnt += 1
                self.log('W', "TARGET NOT RECOGNIZE...")
                #fr = FrameToFits(image, filename, "MERDA", alt_st, az_st, e_time, chip_temp, exp)
                #self.saveFits(fr)   #DEBUG
		if ((newExp is not None) and (counter >= 0)): #bad exposition or bad frame
		   self.log('I', "TRY TO re-EXPOSE")
		   do_repeat = True
		elif ((newExp is not None) and (counter < 0)): #target not present in field
		   self.log('I', "No target found, spiral search move {0:4.6f}".format(counter))
		   if (counter > -9):

                           #myMove = 0.166666667
                           myMove = 0.30
                           if (counter == -1):
                               alt_d = 0.
                               azi_d = 0.

                           alt_st,az_st=self.getValue('position.AA','T0').split()
                           alt_t = float(alt_st)
                           az_t= float(az_st)
                           az_t += 180.
                           az_t %= 360.

                           self.log('I', 'Actual coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_t,az_t))


                           mycnt = abs(counter)

                           if (mycnt == 1):
                               alt_d -= myMove
                           elif (mycnt == 2):
                               azi_d += myMove
                           elif (mycnt == 3):
                               alt_d += myMove
                           elif (mycnt == 4):
                               alt_d += myMove
                           elif (mycnt == 5):
                               azi_d -= myMove
                           elif (mycnt == 6):
                               azi_d -= myMove
                           elif (mycnt == 7):
                               alt_d -= myMove
                           elif (mycnt == 8):
                               alt_d -= myMove

			   alt,az = getAltAzi(alt_d,azi_d)
			   self.log('I', 'Add delta coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_d,azi_d))
			   self.log('I', 'spiral search coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt,az))

			   self.setValue('TEL_','{} {}'.format(alt,az),'T0')
			   time.sleep(5.)

	       		   while 'STOPPED' != self.getValue('status','T0'):
		    		time.sleep(0.1)
		   else:
		      	
		       	 self.log('W', "No target found, probably cloudly, sleep 5 minutes")
		   	 time.sleep(60*5)
				
			 #reset counter for new acquisiion
		    	 counter = 0
		    	 #reset previous spiral search correction
		    	 alt_d = 0.
		    	 azi_d = 0.
		    	 #get coordinates for new time
		    	 alt,az = getAltAzi(alt_d,azi_d)
		    	 self.log('I', 'Recalculated coord after wait -- Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
		    	 self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
		    	 time.sleep(5.)

		    	 while 'STOPPED' != self.getValue('status','T0'):
			 	time.sleep(0.1)
 

		else: #bad weather
		    self.log('W', "No Target Found, Probably cloudly, sleep 5 minutes")
		    time.sleep(60*5)
			
		    #reset counter for new acquisiion
		    counter = 0
		    #reset previous spiral search correction
		    alt_d = 0.
		    azi_d = 0.
		    #get coordinates for new time
		    alt,az = getAltAzi(alt_d,azi_d)
		    self.log('I', 'Recalculated coord after wait -- Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
		    self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
		    time.sleep(5.)

		    while 'STOPPED' != self.getValue('status','T0'):
			time.sleep(0.1)

	    if (targetNotFoundCnt > maxTryToRecognize):
		exit(1)
		    
	#end cicle1 (found planet in the field with appropriate correction to be in the right part centered)
		
        self.log('I', 'NEW Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_t,az_t))

        self.setValue('TEL_','{} {}'.format(alt_t,az_t),'T0')
        time.sleep(5.)

        while 'STOPPED' != self.getValue('status','T0'):
            time.sleep(0.1)

        tilt = Tilt('localhost',8000)
        tl = tilt.getTilt()
        if tl is not None:
            mylog.write("%s,%s,"%tl)
            mylog.flush()

        do_repeat = True
        targetNotFoundCnt = 0
	
	countExposures = 0
	nextTimeOut = False
	meanDiff = None
	d0 = None
	d1 = None

        while do_repeat:
            
            image, e_time, chip_temp = self.gx_exposure(exp,5)
            filename = "%sGUIDE_CAMERA_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp)
	
            alt_st,az_st=self.getValue('position.AA','T0').split()
            alt_t = float(alt_st)
            az_t= float(az_st)
            az_t += 180.
            az_t %= 360.
            
	    fr = FrameToFits(image, filename, countExposures, alt_st, az_st, e_time, chip_temp, exp)
	    fr.setTilt(tl[0], tl[1])

            self.log('I', 'acquired %s' % filename)

	    deltax, deltay, newExp, counter = centrami(image, exp, counter)

	    if (newExp is not None):
	       exp = newExp

            if (deltax is not None) and (deltay is not None) :
		
		countExposures += 1
                deltax *= -1
                self.log('I', 'deltax is  %.4f deltay is  %.4f' % (deltax, deltay))

                self.log('I', '(halfCCDdeg_heigh - 0.02) is  %.4f  and (-halfCCDdeg_heigh +0.005) is  %.4f' % ((halfCCDdeg_heigh - 0.02), (-halfCCDdeg_heigh + 0.005)))
                
                if ((deltay > (halfCCDdeg_heigh - 0.02)) or (deltay < (-halfCCDdeg_heigh + 0.005))):
                    do_repeat = False
                    

                if (deltax > 0.):

                    if (d0 is None):
			d0 = deltax
                    elif (d1 is None):
			d1 = deltax
                    else:
                        d0 = d1
                        d1 = deltax

                    if ((d0 is not None) and (d1 is not None)):
                        self.log('I', 'd0 is  %.4f d1 is %.4f ' % (d0, d1))
 
                        diff = d1 - d0
                        nextPoint = deltax + diff

                        self.log('I', 'nextpoint is  %.4f' % nextPoint)

                        if (nextPoint > abs(halfCCDdeg)):
                            do_repeat = False
                          
                           

                targetNotFoundCnt = 0
                mylog.write("%s,%.4f,%.4f\n"%(filename,alt_t,az_t))
                mylog.flush()

		fr.setCorrection(deltay, deltax)
		fr.setReference(lastPreFitsFile)
                
		self.saveFits(fr)

	    else:
		if (newExp is not None):
		   self.log('I', "TRY TO REDO EXPOSE")
		   do_repeat = True
                   targetNotFoundCnt += 5
		else:
		   self.log('I', 'TARGET LOST: bad correction!!!')
        	   exit(1)

            if (targetNotFoundCnt > maxTryToRecognize):
                self.log('I', 'TARGET LOST!')
                do_repeat = False
                break
                #exit(1)

	    time.sleep(5)

        alt,az = getAltAzi() 
   	
        self.log('I', "REPOINTING go to {} alt {} az".format(alt,az))
  	self.setValue('TEL_','{} {}'.format(alt,az),'T0')
   	time.sleep(5.)

   	while 'STOPPED' != self.getValue('status','T0'):
		time.sleep(0.1)

a = Script()
a.run()

