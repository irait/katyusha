#!/usr/bin/python

import numpy as np
import pygxccd as gx
import pyfits
import ephem as ep
import datetime as dt
import time
import sys

exp_time = 0.002

Info={}
Info['Valid']='2017jan10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Instrument']='G1-2000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='NONE'
Info['Version']='1.1b'



def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')


gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(3247)
chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)
cam.start_exposure(exp_time,False,0,0,chip_w,chip_d)
img = cam.read_image(chip_d,chip_w)



utcNow =dt.datetime.utcnow() - dt.timedelta(hours=8)
jd = ep.julian_date(utcNow)
# saves in a fits file                                                                        
hdulist=pyfits.HDUList()
	
# creates primary hdu 
hdulist.append(pyfits.PrimaryHDU())
ihdu=len(hdulist)-1
hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
hdulist[ihdu].header.set('UTC',now_str(dt.datetime.utcnow()),'Date of creation UTC')
hdulist[ihdu].header.set('Content','FLAT','Content of file')
hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
hdulist[ihdu].header.set('Telescop',Info['Telescope'])
hdulist[ihdu].header.set('Instrum',Info['Instrument'])
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')	
# creates image hduimage
hdulist.append(pyfits.ImageHDU(data=img))
ihdu=len(hdulist)-1
hdulist[ihdu].header.set('extname',"LIGHT")
hdulist[ihdu].header.set('Date',now_str(),'Date of creation')
hdulist[ihdu].header.set('UTC',now_str(),'Date of creation UTC')
hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
hdulist[ihdu].header.set('Telescop',Info['Telescope'])
hdulist[ihdu].header.set('Instrum',Info['Instrument'])
hdulist[ihdu].header.set('Tilt_str',Info['Tiltmeter'])
hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')
hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
hdulist[ihdu].header.set('Expos',exp_time,'Exposition time (sec)')

hdulist.writeto("/home/irait/fits/flat_2017_01_10.fits",clobber=True)
