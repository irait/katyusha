__DESCRIPTION__=""" Handle frames of guidecamera to determine where is a single object and set next parameter of position and exposition"""

import sys
import numpy as np
from IRAIT_Frame import GuideCameraDarkFlat,GuideCameraFrame
from GuideCameraCentering import GaussianCentering

def centrami(filename, exp, counter):

   pix2deg = 0.930205229 / 3600.0
   
   hwidth=60
   gsigma=hwidth/10.
   rconfidence=5*gsigma
   maxCountPeak = 50000 
   threasholdGC = 10000
   thr_sig2noise = 0.001
   criticalCntValue = 60000

   __REPOSITORY__='../../test_files/GuideCamera'
   dark_flat_file=__REPOSITORY__+'/dark_flat/dark_flat_2017jan10.fits'

   df=GuideCameraDarkFlat(dark_flat_file)
   
   cf=GuideCameraFrame(filename,ihdu=0)
		
   imgCenterX =cf.shape[0]/2.
   imgCenterY = cf.shape[1]/2
   cf.darkFlat(df)
   rc,a,Peak,c = cf.findPeakRaw('reduced')
  
   if (Peak > threasholdGC):
	      GC=GaussianCentering(cf,'reduced',hwidth,gsigma,rconfidence)
	      Mbest = GC.result()	         

	      if (Mbest['X0'] is not None):

		   foundX = Mbest['X0']
		   foundY = Mbest['Y0']
		   sig2noise = GC.Mbest.signal2noise(thr_sig2noise)

		   if (sig2noise > 5):
			if (Mbest['A'] + Mbest['b']) > maxCountPeak:
			   exp *= 0.75 
			counter = 0
		 	return ((foundY-imgCenterY)*pix2deg,(foundX-imgCenterX)*pix2deg,exp, counter)
		   else: 			

			  imgMean = cf['reduced'].mean()
	   
	   		  if (Peak > (imgMean*2)):
			      if ((Mbest['A'] + Mbest['b']) < maxCountPeak): #aplitude A + baseline b 
			  	 exp *= 1.5
			   	 counter += 1
			      else:
			         exp = None
			  else:
			      counter -= 1
			 
			  return (None,None,exp,counter)	
	      else:
	         counter+=1
		 return (None, None, None, counter)

   else:
   	no_good =  np.where((cf["data"]>criticalCntValue))
	if (len(no_good[0]) > 0):
	   #print('bad frame incountered, redo exposition, no_good ', len(no_good))
	   return (None,None,exp,counter)
	else:

	   imgMean = cf["data"].mean()

	   #print imgMean
	   if (Peak > (imgMean*2)):
		#print('Frame with low exposition, try more exposition')
		exp *= 1.5
		counter += 1
	   else:
		#print('No target found, spiral search')
		counter -= 1
	   return (None,None,exp,counter)


import pyfits

if __name__ == '__main__' :

	fileOk = "/home/irait/fits/tilt3/PRE_2016-12-16 10:18:05.783449_exp_0.001.fits"

	exp = 0.001
	cnt = 0
	myfileOk = pyfits.open(fileOk)
	myImgOk = myfileOk[0].data
	x0,y0, expTime,cnt = centrami(fileOk, exp, cnt)
