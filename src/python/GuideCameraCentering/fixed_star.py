#!/usr/bin/python
import sys
from collections import OrderedDict
import numpy as np
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
import datetime as dt

#J2000=ephem.date('2000/1/1 00:00:00')

fixedStar=ephem.star('Canopus')
IRAIT.date=ephem.date(dt.datetime.utcnow())
fixedStar.compute(IRAIT)

print fixedStar.name,
print 'airless',
print ephem.julian_date(IRAIT.date),
print np.rad2deg(fixedStar.elong),
print fixedStar._ra,fixedStar._dec,
print fixedStar.ra,fixedStar.dec,
print fixedStar.ra,fixedStar.dec,
print np.rad2deg(fixedStar.alt),np.rad2deg(fixedStar.az),
print
