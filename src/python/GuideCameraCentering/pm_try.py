#!/usr/bin/python

from find_center_object import centrami

import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo
import cv2
import pylab as plt

__BASE_PATH__ = "/home/irait/fits/try_pm_yuri6/"





exp = 1.
exp_g4 = 3.


def getAltAzi(fixedStar):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)+180.0)%360.)



class FrameToFits:
	def __init__(self,image, filename, alt, azi, exp_time_loc, chip_temp, exp):
		self._image = image
        	self._filename =  filename
		self._alt = alt
		self._azi = azi
		self._altCorr = None
		self._aziCorr = None
		self._tiltX = None
		self._tiltY = None
		self._fitsRef = None
		self._exp_time_loc = exp_time_loc
		self._chip_temp = chip_temp
		self._exp = exp 
		self._teoricAlt = None
		self._teoricAz = None

	def setCorrection(self, altCorr, aziCorr):
		self._altCorr = altCorr
		self._aziCorr = aziCorr

	def setTilt(self, tiltX, tiltY):
		self._tiltX = tiltX
		self._tiltY = tiltY

	def setReference(self, filename):
		self._fitsRef = filename
	
	def setTeoricCoord(self,altTeoric, azTeoric):
		self._teoricAlt = altTeoric
		self._teoricAz = azTeoric

class Tilt:
    BUFFER_SIZE = 1024

    def __init__(self,hostname,port):
        self.__s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.connect((hostname,port))

    def __del__(self):
        self.__s.close()

    def __get_tilt(self,i):
        if i > 32767:
            return -(65536 - i)
        else:
            return i

    def getTilt(self):
        MESSAGE = '{"function":"R_REGISTERS","address":"0x0008","count":2}'
        self.__s.send(MESSAGE)
        data = self.__s.recv(Tilt.BUFFER_SIZE)

        j = json.JSONDecoder()
        try:
            jobj = j.decode(data[0:-1])
        except:
            return None
        if jobj.has_key('error'):
            return None

        return(self.__get_tilt(jobj['data'][0]), self.__get_tilt(jobj['data'][1]))

import random
import time
import rts2.scriptcomm
import sys

from datetime import datetime as dtd


from threading import Thread,Lock
import time as tm

class G4_thread:
    def __init__(self):
        self._do_read = 0
        self._cam = gx.GxCCD(6103)
        if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
            print "bad camera, exiting"
            exit(1)
        self._d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
        self._w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
        if(self._d == -1 or self._w == -1):
            print "bad camera, exiting"
            exit(1)
        self._cam.set_filter(6)

    def restartG4Connection(self):
	self._cam = None
	self._cam = gx.GxCCD(6103)
	if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
            print "bad camera, exiting"
            exit(1)
	

    def is_ready(self):
        if tm.time()> self._do_read and self._do_read != 0:
            return True
        else:
            return False
        
    def start_exposure(self,exp_time):
        if self.is_ready() or self._do_read == 0:
            e = self._cam.start_exposure(exp_time,True,0,0,self._w,self._d)
            if e == 0:
                tm.sleep(exp_time+0.1)
                self._do_read = tm.time()+20
            return e == 0

    def get_image(self):
        if self.is_ready():
            image = self._cam.read_image(self._d,self._w)
	    chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
            self._do_read = 0
            return image, chip_temp
        else:
            return None
                


Info={}
Info['Valid']='2016dec10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Guide']='G1-2000 Moravian'
Info['Instrument']='G4-9000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='None'
Info['Version']='1.2'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
        exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
    if(self._chip_d == -1 or self._chip_w == -1):
        exit(1)


    self.g4_th = G4_thread()
        
  def saveFits(self, frame1):
	
	temperature = None
        pressure = None
        humidity = None
        w_dir = None
        w_speed = None
        try:
            mt=Meteo.Meteo(lg=None)
            temperature,pressure,humidity, w_dir, w_speed =mt.retrieve()
        except:
            self.log('W', 'no meteo data available')

	# saves in a fits file
	hdulist=pyfits.HDUList()
	

	# creates image hduimage (science)
	

	hdulist.append(pyfits.CompImageHDU(data=frame1._image))
	ihdu=len(hdulist)-1
	
	timeExp = frame1._exp_time_loc
	utcNow =timeExp - dt.timedelta(hours=8)
	jd = ep.julian_date(utcNow)

	hdulist[ihdu].header.set('extname',"LIGHT")
	hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
	hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Tilt_str',Info['Tiltmeter'])
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')
	hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('CoordAlt',frame1._alt,'Alt coordinates telescope')
	hdulist[ihdu].header.set('CoordAzi',frame1._azi,'Azi coordinates telescope')
	hdulist[ihdu].header.set('TiltX',frame1._tiltX,'Tilt value measured in x axis (ADU)')
	hdulist[ihdu].header.set('TiltY',frame1._tiltY,'Tilt value measured in y axis (ADU)')
	hdulist[ihdu].header.set('DeltaAlt',frame1._altCorr,'Alt correction from  center (deg)')
	hdulist[ihdu].header.set('DeltaAzi',frame1._aziCorr,'Azi correcction from center (deg)')
	hdulist[ihdu].header.set('Chip_t',frame1._chip_temp,'Chip temperature measured (Celsius) ')
	hdulist[ihdu].header.set('Expos',frame1._exp,'Exposition time (sec)')
	if (temperature is not None):
            hdulist[ihdu].header.set('Temp', temperature, 'Temperature (Celsius)')
            hdulist[ihdu].header.set('Press', pressure, 'Pressure (hPa)')
            hdulist[ihdu].header.set('Hum', humidity, 'Humidity (%)')
            hdulist[ihdu].header.set('W_dir', w_dir, 'Wind_dir  (degrees)')
            hdulist[ihdu].header.set('W_speed', w_speed, 'Wind_speed (m/s)')

	hdulist.writeto(frame1._filename,clobber=True)
	



  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp

  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def getPMCorr(self):
    corr_alt_st, corr_az_st = self.getValue('pm_delta', 'T0').split()
    corr_alt = float(corr_alt_st)*3600.
    corr_az = float(corr_az_st)*3600.	
    return corr_alt, corr_az 	 

  def run(self):

    myMinute = dtd.now().minute 
    global exp
    global exp_g4
    alt_d = -0.
    azi_d = -0.
    delta_x_G1 = 0.055 #0.055
    delta_y_G1 = -0.07  #-0.09

    mylog = open("%sG1_ris_pm_%s.csv"%(__BASE_PATH__,now_str()),"wt+")
    mylog_G4 = open("%sG4_ris_pm_%s.csv"%(__BASE_PATH__,now_str()),"wt+")

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    starList = []
   
    
   

    

    '''starList.append(['Ara Beta', 	'17:25:17.96', '-55:31:48.0'])	
    starList.append(['Tra Alpha', 	'16:48:40.06', '-69:01:40.3'])
    starList.append(['Rigel Kent', 	'14:39:18.51', '-60:49:53.9'])

    starList.append(['Antares', 	'16:29:24.45', '-26:25:55.6'])
    starList.append(['Lup Gamma', 	'15:35:08.42', '-41:10:00.8'])
    starList.append(['Menkent', 	'14:06:40.02', '-36:22:20.8'])

    starList.append(['Gacrux', 		'12:31:10.07', '-57:06:52.2'])
    starList.append(['Cen Zeta', 	'13:55:32.25', '-47:17:18.9'])
    starList.append(['Vel Mu', 		'10:46:46.35', '-49:23:13.9'])

    starList.append(['Hya Pi-49', 	'14:06:22.36', '-26:40:58.9'])
    starList.append(['Spica', 		'13:25:11.53', '-11:09:41.3'])
    starList.append(['Algorab', 	'12:29:51.59', '-16:30:58.0'])
    starList.append(['Hya Xi', 		'11:32:59.78', '-31:51:28.2'])		
    starList.append(['Hya Nu',	 	'10:49:37.61', '-16:11:33.7'])  
    starList.append(['Alphard', 	'09:27:35.22', '-08:39:30.4'])

    starList.append(['Alsuhail',	'09:07:59.71', '-43:25:57.1'])    
    starList.append(['Vel delta',	'08:44:42.33', '-54:42:33.6'])
    starList.append(['Avior',		'08:22:30.73', '-59:30:33.8'])
    starList.append(['Suhail',		'08:09:31.93', '-47:20:11.5'])
    starList.append(['Naos', 		'08:03:34.99', '-40:00:11.0'])

    starList.append(['Wesen', 		'07:08:23.48', '-26:23:35.5'])
    starList.append(['Adahara',		'06:58:37.55', '-28:58:19.5'])
    starList.append(['Sirius', 		'06:45:08.23', '-16:43:19.3'])
    starList.append(['Mirzam', 		'06:22:41.99', '-17:57:21.3'])
    starList.append(['Furund', 		'06:20:18.80', '-30:03:48.1'])

    starList.append(['Pup Nu', 		'06:37:45.67', '-43:11:45.4'])
    starList.append(['Canopus', 	'06:23:57.17', '-52:41:44.0'])'''
    starList.append(['Pic Beta', 	'05:47:17.10', '-51:03:58.0'])
    starList.append(['Dor Beta', 	'05:33:37.53', '-62:29:23.1'])
    starList.append(['Dor Alpha', 	'04:33:59.99', '-55:02:41.7'])
    starList.append(['Ret Alpha', 	'04:14:25.71', '-62:28:25.0'])

    starList.append(['Col Alpha', 	'05:39:38.94', '-34:04:27.2'])
    starList.append(['Theemin', 	'04:35:32.96', '-30:33:44.6'])  
    starList.append(['Fornacis', 	'03:12:05.09', '-28:59:04.8'])  
    starList.append(['Acamar', 		'02:58:15.56', '-40:18:16.4'])

    starList.append(['Achernar', 	'01:37:43.20', '-57:14:13.0'])

    starList.append(['Ankaa',   	'00:26:17.54', '-42:18:27.7'])
    starList.append(['Fomalhaut', 	'22:57:39.55', '-29:37:22.9'])

    starList.append(['Gru Beta', 	'22:42:40.39', '-46:53:04.6'])
    starList.append(['Alnair', 		'22:08:14.30', '-46:57:42.1'])
    starList.append(['Peacock', 	'20:25:38.8',  '-56:44:07.8'])
    
    starList.append(['Kaus Australis',	'18:24:10.25', '-34:23:06.8'])
    starList.append(['Sargas', 	'17:37:19.14', '-42:59:52.2'])
    starList.append(['Shaula', 	'17:33:36.51', '-37:06:14.3'])

    G4pixel2deg = 0.15/ 3600.0
    G4pixel2asec = 0.15
    surf = cv2.SURF(20)

    for i in range(0, len(starList)):

	starName = starList[i][0]
	starRA   = starList[i][1]
	starDEC  = starList[i][2]
	#startRAdeg =  starList[i][3]
	#startDECdeg = starList[i][4]

	self.log('I', 'Set position for %s' % starName)
	self.setValue('point','false','T0')

	mystar = None
	mystar = ep.FixedBody()
	mystar._ra = starRA
	mystar._dec = starDEC 

	alt, az = getAltAzi(mystar)

	if (alt < 4.5) or (alt > 76):
		self.log('I', 'Target out of range... alt: %4.3f' % alt)
	else:
		self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
	   	self.setValue('TEL_','{} {}'.format(alt,az),'T0')	

		time.sleep(5.)

	    	while 'STOPPED' != self.getValue('status','T0'):
			time.sleep(0.1)

		self.setValue('point','true','T0')
		time.sleep(20.)

		

		image, e_time, chip_temp = self.gx_exposure(exp,5)
		filename = "%sG1_%s_%s_%.3f.png"%(__BASE_PATH__,now_str(e_time),starName,exp)

		alt_t, az_t = self.getPosTelAdj()
		corr_alt, corr_az = self.getPMCorr() 

		self.log('I', 'acquired %s' % filename)
		plt.imshow(image)
		plt.show()

		plt.figure()
	    	plt.imshow(image)
	    	plt.savefig(filename)
	    	plt.close()

		counter = 0

		deltax, deltay, newExp, counter= centrami(image, exp, counter)
			
	
		#if (deltax is not None) and (deltay is not None) :
		if True:
			if (deltax is not None) and (deltay is not None) :
				self.log('I', 'deltax %.4f deltay %1.4f ' % (deltax, deltay))
				mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n"%(filename,alt_t,az_t,deltax,deltay,corr_alt, corr_az))
				mylog.flush()

			cntG4 = 1
			G4Alt= None
			G4Az=  None
			G4Time = None
			G4corr_alt = None
			G4corr_az = None
			G4name = None
	   
			imgRead = True
	
			while cntG4 > 0:

		  		if self.g4_th.is_ready():
			    		 imgRead = True
					 cntG4 -= 1 
		           		 self.log('I', "G4 image ready")
		          		 image, chip_temp = self.g4_th.get_image() 
				
					 plt.imshow(image)
					 plt.show()
					
					 plt.figure()
					 plt.imshow(image)
					 plt.savefig(G4name+".png")
					 plt.close() 

					 if image.max() > 15000:
			
				    		 fr_g4 = FrameToFits(image, G4name+".fits" , G4Alt, G4Az, G4Time,chip_temp, exp_g4)
				    		 
						 imgray = np.uint8(image/2**8)
						 ret, image = cv2.threshold(imgray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

						 angle = -G4Alt
						 image_center = tuple(np.array(image.shape)/2)
  						 rot_mat = cv2.getRotationMatrix2D(image_center,angle,1.0)
 						 img = cv2.warpAffine(image, rot_mat, image.shape,flags=cv2.INTER_LINEAR)

						 #plt.imshow(img)
						 #plt.show()

						 #plt.figure()
						 #plt.imshow(img)
					    	 #plt.savefig(G4name+".png")
					    	 #plt.close()   
						 
						 kp, des = surf.detectAndCompute(img,None)

						 maxkval = 0
						 maxk = None
						 delta_x = None
						 delta_y = None

						 for k in kp:
						      if k.size > maxkval:
					      	 	  maxkval = k.size
							  maxk = k

						 if maxk is not None:
						      mypoint = maxk
						      x = round(mypoint.pt[1])
						      y = round(mypoint.pt[0])
						      cv2.circle( img, (int(y),int(x)), 120, ( 0, 0, 0 ), 1, 8 );
		
						      plt.imshow(img)
						      plt.show()
	
						      delta_x = (1528 - x) * G4pixel2asec
						      delta_y = (1528 - y) * G4pixel2asec
			
						 if (delta_x is not None) and (delta_y is not None) :

						      self.log('I', 'delta_x_g4:  %0.6f, delat_y_g4: %0.6f ' % (delta_x, delta_y))
				    	              fr_g4.setCorrection( delta_y, delta_x)
				    	              
						      mylog_G4.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%s\n" % (G4name,G4Alt, G4Az, delta_x, delta_y,G4corr_alt, G4corr_az,now_str(G4Time)))
						      mylog_G4.flush()

				                      #self.saveFits(fr_g4)

						      self.log('I', "exposure saved") 
						      #time.sleep(120.)
						      
					
					 else:

			    			 self.g4_th.restartG4Connection()
			      			 mylog_G4.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%s\n" % ("NOT_FOUND_"+G4name,G4Alt, G4Az, 0, 0,G4corr_alt, G4corr_az,now_str(G4Time)))
						 mylog_G4.flush()
						 cntG4 = 0

					 G4name  = None
				         G4Alt= None
				         G4Az = None
				         G4Time = None
				         G4corr_alt = None 
				         G4corr_az = None
		    
				elif self.g4_th.start_exposure(exp_g4) and imgRead:
	   				    
			  		    imgRead = False
					    self.log('I', "G4 exposure")  
					    alt_t, az_t = self.getPosTelAdj()
					
					    G4corr_alt, G4corr_az = self.getPMCorr()
					  
					    G4name = "%sG4_%s_%s_%.3f"%(__BASE_PATH__,now_str(e_time),starName,exp_g4)

					    G4Alt= alt_t
					    G4Az = az_t
					    G4Time = dt.datetime.now()
					    
					    time.sleep(2.)
			self.g4_th.restartG4Connection()	    

		else:
			self.log('I', 'image not in G1 CCD')

	    

	    

   

a = Script()
a.run()

