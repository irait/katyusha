#!/usr/bin/python

from find_center_object import centrami

import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb

__BASE_PATH__ = "/home/irait/fits/sirius_follow/"


fixedStar=ephem.star('Sirius')


exp = 0.001

def getAltAzi(alt_d=0.,azi_d=0.):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt)+alt_d,(np.rad2deg(fixedStar.az)+180.+azi_d)%360.)

def getRADec():
    jd = ep.julian_date(dt.datetime.utcnow())
    return (np.interp(jd,HTI.UT_JD,HTI.Astro_RA),np.interp(jd,HTI.UT_JD,HTI.Astro_Dec))  


class FrameToFits:
	def __init__(self,image, filename, preOrFix, alt, azi, exp_time_loc, chip_temp, exp):
		self._image = image
        	self._filename =  filename
		self._type = preOrFix
		self._alt = alt
		self._azi = azi
		self._altCorr = None
		self._aziCorr = None
		self._tiltX = None
		self._tiltY = None
		self._fitsRef = None
		self._exp_time_loc = exp_time_loc
		self._chip_temp = chip_temp
		self._exp = exp 

	def setCorrection(self, altCorr, aziCorr):
		self._altCorr = altCorr
		self._aziCorr = aziCorr

	def setTilt(self, tiltX, tiltY):
		self._tiltX = tiltX
		self._tiltY = tiltY

	def setReference(self, filename):
		self._fitsRef = filename

class Tilt:
    BUFFER_SIZE = 1024

    def __init__(self,hostname,port):
        self.__s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.connect((hostname,port))

    def __del__(self):
        self.__s.close()

    def __get_tilt(self,i):
        if i > 32767:
            return -(65536 - i)
        else:
            return i

    def getTilt(self):
        MESSAGE = '{"function":"R_REGISTERS","address":"0x0008","count":2}'
        self.__s.send(MESSAGE)
        data = self.__s.recv(Tilt.BUFFER_SIZE)

        j = json.JSONDecoder()
        try:
            jobj = j.decode(data[0:-1])
        except:
            return None
        if jobj.has_key('error'):
            return None

        return(self.__get_tilt(jobj['data'][0]), self.__get_tilt(jobj['data'][1]))

import random
import time
import rts2.scriptcomm
import sys

from datetime import datetime as dtd


Info={}
Info['Valid']='2016dec10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Instrument']='G1-2000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='orange (handmade)'
Info['Version']='1.1'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
        self.log('E','cannot connect to camera, exiting...')
        exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
    if(self._chip_d == -1 or self._chip_w == -1):
        self.log('E', 'got wrong parameters from camera, exiting')
        exit(1)
        
  def saveFits(self,frame):
	timeExp = frame._exp_time_loc
	utcNow =timeExp - dt.timedelta(hours=8)
	jd = ep.julian_date(utcNow)
	# saves in a fits file
	hdulist=pyfits.HDUList()
	
	# creates primary hdu 
	hdulist.append(pyfits.PrimaryHDU())
	ihdu=len(hdulist)-1
	hdulist[ihdu].header.set('Date',time.asctime(),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(dt.datetime.utcnow()),'Date of creation UTC')
	hdulist[ihdu].header.set('Content',frame._type,'Content of file')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')	
	# creates image hduimage
	hdulist.append(pyfits.ImageHDU(data=frame._image))
	ihdu=len(hdulist)-1
	hdulist[ihdu].header.set('extname',"LIGHT")
	hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
	hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
	hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')
	hdulist[ihdu].header.set('Content','Image %s with center correction data ' % (frame._type),'Content of hdu')
	hdulist[ihdu].header.set('Creator',Info['Creator'],'creating program')
	hdulist[ihdu].header.set('Telescop',Info['Telescope'])
	hdulist[ihdu].header.set('Instrum',Info['Instrument'])
	hdulist[ihdu].header.set('Tilt_str',Info['Tiltmeter'])
	hdulist[ihdu].header.set('ElaRaw','Raw','Elaborated or raw')
	hdulist[ihdu].header.set('RefDoc','None','documentation reference')
	hdulist[ihdu].header.set('FitsRef',frame._fitsRef,'fits file reference (dependance)')
	hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
	hdulist[ihdu].header.set('Version',Info['Version'],'File Version')
	hdulist[ihdu].header.set('CoordAlt',frame._alt,'Alt coordinates telescope')
	hdulist[ihdu].header.set('CoordAzi',frame._azi,'Azi coordinates telescope')
	hdulist[ihdu].header.set('DeltaAlt',frame._altCorr,'Alt correction from center (deg)')
	hdulist[ihdu].header.set('DeltaAzi',frame._aziCorr,'Azi correcction from center (deg)')
	hdulist[ihdu].header.set('TiltX',frame._tiltX,'Tilt value measured in x axis (ADU)')
	hdulist[ihdu].header.set('TiltY',frame._tiltY,'Tilt value measured in y axis (ADU)')
	hdulist[ihdu].header.set('Chip_t',frame._chip_temp,'Chip temperature measured (Celsius) ')
	hdulist[ihdu].header.set('Expos',frame._exp,'Exposition time (sec)')

	hdulist.writeto(frame._filename,clobber=True)



  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp
      
  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def waitAndRepoint(self):

      self.log('W', "No target found, probably cloudly, sleep 5 minutes")
      time.sleep(60*5)

      #reset counter for new acquisiion                                                                                      
      counter = 0
      #reset previous spiral search correction                                                                               
      alt_d = 0.
      azi_d = 0.
      #get coordinates for new time                                                                                          
      alt,az = getAltAzi(alt_d,azi_d)
      self.log('I', 'Recalculated coord after wait -- Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
      self.setValue('TEL_','{} {}'.format(alt,az),'T0')
      time.sleep(5.)

      while 'STOPPED' != self.getValue('status','T0'):
          time.sleep(0.1)



  def run(self):

    myMin = dtd.now().minute 
    global exp
    alt_d = -0.5
    azi_d = 0.
    delta_x_G1 = 0.
    delta_y_G1 = 0.
    mylog = open("%snew_dati_%s.csv"%(__BASE_PATH__,now_str()),"wt+")
 
    self.setValue('point','false','T0')

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    #set position of the star
    alt,az = getAltAzi(alt_d,azi_d)
    self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
    self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
    time.sleep(2.)
    while 'STOPPED' != self.getValue('status','T0'):
        time.sleep(0.1)

    targetNotFoundCnt = 0
    maxTryToRecognize = 20
    do_repeat = True
    counter = 0
    while do_repeat:
        # first part: find obj in field and calculate first correction
           
        image, e_time, chip_temp = self.gx_exposure(exp,5)
        filename = "%sGUIDE_CAMERA_FIRST_HIT_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp)

        alt_t, az_t = self.getPosTelAdj()
        self.log('I', 'acquired %s' % filename)
        deltax, deltay, newExp, counter= centrami(image, exp, counter)
        if (newExp is not None):
            exp = newExp

        if (deltax is not None) and (deltay is not None) :
            do_repeat = False
            targetNotFoundCnt = 0
            self.log('I', 'deltax %.4f deltay %.4f ' % (deltax, deltay))
            mylog.write("%s,%.4f,%.4f,%.4f,%.4f,"%(filename,alt_t,az_t,deltay,deltax))
		
            alt_t, az_t = self.getPosTelAdj()
            fr = FrameToFits(image, filename, "First hit", alt_t, az_t, e_time, chip_temp, exp) 
            self.saveFits(fr)
            alt_t -= deltay
            az_t  -= deltax
            az_t %= 360.	     
        else:
            targetNotFoundCnt += 1
            self.log('W', "TARGET NOT RECOGNIZE...")

            if ((newExp is not None) and (counter >= 0)): #bad exposition or bad frame
                self.log('I', "TRY TO re-EXPOSE")
                do_repeat = True
            elif ((newExp is not None) and (counter < 0)): #target not present in field
                self.log('I', "No target found, spiral search move {0:4.6f}".format(counter))
                if (counter > -9):
                    myMove = 0.30
                    if (counter == -1):
                        alt_d = 0.
                        azi_d = 0.
                    alt_t, az_t = self.getPosTelAdj()
                    self.log('I', 'Actual coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_t,az_t))
                    mycnt = abs(counter)
                    if (mycnt == 1):
                        alt_d -= myMove
                    elif (mycnt == 2):
                        azi_d += myMove
                    elif (mycnt == 3):
                        alt_d += myMove
                    elif (mycnt == 4):
                        alt_d += myMove
                    elif (mycnt == 5):
                        azi_d -= myMove
                    elif (mycnt == 6):
                        azi_d -= myMove
                    elif (mycnt == 7):
                        alt_d -= myMove
                    elif (mycnt == 8):
                        alt_d -= myMove

                    alt,az = getAltAzi(alt_d,azi_d)
                    self.log('I', 'Add delta coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt_d,azi_d))
                    self.log('I', 'spiral search coord Alt: {0:4.6f}, Az: {1:4.6f}'.format(alt,az))
                    
                    self.setValue('TEL_','{} {}'.format(alt,az),'T0')
                    time.sleep(1.)
                    while 'STOPPED' != self.getValue('status','T0'):
                        time.sleep(0.1)
                else:
                    #spiral search end with no result
                    self.waitAndRepoint()
		
            else: #bad weather
                self.waitAndRepoint()

        if (targetNotFoundCnt > maxTryToRecognize):
            exit(1)
            
    #end first part (found object in the field and appropriate correction to move in the right position)	
    az_t += delta_x_G1
    alt_t += delta_y_G1
    self.log('I', 'NEW Alt: {0:4.6f}, Az: {1:4.6f} with guide camera correction deltax: {2:4.6f} and deltay: {3:4.6f}'.format(alt_t,az_t, delta_x_G1, delta_y_G1))
    self.setValue('TEL_','{} {}'.format(alt_t,az_t),'T0')
    time.sleep(3.)
    while 'STOPPED' != self.getValue('status','T0'):
        time.sleep(0.1)

    targetNotFoundCnt = 0
    firstCycle = True

    while True: #maintain star in the same point, store correction and save a fits every hour
            
        image, e_time, chip_temp = self.gx_exposure(exp,5)
        filename = "%sGUIDE_CAMERA_PONTING_%s_%.5f.fits"%(__BASE_PATH__,now_str(e_time),exp)

        alt_t, az_t = self.getPosTelAdj()
        fr = FrameToFits(image, filename, "pointing", alt_t, az_t, e_time, chip_temp, exp)

        self.log('I', 'acquired %s' % filename)
                    
        deltax, deltay, newExp, counter = centrami(image, exp,counter)
        if (newExp is not None):
            exp = newExp
        if (deltax is not None) and (deltay is not None) :
            
            targetNotFoundCnt -= 1 #instead of (=0) to deny false positive
            tilt = Tilt('localhost',8000)
            tl = tilt.getTilt()
            if tl is not None:
                mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%s,%s\n"%(filename,alt_t,az_t, deltax, deltay, tl[0], tl[1]))
                fr.setTilt(tl[0], tl[1])
            else:
                mylog.write("%s,%.4f,%.4f,%.4f,%.4f\n"%(filename,alt_t,az_t, deltax, deltay))
                
            mylog.flush()
            self.setValue('point','true','T0')
                
            alt_t -= deltay
            alt_t += delta_y_G1

            az_t -= deltax
            az_t += delta_x_G1
            az_t %= 360.

            self.setValue('TEL_','{} {}'.format(alt_t,az_t),'T0')
            time.sleep(1.)
            while 'POINTING' != self.getValue('status','T0'):
                time.sleep(0.1)

            fr.setCorrection(deltay, deltax)
            newMin = dtd.now().minute

            if (newMin % 5 == 0) or (firstCycle):
                firstCycle = False
                self.saveFits(fr)
                
        else:
            if (newExp is not None):
                self.log('I', "TRY TO REDO EXPOSE")
                targetNotFoundCnt += 1
            else:
                self.log('I', 'TARGET LOST: bad correction!!!')
                exit(1)

        if (targetNotFoundCnt > maxTryToRecognize):
            exit(1)

    time.sleep(3)

a = Script()
a.run()

