__DESCRIPTION__="""HorizonsTableBase is a class to handle Horizons CSV Tables in its most basic form as provided by Horizons service
in particular:
1. split of horizons file in header, body and tail
2. adding to body the columns header
3. convert body into an ordered Dict, with conventional names for columns with empty name
"""

__DESCRIPTION__="GREP like codes (from http://casa.colorado.edu/~ginsbura/pygrep.htm)" 

def grep_simple(string,list):
   "a simple grep"
   import re
   expr = re.compile(string)
   l=[]
   for text in list:
      match = expr.search(text)
      if match != None:
         l.append(match.string)
   return l

def grep_exact(string,list):
   "return just exact matches"
   import re
   expr = re.compile(string)
   return [elem for elem in list if expr.match(elem)]

def grep_substring(string,list):
   "return any item containing the search string as a substring in any position"
   import re
   expr = re.compile(string)
   return filter(expr.search,list)

def hereDoc2table(csv,fsep=',',comment='#',has_header=True,asDict=True,asStruct=False,header=False,list_fields=False) :
   "converts an here document into a dictionary table"
   import numpy as np
   class _struct : 
      def __init__(self) : 
         pass
   def _column2array(x) :
      try :
         return np.array(x,dtype='int')
      except :
         try:
            return np.array(x,dtype='float')
         except :
            return np.array(x)
   Comment = []
   a=[]
   for line in csv.split('\n') :
      l=line.strip()
      if l != '' and l[0] != comment : 
         a.append(l)
      else :
         Comment.append(l)
   if has_header :
      head = a[0].split(',')
      if header :
         return head
      a=a[1:]
   for i in range(len(a)) : a[i] = a[i].split(',')
   a = np.array(a).transpose()
   if not has_header or not (asDict or asStruct):
      for i in range(len(a)) :
         a[i]=_column2array(a[i])
      return a
   if asDict :
      from collections import OrderedDict
      tb=OrderedDict()
      for i in range(len(head)) :
         if list_fields :
            #print i,head[i]
            pass
         tb[head[i]]=_column2array(a[i])
      return tb
   tb=_struct()
   for i in range(len(head)) :
      if list_fields :
         #print i,head[i]
         pass
      tb.__dict__[head[i]]=_column2array(a[i])
   return tb

class HorizonsTableBase :
   def __init__(self,*arg) :
      """HTB=HorizonsTableBase(filename) istantiates an object out of filename file
         Members:
            filename = name of file from which table is extracted
            hdr  = original table header
            body = original body
            tail = original tail
            TableDictionary = table converted into a dictionary
      """
      self.filename=None
      self.hdr=None
      self.body=None
      self.tail=None
      self.TableDictionary=None
      self.TargetName=None
      self.TargetCode=None
      self.CenterName=None
      self.CenterCode=None
      if len(arg) > 0 :
         self.filename=arg[0]
         if self.filename != None and self.filename != '':
            self.hdr,self.body,self.tail=self.split_table(self.filename)
            self.TableDictionary=self.body2OrderedDict(self.body)
            #
            tt=self.getBodyInformation()
            self.TargetName=tt['TargetName']
            self.TargetCode=tt['TargetCode']
            #
            tt=self.getCenterInformation()
            self.CenterName=tt['CenterName']
            self.CenterCode=tt['CenterCode']
            self.CenterGeodeticLong=tt['CenterGeodeticLong']
            self.CenterGeodeticLat=tt['CenterGeodeticLat']
            self.CenterGeodeticAlt=tt['CenterGeodeticAlt']
            self.CenterGeodeticAltUnit=tt['CenterGeodeticAltUnit']
            #
            tt=self.getTimeInformation()
            self.TimeStart=tt['TimeStart']
            self.TimeStop=tt['TimeStop']
            self.TimeStep=tt['TimeStep']
            #
            tt=self.getOptions()
            self.Refracted=tt['Refracted']
            self.RA_format=tt['RA_format']
            self.Time_format=tt['Time_format']
   def __str__(self) :
      return str(self.TargetName)+' ('+str(self.TargetCode)+') : lines '+str(len(self.body)-1 if self.body != None else None)
   def copy(self) :
      """returns a copy of itself"""
      import copy
      return copy.deepcopy(self)
   def getBodyInformation(self,hdr=None) :
      """returns a dictionary with body information"""
      from collections import OrderedDict
      t=OrderedDict()
      l=grep_simple('Target body name:',self.hdr if hdr == None else hdr)[0].split('Target body name:')[1].strip()
      body,code=l.split('(')
      code=code.split(')')[0]
      t['TargetName']=body.strip()
      t['TargetCode']=int(code.strip())
      return t
   def getCenterInformation(self,hdr=None) :
      """returns a dictionary with center information"""
      from collections import OrderedDict
      t=OrderedDict()
      l=grep_simple('Center body name:',self.hdr if hdr == None else hdr)[0].split('Center body name:')[1].strip()
      body,code=l.split('(')
      code=code.split(')')[0]
      t['CenterName']=body.strip()
      t['CenterCode']=int(code.strip())
      l=grep_simple('Center geodetic :',self.hdr if hdr == None else hdr)[0].split('Center geodetic :')[1].strip().split('{')[0].strip().split(',')
      t['CenterGeodeticLong']=float(l[0].strip())
      t['CenterGeodeticLat']=float(l[1].strip())
      t['CenterGeodeticAlt']=float(l[2].strip())*1e3
      t['CenterGeodeticAltUnit']='m'
      return t
   def getTimeInformation(self,hdr=None) :
      """returns a dictionary with time information"""
      from collections import OrderedDict
      t=OrderedDict()
      t['TimeStart']=grep_simple('Start time      :',self.hdr if hdr == None else hdr)[0].split('Start time      :')[1].strip()
      t['TimeStop']=grep_simple('Stop  time      :',self.hdr if hdr == None else hdr)[0].split('Stop  time      :')[1].strip()
      t['TimeStep']=grep_simple('Step-size       :',self.hdr if hdr == None else hdr)[0].split('Step-size       :')[1].strip()
      return t
   def getOptions(self,hdr=None) :
      """returns a dictionary with time information"""
      from collections import OrderedDict
      t=OrderedDict()
      t['Refracted']=grep_simple('Atmos refraction:',self.hdr if hdr == None else hdr)[0].split('Atmos refraction:')[1].strip().split('(')[0].strip()
      t['RA_format']=grep_simple('RA format       :',self.hdr if hdr == None else hdr)[0].split('RA format       :')[1].strip()
      t['Time_format']=grep_simple('Time format     :',self.hdr if hdr == None else hdr)[0].split('Time format     :')[1].strip()
      return t
   def split_table(self,filename) :
      """split a table into header, body and tail"""
      status=0
      hdr = []
      body=[]
      tail=[]
      for l in open(filename,'r') :
         ll=l.strip()
         if ll != '' :
            if status == 0 :
               if ll == '$$SOE' : 
                  status = 1;
                  body.append(hdr[len(hdr)-2])
               else :
                  hdr.append(ll)
            elif status == 1 :
               if ll == '$$EOE' :
                  status = 2
               else :
                  body.append(ll)
            else :
               tail.append(ll)
      return hdr,body,tail
   def canonizedBodyHeader(self,headerLine,UnNamedColumnsFormat) :
      """put in canonic form the header of a table
         headerLine is the line of the header
         UnNamedColumnsFormat specifies the format to label un named columns 
         UnNamedColumnsFormat='Column%02d'
      """
      import numpy as np
      hh=np.array([k.strip() for k in headerLine.split(',')])
      idx = np.where(hh == '')[0]
      if len(idx) > 0 :
         for k in idx : hh[k] = UnNamedColumnsFormat%k
      return ','.join(hh)
   def body2OrderedDict(self,body,UnNamedColumnsFormat='Column%02d') :
      """ convert body into an ordered dict
          keyword UnNamedColumnsFormat specifies the format to label un named columns 
          usually UnNamedColumnsFormat='Column%02d'
          
      """
      canonicalHeader = self.canonizedBodyHeader(body[0],UnNamedColumnsFormat)
      hereDoc = [canonicalHeader]
      for k in body[1:] : hereDoc.append(k)
      return hereDoc2table('\n'.join(hereDoc),asDict=True,asStruct=False)
