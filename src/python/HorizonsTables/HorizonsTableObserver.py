from SmartTable import base_table 

__DESCRIPTION__="""
See HorizonsEphemeridsDatabase for naming resolution of Horizons tables
"""

__Version__="""
1.1 - 2016 nov 20 - 2016 Dec 16 - M.Maris
"""

def ephemUTepoch2JD(*arg) :
   """uses ephem to define JD as a function of UT epoch
      - ephemUTepoch2JD() returns current JD according to current clock
      - ephemUTepoch2JD(EpochString)  the JD for the epoch String (*)
      - ephemUTepoch2JD(DateString,TimeString)  the JD for the epoch String = DateString+' '+TimeString
      - ephemUTepoch2JD(ephem.Date)  the JD for the ephem Date object 
      - ephemUTepoch2JD(Year,MonthNumber,Day)  JD at 00:00 of epoch
      - ephemUTepoch2JD(Year,MonthNumber,Day,Hour,Min,Sec)  JD at epoch
      
      (*) the epoch string has the form 'Year-MonthNumber-Day' or 'Year-MonthNumber-Day HH:MM:SS'
      
      MonthNumber = 1 for January, ... 12 for December
   """
   import ephem
   if len(arg) == 0 :
      return ephem.julian_date()
   if len(arg) == 1 :
      return ephem.julian_date(arg[0])
   if len(arg) == 2 :
      return ephem.julian_date(str(arg[0])+' '+str(arg[1]))
   if len(arg) == 3 :
      return ephem.julian_date(str(arg[0])+'-'+str(arg[1])+'-'+str(arg[2]))
   if len(arg) == 6 :
      return ephem.julian_date(str(arg[0])+'-'+str(arg[1])+'-'+str(arg[2])+' '+str(arg[3])+':'+str(arg[4])+str(arg[5]))

class HorizonsTableObserver(base_table) :
   """ class to handle an Horizons Table Observer """
   def __init__(self,*arg) :
      import numpy as np
      from collections import OrderedDict
      from linearizeSawTooth import linearizeSawTooth
      from HorizonsTables import HorizonsTableBase
      base_table.__init__(self)
      if len(arg) == 0 : 
         pass
      else :
         #
         filename=arg[0]
         HTB=HorizonsTableBase(filename)
         #
         self.__info__=OrderedDict()
         self.__info__['TargetName']=HTB.TargetName
         self.__info__['TargetCode']=HTB.TargetCode
         self.__info__['CenterName']=HTB.CenterName
         self.__info__['CenterCode']=HTB.CenterCode
         self.__info__['CenterGeodeticLong']=HTB.CenterGeodeticLong
         self.__info__['CenterGeodeticLat']=HTB.CenterGeodeticLat
         self.__info__['CenterGeodeticAlt']=HTB.CenterGeodeticAlt
         self.__info__['CenterGeodeticAltUnit']=HTB.CenterGeodeticAltUnit
         self.__info__['TimeStart']=HTB.TimeStart
         self.__info__['TimeStop']=HTB.TimeStop
         self.__info__['TimeStep']=HTB.TimeStep
         self.__info__['Refracted']=HTB.Refracted
         self.__info__['RA_format']=HTB.RA_format
         self.__info__['Time_format']=HTB.Time_format
         self.__info__['len']=len(HTB.body)-1
         #
         d,t = [],[]
         
         if HTB.TableDictionary.has_key('Date__(UT)__HR:MN:SC.fff'):
            date_key = 'Date__(UT)__HR:MN:SC.fff'
         else:
            date_key = 'Date__(UT)__HR:MN:SS'
         for l in HTB.TableDictionary[date_key] :
            ld,lt=l.strip().split(' ')
            d.append(ld)
            t.append(lt)
         self.UT_Date=np.array(d)
         self.UT_TimeStr=np.array(t)
         self.UT_Time=self.sessagesimal2decimal(t,sept=':')
         #
         self.Astro_RA=self.sessagesimal2decimal(HTB.TableDictionary['R.A._(ICRF/J2000.0)'])
         self.Astro_Dec=self.sessagesimal2decimal(HTB.TableDictionary['DEC_(ICRF/J2000.0)'])
         self.Rate_RA=HTB.TableDictionary['dRA*cosD']
         self.Rate_Dec=HTB.TableDictionary['d(DEC)/dt']
         if self.isRefracted() :
            self.App_RA=self.sessagesimal2decimal(HTB.TableDictionary['R.A._(r-app)'])
            self.App_Dec=self.sessagesimal2decimal(HTB.TableDictionary['DEC_(r-app)'])
            self.App_Azi=HTB.TableDictionary['Azi_(r-appr)']
            self.App_Alt=HTB.TableDictionary['Elev_(r-appr)']
         else :
            self.App_RA=self.sessagesimal2decimal(HTB.TableDictionary['R.A._(a-app)'])
            self.App_Dec=self.sessagesimal2decimal(HTB.TableDictionary['DEC_(a-app)'])
            self.App_Azi=HTB.TableDictionary['Azi_(a-app)']
            self.App_Alt=HTB.TableDictionary['Elev_(a-app)']
         self.Rate_Azi=HTB.TableDictionary['dAZ*cosE']
         self.Rate_Alt=HTB.TableDictionary['d(ELV)/dt']
         self.AirMass=HTB.TableDictionary['a-mass']
         self.mag_ex=HTB.TableDictionary['mag_ex']
         self.APmag=HTB.TableDictionary['APmag']
         self.SolarElongation=HTB.TableDictionary['S-O-T'].copy()
         self.UT_JD=HTB.TableDictionary['Date_________JDUT'].copy()
         self.Local_Apparent_Solar_Time=self.sessagesimal2decimal(HTB.TableDictionary['L_Ap_SOL_Time'])
         self.Local_Apparent_Sidereal_Time=self.sessagesimal2decimal(HTB.TableDictionary['L_Ap_Sid_Time'])
         #creates linearized coordinates
         self.Astro_RA_linearized=linearizeSawTooth(self.Astro_RA,360)
         self.App_RA_linearized=linearizeSawTooth(self.App_RA,360)
         self.App_Azi_linearized=linearizeSawTooth(self.App_Azi,360)
         
#   def copy(self) : 
#      import copy
#      return copy.deepcopy(self)
   def isRefracted(self) :
      return self.__info__['Refracted'].lower() == 'yes'
   def __len__(self) : return self.__info__['len']
   def banner(self) :
      for k in self.__info__.keys() :
         #print k,'\t\t',self.__info__[k]
         pass
   def sessagesimal2decimal(self,sessagesimalStringArray,sept=' ') :
      """sexagesimal representation string to decimal """
      import numpy as np
      w=np.array([1,1/60.,1/3600.])
      a=[]
      for k in sessagesimalStringArray :
         x=np.array(k.strip().split(sept),dtype=float)
         if len(x) == 1 :
            a.append(x[0])
         else :
            sg=1 if x[0] >=0 else -1
            x[0]=abs(x[0])
            a.append(sg*x.dot(w))
      return np.array(a)
   def decimal2sessagesimalStringArray(self,decDec,sept=':') :
      """decimal to sexagesimal representation string """
      dg,sm,ss=self.dec2sex(decDec)
      return '%03d%s%02d%s%f02.8'%(dg,sept,sm,sept,ss)
   def dec2sex(self,Deci) :
      """decimal to sexagesimal representation """
      import math
      sg=1 if Deci >= 0 else -1
      deci=abs(Deci)
      (hfrac,hd)=math.modf(deci)
      (minfrac,m)=math.modf(hfrac*60)
      s=minfrac*60
      return (sg*int(hd),int(m),s)
   def sex2dec(self,hd,Min,Sec):
      """returns sexagesimal to decimal representation """
      sg=1 if hd >=0 else -1
      return sg*(abs(float(hd))+float(Min)/60.+float(Sec)/3600.)
   def UTepoch2TabulatedJD(self,*arg) :
      """UTepoch2JD() returns calendar to jd using internal horizons tables
         UTepoch2JD("2016-Nov-23") returns UT_JD of Nov23 2016 at 00:00:00
         UTepoch2JD("2016-Nov-23","12:30:45") returns UT_JD of Nov23 2016 at 12:30:45
      """
      import numpy as np
      date=arg[0]
      if len(arg) == 1 :
         hr=self.sessagesimal2decimal(["00:00:00"],sept=':')      
      else :
         hr=self.sessagesimal2decimal([arg[1]],sept=':')
      idx = list(self.argselect(UT_Date=date))
      idx.append(idx[-1]+1)
      idx=np.array(idx)
      tt=self.UT_Time[idx]
      if tt[-1]==tt[0] : tt[-1]+=24
      return np.interp(hr,tt,self.UT_JD[idx])  
   def interp(self,UT_JD) :
      import numpy as np
      out=HorizonsTableObserver()
      for k in self.keys() :
         if k != "UT_Date" :
            out[k]=np.interp(UT_JD,self['UT_JD'],self[k])
      return out
   def MonthString2MonthNunber(self,month) :
      from collections import OrderedDict 
      try :
         idx = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'].index((month.strip()+'---').lower()[0:3])+1
      except :
         idx = None
      return idx
   def HorizonsStringDate2EphemStringDate(self,HDate) :
      a=HDate.split('-')
      a[1]=str(self.MonthString2MonthNunber(a[1]))
      return '-'.join(a)
   
   def linearInterpAltAz(self,date,time,useLinearizedCoords=True) :
      "linear interpolation over table"
      import numpy as np
      jd = self.ephemUTepoch2JD(date,time)
      A_Dec=np.interp(jd,self.UT_JD,self.Astro_Dec)
      A_Alt=np.interp(jd,self.UT_JD,self.App_Alt)
      if useLinearizedCoords == True :
         A_RA=np.mod(np.interp(jd,self.UT_JD,self.Astro_RA_linearized),360.)
         A_Azi=np.mod(np.interp(jd,self.UT_JD,self.App_Azi_linearized),360.)
      else :
         A_RA=np.interp(jd,self.UT_JD,self.Astro_RA)
         A_Azi=np.interp(jd,self.UT_JD,self.App_Azi)
      return jd,A_RA,A_Dec,A_Alt,A_Azi

   def return_ephemObserver(self,temperature=None,pressure=None) :
      """returns a pyephem observer for the Center in the table"""
      import numpy as np
      import ephem
      observer=ephem.Observer()
      observer.lat = np.deg2rad(self.__info__['CenterGeodeticLat'])
      observer.lon = np.deg2rad(self.__info__['CenterGeodeticLong']) 
      observer.elev = self.__info__['CenterGeodeticAlt'] * (1 if self.__info__['CenterGeodeticAltUnit'].lower().strip() == 'm' else 1000.)
      observer.temp = 0. if temperature == None else float(temperature)
      observer.pressure = 0. if pressure == None else float(pressure)
      return observer
   def return_ephemDate(self,idx) :
      import ephem
      a=self.HorizonsStringDate2EphemStringDate(self.UT_Date[idx])
#      b=self.decimal2sessagesimalStringArray(self.UT_Time[idx],sept=':')
      b=self.UT_TimeStr[idx]
      return ephem.Date(a+' '+b)
   def Name(self) :
      return self.__info__['TargetName']
   def return_ephemFixedObject(self,idx,epoch=None):
      import numpy as np
      import ephem
      star = ephem.FixedBody()
      star._ra,star._dec,star._epoch = np.deg2rad(self.Astro_RA[idx]*15),np.deg2rad(self.Astro_Dec[idx]),ephem.J2000 if epoch == None else epoch
      star.name=self.Name()
      return star
   def ephemUTepoch2JD(self,*arg) :
      """uses ephem to define JD as a function of UT epoch
         - ephemUTepoch2JD() returns current JD according to current clock
         - ephemUTepoch2JD(EpochString)  the JD for the epoch String (*)
         - ephemUTepoch2JD(DateString,TimeString)  the JD for the epoch String = DateString+' '+TimeString
         - ephemUTepoch2JD(ephem.Date)  the JD for the ephem Date object 
         - ephemUTepoch2JD(Year,MonthNumber,Day)  JD at 00:00 of epoch
         - ephemUTepoch2JD(Year,MonthNumber,Day,Hour,Min,Sec)  JD at epoch
         
         (*) the epoch string has the form 'Year-MonthNumber-Day' or 'Year-MonthNumber-Day HH:MM:SS'
         
         MonthNumber = 1 for January, ... 12 for December
      """
      import ephem
      if len(arg) == 0 :
         return ephem.julian_date()
      if len(arg) == 1 :
         return ephem.julian_date(arg[0])
      if len(arg) == 2 :
         return ephem.julian_date(str(arg[0])+' '+str(arg[1]))
      if len(arg) == 3 :
         return ephem.julian_date(str(arg[0])+'-'+str(arg[1])+'-'+str(arg[2]))
      if len(arg) == 6 :
         return ephem.julian_date(str(arg[0])+'-'+str(arg[1])+'-'+str(arg[2])+' '+str(arg[3])+':'+str(arg[4])+str(arg[5]))

   def ephemInterpAltAzi(self,date,time,pressure_hPa=0.,TemperatureC=0.,returnAstropyObjects=False) :
      """performs interpolation using ephem
         Uses Astro_RA, Astro_DEC (icrs/J2000) 
         interpolates them and uses interpolated RA,DEC to compue apparent airless alt,azi.
         return jd,Astro_RA,Astro_Dec,App_Azi,App_Dec
      """
      import numpy as np
      import ephem
      jd = self.ephemUTepoch2JD(date,time)
      #
      A_RA=np.interp(jd,self.UT_JD,self.Astro_RA)
      A_Dec=np.interp(jd,self.UT_JD,self.Astro_Dec)
      #
      Observer=self.return_ephemObserver()
      Observer.date=date+' '+time
      FObj=ephem.FixedBody()
      FObj._ra,FObj._dec = np.deg2rad(A_RA*15),np.deg2rad(A_Dec)
      FObj.compute(Observer)
      return jd,A_RA,A_Dec,np.rad2deg(FObj.alt),np.rad2deg(FObj.az)
   
   def return_astropyLocation(self) :
      "returns astropy EarthLocation for the table"
      import numpy as np
      import astropy
      from astropy import units as u
      from astropy.coordinates import EarthLocation
      Long=self.__info__['CenterGeodeticLong']
      Lat=self.__info__['CenterGeodeticLat']
      height=self.__info__['CenterGeodeticAlt'] * (1 if self.__info__['CenterGeodeticAltUnit'].strip().lower()=='m' else 1000)
      return EarthLocation(lon=Long*u.degree,lat=Lat*u.degree,height=height*u.m)

   def astropyInterpAltAzi(self,date,time,pressure_hPa=0.,TemperatureC=0.,returnAstropyObjects=False) :
      """performs interpolation using astropy
         Uses Astro_RA, Astro_DEC (icrs/J2000) 
         interpolates them and uses interpolated RA,DEC to compue apparent airless alt,azi.
         return jd,Astro_RA,Astro_Dec,App_Azi,App_Dec
      """
      import numpy as np
      import astropy
      from astropy import units as u
      from astropy.time import Time
      from astropy.coordinates import SkyCoord, EarthLocation, AltAz,get_sun
      #
      jd = self.ephemUTepoch2JD(date,time)
      epoch=date+' '+time
      #
      A_RA=np.interp(jd,self.UT_JD,self.Astro_RA)
      A_Dec=np.interp(jd,self.UT_JD,self.Astro_Dec)
      Object=SkyCoord(ra=(A_RA*15)*u.degree,dec=A_Dec*u.degree,frame='icrs',equinox='J2000')
      #
      altazFrame=AltAz(obstime=epoch,location=self.return_astropyLocation(),pressure=pressure_hPa*u.hPa,temperature=TemperatureC*u.Celsius)
      ObjectAltAz=Object.transform_to(altazFrame)
      if returnAstropyObjects : 
         return {'jd':jd,'Astro_RA':A_RA,'Astro_Dec':A_DEC.Object,'altazFrame':altazFrame,'Object':Object,'ObjectAlt':ObjectAltAz}
      return jd,A_RA,A_Dec,float(ObjectAltAz.alt/u.deg),float(ObjectAltAz.az/u.deg)

if __name__=='__main__' :
   print
   print "Example of usage:"
   print
   print "Test for the Sun as Observer Nov 23th"
   #Gets the table for the Sun
   HorizonsTableName=HorizonsEphemeridsDatabase('Sun','Refracted',Start='20161122',End='20161231')()
   print HorizonsTableName
   HTI=HorizonsTableObserver(HorizonsTableName)

   # Table of observations
   TabObserv=[
      ["2016-Nov-23","02:08:00","26 21 37.33","31 52 49.59"],
      ["2016-Nov-23","02:36:00","19 32 33.74","34 25 34.01"]
         ]

   # Compares observation with interpolated, tabulated values
   for obs in TabObserv :
      x=HTI.interp(HTI.UTepoch2TabulatedJD(obs[0],obs[1])).App_Azi
      y=HTI.sessagesimal2decimal([obs[2]])
      print obs[0],obs[1],obs[2],obs[3],
      print x[0],(x-y)[0],
      x=HTI.interp(HTI.UTepoch2TabulatedJD(obs[0],obs[1])).App_Alt
      y=HTI.sessagesimal2decimal([obs[3]])
      print x[0],(x-y)[0]


"""
Example fft intperolation

N=len(HTI) # 1354
n=N/2  # 672

mT=HTI.UT_JD.mean()
T=HTI.UT_JD-mT

freq=1/T.ptp()*arange(-n,n+1)

print freq.min(), freq.max() 
# -12,12 having 1 sample every hour

omega = 2*pi*freq

mAzi=HTI.App_Azi.mean()
dAzi=HTI.App_Azi-mAzi
cAz=np.array([(cos(omega[i]*T)*dAzi).sum()/(cos(omega[i]*T)**2).sum() for i in range(len(omega))])
sAz=np.array([(sin(omega[i]*T)*dAzi).sum()/(sin(omega[i]*T)**2).sum() for i in range(len(omega))])
sAz[where(omega==0)]=0.

Titip=np.arange(0,T.max()+0.1,0.1)

AzItp = np.array(  [cAz.dot(cos(omega*Titip[i]) for i in range(len(T))])
#AzItp += complex(0,1)*np.array(  [sAz.dot(cos(omega*Titip[i]) for i in range(len(T))])


"""
