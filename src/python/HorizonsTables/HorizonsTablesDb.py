__DESCRIPTION__="""
See HorizonsEphemeridsDatabase for naming resolution of Horizons tables
"""

class HorizonsEphemeridsNameResolver :
   """Support class to handle naming of Horizons Tables
      Horizons tables are stored inside 
         $PROJECT_FOLDER/db/Horizons_<START>_<END>
         
         START = yyyymmdd Start epoch of the table
         END   = yyyymmdd End epoch of the table
         
      Observer tables are named as:
         observer_<refraction>_<body>.txt
         
         refraction = either refracted or airless
         body       = Sun, Moon, Venus, Mars, Jupiter, Saturn
   """
   def __init__(self,Body,Refraction,Kind='observer',Start=None,End=None,StartDefault="20161206",EndDefault="20170131",stepString=None) :
      aa="db/Horizons_%s_%s/"%(StartDefault if Start==None or Start=='' else Start,EndDefault if End==None or End=='' else End)
      aa+=Kind+"_"+Refraction.lower()+"_"+Body.lower()
      if stepString != None :
         if stepString.strip() != '' :
            aa+=stepString.strip().lower()
      aa+=".txt"
      self.resolved_name=aa
   def __call__(self) :
      return self.resolved_name

from SmartTable import csv_table
class HorizonsObserverTablesDb(csv_table) :
   def __init__(self,csvname) :
      import numpy as np
      #
      # Reads file 
      csv_table.__init__(self,csvname)
      #
      # add new columns
      self.TargetName_lower=None
      self.TargetName_flag=None
      self.TimeStepUnit=None
      self.TimeStepScale=None
      self.TimeStepMin=None
      if len(self) > 0 :
         self.TargetName_lower=np.array([k.strip().lower() for k in self.TargetName])
         self.Refracted_flag=np.array([k.strip().lower()=='yes' for k in self.Refracted])
         _TimeStepScale={'m':1.,'s':1./60,'h':60.,'d':24.*60.}
         self.TimeStepUnit=np.array([self.TimeStep[i].strip().split(' ')[-1].lower() for i in range(len(self))])
         self.TimeStepScale=np.array([_TimeStepScale[self.TimeStepUnit[i][0]] for i in range(len(self))])
         self.TimeStepMin=np.array([float(self.TimeStep[i].split(' ')[0])*self.TimeStepScale[i] for i in range(len(self))])
   def TimeStepScale(self,this) :
      return {'m':1.,'s':1./60,'h':60.,'d':24.*60.}[this]
   def select_by_jd(self,body,airless_refracted,jd,return_arg=False,motivate=False) :
      """select the best db name for given atmosphere (airless or refracted), body and jd
         the best db name is that with the shortest stepsize which encompasses the requested jd
         if not found returns None
      """
      import numpy as np
      # decodes the kind of atmosphere
      if airless_refracted.lower() == 'airless' or airless_refracted.lower() == 'no' :
         fg=False
      else :
         fg = True
      # find the list of possible matches
      idx=np.where((self.TargetName_lower==body.lower())*(self.Refracted_flag==fg)*(self.UT_JD_Start <= jd)*(jd <= self.UT_JD_Stop))[0]
      if motivate :
         #print
         #print "Found %d entries: "%len(idx)
         if len(idx)>0 :
            for ii in idx :
               #print ii,self.path[ii],self.Filename[ii],self.TimeStep[ii]
               pass
            #print
      if len(idx) == 0 : return
      if len(idx) == 1 :
         if return_arg : return idx[0]
         return self.path[idx[0]]+'/'+self.Filename[idx[0]]
      # if multiple matches are found, find the match with the minimum stepsize
      idxMin=idx[self.TimeStepMin[idx].argmin()]
      if motivate :
         #print "The shortest stepsize is: %d "%idxMin
         #print idxMin,self.path[idxMin],self.Filename[idxMin],self.TimeStep[idxMin]
         #print
         pass
      if return_arg : return idxMin
      return self.path[idxMin]+'/'+self.Filename[idxMin]
