class CmdLineBase() :
   def __init__(self,Name,ArgList,Version=None,OptionParameter=None,OptionFlag=None,Explanation=None) :
      """
         CmdLineBase(Name,ArgList, Version=string, OptionParameter=list, OptionFlag=list,Explanation=string)

         Name = Name of program, string
         ArgList = list of strings
         
         OptionParameter = list of tuples, each tuple formatted as follow
                           (letter,extendedname,dest,default,help)
                           example: ("-E","--EndHorizons",dest="End", default='',help='End Date for Horizons Table')
                           equivalent to: cmdln.add_option("-E","--EndHorizons",dest="End", default='',help='End Date for Horizons Table')
                           
         OptionFlag = list of tuples, each tuple formatted as follow
                           (letter,extendedname,dest,action,default,help)
                           action=store_true, store_false
                           default=True, False
                           example: ("-d","--dry-run",dest="DryRun", action = 'store_true',default=False,help='Performs a dry run')
                           equivalent to : cmdln.add_option("-d","--dry-run",dest="DryRun", action = 'store_true',default=False,help='Performs a dry run')
                           
         Explanation: an Explanation to be printed when help is asked
      """
      from collections import OrderedDict
      import sys
      from optparse import OptionParser

      self.Name=Name
      self.ArgList = ArgList 
      self.argument=OrderedDict()
      
      cmdln = OptionParser(usage="usage: %prog [options] "+
         " ".join(self.ArgList)
         +" [options]",version=Version if Version != None else '')
      cmdln.add_option("-d","--dry-run",dest="DryRun", action = 'store_true',default=False,help='Performs a dry run')
      cmdln.add_option("-S","--StartHorizons",dest="Start", default='',help='Start Date for Horizons Table')
      cmdln.add_option("-E","--EndHorizons",dest="End", default='',help='End Date for Horizons Table')
      cmdln.add_option("-i","--rcFile",dest="rcFile", default='katyusharc',help='Name of init file')
      
      if OptionParameter != None :
         for lo in OptionParameter :
            cmdln.add_option(lo[0],lo[1],dest=lo[2],default=lo[3],help=lo[4])

      if OptionFlag != None :
         for lo in OptionFlag :
            cmdln.add_option(lo[0],lo[1],dest=lo[2],action=lo[3],default=(lo[4].lower().strip()=='true'),help=lo[5])
                             
      (self.Option,self.Args) = cmdln.parse_args()
      
      if len(self.Args) < 1 : #len(self.ArgList) :
         #print
         #print self.Name 
         cmdln.print_version()
         #print Explanation if Explanation != None else ''
         cmdln.print_help()
         #print
         sys.exit(0)
      else :
         # handling of arguments
#         #print "Arguments: ",self.ArgList
         self._handler_arguments()
      self.DryRun()
   ### handlers
   def _handler_arguments(self) :
      from collections import OrderedDict
      self.argument=OrderedDict()
      i_key = 0
      for key in self.ArgList :
         self.argument["%s" % key] = self.Args[i_key]
         i_key += 1
   #
   def DryRun(self) :
      if self.Option.DryRun :
         #print "Dry Run\nExit\n"
         sys.exit(1)
   #
   def __getitem__(self,this) :
      """ 
         returns one element
      """
      try : 
         return self.argument[this]
      except :
         return None
      
if __name__=='__main__' :
   cmdline=CmdLineBase()
