def linearizeSawTooth(data,period) :
   """ ldata = linearizeSawTooth(data,period)
      linearizes a sawtooth 
        the list of data to be linearized is in x
        period is the period of the sawtooth
     
      a linearized sawtooth convert the sawtooth into a monotonous increasing or decreasing line
      
      example: negative sawtooth

      import numpy as np

      # period=10, 10 cycles
      data=np.mod(arange(99,-1,-1),10)

      print data

      > [9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3
 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6
 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0]

      linearized_data=linearizeSawTooth(data,10)
      
      print linearized_data

      > [  9   8   7   6   5   4   3   2   1   0  -1  -2  -3  -4  -5  -6  -7  -8
  -9 -10 -11 -12 -13 -14 -15 -16 -17 -18 -19 -20 -21 -22 -23 -24 -25 -26
 -27 -28 -29 -30 -31 -32 -33 -34 -35 -36 -37 -38 -39 -40 -41 -42 -43 -44
 -45 -46 -47 -48 -49 -50 -51 -52 -53 -54 -55 -56 -57 -58 -59 -60 -61 -62
 -63 -64 -65 -66 -67 -68 -69 -70 -71 -72 -73 -74 -75 -76 -77 -78 -79 -80
 -81 -82 -83 -84 -85 -86 -87 -88 -89 -90]
      
      #as expected np.mod(linearized_data,10) returns data
      
      print np.mod(linearized_data,10)
      
      > [9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3
 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6
 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0]
 
      example: positive sawtooth
      data=np.mod(arange(0,100),10)
      
      print data
      
      >[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2,
       3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5,
       6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8,
       9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1,
       2, 3, 4, 5, 6, 7, 8, 9]
       
      linearized_data=linearizeSawTooth(data,10)
      
      print linearized_data
      
     >[ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
       17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
       34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
       51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
       68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
       85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
       
      #as expected np.mod(linearized_data,10) returns data
      
      print np.mod(linearized_data,10)
      
     >[0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3
 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9]
      
   """
   import numpy as np
   if type(data) != type(np.array([])) : return data
   if len(data) < 3 : return data
   #
   #decreasing sawtooth
   if data[1]-data[0] < 0 :
      idx=np.where(data[1:]>data[:-1])[0]
      ldata=data.copy()
      # no 1 cycle or less in data
      if len(idx) == 0 : return ldata
      idx+=1
      for k in idx : ldata[k:]+=-period
      return ldata
   #growing sawtooth
   elif data[1]-data[0] > 0 :
      idx=np.where(data[1:]<data[:-1])[0]
      ldata=data.copy()
      # no 1 cycle or less in data
      if len(idx) == 0 : return ldata
      idx+=1
      for k in idx : ldata[k:]+=period
      return ldata
   #impossible to fix direction
   else :
      return None

