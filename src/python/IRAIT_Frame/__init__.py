from grid2d import MapGrid

__DESCRIPTION__="""IRAITFrame 
a python library to handle IRAIT images and frames

"""

__VERSION__="""V 0.0 - M. Maris, Yuri De Pra, S.Sartor - 2016 dec 14"""

class IraitCameraBaseFrame(MapGrid) :
   """Base class to handle frames from IRAIT
      It uses grid2d library from M.Maris
   """
   def __init__(self,*arg,**karg) :
      """obj=IraitCameraBaseFrame() creates empty object
         obj=IraitCameraBaseFrame(fitsname) creates object and open the fits file of name "fitsname" 
                                            the anchor to the file is in obj.___fitsfile__
      """
      from collections import OrderedDict
      import pyfits
      self.__classversion__=__VERSION__
      if len(arg) == 0 : 
         MapGrid.__init__(self)
         self.clean()
         return
      if type(arg[0]) == type('') :
         try :
            self.__fitsname__=arg[0]
            self.__fitsfile__=pyfits.open(arg[0])
         except :
            #print "Error no filename or file not found"
            self.__fitsname__=None
            self.__fitsfile__=None
            return
      else :
         #print "Error first parameter not a filename"
         self.__fitsname__=None
         self.__fitsfile__=None
         return
   def clean(self,shape=None) :
      """obj.clean() the object,
         obj.clean(shape=[100,200]) 
         creates a map of 100 rows, 200 columns"""
      MapGrid.clean(self,shape=list(shape) if shape != None else None)
      if shape != None :
         self.createBaseGrid(shape[0],shape[1])
      self.size=self.shape[0]*self.shape[1]
      self.__fitsname__=None
      self.__fitsfile__=None
      self.info['Telescope']=None
      self.info['camera']=None
      self.info['filter']=None
      self.info['fits_header']=None
      self.info['fits_ihdu']=None
   def close(self) :
      """close the __fitsfile__ """
      if self.fitsclosed() : return
      self.__fitsfile__.close()
      self.__fitsfile__=None
   def fitsclosed(self) : 
      """true if the __fitsfile__ is closed"""
      return self.__fitsfile__==None

class GuideCameraDarkFlat(IraitCameraBaseFrame) :
   def __init__(self,filename,flatHDU='flat',darkHDU='dark') :
      """GuideCameraDarkFlat handles dark and flat for GuideCamera 
         it is assumed dark and flat are stored in a fits file whith two separate
         ImageHDU, one named 'flat' and the other 'dark'
      """
      import pyfits
      IraitCameraBaseFrame.__init__(self,filename)
      self.clean(shape=self.__fitsfile__[darkHDU].data.shape)
      self.__fitsname__=filename
      self.__fitsfile__=pyfits.open(self.__fitsname__)
      self.info['dark_header']=self.__fitsfile__[darkHDU].header.copy()
      self.info['flat_header']=self.__fitsfile__[flatHDU].header.copy()
      self.newmap('dark',value=self.__fitsfile__[darkHDU].data.copy(),comment='dark frame')
      self.newmap('flat',value=self.__fitsfile__[flatHDU].data.copy(),comment='flat frame')
   def apply(self,Frame) :
      """applyes to a Frame dark and flat
         Frame is an array with same shape of dark and flat"""
      return (Frame-self['dark'])/self['flat']

class GuideCameraFrame(IraitCameraBaseFrame) :
   def __init__(self,argument,ihdu=None) :
      """
This class handles a generic camera frame.

   obj=GuideCameraFrame(FitsFileName) returns a GuideCameraFrame from a fits file
                                      the hdu from where to read is assumend to be 1
   obj=GuideCameraFrame(FitsFileName,ihdu=0) as before but image is read from hdu 0 
   
   obj=GuideCameraFrame(anArray) where anArray is an array of nrovs X ncols, creates the object out of the array
   
   by default a frame is stored in the frame 'data'
      """
      import pyfits
      import numpy as np
      if type(argument) == type('') :
         IraitCameraBaseFrame.__init__(self,argument)
         self.clean(shape=self.__fitsfile__[ihdu].shape)
         self.__fitsname__=argument
         self.__fitsfile__=pyfits.open(self.__fitsname__)
         self.info['source']='fits'
         self.info['fits_ihdu']=ihdu if ihdu!=None else 1
         self.info['fits_header']=self.__fitsfile__[self.info['fits_ihdu']].header.copy()
         self.newmap('data',value=self.__fitsfile__[self.info['fits_ihdu']].data.copy())
         self.size=self.shape[0]*self.shape[1]
      elif type(argument) == type(np.zeros(1)) :
         IraitCameraBaseFrame.__init__(self)
         self.clean(shape=argument.shape)
         self.__fitsname__=None
         self.__fitsfile__=None
         self.info['source']='buffer'
         self.info['fits_ihdu']=-1
         self.info['fits_header']=None
         self.newmap('data',value=argument.copy())
         self.size=self.shape[0]*self.shape[1]
   def darkFlat(self,DarkFlat,input_name=None,output_name=None) :
      """apply Dark and Flat corrections to a frame 
      
         accept in input guideCameraDarkFlat, a GuideCameraDarkFlat object
      
         > obj.darkFlat(guideCameraDarkFlat) 
         
         applies dark and flat to the immage named 'data' and creates an image named 'reduced',
         so that typing 
         > obj.keys()
         will produce the list
            'data'
            'reduced'
            
         > obj.darkFlat(guideCameraDarkFlat,input_name='pinco',output_name='pallo')
         
         will do the same for the freame 'pico' producing the frame 'pallo'
      """
      oname='reduced' if output_name == None else output_Name
      if input_name==None :
         self.newmap(oname,value=DarkFlat.apply(self['data']),comment='removed dark, flat for: data')
         return
      if type(input_name)==type('') :
         self.newmap(oname,value=DarkFlat.apply(self[name]),comment='removed dark, flat for: '+name)
         return
   def findPeakRaw(self,argument,width=None) :
      """a very semplified method to extract the peak value from a frame 
         > obj.findPeakRaw('data') 
         will look for peak in data
         
      """
      import numpy as np
      if type(argument)==type('') : 
         name=argument
      else :
         name='data'
      idx=np.nanargmax(self[name])
      c,r=self.idx2colrow(idx)
      return (c,r),idx,self[name][r,c],np.nanmax(self[name])
