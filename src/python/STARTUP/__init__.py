class rcfile :
   """class to handle an rc file"""
   def __init__(self,filename) :
      p=open(filename,'r')
      for k in p :
         l=k.strip()
         if len(l) > 0 :
            if l[0]!='#' :
               ll=l.split('=')
               self[ll[0].strip()]=ll[1].strip() if ll[1].strip()!='' else None
   def copy(self) :
      import copy
      return copy.deepcopy(self)
   def keys(self) :
      return self.__dict__.keys()
   def __setitem__(self,this,that) :
      self.__dict__[this]=that
   def __getitem__(self,this) : 
      if len(self.__dict__) == 0 : return
      return self.__dict__[this]
