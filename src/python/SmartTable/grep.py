__DESCRIPTION__="GREP like codes (from http://casa.colorado.edu/~ginsbura/pygrep.htm)" 

def simple(string,list):
   "a simple grep"
   import re
   expr = re.compile(string)
   l=[]
   for text in list:
      match = expr.search(text)
      if match != None:
         l.append(match.string)
   return l

def exact(string,list):
   "return just exact matches"
   import re
   expr = re.compile(string)
   return [elem for elem in list if expr.match(elem)]

def substring(string,list):
   "return any item containing the search string as a substring in any position"
   import re
   expr = re.compile(string)
   return filter(expr.search,list)