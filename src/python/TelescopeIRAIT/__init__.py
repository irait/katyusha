
class _Telescope :
   def __init__(self,Name,Long,Lat,Elev,Pressure,Temp,Comment=None) :
      self.Name=Name
      self.Long=Long
      self.Lat=Lat
      self.Elev=Elev
      self.Pressure=Pressure
      self.Temp=Temp
      self.Date='2000/01/01 00:00:00'
      self.Comment = Comment
   def ephem(self) :
      """returns telescope as ephem Observer"""
      import ephem
      OBS=ephem.Observer()
      OBS=ephem.Observer()
      OBS.lat, OBS.lon, OBS.elev, OBS.temp, OBS.pressure  = self.Lat, self.Long, self.Elev, self.Temp, self.Pressure
      OBS.date = self.Date
      return OBS
   def astropy(self,Pressure=None,Temp=None) :
      """returns telescope as astropy EarthLocation"""
      import numpy as np
      from astropy import units as u
      from astropy.coordinates import EarthLocation
      #
      X=np.array(self.Long.split(':'),dtype=float)
      s=1 if X[0]>=0 else -1
      Long=s*(abs(X[0])+X[1]/60.+X[2]/3600.)
      #
      X=np.array(self.Lat.split(':'),dtype=float)
      s=1 if X[0]>=0 else -1
      Lat=s*(abs(X[0])+X[1]/60.+X[2]/3600.)
      #
      P=(self.Pressure if Pressure == None else Pressure) *u.hPa
      T=(self.Temp if Temp == None else Temp) *u.hPa
      OBS=EarthLocation(lon=Long*u.degree,lat=Lat*u.degree,height=self.Elev*u.m)
      return OBS

# IRAIT GPS Coordinates according to Maris, Sartor, 2016 dec 2
#
# Lon=123:19:10.41 pm 0.18 arcsec
# Lat=-75:05:59.494 pm 0.043 arcsec
#
# Lon=123.319558333
# Lat = -75.099859444
#
TelescopeIRAIT=_Telescope('IRAIT','123:19:10.41','-75:05:59.494',3237., -41., 680. , Comment='IRAIT GPS Coordinates according to Maris, Sartor, 2016 dec 2')

EPHEM = TelescopeIRAIT.ephem()
ASTROPY = TelescopeIRAIT.astropy()
