import numpy as np
import pylab as plt
import pyfits
from scipy.misc import imread, imsave
from PIL import Image
import cv2



def getImgFromFits(nome):
	
	myfits = pyfits.open(nome)
	imgray = myfits[0].data.astype(np.float32)
	t_img = imgray / 2**16
	imgray = t_img * 2 ** 8
	imgray = np.round(imgray)
	imgray = np.uint8(imgray)
	return imgray

img_blu = getImgFromFits('jupiter_blu.fits')
img_green = getImgFromFits('jupiter_green.fits')
img_red = getImgFromFits('jupiter_red.fits')


surf = cv2.SURF(20)
kp, des = surf.detectAndCompute(img_blu,None)

maxkval = 0
maxk = None
starFound = False

for k in kp:
	if k.size > maxkval:
		maxkval = k.size
		maxk = k
	
if maxk is not None:
	mypoint = maxk
	dim = mypoint.size*1.5
	x = round(mypoint.pt[1])
	y = round(mypoint.pt[0])
	print x, y, dim

size = img_red[0].size, img_red[1].size, 3

m= np.zeros(size,dtype=np.uint8)

m[:,:,0]= img_red
m[:,:,1]= img_green
m[:,:,2]= img_blu

plt.subplot(221)
plt.imshow(img_red, cmap=plt.cm.Reds_r)
plt.subplot(222)
plt.imshow(img_green, cmap=plt.cm.Greens_r)
plt.subplot(223)
plt.imshow(img_blu, cmap=plt.cm.Blues_r)  
plt.subplot(224)
plt.imshow(m)  
plt.show()

