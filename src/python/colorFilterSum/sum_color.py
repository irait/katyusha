import numpy as np
import pylab as plt
import pyfits
from scipy.misc import imread, imsave
from PIL import Image
import cv2



def getImgFromFits(nome):
	
	myfits = pyfits.open(nome)
	imgray = myfits[0].data.astype(np.float32)
		
	imgMin = imgray.min()
	imgray = imgray - imgMin	
	t_img = imgray / 2**16
	imgray = t_img * 2 ** 8
	imgray = np.round(imgray)
	imgray = np.uint8(imgray)
	return imgray

def getCenterStar(img):

	surf = cv2.SURF(50)
	ret, img0 = cv2.threshold(img,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
	kp, des = surf.detectAndCompute(img0,None)

	maxkval = 0
	maxk = None
	x = 0
	y = 0
	dim = 0
	starFound = False

	for k in kp:
		if k.size > maxkval:
			maxkval = k.size
			maxk = k
	
	if maxk is not None:
		mypoint = maxk
		dim = mypoint.size*8
		x = round(mypoint.pt[1])
		y = round(mypoint.pt[0])

		
	return x,y,dim

img_blu = getImgFromFits('jupiter_blu.fits')
img_green = getImgFromFits('jupiter_green.fits')
img_red = getImgFromFits('jupiter_red.fits')

x0,y0,dim0 = getCenterStar(img_blu)
x1,y1,dim1 = getCenterStar(img_green)
x2,y2,dim2 = getCenterStar(img_red)


dim = (dim0+dim1+dim2) / 3
print x0, y0, x1 ,y1, x2, y2

brx= x0 -x2 -10
bry= y0 -y2 -30

grx = -10
gry = 0
size = dim*2, dim*2, 3

m= np.zeros(size,dtype=np.uint8)

m[:,:,0]= img_red[round(x2-dim):round(x2+dim), round(y2-dim): round(y2+dim)]
m[:,:,1]= img_green[round(x1-dim+grx):round(x1+dim+grx), round(y1-dim-gry): round(y1+dim-gry)]
m[:,:,2]= img_blu[round(x0-dim-brx):round(x0+dim-brx), round(y0-dim-bry): round(y0+dim-bry)]

plt.subplot(221)
plt.imshow(img_red, cmap=plt.cm.Reds_r)
plt.subplot(222)
plt.imshow(img_green, cmap=plt.cm.Greens_r)
plt.subplot(223)
plt.imshow(img_blu, cmap=plt.cm.Blues_r)  
plt.subplot(224)
plt.imshow(m)  
plt.show()

