#!/usr/bin/python
import sys
import os
import re
import numpy as np
from HorizonsTables import HorizonsEphemeridsNameResolver
from HorizonsTables import HorizonsTableObserver


if len(sys.argv)<1+2 :
   print "%s dbName OUTFILENAME"%sys.argv[0]
   sys.exit(0)

dbName=sys.argv[1]+'/'
OUTFILENAME=sys.argv[2]

header="path,Filename,UT_JD_Start,UT_JD_Stop,TargetName,TargetCode,CenterName,CenterCode,CenterGeodeticLong,CenterGeodeticLat,CenterGeodeticAlt,CenterGeodeticAltUnit,TimeStart,TimeStop,TimeStep,Refracted,RA_format,Time_format,len"
dbList=[header]

for k in os.listdir(dbName) :
   if re.match('^Horizons_[0-9]{8}_[0-9]{8}\0',k.strip()+'\0') :
      k1=dbName+k
      if os.path.isdir(k1) :
         k1+='/'
         for kk in os.listdir(k1) :
            if ~os.path.isdir(k1+kk) :
               if re.match('^observer_[a-zA-Z]+_[a-zA-Z]+[.]txt',kk.strip()) :
                  #print kk.strip(),re.match('^observer_[a-zA-Z]+_[a-zA-Z]+',kk.strip())
                  HTO=HorizonsTableObserver(k1+kk)
                  out=k1
                  out+=','+kk
                  out+=",%7.12f"%(HTO.UT_JD.min())
                  out+=",%7.12f"%(HTO.UT_JD.max())
                  for kh in header.split(',')[4:]:
                     if HTO.__info__.has_key(kh) :
                        out+=','+str(HTO.__info__[kh])
                     else :
                        print 'Key ',kh,' not found in __info__'
                        out+=',None'
                  print out
                  dbList.append(out)

open(OUTFILENAME,'w').write("\n".join(dbList)+"\n")
