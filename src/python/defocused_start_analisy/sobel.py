from scipy import misc
from scipy import ndimage
from scipy import optimize

import pyfits
import math
import numpy as np
import matplotlib.pyplot as plt

import argparse
import datetime as dt
import sys
import os

import cv2

folder='.'
__BASE_PATH__ = "."
debugMode = True


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ', 'T')

pdd = None
def find_contour(img_orig):
    global debugMode
    global interactive
    global pdd

    # clahe = cv2.createCLAHE()
    # img_orig = clahe.apply(img_orig)
    img_orig = cv2.equalizeHist(img_orig)

    img = ndimage.gaussian_filter(img_orig, sigma=10)

    if debugMode:
        plt.subplot(1, 2, 2)
        plt.imshow(img)
        plt.show()

    img_mean = np.mean(img)
    img_max = np.max(img)
    img_min = np.min(img)

    img_mean *= 1.3

    print(img_max, img_min, img_mean)

    idx_0 = np.where(img < img_mean)
    idx_1 = np.where(img >= img_mean)

    img[idx_0] = 0
    img[idx_1] = 1

    ret, img = cv2.threshold(
        img, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

    open_img = ndimage.binary_opening(img)
    img = ndimage.binary_closing(open_img)

    # imgray = np.uint8(img.copy())
    # edges = cv2.Canny(imgray,10,30)
    # print edges
    # if debugMode:
    #	plt.subplot(1,3,2)
    #	plt.imshow(edges)

    sx = ndimage.sobel(img, axis=0, mode='constant')
    sy = ndimage.sobel(img, axis=1, mode='constant')
    sob = np.hypot(sx, sy)

    cnt, hier = find_cont(img)
    pdd = cnt

    circles = []
    ellipses = []

    for c in cnt:
        region = c

        try:
            (x1, y1), radius = cv2.minEnclosingCircle(region)
            sob[y1, x1] = 255

            t_sob = sob.copy()
            t_sob[region] = 255
            idx = np.where(t_sob < 255)
            t_sob[idx] = 0
            center = x1, y1
            x, y = np.where(t_sob == 255)
            circle0 = plt.Circle(center, radius, color='g', fill=False)

            imgray = np.uint8(t_sob)
            cimg = cv2.cvtColor(imgray, cv2.COLOR_GRAY2BGR)

            circles.append(circle0)
        except:
            print("no circle found")
        try:
            ellipse = cv2.fitEllipse(region)
            ellipses.append(ellipse)
            # print ellipse

        except:
            print("no ellipse found")

    return sob, img, circles, ellipses


################ not working for this porpuse ###########################################################

def find_circles(img, center):
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    img = np.uint8(img)
    circles = cv2.HoughCircles(img, cv2.CV_HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=int(
        round(center.radius*0.95)), maxRadius=int(round(center.radius*1.05)))
    if circles is not None:
        circles = np.uint16(np.around(circles))

        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)

    else:
        print("can't find circles")

    return cimg

###########################################################################################


def find_cont(imgray):

    global debugMode
    imgray = np.uint8(imgray)

    im = imgray.copy()
    # ret,thresh = cv2.threshold(imgray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(
        imgray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(imgray, contours, -1, (0, 255, 0), 3)

    if debugMode:
        # plt.subplot(1,3,3)
        plt.imshow(imgray)
        plt.show()

    return contours, hierarchy


def print_data(name, x, y, c, e):

    global mylog
    result_str = ""
    result = True

    print("\nImmagine "+name)

    if len(c) == 0:
        result_str = "no circles found"

        mylog.write("%s,%.4f,%.4f,,,,,,,,,%s\n" % (name, x, y, result_str))
        mylog.flush()

    elif len(c) == 1:
        result_str = "one circle found... image is focused"
        print("raggio ext: " + str(c[0].radius))

        if e:

            MajorAxis = e[0][1][0]
            minorAxis = e[0][1][1]
            angleOut = e[0][2]

            delta_MA_ma = abs(MajorAxis - minorAxis)
            deltaPercOut = delta_MA_ma*100/MajorAxis

            if (deltaPercOut > 15):
                result_str += " STAR IS ELLPTICAL"

            mylog.write("%s,%.4f,%.4f,%.4f,,,,%.4f,,%.4f,,%s\n" % (
                name, x, y, c[0].radius, deltaPercOut, angleOut, result_str))
            mylog.flush()
        else:
            mylog.write("%s,%.4f,%.4f,%.4f,,,,,,,,%s\n" %
                        (name, x, y, c[0].radius, result_str))
            mylog.flush()
    else:

        x = c[0].center[0]
        y = c[0].center[1]

        xc = c[1].center[0]
        yc = c[1].center[1]

        err = np.sqrt((x-xc)**2 + (y-yc)**2)
        centeringError = err*c[1].radius/100

        print("raggio ext: " + str(c[1].radius))
        print("raggio int: " + str(c[0].radius))

        if centeringError < 100:
            print("errore centro: " + str(err))
            print("errore % su raggio ext " + str(centeringError) + " % \n")

        if len(e) > 0:

            MajorAxis = e[0][1][0]
            minorAxis = e[0][1][1]
            angleIn = e[0][2]

            delta_MA_ma = abs(MajorAxis - minorAxis)
            deltaPercIn = delta_MA_ma*100/MajorAxis

            if (deltaPercIn > 15):
                result_str += " M2 SHADOW IS ELLIPTICAL"
                result = False
            if (centeringError > 10):
                result_str += " M2 SHADOW IS NOT CENTERED"
                result = False
            if (result):
                result_str += " OK"

            if len(e) > 1:
                MajorAxis = e[1][1][0]
                minorAxis = e[1][1][1]
                angleOut = e[1][2]

                delta_MA_ma = abs(MajorAxis - minorAxis)
                deltaPercOut = delta_MA_ma*100/MajorAxis

                result = True

                if (deltaPercOut > 15):
                    result_str += " STAR IS ELLPTICAL"
                    result = False

                mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%s\n" % (
                    name, x, y, c[1].radius, c[0].radius, err, centeringError, deltaPercOut, deltaPercIn, angleOut, angleIn, result_str))
                mylog.flush()

            else:
                angleOut = angleIn
                deltaPercOut = deltaPercIn

                mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,,%.4f,,%s\n" % (
                    name, x, y, c[1].radius, c[0].radius, err, centeringError, deltaPercOut, angleOut, result_str))
                mylog.flush()

        else:
            mylog.write("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,,,,,%s\n" % (
                name, x, y, c[1].radius, c[0].radius, err, centeringError, result_str))
            mylog.flush()

        print(result_str)

    return result_str


if __name__ == '__main__':
    procAlreadyExist = False

    if len(sys.argv) > 1:

        if len(sys.argv) > 2:
            parser = argparse.ArgumentParser(
                prog=sys.argv[1], description='Acquire not yet observed positions')
            parser.add_argument('--interactive', dest='interactive',
                                default=True, help=': %(default)s, interactive mode')
            parser.add_argument('--debug', dest='debugMode',
                                default=False, help=': %(default)s, debug mode')
            args = parser.parse_args()

            if args.interactive:
                print("Interactive Mode")
                interactive = True

            if args.debugMode:
                print("Debug Mode")
                debugMode = True
            else:
                debugMode = False

            folder = sys.argv[2]
        else:
            folder = sys.argv[1]
            print("automatic mode")
            interactive = False
            debugMode = False

        procFolder = folder+'proc/'
        filelist = os.listdir(folder)
        try:
            os.mkdir(procFolder)
        except:
            procAlreadyExist = True
            print("cant' create folder proc/")

        mylog = open("%sanalisi.csv" % procFolder, "wt+")
        mylog.write("filename,x_center_img_orig,y_center_img_orig,radius_ext,radius_int,abs_err_centering,perc_err_centering,Ma_ma_perc_ext,Ma_ma_perc_int,angle_out, angle_in,result\n")
        mylog.flush()

        myflatfile = cv2.imread(__BASE_PATH__+'/g4Flat_mirror.png', 0)

        for myfile in filelist:
            # open image in grayscale
            procfile = None
            if (procAlreadyExist):

                if (myfile.endswith('.bmp')):
                    tfile = myfile.replace('.bmp', '.png')

                elif (myfile.endswith('.fits')):
                    tfile = myfile.replace('.fits', '.png')
                else:
                    tfile = myfile

                procfile = cv2.imread(folder+"proc/"+tfile)

            if (procfile is not None):
                print("image already processed")
            else:

                myfits = pyfits.open(folder+myfile)

                imgray = myfits[1].data

                imgmin = imgray.min()

                imgray = imgray - imgmin

                t_img = imgray / 2**16
                imgray = t_img * 2 ** 8
                imgray = np.round(imgray)
                imgray = np.uint8(imgray)

                if (np.shape(imgray) == np.shape(myflatfile)):
                    imgray = imgray - myflatfile
                else:
                    print("not subtract flat")
                # imgpl = np.hstack((imgray,myflatfile, img))

                # plt.imshow(imgpl)
                # plt.show()

                # imgray = cl1

                # img = imgray.copy()
                # img = imgray[800:1800, 1000:2000] -  (myflatfile[800:1800, 1000:2000])

                # print imgray.min(), imgray.max()

                # imgray = cl1
                # imgray = cv2.imread(folder+myfile,0)

                if (imgray is None):
                    print(myfile + " not a valid image, skip!")
                else:
                    surf = cv2.SURF(20)
                    # surf.upright = True
                    # Find keypoints and descriptors directly
                    kp, des = surf.detectAndCompute(imgray, None)

                    # img2 = cv2.drawKeypoints(imgray,kp,None,(255,0,0),4)

                    maxkval = 0
                    maxk = None
                    starFound = False

                    for k in kp:
                        if k.size > maxkval:
                            maxkval = k.size
                            maxk = k

                    if maxk is not None:
                        starFound = True
                    # if not interactive:
                    #	if maxk is not None:
                    #		starFound = True
                    # else:
                    #	if kp:
                    #		while starFound == False:
                    #			img2 = imgray.copy()
                    #			cv2.drawMarker(img2, (int(maxk.pt[0]), int(maxk.pt[1])), (255,255,0),4, int(maxk.size), 2)
                    #			plt.imshow(img2),plt.show()
                    #		  	right = raw_input("is the region right? ")
                    #			if (right.lower() == 'y'):
                    #				starFound = True
                    #			else:
                    #				kp.remove(maxk)
                    #
                    #				if kp:
                    #					maxkval = 0
                    #					maxk = None
                    #					for k in kp:
                    #						if k.size > maxkval:
                    #							maxkval = k.size
                    #							maxk = k
                    #				else:
                    #					print "star not found in img by user"
                    #					break

                    if (not starFound):
                        print("star not found, skip")
                    else:
                        mypoint = maxk
                        dim = mypoint.size*1.5
                        x = round(mypoint.pt[1])
                        y = round(mypoint.pt[0])
                        min_x = round(x-dim)
                        max_x = round(x+dim)
                        min_y = round(y-dim)
                        max_y = round(y+dim)

                        star = imgray[min_x:max_x, min_y:max_y]
                        height, width = star.shape[:2]

                        if (width > 1 and height > 1):

                            while width < 800:
                                width *= 2
                                height *= 2

                            star = cv2.resize(
                                star, (width, height), interpolation=cv2.INTER_CUBIC)

                            if debugMode:
                                plt.figure()
                                plt.subplot(1, 2, 1)
                                plt.imshow(star)

                            sobel, img,  circles, ellipsis = find_contour(star)

                            # title =

                            plt.figure()
                            plt.subplot(121)  # , plt.title(title),
                            plt.imshow(star)
                            plt.subplot(122)
                            for e in ellipsis:
                                cv2.ellipse(star, e, (255, 255, 255), 2)

                            plt.imshow(sobel), plt.contour(
                                sobel, [0.5], linewidths=1, colors='r')
                            ax = plt.gca()
                            for c in circles:
                                ax.add_artist(c)

                            if (myfile.endswith('.bmp')):
                                myfile = myfile.replace('.bmp', '.png')

                            if (myfile.endswith('.fits')):
                                myfile = myfile.replace('.fits', '.png')

                            plt.savefig(folder+"proc/"+myfile)

                            if (interactive):
                                plt.show()
                                right = input("is this ok? ")
                                if (right.lower() == 'y'):
                                    print_data(myfile, x, y, circles, ellipsis)
                                else:
                                    os.remove(folder+"proc/"+myfile)

                            else:
                                plt.savefig(folder+"proc/"+myfile)
                                print_data(myfile, x, y, circles, ellipsis)

                            plt.close()

    else:
        mylog = open("%sanalisi.csv" % (__BASE_PATH__), "wt+")
        mylog.write(
            "filename,radius_ext,radius_int,abs_err_centering,perc_err_centering,Ma_ma_perc_ext,Ma_ma_perc_int,angle_out, angle_in,result\n")
        mylog.flush()

        im0 = 'defocused_star_bad.jpg'
        im1 = 'def_star_bad3.jpg'
        im2 = 'def_star_ok.jpg'
        im3 = 'def_star.jpg'

        star1 = cv2.imread(im0, 0)
        star2 = cv2.imread(im1, 0)
        star3 = cv2.imread(im2, 0)
        star4 = cv2.imread(im3, 0)

        # star_ok = cv2.medianBlur(star_ok,5)
        sob0, img,  c0, e0 = find_contour(star1)
        sob1, img1, c1, e1 = find_contour(star2)
        sob2, img2, c2, e2 = find_contour(star3)
        sob3, img3, c3, e3 = find_contour(star4)

        title0 = print_data(im0, 0, 0, c0, e0)
        title1 = print_data(im1, 0, 0, c1, e1)
        title2 = print_data(im2, 0, 0, c2, e2)
        title3 = print_data(im3, 0, 0, c3, e3)

        plt.figure()

        plt.subplot(241)
        plt.title(title0)
        plt.imshow(star1)
        plt.subplot(242)

        for e in e0:
            cv2.ellipse(star1, e, (255, 255, 255), 2)

        plt.imshow(sob0)
        plt.contour(sob0, [0.5], linewidths=1, colors='r')
        ax = plt.gca()
        for c in c0:
            ax.add_artist(c)

        plt.subplot(243)
        plt.title(title1)
        plt.imshow(star2)
        plt.subplot(244)

        for e in e1:
            cv2.ellipse(star2, e, (255, 255, 255), 2)

        plt.imshow(sob1)
        plt.contour(sob1, [0.1], linewidths=1, colors='r')
        ax = plt.gca()
        for c in c1:
            ax.add_artist(c)

        plt.subplot(245)
        plt.title(title2)
        plt.imshow(star3)
        plt.subplot(246)
        for e in e2:
            cv2.ellipse(star3, e, (255, 255, 255), 2)

        plt.imshow(sob2)
        plt.contour(sob2, [0.1], linewidths=1, colors='r')
        ax = plt.gca()
        for c in c2:
            ax.add_artist(c)

        plt.subplot(247)
        plt.title(title3)
        plt.imshow(star4)
        for e in e3:
            cv2.ellipse(star4, e, (255, 255, 255), 2)

        plt.subplot(248)
        plt.imshow(sob3)
        plt.contour(sob3, [0.1], linewidths=1, colors='r')
        ax = plt.gca()
        for c in c3:
            ax.add_artist(c)

        # plt.savefig('figure.png')
        # plt.close()
        # plt.show()
        plt.savefig(folder+"proc/"+myfile)
        plt.close()
