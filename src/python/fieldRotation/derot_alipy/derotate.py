import os
import sys

import alipy
import pyfits
import numpy as np

from itm_cli.config import OBSERVATION_PATH
from itm_cli.utils import script_args, RTS2Wrapper



# Verificare che salti le directory in cui e' presente il final.fits
# todo: quando funziona con queste immagini, provare con un filtro B o I e testare sex.cfg
# oppure i parametri di indent.run()


class Derotate(RTS2Wrapper):

    def __init__(self, catalog, observation_dir):
        RTS2Wrapper.__init__(self)
        # For instance: catalog=agb, observation_dir=2018_obs02
        # self.root_dir = os.path.join(OBSERVATION_PATH, catalog, observation_dir)
        self.root_dir = './images'

    def get_files(self):
        result = []  # [(star_name, filter, [files])]
        for star in os.listdir(self.root_dir):
            star_dir = os.path.join(self.root_dir, star)
            for filter in os.listdir(star_dir):
                filter_dir = os.path.join(star_dir, filter)
                final_fits = os.path.join(filter_dir, 'final.fits')
                if os.path.exists(final_fits):
                    continue
                filter_files = os.listdir(filter_dir)
                files = []
                for file in filter_files:
                    if not file.startswith('elab') or not file.endswith('.fits'):
                        continue
                    else:
                        file_path = os.path.join(filter_dir, file)
                        files.append(file_path)
                files.sort()
                result.append((star, filter, files))
        result.sort()
        return result

    def derotate(self, images):
        # ref_image = images.pop()
        ref_image = images[0]
        images = images[1:]
        # ref_image = './pippo.fits'
        ref = pyfits.open(ref_image)
        ref_header = ref[0].header
        
        hdulist = pyfits.HDUList()
        hdulist.append(pyfits.ImageHDU(data=None))
        ihdu= len(hdulist)-1
        for card in ref_header.cards:
            key, value, description = card
            hdulist[ihdu].header.set(key, value, description)
        
        try:
            identifications = alipy.ident.run(ref_image, images, visu=True, n=4)
        except RuntimeError:
            self.log('I', 'Cannot find objects in the reference image %s' % ref_image)
            sys.exit(1)
        
        for i, id in enumerate(identifications): 
            if id.ok == True:
                info = "%15s : %20s, flux ratio %.2f" % (id.ukn.name, id.trans, id.medfluxratio)
                self.log('I', info)
                hdulist[ihdu].header.set('Img_%i ' % i, info)
            else:
                info= "%20s : no transformation found !" % (id.ukn.name) 
        
        outputshape = alipy.align.shape(ref_image)
        images_dir = os.path.dirname(ref_image)
        try:
            # temp_dir = os.path.join(images_dir, 'alipy_out')
            temp_dir = './alipy_out'
            # if not os.path.exists(temp_dir):
                # os.mkdir(temp_dir)
            
            for id in identifications:
                if id.ok == True:
                    alipy.align.affineremap(
                        id.ukn.filepath,
                        id.trans,
                        # outdir=temp_dir,
                        shape=outputshape,
                        makepng=True)
            
            final_data = np.zeros((3056,3056))
            for file in sorted(os.listdir(temp_dir)):
                if file.endswith('.fits'):
                    full_path = os.path.join(temp_dir, file)
                    myfile = pyfits.open(full_path)
                    data = myfile[0].data
                    final_data += data
            
            hdulist[ihdu].data = final_data
            final_file = './final.fits'
            # final_file = os.path.join(images_dir, 'final.fits')
            hdulist.writeto(final_file, clobber=True)
        finally:
            if os.path.exists(temp_dir):
                pass
                # os.system('rm %s -r' % temp_dir)

    def run(self):
        self.log('I', 'Staring the derotating procedure ...')
        for star_name, filter, files in self.get_files():
            self.derotate(files)


if __name__ == '__main__':
    kargs = script_args('derotate')
    # script = Derotate(**kargs)
    script = Derotate(catalog='agb', observation_dir='foo')
    script.run()
