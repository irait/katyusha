import alipy
import glob
import numpy as np

import datetime as dt
import time as tm
import pyfits
import ephem as ep

import sys
import os



def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

Info={}

Info['Telescope']='IRAIT'
Info['Instrument']='G4-9000 Moravian'
Info['Filter']='None'
Info['Version']='1.0'

images_to_align = sorted(glob.glob("images/FY/C/*.fits"))
ref_image = "0.fits"

ref = pyfits.open(ref_image)
ref_header = ref[0].header

hdulist=pyfits.HDUList()
hdulist.append(pyfits.ImageHDU(data=None))
ihdu= len(hdulist)-1
timeExp = dt.datetime.now()
utcNow =timeExp - dt.timedelta(hours=8)
jd = ep.julian_date(utcNow)

hdulist[ihdu].header.set('extname',"LIGHT")
hdulist[ihdu].header.set('Date',now_str(timeExp),'Date of creation')
hdulist[ihdu].header.set('UTC',now_str(utcNow),'Date of creation UTC')
hdulist[ihdu].header.set('JD',now_str(jd),'Date of creation UTC')

hdulist[ihdu].header.set('Telescop',Info['Telescope'])
hdulist[ihdu].header.set('Instrum',Info['Instrument'])
hdulist[ihdu].header.set('ElaRaw','Ela','Elaborated or raw')
hdulist[ihdu].header.set('RefDoc','None','documentation reference')
hdulist[ihdu].header.set('Filter',Info['Filter'],'filter for frame')
hdulist[ihdu].header.set('Version',Info['Version'],'File Version')

hdulist[ihdu].header.set('Chip_t',ref_header.get('CHIP_T'),'Chip CCD temperature (Celsius) ')
hdulist[ihdu].header.set('Camera_t',ref_header.get('CAMERA_T'),'Camera temperature (Celsius) ')
hdulist[ihdu].header.set('Expos',ref_header.get('EXPOS'),'Exposition time (sec)')




identifications = alipy.ident.run(ref_image, images_to_align, visu=True, n=4)

i = 0

for id in identifications: 
	
	if id.ok == True:
		info = "%15s : %20s, flux ratio %.2f" % (id.ukn.name, id.trans, id.medfluxratio)
		print info
		hdulist[ihdu].header.set('Img_%i '%i,info)
		i += 1
	else:
		info= "%20s : no transformation found !" % (id.ukn.name) 

	
	


outputshape = alipy.align.shape(ref_image)

for id in identifications:
	if id.ok == True:
		
		alipy.align.affineremap(id.ukn.filepath, id.trans, shape=outputshape, makepng=True)
		

final = np.zeros((3056,3056))

folder = './alipy_out'

filelist = os.listdir(folder)
filelist.sort()

for f in filelist:
	if f.endswith('.fits'):
		myfile = pyfits.open(''+folder+'/'+f)
		data = myfile[0].data

		final += data


hdulist[ihdu].data = final
hdulist.writeto('./final.fits',clobber=True)
