#!/usr/bin/python

import numpy as np
import math
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
import datetime as dt
import pylab as plt
import sys
import calendar


#dome_c_costant 
observer_constant=  15.04106858*math.cos(math.radians(-75.099814))


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')


def calculateRotation(alt, az):

	global observer_constant
	return observer_constant*math.cos(math.radians(az))/math.cos(math.radians(alt))


def getAltAzi(fixedStar, date):

    try:
	    #print date
	    IRAIT.date=ephem.date(date)
	    #print IRAIT.date
	  
	    fixedStar.compute(IRAIT) 
	    
	    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)))
    except Exception as e:
	    print e
	    return -1


if __name__ == '__main__' :

	if len(sys.argv) > 2 :
		ra= str(sys.argv[1])
		dec = str(sys.argv[2])
		name = str(sys.argv[3])
		 

		date_array = []
		alt_array = []
		az_array = []
		rot_array = []

		rot_good = []
		
		initdate = dt.datetime.utcnow()
		
		in_good_interval = False

		x1=0
		x2=0
		x3=0
		x4=0

		for i in range(0, 120):
			try:

					
				#print "begin"
				mystar = ephem.FixedBody()
				mystar._ra = ra
				mystar._dec = dec
				mydate = initdate + dt.timedelta(0,i*60*10)
				mydate = mydate.replace(microsecond=0)

				alt, az = getAltAzi(mystar, mydate)


				#print "alt: "+ str(alt) + " az: "+str(az)

				rot = calculateRotation(alt, az)
			
				date_array.append(mydate+dt.timedelta(0,8*3600))
				alt_array.append(alt)
				az_array.append(az)
				rot_array.append(rot)
				if i == 0:
					print "now... alt:"+str(alt)+ " az:"+ str(az) + ". rot: "+str(rot)
			

				if (abs(rot) < 1.) and not in_good_interval and alt > 4:
					in_good_interval = True 	
					ttime = mydate+dt.timedelta(0,8*3600)
					print "start", ttime ,alt,az,rot
					if x1 == 0:
						x1 = ttime
					else:
						x3 = ttime

				elif ((abs(rot) > 1.) or alt < 4) and in_good_interval :
					in_good_interval = False	
					ttime = mydate+dt.timedelta(0,8*3600)
					print "stop",ttime,alt,az,rot
					if x2 == 0:
						x2 = ttime
					else:
						x4 = ttime

				mystar = None
				#print "end"
			except Exception as e:
				print e


		#print rot_array
		#plt.plot(date_array, alt_array, 'r')
		#plt.plot(date_array, az_array, 'b')
		#plt.savefig('./alt_az.png')
		#plt.close()
		
		#plt.figure(figsize=(800, 400))
		plt.plot(date_array, alt_array, 'r', label="altitude (degrees)")
		plt.plot(date_array, rot_array, 'g*', label="rotation (asec)")
		plt.axvspan(x1, x2, facecolor='g', alpha=0.5)
		plt.axvspan(x3, x4, facecolor='g', alpha=0.5)
		#plt.axvline(x1, label = x1)
		plt.axhline(linewidth=1, color='r', y = 4)
		plt.title('%s - ra: %s, dec: %s ' % (name, ra, dec))
		#plt.xticks(np.arange(6),  ('00:00', '04:00', '08:00'))
		plt.legend()
			
		plt.savefig('./rot_%s.png' % name)
		plt.close()
		print ("done")
		exit(0)


	else:
		print "specify ra and dec of the field"
