#!/usr/bin/python
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import math

observer_constant = 10.8575984016 
fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.arange(0, 370, 10)
Y = np.arange(0, 100, 10)
#print Y
#Altitude goes to 85 degrees and not 90 because numbers are too large at 90 degrees
Y[9]=85
#print Y
X, Y = np.meshgrid(X, Y)
Z = np.arange(10.*37)
Z.shape = (10, 37)

for alt_index in range(0, 10):
    for az_index in range(0, 37):        
        if alt_index == 9:
                alt_num = 85
        else:
                alt_num = alt_index*10           
        Z[alt_index,az_index] = observer_constant* \
        math.cos(math.radians(az_index*10))/math.cos(math.radians(alt_num))

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
#cset = ax.contourf(X, Y, Z, zdir='z', offset=-140, cmap=cm.coolwarm)
#cset = ax.contourf(X, Y, Z, zdir='x', offset=0, cmap=cm.coolwarm)
#cset = ax.contourf(X, Y, Z, zdir='y', offset=90, cmap=cm.coolwarm)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%d'))
ax.set_title("Star Field Rotation Rate for\n +43.79145 degrees altitude\n", fontsize=16, fontweight='bold')

fig.colorbar(surf, shrink=1, aspect=20)

ax.set_xlabel('Star Azimuth Degrees')
ax.set_xlim(0, 360)
ax.set_ylabel('Star Altitude Degrees')
ax.set_ylim(0, 90)
ax.set_zlabel('Arc Seconds/Sidereal Arc Second')
ax.set_zlim(-130, 130)

plt.show()

#print 'X size: ', X.size 
#print 'X dimensions: ', X.ndim
#print 'X shape: ', X.shape
#print X

#print 'Y size: ', Y.size 
#print 'Y dimensions: ', Y.ndim
#print 'Y shape: ', Y.shape
#print Y

#print 'Z size: ', Z.size 
#print 'Z dimensions: ', Z.ndim
#print 'Z shape: ', Z.shape
#print Z
