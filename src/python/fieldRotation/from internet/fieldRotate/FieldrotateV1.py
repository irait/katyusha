# -*- coding: utf-8 -*-
#Calculate the field rotation of a star given 
#an observer's latitude, 
#the star's azimuth and altitude
#
#The data is in a comma seperated variable (CSV) file format with one line per
#star and its azimuth and altitude position.
#
#CSV line format
#Star name, date, time, Azimuth, degrees, minutes, seconds, Altitude, degrees, minutes, seconds, Observer location, decimal latitude  
#
#Example line without the leading #
#Albireo, 7-7-2013, 22:00, Azimuth, 92, 45, 18.9, Altitude, 45, 30, 25.3, PMO, 43.79145

#Python script developed with Enthought Canopy Express (Free) V1.0.3 (32bit) for OS X
#http://www.enthought.com

#Apple MacBook Pro
#http://www.stargazing.net/david/equip/MacBookPro.html

#Script based on matplotlib Examples
#http://matplotlib.org/examples/index.html

#David Haworth
#http://www.stargazing.net/david/

import math
import numpy as np

version = '1.00'
csv_file_name = 'StarData.csv'

def az_string(deg, min, sec):
    degree_chr =  u'\u00b0'
    return str(deg)+degree_chr+str(min)+"'"+str(sec)+'"'

def alt_string(deg, min, sec):
    degree_chr =  u'\u00b0'
    return str(deg)+degree_chr+str(min)+"'"+str(sec)+'"'

def day_seconds():
    hours = 24
    minutes = 0
    seconds = 0    
    return  (hours*3600 + minutes*60 + seconds)

def sideral_day_seconds():
#A mean sidereal day is about 23 hours, 56 minutes, 4.0916 seconds
#http://en.wikipedia.org/wiki/Sidereal_time
#http://www.merriam-webster.com/dictionary/sidereal%20day
#http://www.astro.cornell.edu/academics/courses/astro201/sidereal.htm
#http://astro.unl.edu/naap/motion3/sidereal_synodic.html

    sideral_hours = 23
    sideral_minutes = 56
    sideral_seconds = 4.0916
#    print 'Sideral day is', sideral_hours, 'hours', sideral_minutes, 'minutes', sideral_seconds, 'seconds long.'   
#    print 'Sideral day is', (sideral_hours*3600 + sideral_minutes*60 + sideral_seconds), 'seconds long.' 
    return  (sideral_hours*3600 + sideral_minutes*60 + sideral_seconds)    

def day_hours_decimal(hours, minutes, seconds):
    return  (hours + minutes/60. + seconds/3600.)

def decimal_latitude(degrees, minutes, seconds):
    return (degrees+minutes/60.0+seconds/3600.0)

def decimal_azimuth(degrees, minutes, seconds):
    return (degrees+minutes/60.0+seconds/3600.0)

def decimal_altitude(degrees, minutes, seconds):
    return (degrees+minutes/60.0+seconds/3600.0)

def observer_constant(observer_latitude_decimal):
    arc_seconds_in_360_degrees = 360*60*60
    return (arc_seconds_in_360_degrees/sideral_day_seconds())*math.cos(math.radians(observer_latitude_decimal))

#print ('Observer constant', observer_constant(obs_latitude_dec), 'for latitude', obs_latitude_dec, 'degrees')

def field_rotation_rate(observer_constant, star_azimuth_degrees, star_altitude_degrees):
    return observer_constant*math.cos(math.radians(star_azimuth_degrees))/math.cos(math.radians(star_altitude_degrees)) 

print 'Script version', version

az_deg, az_min, az_sec, alt_deg, alt_min, alt_sec, obs_latitude_dec=np.loadtxt(csv_file_name, delimiter=',', usecols=(4,5,6,8,9,10,12), unpack=True)
num_CSV_rows = alt_deg.itemsize
print 'File', csv_file_name, 'has', (num_CSV_rows-1), 'rows'

print 'Sideral day is', sideral_day_seconds(), 'seconds long.\n' 

csv_file = open(csv_file_name, 'r')

for num in range(num_CSV_rows-1):
    print 'Row %d:'% num,    
    csv_row = csv_file.readline()
    print csv_row.rstrip('\n')
    obs = observer_constant(obs_latitude_dec[num])
    
    print 'Object azimuth', az_string(int(az_deg[num]), int(az_min[num]), az_sec[num])+',', \
    'altitude', alt_string(int(alt_deg[num]), int(alt_min[num]), alt_sec[num])
    
    print 'Observer constant; %.3f arc seconds per sideral second.' % obs
#    print 'cos az', math.cos(math.radians(decimal_azimuth(az_deg[num], az_min[num], az_sec[num])))
#    print 'cos alt', math.cos(math.radians(decimal_altitude(alt_deg[num], alt_min[num], alt_sec[num])))
    print 'Field rotation: %.3f arc seconds per sideral second.' % field_rotation_rate(obs, decimal_azimuth(az_deg[num], az_min[num], az_sec[num]), \
    decimal_altitude(alt_deg[num], alt_min[num], alt_sec[num])),'\n'

#    field_rotation_rate(observer_constant(obs_latitude_dec), decimal_azimuth(92, 45, 18.9), decimal_altitude(45, 30, 25.3))

csv_file.close()

def exposure_time_sideral(seconds):
    return seconds*sideral_day_seconds()/day_seconds()

print 'One second exposure is %.4f' % exposure_time_sideral(1), 'sideral seconds exposure'
