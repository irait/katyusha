#!/usr/bin/python
import sys
from collections import OrderedDict
import numpy as np
import ephem
from TelescopeIRAIT import EPHEM as IRAIT

#J2000=ephem.date('2000/1/1 00:00:00')

if len(sys.argv) < 1+3:
   print "%s starName UT_Epoch UT_Time"%sys.argv[0]
   print 
   print "object refraction jd SolarElongation J2000_RA J2000_DEC App_RA App_Dec App_Alt App_Azi" 
   sys.exit()

fixedStar=ephem.star(sys.argv[1].capitalize())
IRAIT.date=ephem.date(sys.argv[2]+' '+sys.argv[3])
fixedStar.compute(IRAIT)

print fixedStar.name,
print 'airless',
print ephem.julian_date(IRAIT.date),
print np.rad2deg(fixedStar.elong),
print fixedStar._ra,fixedStar._dec,
print fixedStar.ra,fixedStar.dec,
print fixedStar.ra,fixedStar.dec,
print np.rad2deg(fixedStar.alt),np.rad2deg(fixedStar.az),
print
