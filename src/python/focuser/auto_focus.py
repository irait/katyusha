from pipollux import *
import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt
import time as tm
import scipy.misc
import datetime as dt
import meteo_dome_c as Meteo
from datetime import datetime as dtd

exp = 1.

print "init"

gx.configure_eth('10.10.18.21',gx.GXETH_DEFAULT_PORT)
cam = gx.GxCCD(6103)

chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)

cam.set_filter(0)

def shoot(name):
    
    cam.start_exposure(exp,True,0,0,chip_w,chip_d)
    tm.sleep(exp+0.5)
    img = cam.read_image(chip_d,chip_w)
#    scipy.misc.toimage(img).save(name+'.bmp')
    plt.figure()
    imgpolt = plt.imshow(img)
    plt.savefig(name+'.png')
    plt.close()
    #plt.show()


if __name__ == '__main__':
    ax = PolluxAxis(1,'/dev/ttyS0')
#    ax.calibrate_range()

    myMinute =0
    firstTime = True
    
    pos = ax.current_position()
    print "now position is %.4f" % pos
    ax.move_absolute(106.6)
    ax.wait_move()
    temperature = None
    while abs(ax.current_position() - 106.75) > 0. :
        newMinute = dtd.now().minute
        if ((newMinute != myMinute) and (newMinute % 2 == 0)) or firstTime:
            myMinute = newMinute
            firstTime = False
            try:
                mt=Meteo.Meteo(lg=None)
                temperature,pressure,humidity, w_dir, w_speed =mt.retrieve()
                temperature = float(temperature)
            except:
                temperature = None
                print 'no meteo data available'

        pos = ax.current_position()
        i = 0
        while i < 2:
            i += 1
            print "shooted %.4f" % pos
            if (temperature is not None):
                shoot('eight_sample_%s__%.4f___%.1f'%(str(dt.datetime.now()).replace(' ','_'),pos, temperature))
            else:
                shoot('eight_sample_%s__%.4f'%(str(dt.datetime.now()).replace(' ','_'),pos))
        print "now can stop"
        tm.sleep(5.)
        ax.move_relative(0.05)
        ax.wait_move(0.1)

