
from pipollux import *
import pygxccd as gx
import numpy as np
import matplotlib.pyplot as plt
import time as tm
import scipy.misc

import meteo_dome_c as Meteo
import pyfits
from datetime import datetime as dtd
import datetime as dt

import sys
import os
import argparse


exp = 0.2
cam = None
filterDay = True
boundRange = 12
learnMoveFactor = 0.06
workingFolder = ''


def initCamera():
    global cam
    global chip_d, chip_w
    gx.configure_eth('10.10.18.21', gx.GXETH_DEFAULT_PORT)
    cam = gx.GxCCD(6103)

    chip_d = cam.get_integer_parameter(gx.GIP_CHIP_D)
    chip_w = cam.get_integer_parameter(gx.GIP_CHIP_W)
    cam.set_filter(0)


def setDay():
    global exp
    global filterDay
    cam.set_filter(0)
    exp = 0.2
    filterDay = True


def setNight():
    global exp
    global filterDay
    cam.set_filter(6)
    exp = 2.
    filterDay = False


def calculatePos(temp):
    x1 = -58.9  # -55
    y1 = 110.2  # 111.3

    x2 = -61.7  # -60
    y2 = 111  # 107.5

    y = y1 + (temp - x1) * (y2-y1)/(x2-x1)
    return y


def shoot(name):
    global cam
    global filterDay
    global exp

    print exp

    tm.sleep(2.)

    if (cam is None):
        cam = gx.GxCCD(6103)
    cam.start_exposure(exp, True, 0, 0, chip_w, chip_d)
    tm.sleep(exp+0.5)
    image = cam.read_image(chip_d, chip_w)

    # remove defected pixels
   # image[image > 65000] = 0
    # adapt exposure time
    g4max = image.max()
  #  if g4max < 30000:
#	exp *= 2.
 #   elif g4max > 60000:
#	exp /= 2.

    if exp > 10. and filterDay:
        setNight()
    if exp < 0.4 and not filterDay:
        setDay()

    # plt.figure()
    #imgpolt = plt.imshow(image)
    # plt.savefig(name+'.png')
    # plt.close()

    hdulist = pyfits.HDUList()
    hdulist.append(pyfits.ImageHDU(data=image))
    hdulist.writeto(name+'.fits', clobber=True)

    cam = None


if __name__ == '__main__':
    ax = PolluxAxis(1, '/dev/ttyS0')
    myMinute = 0
    firstTime = True
    testMode = False
    learnMode = False

    if len(sys.argv) > 1:

        #test and auto-learn
        parser = argparse.ArgumentParser(prog=sys.argv[1])
        parser.add_argument('--test', dest='test',
                            default=False, help=': %(default)s, test mode')
        parser.add_argument('--learn', dest='learn',
                            default=False, help=': %(default)s, learn mode')
        args = parser.parse_args()

        myFolder = 'samples_%s' % (str(dt.datetime.now()).replace(' ', '_'))

        if args.learn:
            learnMode = True
            initCamera()
            # try:
            #	os.mkdir("learn_"+myFolder)
            #	workingFolder = "learn_"+myFolder+"/"
            # except:
            #	print "cant' create folder learn_"+myFolder+"/"

        if args.test:
            initCamera()
            testMode = True
            try:
                os.mkdir("test_"+myFolder)
                workingFolder = "test_"+myFolder+"/"
            except:
                print "cant' create folder test_"+myFolder+"/"

    else:
        print "Autofocuser working mode"
        # working mode

    pos = ax.current_position()
    print "Initial position is %.4f" % pos

    print "limits "+str(ax.get_limits())

    temperature = None
    oldTemp = None
    while True:
        newMinute = dtd.now().minute
        if ((newMinute != myMinute) and (newMinute % 1 == 0)) or firstTime:
            myMinute = newMinute
            firstTime = False
            try:
                oldTemp = temperature
                mt = Meteo.Meteo(lg=None)
                temperature, pressure, humidity, w_dir, w_speed = mt.retrieve()
                temperature = float(temperature)
            except:
                temperature = None
                print 'no meteo data available'

            if (temperature is not None):
                new_pos = calculatePos(temperature)
                print "new position: ", str(new_pos)
                #pos = ax.current_position()
                if new_pos > 100. and new_pos < 150:
                    ax.move_absolute(new_pos)
                    ax.wait_move(0.1)
                    if testMode:
                        pos = ax.current_position()
                        i = 0
                        while i < 3:
                            i += 1
                            print "shooted %.4f" % pos
                            shoot('%stest_sample_%s__%.4f___%.1f' % (workingFolder, str(
                                dt.datetime.now()).replace(' ', '_'), pos, temperature))

                        print "now can stop"
                        tm.sleep(5.)

                    elif learnMode and (oldTemp != temperature):

                        myFolder = 'samples_%s' % (
                            str(dt.datetime.now()).replace(' ', '_'))
                        try:
                            os.mkdir("learn_"+myFolder)
                            workingFolder = "learn_"+myFolder+"/"
                        except:
                            print "cant' create folder learn_"+myFolder+"/"

                        pos = ax.current_position()
                        for y in range(-boundRange, boundRange+1):
                            if (y > 0.):
                                newPos = pos + (learnMoveFactor*(abs(y)**1.5))
                            elif (y < 0.):
                                newPos = pos - (learnMoveFactor*(abs(y)**1.5))
                            else:
                                newPos = pos

                            print "y is:"+str(y)
                            print "new pos: "+str(newPos)

                            ax.move_absolute(newPos)
                            ax.wait_move(0.1)
                            Herr = ax.getHError()
                            if Herr != 0:
                                print "hardware error: "+str(Herr)
                            Serr = ax.getSError()
                            if Serr != 0:
                                print "software error: "+str(Serr)
                            tm.sleep(2.)
                            i = 0
                            while i < 3:
                                i += 1
                                learn_pos = ax.current_position()
                                print "shooting %.4f" % learn_pos
                                shoot('%slearn_%.4f_try_%d_%.4f_%.1f' % (
                                    workingFolder, pos, i, learn_pos, temperature))

                            print "now can stop"
                            tm.sleep(3.)

                else:
                    print "bad position"
