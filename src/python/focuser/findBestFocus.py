import numpy as np
import pylab as plt
import pyfits
from scipy.misc import imread, imsave
from PIL import Image
import cv2
import sys
import os


surf = cv2.SURF(10)

def getImgFromFits(nome):
	
	myfits = pyfits.open(nome)
	imgray = myfits[0].data.astype(np.float32)
		
	imgMin = imgray.min()
	imgray = imgray - imgMin	
	t_img = imgray / 2**16
	imgray = t_img * 2 ** 8
	imgray = np.round(imgray)
	imgray = np.uint8(imgray)
	return imgray

def getCenterStar(img):

	
	#ret, img0 = cv2.threshold(img,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
	kp, des = surf.detectAndCompute(img,None)
	x = 0
	y = 0
	dim = 0
	starFound = 0
        
	starFound = len(kp)

        if len(kp) == 1:
                for k in kp:
                        
                        dim = k.size
                        x = k.pt[1]
                        y = k.pt[0]
        elif len(kp) > 1:
                dim = 1000
       
        return starFound,x,y,dim


mylog = open("focusAnalisi.csv","wt+")
mylog.write("filename,pointLinear, pointShoot, temp, trial, foundStar,x,y,dim\n")
mylog.flush()


if len(sys.argv) > 1 :
	folder=sys.argv[1]
	filelist = os.listdir(folder)
	for f in filelist:
		if f.endswith('.fits'):
			img = getImgFromFits(f)
			numStar, x , y , dim = getCenterStar(img)
			data = f.split('_')
			pointLinear = data[1]
			pointShoot = data[4]
			temp = data[5]
			trial = data[3]
			mylog.write("%s, %s,%s, %s, %s, %d, %d, %d ,%.2f\n" %(f,pointLinear, pointShoot, temp, trial, numStar,x,y,dim))
			mylog.flush()

else:

	img0 = getImgFromFits('img0.fits')
	img1 = getImgFromFits('img1.fits')
	img2 = getImgFromFits('img2.fits')

	x0,y0,dim0 = getCenterStar(img0)
	x1,y1,dim1 = getCenterStar(img1)
	x2,y2,dim2 = getCenterStar(img2)



