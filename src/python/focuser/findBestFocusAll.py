
import numpy as np
import pylab as plt
import pyfits
from scipy.misc import imread, imsave
from scipy import ndimage

import scipy
from PIL import Image

import sys
import os



def getImgFromFits(nome):
	
	myfits = pyfits.open(nome)
	imgray = myfits[0].data.astype(np.float32)
		
	imgMin = imgray.min()
	imgray = imgray - imgMin	
	t_img = imgray / 2**16
	imgray = t_img * 2 ** 8
	imgray = np.round(imgray)
	imgray = np.uint8(imgray)
	return imgray


myflatfile = getImgFromFits('../flat.fits')


if len(sys.argv) > 1 :
	folder=sys.argv[1]
	filelist = os.listdir(folder)
	filelist.sort()

	FocPos=[]
	fwhm = []
	flux = []

	for f in filelist:
		if f.endswith('.fits') and not f.endswith('flat.fits'):


			data = f.split('_')
			pointLinear = data[1]
			trial = data[3]			
			pointShoot = data[4]
			temp = data[5].split('.f')[0]

			
		
			

			exitos=os.system('sex -c /home/irait/Downloads/sex/sex.cfg '+f+' >/dev/null 2>&1')
			if exitos == 0:
				print('sex succesful')
			if exitos != 0:
				print('sex unsuccesful')    
		    
		   	file='/home/irait/Downloads/sex/sex.cat'
		   	input = open(file,'r')
		    	star_infos = input.readlines()
			numobj = len(star_infos)
			print "num obj %i"%numobj
		    	tot_fwhm =0.0
			tot_flux =0.0

			for j in range(0, numobj):
				tot_fwhm += float(star_infos[j].split()[1])
		    		tot_flux += float(star_infos[j].split()[0])
			if (numobj > 0):
				FocPos.append(float(pointShoot))
		    		fwhm.append(tot_fwhm/numobj)
				flux.append((tot_flux/numobj)/1000)

	fway = scipy.array(fwhm)
	fluxay = scipy.array(flux)
	fpax = scipy.array(FocPos)
	
	a = []
	for i in range(0,len(fway)):
	    a.append((FocPos[i],fway[i]))
	a.sort()
	lastX = a[0][0]
	b = []
	x= []

	xpos = []
	for i in range(0, len(a)):
		

		if a[i][0] == lastX:
			b.append(a[i][1])
		else:
			x.append(b)
			xpos.append(lastX)
			lastX = a[i][0]
			b = None
			b = []
			b.append(a[i][1])

	x.append(b)
	xpos.append(lastX)
	xx= np.array(x)

	mins = xx.min(1)
	maxs = xx.max(1)
	means= xx.mean(1)



	#polycoeffs = scipy.polyfit(fpax, fway, 2)
	polycoeffs = scipy.polyfit(xpos, means, 2)
	yfit=scipy.polyval(polycoeffs, xpos)
    
	polycoeffs3 = scipy.polyfit(xpos, means, 3)
	yfit3=scipy.polyval(polycoeffs3, xpos)

	polycoeffsF = scipy.polyfit(fpax, fluxay, 2)
	yfitF=scipy.polyval(polycoeffsF, fpax)

	#plt.plot(fpax, fway, 'k.')
	plt.plot(xpos, yfit, 'r.')
	#plt.plot(xpos, yfit3, 'g')
	#plt.plot(fpax, fluxay, 'b.')
	plt.plot(fpax, yfitF, 'b.')
	

	
	
	plt.errorbar(xpos, means, [means-mins, maxs-means], fmt=".k", ecolor='gray', lw=1)
	
    	#print "fwhm coeff[0]:"+str(polycoeffs[0]) + ", coeff[1]:"+str(polycoeffs[1])
     	#print "flux coeff[0]:"+str(polycoeffsF[0]) + ", coeff[1]:"+str(polycoeffsF[1])

	

	if polycoeffs[0] > 0: 
		optfoc=float((-polycoeffs[1])/(2*polycoeffs[0]))
		minfway=polycoeffs[0]*optfoc*optfoc+polycoeffs[1]*optfoc+polycoeffs[2]
		
		if polycoeffsF[0] < 0:
			optfocF=float((-polycoeffsF[1])/(2*polycoeffsF[0]))

		optfocFinal = (optfoc + optfocF) / 2

	else:
		lincoeff = scipy.polyfit(fpax, fway, 1)
		ylinfit = scipy.polyval(lincoeff, fpax)
		for i in range(0, len(ylinfit)):
     			if ylinfit[i] == min(ylinfit):
	     			optfoc=fpax[i]
				minfway=fway[i]
				
		optfocFinal = optfoc
		print "maybe is not the best focus"
	
	
	strTitle= "T_%s, optFoc: %.4f, minfway: %.2f" % (temp,optfocFinal,minfway)
	plt.title(strTitle)     
	#os.system('mkdir /etc/rts2/bin/af/')   
	plt.savefig('./all.png')
	plt.cla()

	print (" temperature: %s, optimal focus: %.6f, minfway: %.4f" % ( temp,optfocFinal,minfway))
		
else:
	print "specify flder where to work"


