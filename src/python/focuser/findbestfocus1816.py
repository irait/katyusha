import numpy as np
import pylab as plt
import pyfits
from scipy.misc import imread, imsave
from scipy import ndimage

import scipy
from PIL import Image
import cv2
import cv2.cv as cv
import sys
import os


surf = cv2.SURF(200)


def getImgFromFits(nome):
	
	myfits = pyfits.open(nome)
	imgray = myfits[0].data.astype(np.float32)
		
	imgMin = imgray.min()
	imgray = imgray - imgMin	
	t_img = imgray / 2**16
	imgray = t_img * 2 ** 8
	imgray = np.round(imgray)
	imgray = np.uint8(imgray)
	return imgray

def getCenterStar(img, i, pos):
        global myflatfile
	#print myflatfile
	img0 = img - myflatfile	

	#img = cv2.equalizeHist(img0)
	#ret, img0 = cv2.threshold(img0,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
	
	img_mean = np.mean(img0)
	img_mean *= 1.3


	idx_0 = np.where(img0 < img_mean)
	idx_1 = np.where(img0 >= img_mean) 

	img0[idx_0] *= 0.2
	img0[idx_1] *= 1.8

	img = ndimage.gaussian_filter(img0, sigma=6)
	
	img_max = np.max(img)

	kp, des = surf.detectAndCompute(img,None)
	img2 = cv2.drawKeypoints(img,kp,None,(255,0,0),4)
	


	

	x = 0
	y = 0
	dim = -1000
	starFound = 0
        
	starFound = len(kp)

        if len(kp) == 1:
                for k in kp:
                        
                        dim = k.size
                        x = k.pt[1]
                        y = k.pt[0]
		
			t_dim = 110
			min_x =  round(x-t_dim)
			max_x =  round(x+t_dim)
			min_y = round(y-t_dim)
			max_y = round(y+t_dim)

			star = img2[min_x:max_x, min_y:max_y]
			
			
			plt.subplot(4,4,i) 
			#plt.title(str(i)+"_"+pos)
			plt.title(str(dim))
			axes = plt.gca()
			axes.tick_params(length=0, width=0)
			axes.label_outer()
			plt.imshow(star)

        elif len(kp) > 1:
                dim = 1000
       
        return starFound,x,y,dim, img_max



myflatfile = getImgFromFits('../flat.fits')


if len(sys.argv) > 1 :
	folder=sys.argv[1]
	filelist = os.listdir(folder)
	filelist.sort()


	#max 14
	itry = 1
	itryMax = 2

	

	while itry <= itryMax:
		FocPos=[]
		fwhm = []

		for f in filelist:
			if f.endswith('.fits') and not f.endswith('flat.fits') and (f.find("try_"+str(itry)) > -1):


				data = f.split('_')
				pointLinear = data[1]
				trial = data[3]			
				pointShoot = data[4]
				temp = data[5].split('.f')[0]

				FocPos.append(float(pointShoot))
			
				img = getImgFromFits(f)
				exitos=os.system('sex -c /home/irait/Downloads/sex/sex.cfg '+f+' >/dev/null 2>&1')
				if exitos == 0:
					print('sex succesful')
				if exitos != 0:
					print('sex unsuccesful')    
			    
			   	file='/home/irait/Downloads/sex/sex.cat'
			   	input = open(file,'r')
			    	fwhms = input.readlines()
				numobj = len(fwhms)
			    	
				print "numObj "+ str(numobj)
				tot=0.0

				for j in range(0, numobj):
					tot=tot+float(fwhms[j])
			    
				if (numobj > 0):
			    		fwhm.append(tot/numobj)

				else:
					fwhm.append(0)
				
				#if abs(dim) < min_dim:
				#	min_dim = dim
				#	min_shoot = pointShoot

				#if img_max > max_force:
				#	max_force = img_max				
				#	max_shoot = pointShoot 

		fway = scipy.array(fwhm)
		fpax = scipy.array(FocPos)
		polycoeffs = scipy.polyfit(fpax, fway, 2)
		yfit=scipy.polyval(polycoeffs, fpax)
	    
	
		plt.plot(fpax, fway, 'k.')
		plt.plot(fpax, yfit, 'r.')
	    
	     

		if polycoeffs[0] > 0: 
			optfoc=int((-polycoeffs[1])/(2*polycoeffs[0]))
			minfway=polycoeffs[0]*optfoc*optfoc+polycoeffs[1]*optfoc+polycoeffs[2]
			fpaxdist=[abs(fpax[0]-optfoc)]
			for i in range(1,len(fpax)):
				fpaxdist.append(abs(fpax[i]-optfoc))
			good=0
			if min(fpaxdist)>1400:
				lincoeff = scipy.polyfit(fpax, fway, 1)
		        	ylinfit = scipy.polyval(lincoeff, fpax)
		        	for i in range(0, len(ylinfit)):
		                	if ylinfit[i] == min(ylinfit):
		                        	optfoc=fpax[i]
		                        	minfway=fway[i]
		                        	good=1
		
		else:
			lincoeff = scipy.polyfit(fpax, fway, 1)
			ylinfit = scipy.polyval(lincoeff, fpax)
			for i in range(0, len(ylinfit)):
	     			if ylinfit[i] == min(ylinfit):
		     			optfoc=fpax[i]
					minfway=fway[i]
					good=1

		strTitle= "try:%d, T_%s, optFoc: %.4f, minfway: %.2f, ok: %i" % (itry, temp,optfoc,minfway,good)
		plt.title(strTitle)     
		#os.system('mkdir /etc/rts2/bin/af/')   
		plt.savefig('./try_'+str(itry)+'.png')
		plt.cla()

		print ("try: %d, temperature: %s, optimal focus: %.6f, minfway: %.4f, good: %i" % (itry, temp,optfoc,minfway,good))
		itry += 1	
else:
	print "specify flder where to work"


