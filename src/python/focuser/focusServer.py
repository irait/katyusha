import socket
import sys
from thread import *

from pipollux import *
import meteo_dome_c as Meteo
import os

from datetime import datetime as dtd
import datetime as dt
import time as tm


HOST = ''
PORT = 10666


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ', 'T')


def initTCPServer():
    global s

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mylog.write('%s - Socket created \n' % now_str())

    connFailure = True

    i = 0

    while i < 22 and connFailure:
        i += 1
        try:
            s.bind((HOST, PORT))
            connFailure = False
        except socket.error as msg:
            mylog.write('%s - Bind failed. Error Code : %s, Message: %s \n' %
                        (now_str(), str(msg[0]), msg[1]))
            tm.sleep(i)

    if connFailure:
        sys.exit()
    else:
        mylog.write('%s - Socket bind complete \n' % now_str())

        s.listen(10)
        mylog.write('%s - Socket now listening \n' % now_str())


def checkAndLogError(ax):
    global mylog

    Herr = ax.getHError()
    if Herr != 0:
        mylog.write('%s - hardware error: %d \n ' % (now_str(), Herr))
    serr = ax.getSError()
    if Serr != 0:
        mylog.write('%s - software error: %d \n ' % (now_str(), Serr))


def clientthread(conn, ax):
    global currtemp
    global limitLo
    global limitUp
    global mylog

    isCalibrating = False

    limits = ax.get_limits()
    try:
        lo_s, up_s, end = limits.split(' ')
        limitLo = float(lo_s)
        limitUp = float(up_s)
    except:
        limitLo = -1000
        limitUp = 1000

    while True:
        data = conn.recv(64)

        if not data:
            break

        elif data.find("emergencyStop()") >= 0:
            mylog.write('%s - received emergency stop CMD \n' % now_str())
            ax.stop()

        elif data.find("getStatus()") >= 0:

            status = ax.is_stage_moving()
            if status:
                stNum = 1
            else:
                stNum = 0

            if (isCalibrating):
                if status:
                    stNum = 2
                else:
                    stNum = 0
                    isCalibrating = False
            conn.send(str(stNum))

        elif (isCalibrating == False):

            if data.find("initFocuserLow()") >= 0:
                isCalibrating = True
                ax.calibrate_low_limit()
                mylog.write('%s - received calibrate Low CMD \n' % now_str())

            elif data.find("initFocuserUp()") >= 0:
                isCalibrating = True
                ax.calibrate_up_limit()  # it seems not work

                # this is only a trik
                # ax.set_limits()
                #isCalibrating = False

                mylog.write('%s - received calibrate Low CMD \n' % now_str())

            elif data.find("getPosition()") >= 0:
                pos = ax.current_position()
                floatpos = float(pos)
                strpos = "%010.3f" % (floatpos)
                conn.send(strpos)
            elif data.find("setPosition(_") >= 0:
                parData = data.split('_')
                pos = parData[1]
                mylog.write('%s - received calibrate new position: %s \n' %
                            (now_str(), str(pos)))
                numpos = float(pos)
                ax.move_absolute(numpos)

            elif data.find("getTemperature") >= 0:
                conn.send(str(currtemp))

            elif data.find("getLimitSwitchUp()") >= 0:
                limits = ax.get_switch_state()
                lo_s, up_s, end = limits.split(' ')
                conn.send(str(up_s))

            elif data.find("getLimitSwitchDown()") >= 0:
                limits = ax.get_switch_state()
                lo_s, up_s, end = limits.split(' ')
                conn.send(str(lo_s))

            elif data.find("getLimitDown()") >= 0:
                limits = ax.get_limits()
                try:
                    lo_s, up_s, end = limits.split(' ')
                    limitLo = float(lo_s)
                    limitUp = float(up_s)
                except:
                    limitLo = -1000
                    limitUp = 1000
                conn.send("%08.1f" % limitLo)

            elif data.find("getLimitUp()") >= 0:
                limits = ax.get_limits()
                try:
                    lo_s, up_s, end = limits.split(' ')
                    limitLo = float(lo_s)
                    limitUp = float(up_s)
                except:
                    limitLo = -1000
                    limitUp = 1000
                conn.send("%08.1f" % limitUp)

            elif data.find("resetFocuser()") >= 0:
                mylog.write('%s - received RESET CMD \n' % now_str())
                ax.reboot()
                limits = ax.get_limits()
                try:
                    lo_s, up_s, end = limits.split(' ')
                    limitLo = float(lo_s)
                    limitUp = float(up_s)
                except:
                    limitLo = -1000
                    limitUp = 1000

        else:
            pass
            reply = 'UNK_REQUEST: ' + data
            mylog.write('%s - received: %s \n' % (now_str(), reply))
            conn.send(reply)

    conn.close()


def initSerialPolluxConn():
    global ax
    ax = PolluxAxis(1, '/dev/ttyS0')


def updateTemp():
    global currtemp
    global myMinute
    global mylog

    newMinute = dtd.now().minute
    if ((newMinute != myMinute) and (newMinute % 1 == 0)):
        myMinute = newMinute
        try:
            mt = Meteo.Meteo(lg=None)
            temperature, pressure, humidity, w_dir, w_speed = mt.retrieve()
            currtemp = float(temperature)
        except:
            currtemp = -50.
            mylog.write('%s - no meteo data available \n' % now_str())


if __name__ == '__main__':

    global ax
    global s
    global currtemp
    global myMinute
    global mylog

    myMinute = None
    ax = None
    s = None
    currtemp = -50

    mylog = open("/var/log/irait_m2/log.txt", "a")

    mylog.write(
        "\n\n ******************************** \n\nStart TCP/serial server \n")
    mylog.write("Version v1.0 \n")
    mylog.flush()

    initTCPServer()
    initSerialPolluxConn()

    # now keep talking with the client
    updated = False
    counter = 1
    while 1:
        counter += 1
        if not updated:
            updateTemp()
            updated = True
        if counter % 300 == 0:
            updated = False
            counter = 1

        conn, addr = s.accept()
        mylog.write('%s - Connected with %s : %s\n' %
                    (now_str(), addr[0], str(addr[1])))
        mylog.flush()

        start_new_thread(clientthread, (conn, ax))

    s.close()
