#!/usr/local/bin/python2.7

import string
import commands
import time
import os
import math
import scipy
import matplotlib
matplotlib.use('agg')
import pylab
import rts2comm


from rts2comm import Rts2Comm
from time import gmtime, strftime

from rts2action import Rts2Action

##
filter='I'
telescope='T0'    
#rts2comm.DEVICE_TELESCOPE
focuser='F0'
#rts2comm.DEVICE_FOCUS

###
rac=Rts2Action(telescope, focuser, filter)
rc=Rts2Comm()

##
####Script per prendere immagini

######

rac.telsleep()
       
rc.setValue('filter', filter)
rc.setValue('SHUTTER', 'LIGHT')
          
rac.focusing()
time.sleep(2)

#####Esposizione di prova per verificare la messa a fuoco

x='10'

rc.setValue('exposure', x)
image=rc.exposure()
dataimg=strftime("%y%m%d_%H%M%S", gmtime())
renamed=rc.rename(image, '/images/af/%N/a1/a'+dataimg+'_optfr.fits')
try:
   rac.rec_ostel(renamed)
   rc.log('I', 'Autofocus: rec ostel filled')
except:
   rc.log('I', 'Autofocus: rec ostel NOT filled')

######
####
#Per la calibrazione del fuconco in funzione della temp
time.sleep(3)

