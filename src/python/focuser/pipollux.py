import serial
import time as tm


class PolluxAxis:
    TIMEOUT = 0.1
    LINE_TERMINATION = '\r\n'

    def __init__(self, axis_number, serialPort, baud=19200):
        self.connection = serial.Serial(
            serialPort, baudrate=baud, timeout=self.TIMEOUT)
        self.axis_number = axis_number
        self.send('AX setaxisno')
        self.lo_limit = None
        self.up_limit = None

    def __del__(self):
        self.connection.close()

    def send(self, command, rb=100):
        command = command.replace('AX', str(self.axis_number))
        command += PolluxAxis.LINE_TERMINATION
        self.connection.write(command)
        return self.connection.read(rb).replace(PolluxAxis.LINE_TERMINATION, '')

    def set_pitch(self, ratio):
        self.send('%f AX setpitch' % (ratio))

    def get_pitch(self):
        return self.send('AX getpitch')

    def get_limits(self):
        return self.send('AX getnlimit')

    def get_switch_state(self):
        return self.send('AX getswst')

    def reboot(self):
        return self.send('AX nreset')

    def get_up_limit(self):
        return self.up_limit

    def calibrate_range(self):
        # set velocity towards the limit-switch (mm/s)
        self.send('1.0 1 AX setncalvel')
        # set velocity out of the limit-switch (mm/s)
        self.send('2.0 2 AX setncalvel')
        # set velocity towards the limit-switch (mm/s)
        self.send('1.0 1 AX setnrmvel')
        # set velocity out of the limit-switch (mm/s)
        self.send('2.0 2 AX setnrmvel')
        self.send('AX ncal')
        self.wait_move()
        self.send('AX nrm')
        self.wait_move()
        limits = self.send('AX getnlimit')
        lo_s, up_s, end = limits.split(' ')
        self.lo_limit = float(lo_s)
        self.up_limit = float(up_s)

    def calibrate_low_limit(self):
        # set velocity towards the limit-switch (mm/s)
        self.send('1.0 1 AX setncalvel')
        # set velocity out of the limit-switch (mm/s)
        self.send('2.0 2 AX setncalvel')
        self.send('AX ncal')

    def calibrate_up_limit(self):
        # set velocity towards the limit-switch (mm/s)
        self.send('1.0 1 AX setnrmvel')
        # set velocity out of the limit-switch (mm/s)
        self.send('2.0 2 AX setnrmvel')
        self.send('AX nrm')

    def set_limits(self):
        self.send('0.0 155.0 AX setnlimit')

    def set_max_speed(self, mms):
        self.send('%f AX snv' % mms)

    def get_max_speed(self):
        return float(self.send('AX gnv'))

    def set_accel(self, mms2):
        self.send('%f AX sna' % mms2)

    def get_accel(self):
        return float(self.send('AX gna'))

    def current_position(self):
        s = self.send('AX np')
        if s.strip() == '':
            tm.sleep(0.1)
            return self.current_position()
        else:
            return float(s)

    def move_relative(self, by):
        self.send('%f AX nr' % (by))

    def move_absolute(self, to):
        self.send('%f AX nm' % (to))

    def is_stage_moving(self):
        return self.send('AX nst').strip() == '1'

    def wait_move(self, wait=1.):
        tm.sleep(wait)
        while self.is_stage_moving():
            tm.sleep(wait)

    def stop(self):
        self.send('\x03')

    def getSError(self):
        return int(self.send('AX gne'))

    def getHError(self):
        return int(self.send('AX gme'))
