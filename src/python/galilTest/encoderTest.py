import pyeib as eib
import datetime as dt
import time


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

myEib = eib.Eib("10.10.18.105")
mylog = open("encoderVal_%s.csv"%(now_str()),"wt+")

while True:
	timeNow = str(dt.datetime.now())
	pos = myEib.position()

	mylog.write("%s,%.14f,%.14f\n"%(timeNow,pos[0],pos[1]))
 	mylog.flush()
	time.sleep(0.05)
