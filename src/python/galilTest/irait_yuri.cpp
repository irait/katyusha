/* 
 *  Irait mount 
 *  IRAIT mount driver class for RTS2
 *
 *  File:    irait.cpp
 *  Project: IRAIT/ITM
 *  Version: see git log
 * 
 * Markus Wildi, Dome-C WO 2015, wildi.markus@bluewin.ch
 *
 * based on now named irait_device.cpp initially written by
 *
 *           D.Tavagnacco - <tavagnacco@oats.inaf.it> 
 *                          <daniele.tavagnacco@concordiastation.aq>
 *           JM.Christille- <jeanmarc.christille@gmail.com>
 *
 * 
 * Dependencies: see src/teld/Makefile.am
 *
 * Compile/link and installation see: 
 * http://10.10.18.16/mediawiki/index.php/RTS2_installation
 *
 * Copyright (C) 2003-2008 Petr Kubanek <petr@kubanek.net>
 * Copyright (C) 2011 Petr Kubanek, Institute of Physics <kubanek@fzu.cz>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <math.h>
#include <string>
#include <vector>
#include <iostream>
#include <cstdint>           
#include <mutex>
#include <memory>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
typedef int int_fast_32_t; 

#include "Galil.h"           // for motors
// libnova: 0 = South, 90 deg = West
// IRAIT:   0 = North, 90 deg = East
// AltAz data fed via rts2-mon are converted to libnova frame
// and back as necessary.
// Encoder steps are counted in "IRAIT AltAz frame"
#include "libnova/libnova.h" // for astronomy

#include "teld.h"
#include "configuration.h"   // get long lat of tel from rts2 configuration
#include "connection/tcp.h"  // for tcp encoders
#include "hoststring.h"  
#include "eib7.h"

static_assert(__cplusplus > 199711L, "Program requires C++11 capable compiler");

std::recursive_mutex distCurr_en_mutex;
static void* thread_point (void *arg);

// maintainability vs. recommended implementation as a language feature
// independent axis encoders (10.10.18.210)
#define MAX_ENC 536870912.0
#define OFFSET_VALUES {14286199.0 , 114212084.0};//{14286199.0, 114212084.0}
#define HALF_ENC (MAX_ENC / 2.)
#define ENC_2_DEG 360.0 / MAX_ENC
#define DEG_2_ENC MAX_ENC / 360.0
// motor encoder (Galil, 10.10.18.50)
#define MAX_MOT 55296000.0 // Rid=1440 * RidMot=3 * AsseRis=12800
#define ENC_2_MOT MAX_MOT / MAX_ENC
#define MOT_2_ENC MAX_ENC / MAX_MOT
// mount 
#define MIN_ALT  3. // mount minimum altitude
#define MAX_ALT 83. // maximum
// distinguish exit code of HeidenhainEncoder::position() ONLY (no implementatin for TiltmeterEncoder::position()
#define OUT_OF_BOUNDS -1
#define READ_ERROR -2

// pointing accuracy: X * 20arcsec (x * 0.0055 deg)
#define TWENTY_ARCSEC_TIMES 2. 
#define FUDDEL_FACTOR 2. // factor where coordinates are considered as a run over target

class GalilMotor
{
  // note: Alt and Az motor are treated identical!
public : // do not change these values without talking with Jean-Marc!
  double fast =  200000.; // wildi: at the skype conference: 200000 is the absolute maximum for the moment
  double slow = 10000.;
  double spd_enc = NAN;   // axis current speed (encoder-step)

  int connect(HostString* _addrGalil);
  int motorOn(); //clumsy
  int motorOff();
  int motorStop();
  int motorSpeed(double speed);
  int motorAccel();
  int motorStep(double step);
  int motorTurning();
  // unused: TRYIPOS
  enum class motor_t { ON, OFF, STOP, SPEED, ACCEL, TRYIPOS, IPOS,TURNING };

  GalilMotor(const std::string& a_nameGalil, const std::string& a_nameHuman, const double& a_inverted):nameGalil(a_nameGalil), nameHuman(a_nameHuman), inverted(a_inverted){};

private:
  const std::string nameGalil; // axis name in the galil controller
  const std::string nameHuman; // physical axis name
  const double inverted;       // sign of the motor inversion (1 = NO-INVERTED, -1 = INVERTED)
  HostString* _addrGalil; // address of galil controller (IP or serial port)
  std::unique_ptr<Galil> _connGalil; // connection to galil interface

  double speedMax=fast;  // maximum speed sustainable in MOTOR COUNTS
  double speedMin=0;     // minimum speed allowed in MOTOR COUNTS
  double accelMin=1000;  // minimum acceleration in MOTOR COUNTS
  double accelMax=99328; // maximum sustainable acceleration in MOTOR COUNTS
  void range();
  int motor(const motor_t& status, const double& value);
};

int GalilMotor::connect(HostString* _addr)
{
  _addrGalil = _addr;
  try
    {
      _connGalil.reset(new Galil(_addrGalil->getHostname()));
    }
  catch (...)
    {
      logStream(MESSAGE_ERROR) << "Mount motors: " << _addrGalil->getHostname() << " FAILED." << sendLog;
      return -1;
    }
  motorSpeed(speedMax);
  motorAccel();

  if(motorOn() == 0)
    logStream(MESSAGE_INFO) << nameHuman << " motor switched on" << sendLog;
  else
    {
      logStream(MESSAGE_INFO) << nameHuman << " motor failed to switch on" << sendLog;
      return -1;
    }
  return 0;
}
  double step_enc;

// Range the current motor speed inside the mount limits
void GalilMotor::range()
{
  if( isnan(spd_enc))
      return;

  if(fabs(spd_enc) > (speedMax * MOT_2_ENC))
      spd_enc = speedMax * MOT_2_ENC;

  if(fabs(spd_enc) < (speedMin * MOT_2_ENC))
      spd_enc = speedMin * MOT_2_ENC;
}
int GalilMotor::motorOn(){  return motor(motor_t::ON,0);}
int GalilMotor::motorOff(){  return motor(motor_t::OFF,0);}
int GalilMotor::motorStop(){  return motor(motor_t::STOP,0);}
int GalilMotor::motorSpeed(double speed)
{
  spd_enc = speed;
  range();
  return motor(motor_t::SPEED,spd_enc); // lazy 
}
int GalilMotor::motorAccel(){  return motor(motor_t::ACCEL,accelMax-1);} // lazy
int GalilMotor::motorStep(double step){  return motor(motor_t::IPOS,step);}
int GalilMotor::motorTurning(){  return motor(motor_t::TURNING,0);}

// see manual: /home/wildi/manuals/com_2xx0.pdf
int GalilMotor::motor(const motor_t& action, const double& value)
{
  try
    {
      std::stringstream tmpCommand;
      tmpCommand.precision(20);
      switch(action)
	{
	case motor_t::ON :
	  // _BGn contains a ‘0’ if motion complete on the specified axis or coordinate system, otherwise contains a ‘1’.
	  if(_connGalil->commandValue("MG _BG" + nameGalil) == 0)
	    tmpCommand << "SH" << nameGalil; //SH: servo here, is a kind of reset, what position concerns
	  break;
	case motor_t::OFF :
	  if(_connGalil->commandValue("MG _BG" + nameGalil) == 0)
	    tmpCommand << "MO" << nameGalil; // MO: motor off
	  else
	    logStream(MESSAGE_DEBUG) << nameHuman << "GalilMotor::motor: motor running: can not turn motor off." << sendLog;
	  break;
	case motor_t::STOP : // ST: Stop motion, AM: After motion complete
	  tmpCommand << "ST" << nameGalil << ";AM" << nameGalil;
	  break;
	case motor_t::SPEED : // SP: This command sets the slew speed of any or all axes for independent moves. 
	                      // Negative values will be interpreted as the absolute value.
	  if((fabs(value) > speedMax*MOT_2_ENC) || (fabs(value) < speedMin*MOT_2_ENC))
	    logStream(MESSAGE_WARNING) << nameHuman << "GalilMotor::moto: speed out of limits: can not set speed." << sendLog;
	  else
	    tmpCommand << "SP" << nameGalil << "=" << fabs(value) * ENC_2_MOT;
	  break;
	case motor_t::ACCEL :
	  if((fabs(value) > accelMax*MOT_2_ENC) || (fabs(value) < accelMin*MOT_2_ENC))
	    logStream(MESSAGE_WARNING) << nameHuman << "GalilMotor::motor: can't AC/DC, accel out of limits: " << value << sendLog;
	  else // AC: acceleration, DC: deceleration
	    tmpCommand <<  "AC" << nameGalil << "=" << fabs(value) * ENC_2_MOT
		       << ";DC" << nameGalil << "=" << fabs(value) * ENC_2_MOT;
	  break;
	case motor_t::TRYIPOS : // Not used, set new relative (IP) position IF MOTOR NOT MOVING
	  if(_connGalil->commandValue("MG _BG" + nameGalil) == 1)
	    {
	      logStream(MESSAGE_DEBUG)  << nameHuman << "GalilMotor::motor motor is moving: won't IP." << sendLog;
	      break;
	    }
	case motor_t::IPOS : // increment position (IP)  in the same moving direction (careful here), wildi indeed!
	  // manual COMMAND REFERENCE Manual Rev. 1.0m1, DMC-2xxx, p. 93/113
	  //Case 1: Motor is standing still
	  //An IP a,b,c,d command is equivalent to a PR (=position relative) a,b,c,d and BG command. The motor will
	  //move to the specified position at the requested slew speed and acceleration.
	  //Case 2: Motor is moving towards a position as specified by PR, PA, or IP.
	  //An IP command will cause the motor to move to a new position target, which is the old
	  //target plus the specified increment. The incremental position must be in the same direction
	  //as the existing motion.
	  //Case 3: Motor is in the Jog Mode
	  //An IP command will cause the motor to instantly try to servo to a position which is the
	  //current instantaneous position plus the specified increment position. The SP and AC
	  //parameters have no effect. This command is useful when synchronizing 2 axes in which
	  //one of the axis' speed is indeterminate due to a variable diameter pulley.
	  //Warning: When the mode is in jog mode, an IP will create an instantaneous position error.

	  tmpCommand << "IP" << nameGalil << "=" << (inverted * value * ENC_2_MOT);
	  break;
	case motor_t::TURNING :

	  if(_connGalil->commandValue("MG _BG" + nameGalil) == 1)
	    return 0;
	  else
	    return -1;
	}
      if(!tmpCommand.str().empty())
	std::string response(_connGalil->command(tmpCommand.str()));
    }
  catch (std::string err)
    {
      logStream(MESSAGE_ERROR) << "GalilMotor::motor: Failed to command motor: " << nameHuman << " error: " << err << sendLog;
      return 1;
    }
  catch (...)
    {
      logStream(MESSAGE_ERROR) << "GalilMotor::motor: Failed to command motor: " << nameHuman << ", unknown error" << sendLog;
      return 1;
    } 
  return 0;  
}

class Lock {
public:
  Lock(std::recursive_mutex& m): m_(m){m_.lock(); }
  Lock(const Lock& l) : m_(l.m_){m_.lock();}
  Lock(const Lock&& l) : m_(l.m_){m_.lock();}
  Lock& operator=(const Lock&) = delete;
  ~Lock(){m_.unlock();}
private:
  std::recursive_mutex& m_;
};


class Encoder
{
public :
  typedef enum { ALT=0, AZ=1 } axis_t;
  const double& steps(axis_t ax);
  void steps(axis_t ax, const double& val);

  std::vector<double> pos_deg = {NAN,NAN};    // mount position [deg]
  Encoder(HostString* hs) : _addrEncoders(hs){};
  virtual int testConnection() {return 0;};
  virtual int position() {return 0;};
protected:
  HostString* _addrEncoders;        // IP address of encoder device  
  std::vector<double> OFFSET_ENC = OFFSET_VALUES;

  template<class T, class U> T conversion(const U& inValue);
  void range();
private:
    std::recursive_mutex enc_mtx;
    std::vector<double> enc_steps = {NAN,NAN};    // mount position [encoder-step]
protected:

    Lock lock(){return Lock(enc_mtx);}
};

const double& Encoder::steps(axis_t ax)
{
    Lock l(enc_mtx);
    return enc_steps[ax];
}

void Encoder::steps(axis_t ax,const double& val)
{
    Lock l(enc_mtx);
    enc_steps[ax] = val;
}


void Encoder::range()
{ 
    // wildi: hm, if these encoders are absolute, this should not be necessary/possible!
    for (auto ax : {ALT,AZ}) // done for both axis knowing Alt is limited [3.,90.]
    {
        double pos_enc = steps(ax);
        if(pos_enc < 0)
            steps(ax,pos_enc + MAX_ENC);
        else if(pos_enc > MAX_ENC)
            steps(ax,pos_enc - MAX_ENC);
    }
    pos_deg = {steps(ALT) * ENC_2_DEG, steps(AZ) * ENC_2_DEG};
}
template<class T, class U> T Encoder::conversion (const U& inValue)
{
    T result;
    std::stringstream tmpConversionString;

    tmpConversionString << inValue;
    tmpConversionString >> result;

    return result;
}

class TiltmeterEncoder:public Encoder
{
public:
  TiltmeterEncoder(HostString* hs, rts2core::Block* m) : Encoder(hs),_master(m) {};

  int testConnection();
  int position();

private:
  rts2core::Block* _master;
  std::unique_ptr<rts2core::ConnTCP> _connEncoders; // connection to mount encoders

};
int TiltmeterEncoder::testConnection()
{
  try
    {
      _connEncoders.reset(new rts2core::ConnTCP(_master, _addrEncoders->getHostname(),_addrEncoders->getPort()));
      _connEncoders->setDebug(true);
      _connEncoders->init(); 
      //_connEncoders->connectionClose();  //FIXME: why?!!?
    }
  catch (...)
    {
      logStream(MESSAGE_ERROR) << "Mount encoder FAILED: " << _addrEncoders->getHostname() << sendLog;
      return -1;
    } 	  
  return 0 ;
}

int TiltmeterEncoder::position()
{
  Lock l = lock();// forbid the other thread to read the steps while they are set to NAN
  steps(ALT,NAN);// a bit dangerous
  steps(AZ,NAN); // a bit dangerous
  try
    {
      _connEncoders.reset(new rts2core::ConnTCP(_master, _addrEncoders->getHostname(),_addrEncoders->getPort()));
      _connEncoders->setDebug(false);
      _connEncoders->init(); 
      _connEncoders->sendData("getpos\r\n"); 
      // -- get X (AZ) position by reading 12 ascii char -- //
      std::string outputx(12,'\0');  // fill space to avoid memory leak
      _connEncoders->receiveData(&outputx[0], 12, 2, false); // (data, len, wtime, binary)
      steps(AZ,(conversion<double,std::string>(outputx.c_str()) - OFFSET_ENC[AZ]));
      // -- get Y (ALT) position by reading 12 next ascii char -- //
	std::string outputy(12,'\0');  // fill space to avoid memory leak
      _connEncoders->receiveData(&outputy[0], 12, 2, false); // (data, len, wtime, binary)
      steps(ALT,(conversion<double,std::string>(outputy.c_str()) - OFFSET_ENC[ALT]));
      //_connEncoders->connectionClose(); //FIXME: WHY?!?!?!
      range();
      
      if((steps(ALT)* ENC_2_DEG < MIN_ALT) || (steps(ALT)* ENC_2_DEG > MAX_ALT))
	{
          logStream(MESSAGE_ERROR) << "TiltmeterEncoder::position: altitude encoder reading out of bounds ]" << MIN_ALT << ","<< MAX_ALT <<"[:, altitude:  "<< steps(ALT) * ENC_2_DEG  << sendLog;
          logStream(MESSAGE_ERROR) << "TiltmeterEncoder::position: treated as error" << sendLog;
	  // NO! the reading is valid steps(ALT,NAN);  steps(AZ,NAN);
	  return -1;
	}
    }
  catch(std::string err)
    {
      logStream(MESSAGE_ERROR) << "TiltmeterEncoder::position<<<<<<<<<XXXX>>>>>>> Error: " << err << sendLog;
      return -1;
    }
  catch (...)
    {
      logStream(MESSAGE_ERROR) << "TiltmeterEncoder::position: get mount position FAILED" << sendLog;
      return -1;
    } 
  return 0;
}

class HeidenhainEncoder:public Encoder
{
public: 
  HeidenhainEncoder(HostString* hs) : Encoder(hs) {};
  int testConnection();
  int position();

private:
    bool _eib741 = false ;
#define EIB_TCP_TIMEOUT       5000   // timeout for TCP connection in ms 
#define NUM_OF_AXIS           4      // number of axes of the EIB        
    EIB7_HANDLE eib;                 // EIB handle               
    unsigned long ip;                // IP address of EIB        
    unsigned long num;               // number of encoder axes   
    EIB7_AXIS eibAxis[NUM_OF_AXIS];  // axes array               
    char fw_version[20];             // firmware version string  
    int CheckEibError(EIB7_ERR error);  
};
int HeidenhainEncoder::CheckEibError(EIB7_ERR error)
 {
   if(error != EIB7_NoError)
     {
       char mnemonic[32];
       char eibMessage[256];

       EIB7GetErrorInfo(error, mnemonic, 32, eibMessage, 256);
       //fprintf(stderr, "\nError %08X (%s): %s\n", error, mnemonic, eibMessage);
       logStream(MESSAGE_ERROR) << " HeidenhainEncoder::CheckEibError, error:  " << error << ", mnemonic: " << mnemonic << ", eibMessage: " << eibMessage << sendLog;
       return -1;
     }
   return 0 ;
 }
int HeidenhainEncoder::testConnection()
{
  logStream(MESSAGE_ERROR) << " HeidenhainEncoder::";
  // open connection to EIB 
  if(CheckEibError(EIB7GetHostIP(_addrEncoders->getHostname(), &ip)))
    return -1;
  // open connection to EIB 
  if(CheckEibError(EIB7Open(ip, &eib, EIB_TCP_TIMEOUT, fw_version, sizeof(fw_version))))
    return -1;
  // get axes array 
  if(CheckEibError(EIB7GetAxis(eib, eibAxis, NUM_OF_AXIS, &num)))
    return -1;
  // initialize axis 
  if(CheckEibError(EIB7InitAxis(eibAxis[2],
                 /* with EnDat 2.2 we enable the EnDat propagation time compensation */
                 EIB7_IT_EnDat22 | EIB7_IT_EnDatDelayMeasurement,
                 EIB7_EC_Rotary,
                 EIB7_RM_None,
                 0,                    /* not used for EnDat */
                 0,                    /* not used for EnDat */
                 EIB7_HS_None,
                 EIB7_LS_None,         
                 EIB7_CS_None,         /* not used for EnDat */
                 EIB7_BW_High,         /* not used for EnDat */
                 EIB7_CLK_Default,     /* we use the default clock */
                 EIB7_RT_Long,         /* long EnDat recovery time I */
                 EIB7_CT_Long          /* encoder with long calculation timeout */
                 )))
    {
      return -1;
    }
    // initialize axis
    if(CheckEibError(EIB7InitAxis(eibAxis[3],
                 /* with EnDat 2.2 we enable the EnDat propagation time compensation */
                 EIB7_IT_EnDat22 | EIB7_IT_EnDatDelayMeasurement,
                 EIB7_EC_Rotary,
                 EIB7_RM_None,
                 0,                    /* not used for EnDat */
                 0,                    /* not used for EnDat */
                 EIB7_HS_None,
                 EIB7_LS_None,
                 EIB7_CS_None,         /* not used for EnDat */
                 EIB7_BW_High,         /* not used for EnDat */
                 EIB7_CLK_Default,     /* we use the default clock */
                 EIB7_RT_Long,         /* long EnDat recovery time I */
                 EIB7_CT_Long          /* encoder with long calculation timeout */
                 )))
    {
      return -1;
    }
  return 0;
}

int HeidenhainEncoder::position()
{
    unsigned short status;
    ENCODER_POSITION pos;

    Lock l = lock();// forbid the other thread to read the steps while they are set to NAN
    steps(ALT,NAN);// a bit dangerous
    steps(AZ,NAN); // a bit dangerous

    // read position from EIB
    if(CheckEibError(EIB7GetPosition(eibAxis[2], &status, &pos)))
        return READ_ERROR;
    steps(AZ,pos - OFFSET_ENC[AZ]);

    if( CheckEibError(EIB7GetPosition(eibAxis[3], &status, &pos)))
        return READ_ERROR;
    steps(ALT,pos - OFFSET_ENC[ALT]);

    range();

    if((steps(ALT)* ENC_2_DEG < MIN_ALT) || (steps(ALT)* ENC_2_DEG > MAX_ALT))
    {
        logStream(MESSAGE_ERROR) << "HeidenhainEncoder::position: altitude encoder reading out of bounds ]" << MIN_ALT << ","<< MAX_ALT <<"[:, altitude:  "<< steps(ALT) * ENC_2_DEG  << sendLog;
        logStream(MESSAGE_ERROR) << "HeidenhainEncoder::position: treated as error" << sendLog;
	// NO! the reading is valid steps(ALT,NAN);  steps(AZ,NAN);
        return OUT_OF_BOUNDS;
    }
    return 0;
}
namespace rts2teld
{
  class Irait:public Telescope
  {
  public:
    // 1., minimum refresh time for pointing 0.3s
    // reason: the motors must complete movement and stop.
    // This is not mandatory, but the Galil increment command
    // does only work, if the direction is the same as the 
    // current direction the motor turns.
    static constexpr double  REFRESH_TIME = .4;

    //yuri add
    static constexpr double ONE_SEC_IN_JD = .0000116;


    Irait(int argc, char **argv);
    ~Irait();
    // ToDo wildi not yet understood
    // virtual int setTracking (int track, bool addTrackingTimer = false, bool send = true);
    // point is called via thread thread_point
    int pointMnt();
    pthread_t pointid; 
    // http://stackoverflow.com/questions/8482314/segmentation-fault-caused-by-pthread-kill
    // answer 1
    bool pointRun; // false: thread thread_point stops
  protected:
    /* sartor: override this function gets avoids double conversion of cordinates (ALT-AZ -> EQ -> ALT-AZ) in updateDisplay function
       which caused errors i.e. phantom telecope movements
   */
    virtual void getTelAltAz (struct ln_hrz_posn *hrz) override{
        hrz->alt = _mntPosAltAz_deg->getRa();
        hrz->az =  ln_range_degrees(_mntPosAltAz_deg->getDec() +180.0);
    }

  private:
    std::vector<double> MNT_MAX_ENC = {MAX_ALT * DEG_2_ENC, 0.0};
    std::vector<double> MNT_MIN_ENC = {MIN_ALT * DEG_2_ENC, 0.0};
    std::vector<double> POINT_ACCURACY_ENC = {TWENTY_ARCSEC_TIMES * 0.0055*DEG_2_ENC, TWENTY_ARCSEC_TIMES * 0.0055*DEG_2_ENC} ; 
    // may be not so a good idea
    //static constexpr double UPPER_THRESHOLD_RESULT = 1. * DEG_2_ENC; // limit to accept tiltmeter_enc errors
    // limiting circle
    double pointing_limit = FUDDEL_FACTOR * sqrt(pow(POINT_ACCURACY_ENC[ALT],2)+pow(POINT_ACCURACY_ENC[AZ],2));

    typedef enum { ALT=0, AZ=1 } axis_t;
    HostString* _addrGalil;           // address of galil controller (IP or serial port)
    HostString* _addrEncoders;        // IP address of tiltmeter_enc  
    rts2core::ValueSelection* _actionMnt;   // action to perform with mount    
    rts2core::ValueRaDec* _tarRealPos_deg;   // real position of target
    rts2core::ValueString* _mntStatus_str;   // mount status in string
    std::vector<std::string> __mntStatus_name;
    rts2core::ValueBool *exit_on_danger;// true: if Alt is out of bounds, stopMnt() followed by exit
    rts2core::ValueBool *apply_pointing_model;// true: corrections from the pointing model are applied
    rts2core::ValueBool *ignore_motor_failure;// true: motor command failure is not reported as HW ERROR to RTS2
    rts2core::ValueBool *ignore_run_over;// true: target run overs while MOVING are ignored
    rts2core::ValueBool *point_after_move;   // if the IR source Sauron is targeted
                                             // it is necessary, that the mount remains at this place
    rts2core::ValueRaDec*  _mntPosAltAz_deg; // mount position AltAz
    rts2core::ValueRaDec*  _mntSpeed_deg;    // mount current speed AltAz
    rts2core::ValueRaDec*  _mntPosRaDec_deg; // mount position RaDec
    rts2core::ValueRaDec* _distCurr_deg;     // mount-target current distance AltAz
    rts2core::ValueRaDec* _pmDelta_deg;      // pointing model corrections
    rts2core::ValueString* dummyLine;

    std::vector<GalilMotor*> __iraitMotors;
    GalilMotor* motorAlt ;
    GalilMotor* motorAz ;

    bool _tiltmeter_enc = false; //ugly
#define TRIALS 5
    int tiltmeter_reach_cnt=TRIALS;  //ugly, after count down the driver exits, -x true is set
                                 // may be not used after Heidenhain EIB 741 is in use
    bool _eib741 = false ; //ugly
    Encoder *encdr;

    ln_equ_posn   __tarEquPos_deg;       // target EQU position for libnova calculations
    ln_lnlat_posn __mntLatLonPos_deg;    // mount position for libnova calculations
    std::vector<double> __tarRealPos_enc;// target position real, from libnova (encoder-step)
    std::vector<double> __tarLastPos_enc;// 
    std::vector<double> __pmDelta_enc;   // pointing model delta 

    std::vector<double> __distCurr_enc;  // mount-target relative distance actual (encoder-step)
    std::vector<double> __distLast_enc;  // mount-target relative, last computation (encoder-step)
    std::vector<double> __distInit_enc;  // mount-target relative, at the beginning of the motion (encoder-step)
    std::vector<double> lastResult ; // last correction while pointing, the tiltmeter_enc device makes errors

    enum class mnt_t { FAILED=0, STOPPED, MOVING, POINTING};
    mnt_t __mntStatus;

    bool __altDirectionNotChanged = true;
    bool __altIsGrowing = false;

    virtual int init() override;
    virtual int processOption(int in_op) override;
    virtual void valueChanged(rts2core::Value* changed_value) override;
    virtual int idle() override;
    virtual int initValues ();
    //virtual int info (){ return Telescope::info ();}
    virtual int startResync ();
    virtual int stopMove ();
    virtual int startPark (){ return 0;}
    virtual int endPark (){ return 0; }
    virtual int isMoving ();
    virtual int isParking () { return isMoving ();}
    virtual int commandAuthorized (rts2core::Connection * conn);

    bool targetOutOfLimits(const int& axis);
    bool runOver();
    int calculateDistance();
    void mntAction(const int& action);
    void stopMnt(bool stopPoint);
    int setRTS2MntPosition();
    void calculatePointingModel(struct ln_hrz_posn *currHrzPos_deg);

    int setIRAITTargetPosition();
    void targetReached();
    int turnAxis();
    void updateDisplay();
    void setMntStatus(const mnt_t& status); 
  };
}

using namespace rts2teld;
//bool diffTrack, bool hasTracking, bool hasUnTelCoordinates
Irait::Irait (int argc, char **argv):Telescope(argc,argv,false,false,false)
{
  addOption ('g', "galil-address", 1, "motors galil controller IP address");
  addOption ('e', "encoder-address", 1, "encoders IP:port address");
  addOption ('n', "heidenhain", 1, "Heidenhain EIB 741 IP address");
  addOption ('x', "exit-on-danger", 1, "true: exit, false: do not exit driver if the mount is in danger");
  addOption ('y', "ignore-motor-failure", 1, "true: ignore failure of motor command, false: report HW ERROR to RTS2");
  addOption ('z', "ignore-run-over", 1, "true: ignore target run over");
  addOption ('a', "apply-pointing-model", 1, "true: apply pointing model corrections");
}

Irait::~Irait() {}

int Irait::init()
{
  motorAlt = new GalilMotor("B","Altitude", 1); // motors: galilName, HumanName, inverted
  motorAz = new GalilMotor("A","Azimuth" , -1); //inverted motion

  __iraitMotors = {motorAlt, motorAz};
 
  // get current configuration (rts2.ini)
  rts2core::Configuration *config = rts2core::Configuration::instance();
  config->loadFile();

  // store long-lat position of mount for libnova
  __mntLatLonPos_deg.lng = config->getObserver()->lng;
  __mntLatLonPos_deg.lat = config->getObserver()->lat;
  telLatitude->setValueDouble (config->getObserver ()->lat);
  telLongitude->setValueDouble (config->getObserver ()->lng);
  telAltitude->setValueDouble (config->getObservatoryAltitude ());
  //strcpy (telType, "Irait"); //FIXME what happened to this variable?

  createValue(dummyLine, "-------------------", "", false);
  dummyLine->setValueString("-----------------------------");

  createValue(exit_on_danger, "Exit","true: exit if Alt is out of bounds or encoders could not be read",false,RTS2_VALUE_WRITABLE);
  exit_on_danger->setValueBool(true);

  createValue(apply_pointing_model, "Apply_pointing_model","true: apply pointing model correction",false,RTS2_VALUE_WRITABLE);
  apply_pointing_model->setValueBool(false);

  createValue(ignore_motor_failure, "Ignore_motor_fails","true: ignore failure of motor command, false: report HW ERROR to RTS2",false,RTS2_VALUE_WRITABLE);
  ignore_motor_failure->setValueBool(true);

  createValue(ignore_run_over, "Ignore_run_over","true: ignore target run over",false,RTS2_VALUE_WRITABLE);
  ignore_run_over->setValueBool(true);

  createValue(_actionMnt, "Mount", "action to perform with mount",false, RTS2_VALUE_WRITABLE, 0);
  _actionMnt->addSelVal("Stand-by");  // 0, dummy but necessary because rts2-mon needs a changed value
  _actionMnt->addSelVal("STOP");      // 1
  
  createValue(_mntStatus_str, "status","Status of the mount",false);
  createValue(point_after_move, "point","point after target reached",false,RTS2_VALUE_WRITABLE);
  point_after_move->setValueBool(true);
  createValue(_mntSpeed_deg,  "speed","Alt/Az set speed of the mount, not the actual", false, RTS2_DT_DEGREES);
  createValue(_mntPosAltAz_deg,    "position.AA","Alt/Az mount position (N=0,E=90)",false,RTS2_DT_DEGREES);
  createValue(_mntPosRaDec_deg,    "position.EQ","RA/Dec mount position",false,RTS2_DT_DEGREES);
  createValue(_tarRealPos_deg,"target.AA","Alt/Az target position",false,RTS2_DT_DEGREES);
  createValue(_distCurr_deg,  "distance",  "Alt/Az distance target-mount",false,RTS2_DT_DEGREES);
  createValue(_pmDelta_deg,  "pm_delta",  "Alt/Az correction pointing model",false,RTS2_DT_DEGREES);

  __mntStatus_name = { "FAILED", "STOPPED", "MOVING", "POINTING"};

  pointRun=false;
  // log messages where to????
  if(_tiltmeter_enc)
    {
      lastResult = {NAN,NAN};
      logStream(MESSAGE_WARNING) << "Irait::init: tiltmeter encoder correction enabled" << sendLog;
    }

  __tarRealPos_enc = {NAN, NAN};  
  __tarLastPos_enc = {NAN,NAN}; 
  __distCurr_enc   = {NAN,NAN};
  __distLast_enc   = {NAN,NAN};
  __distInit_enc   = {NAN,NAN};
  __pmDelta_enc    = {NAN,NAN};
  return Telescope::init();
};

int Irait::processOption (int in_opt)
{
  switch (in_opt)
    {
    case 'g':
      _addrGalil = new HostString(optarg,"0000"); // Galil address: no port needed
      break;
    case 'e':
      _addrEncoders = new HostString(optarg,"3490"); // Encoders  controller address
      _tiltmeter_enc = true; // 10.10.18.210-214 (see box 0 pc, dhcp server for actual address)
      break;
    case 'n':
      _addrEncoders = new HostString(optarg,"0000");
      _eib741 = true ;
      break;
    case 'x':
#include <sstream> // do not forget me
      bool b;
      std::istringstream(optarg) >> std::boolalpha >> b;	
      exit_on_danger->setValueBool(b);
      break;
    case 'a':
      bool a; 
      std::istringstream(optarg) >> std::boolalpha >> a;
      apply_pointing_model->setValueBool(a);
      break;
    case 'y':
      bool c; // for the sake of symmetry
      std::istringstream(optarg) >> std::boolalpha >> c;	
      ignore_motor_failure->setValueBool(c);
      break;
    case 'z':
      bool d; // for the sake of symmetry
      std::istringstream(optarg) >> std::boolalpha >> d;	
      ignore_motor_failure->setValueBool(d);
      break;
    default:
      return Telescope::processOption (in_opt);
    }
  return 0;
}

int Irait::initValues ()
{

  logStream(MESSAGE_INFO) << "Connecting mount." << sendLog;

  if(( _tiltmeter_enc && _eib741) || (!_tiltmeter_enc && !_eib741)) //ugly
    {
      logStream(MESSAGE_ERROR) << "Connecting mount: specify either -e IP (tiltmeter_enc) or -n IP (Heidenhain EIB 741), exiting" << sendLog;
      exit(1);
    }
  if(_tiltmeter_enc)
    encdr = new TiltmeterEncoder(_addrEncoders, this); 
  else if(_eib741)
    encdr = new HeidenhainEncoder(_addrEncoders);

  if(encdr->testConnection())
    return -1;
  logStream(MESSAGE_DEBUG) << "Encoders connected" << sendLog;

  if(motorAz->connect(_addrGalil) || motorAlt->connect(_addrGalil))
    {
      setMntStatus(mnt_t::FAILED);
      return -1;
    }
  logStream(MESSAGE_DEBUG) << "Motors connected" << sendLog;

  stopMnt(false);
  if(__mntStatus == mnt_t::FAILED)
    return -1;

  if(setRTS2MntPosition())
    return -1;
  // use mount position as AltAz sky position 
  ln_hrz_posn mntAltAzPos;                   
  mntAltAzPos.alt = encdr->pos_deg[ALT];
  // libnova 0 = South, IRAIT 0 = North
  mntAltAzPos.az = encdr->pos_deg[AZ] + 180.; 
  ln_get_equ_from_hrz(&mntAltAzPos, &__mntLatLonPos_deg,ln_get_julian_from_sys(),&__tarEquPos_deg);
  // strictly spoken this is wrong: this is the mount EQU system and not J2000
  // do that only in this method (oriRaDec is calculated by base class)
  // Do not call setOrigin() it is done in the base class
  // This value appears as "ORI" in rts2-mon 
  // setTelRa, setTelDec set telRaDec.  This value appears as "TEL" in rts2-mon
  setTelRa(__tarEquPos_deg.ra);
  setTelDec(__tarEquPos_deg.dec);

  updateDisplay();
  _actionMnt->setValueInteger(0); // "Stand-by"
  sendValueAll(_actionMnt);

  return Telescope::initValues ();
}

int Irait::idle()
{
  updateDisplay();
  return Telescope::idle();
}

void Irait::updateDisplay()
{
  int ret=encdr->position();
  // since we have no hardware end switches, insist on a successful read
  // or the mount must be stopped
  // NO loop here since updateDisplay() is called by idle()
  // do not delay the execution too long
  // These lines of codes may be dropped, once the Heidenhain EIB 741 is in place
  if(_tiltmeter_enc)
    {
      if(!ret)
	tiltmeter_reach_cnt=TRIALS; //after one success reset counter
      else
	{
	  tiltmeter_reach_cnt--;
	  if(tiltmeter_reach_cnt==0) // but bail out here if requested
	    {
	      logStream (MESSAGE_WARNING) << "Irait::updateDisplay: error received from encdr->position(), counter: " << tiltmeter_reach_cnt << "/" << TRIALS << sendLog;
	      if(exit_on_danger->getValueBool())
		{
		  stopMnt(true);
		  logStream (MESSAGE_WARNING) << "Irait::updateDisplay: motor stopped, EXITING, after: " << TRIALS <<" unsuccessful encoder read trials" << sendLog;
		  exit(1);
		}
	    }
	  else
	    {
	      logStream (MESSAGE_WARNING) << "Irait::updateDisplay: IGNORING error received from encdr->position(), counter: " << tiltmeter_reach_cnt << "/" << TRIALS << sendLog;
	    }
	}
    }
  // end Heidenhain drop 
  ln_hrz_posn altAzPos;
  if(ret==READ_ERROR) { // 2015-11-30, do not yet know what really to do
    logStream (MESSAGE_WARNING) << "Irait::updateDisplay: READ_ERROR received from encdr->position()" << sendLog;
    logStream (MESSAGE_WARNING) << "Irait::updateDisplay: do not update displayed values, returning" << sendLog;
    return;
  }
  // case ret==OUT_OF_BOUNDS is handled here too
  // the displayed mount position is in IRAIT frame, no + 180. necessary
  _mntPosAltAz_deg->setValueRaDec(encdr->pos_deg[ALT], encdr->pos_deg[AZ]);
  sendValueAll(_mntPosAltAz_deg);
  altAzPos.alt= encdr->pos_deg[ALT];
  // transformation to EQU frame is done in libnova frame, +180.
  altAzPos.az = encdr->pos_deg[AZ] + 180.;
  // since we have no hardware end switches at Alt=3 deg and Alt=85 deg 
  // may be the motors can be stopped here
  // this not a replacement for those switches and it may fail for the same reason as the earlier stop commend
  if(altAzPos.alt < (MNT_MIN_ENC[ALT] * ENC_2_DEG))
    {
      logStream (MESSAGE_ERROR) << "Irait::updateDisplay: altitude:" << altAzPos.alt << " is lower than the limit: " << MNT_MIN_ENC[ALT] * ENC_2_DEG << sendLog;
      if(exit_on_danger->getValueBool())
	{
	  logStream (MESSAGE_INFO) << "Irait::updateDisplay: motor stopping" << sendLog;
	  stopMnt(true); // it returns only if the motors are stopped
	  logStream (MESSAGE_INFO) << "Irait::updateDisplay: motor stopped, EXITING" << sendLog;
	  exit(1);
	}
      else
	logStream (MESSAGE_WARNING) << "Irait::updateDisplay: motors NOT stopped and not EXITING (exit_on_danger==false)" << sendLog;
    }
  if(altAzPos.alt > (MNT_MAX_ENC[ALT] * ENC_2_DEG))
    {
      logStream (MESSAGE_ERROR) << "Irait::updateDisplay: altitude:" << altAzPos.alt << " is above the limit: " << MNT_MAX_ENC[ALT] * ENC_2_DEG << sendLog;
      if(exit_on_danger->getValueBool())
	{
	  logStream (MESSAGE_INFO) << "Irait::updateDisplay: motor stopping" << sendLog;
	  stopMnt(true); // it returns only if the motors are stopped
	  logStream (MESSAGE_INFO) << "Irait::updateDisplay: motor stopped, EXITING" << sendLog;
	  exit(1);
	}
      else
	logStream (MESSAGE_WARNING) << "Irait::updateDisplay: motors NOT stopped and not EXITING (exit_on_danger==false)" << sendLog;
    }

  ln_equ_posn raDecPos;
  ln_get_equ_from_hrz(&altAzPos,&__mntLatLonPos_deg,ln_get_julian_from_sys(),&raDecPos); 
  
  _mntPosRaDec_deg->setValueRaDec(raDecPos.ra,raDecPos.dec);
  sendValueAll(_mntPosRaDec_deg);
      
  _mntSpeed_deg->setValueRaDec(__iraitMotors[ALT]->spd_enc*ENC_2_DEG, __iraitMotors[AZ]->spd_enc*ENC_2_DEG);
  sendValueAll(_mntSpeed_deg);

  _tarRealPos_deg->setValueRaDec(__tarRealPos_enc[ALT]*ENC_2_DEG, __tarRealPos_enc[AZ]*ENC_2_DEG);
  sendValueAll(_tarRealPos_deg);
      
  _distCurr_deg->setValueRaDec(__distCurr_enc[ALT]*ENC_2_DEG, __distCurr_enc[AZ]*ENC_2_DEG);
  sendValueAll(_distCurr_deg);  

  _pmDelta_deg->setValueRaDec(__pmDelta_enc[ALT]*ENC_2_DEG, __pmDelta_enc[AZ]*ENC_2_DEG);
  sendValueAll(_pmDelta_deg);  

}

void Irait::valueChanged(rts2core::Value* changed_value)
{
  if(changed_value == _actionMnt)
    {
      mntAction(_actionMnt->getValueInteger());
      return;
    }
  else if(changed_value==exit_on_danger)
    {// a reminder, nothing to do here
    }
  else if(changed_value==apply_pointing_model)
    {
      __pmDelta_enc[AZ]=0.;
      __pmDelta_enc[ALT]=0.;
    }
  else if(changed_value==ignore_motor_failure)
    { // a reminder, nothing to do here
    }
  else if(changed_value==ignore_run_over)
    { // a reminder, nothing to do here
    }
  else if(changed_value==point_after_move)
    {
      if(point_after_move->getValueBool())
	{
	  setMntStatus(mnt_t::POINTING);
	  pthread_create (&pointid,NULL,thread_point, (void *) this);
	}
      else
	{
	  if(pointRun)
	    {
	      pointRun = false;
	      usleep( REFRESH_TIME * 1.e6 * 2.) ; // let thread exit
	    }
	  setMntStatus(mnt_t::STOPPED);
	}
    }
  return Telescope::valueChanged(changed_value);
}

//! \brief Manage actions to perform with the mount
void Irait::mntAction(const int& action)
{
  switch(action)
    {
    case 0 : // stand-by
      if(__mntStatus == mnt_t::STOPPED)
	{
	  if((motorAlt->motorOn() == 1) ||
	     (motorAz->motorOn() == 1))
	    {
	      logStream(MESSAGE_ERROR) << "Irait::mntAction: Failed to start the mount." << sendLog;
	      setMntStatus(mnt_t::FAILED);
	    }
	  else
	    logStream(MESSAGE_INFO) << "Irait::mntAction: turned motors ON" << sendLog;
	}
      return;
    case 1 :
      stopMnt(true); // unconditionally
      logStream(MESSAGE_INFO) << "Irait::mntAction: STOPPED motors" << sendLog;
      break;
    }
}
// Commands issued in rts2-mon in tab T0 (mount) in pane above the log pane
int Irait::commandAuthorized (rts2core::Connection *conn)   // nothing defined here
{
  return Telescope::commandAuthorized (conn);
}

int Irait::startResync () // RTS2' first step of a move
{
  stopMnt(false);
  struct ln_equ_posn tar;
  struct ln_hrz_posn hrz;
  // 2016-09-02, wildi not used struct ln_equ_posn model_change;

  if(__mntStatus != mnt_t::STOPPED) 
    {
      logStream(MESSAGE_WARNING) << "Irait::startResync: Can't set a new target while not in state STOPPED." << sendLog;
      return -1;
    }
  double JD = ln_get_julian_from_sys ();
  getTarget (&tar);
  // 2016-09-02, wildi
  // applyModel (&tar, &model_change, telFlip->getValueInteger (), JD);
  ln_get_hrz_from_equ (&tar, rts2core::Configuration::instance ()->getObserver (), JD, &hrz);
  // poiting model
  // hrz + correction
  // _pm values
  setTarget (tar.ra, tar.dec);
  // Check the inital values. During POINTING the limits are checked as well.
  if(hrz.alt < (MNT_MIN_ENC[ALT] * ENC_2_DEG))
    {
      logStream (MESSAGE_ERROR) << "Irait::startResync: cannot move to altitude:" << hrz.alt << ", which is lower than the limit: " << MNT_MIN_ENC[ALT] * ENC_2_DEG << sendLog;
      return -1;
    }
  if(hrz.alt > (MNT_MAX_ENC[ALT] * ENC_2_DEG))
    {
      logStream (MESSAGE_ERROR) << "Irait::startResync: cannot move to altitude:" << hrz.alt << ", which is above than the limit: " << MNT_MAX_ENC[ALT] * ENC_2_DEG << sendLog;
      return -1;
    }
  __tarEquPos_deg = {tar.ra, tar.dec};  // set IRAIT target values

  if(setIRAITTargetPosition()) // as of 2016-07-22 returns always 0  
    return -1;

  setMntStatus(mnt_t::MOVING);
  logStream(MESSAGE_INFO) << "Irait::startResync: target set." << sendLog;
  return 0;
}

int Irait::isMoving()
{
  distCurr_en_mutex.lock();
  int ret=pointMnt();
  distCurr_en_mutex.unlock();

  if(ret && (!ignore_motor_failure->getValueBool()))
    {
      logStream(MESSAGE_ERROR) << "Irait::isMoving: error in  Irait::pointMnt(), error: " << ret << sendLog;
      return -1; // this is a "HW ERROR" (see rts2-mon, tab T0), which is unhandled by RTS2
    }
  if(__mntStatus == mnt_t::POINTING) // target reached
    {
      //setTracking(track=2, addTrackingTimer=false, send=true); // sidereal
      //setTracking(2, false, true); // sidereal
      return -2;
    }
  else if((__mntStatus == mnt_t::STOPPED) && (!point_after_move->getValueBool()))
    {
      //setTracking(track=2, addTrackingTimer=false, send=true); // sidereal
      //setTracking(2, false, true); // sidereal
      return -2;
    }
  return REFRESH_TIME * 1e6;
}

int Irait::stopMove ()
{
  stopMnt(false);
  // may be later: setTracking(0);
  return 0;
}

void Irait::setMntStatus(const mnt_t& status)
{
  __mntStatus = status;
  _mntStatus_str->setValueString(__mntStatus_name[static_cast<int>(status)]);
  sendValueAll(_mntStatus_str);
  //logStream(MESSAGE_ERROR) << "Irait:setMntStatus: " <<  __mntStatus_name[static_cast<int>(status)] << sendLog;
}
// \brief Get the mount absolute position
int Irait::setRTS2MntPosition()
{
  int ret=encdr->position();
  if(ret==READ_ERROR)
    return ret;

  double jd = ln_get_julian_from_sys();
  ln_hrz_posn currHrzPos_deg;
  currHrzPos_deg.alt = encdr->pos_deg[ALT] ;
  currHrzPos_deg.az = fmod(encdr->pos_deg[AZ] + 180., 360.);
  
  ln_get_equ_from_hrz(&currHrzPos_deg,&__mntLatLonPos_deg,jd,&__tarEquPos_deg); 
  setTelRa(__tarEquPos_deg.ra); 
  setTelDec(__tarEquPos_deg.dec);

  return 0;
}
void Irait::calculatePointingModel(struct ln_hrz_posn *currHrzPos_deg)
{
  float d_az, d_alt;
  d_az=0.;
  d_alt=0.;
  // there is no C8
  float C01,C02,C03,C04,C05,C06,C07,C08,C09,C10,C11;    
  //2016-09-03:
  // C01=-0.0016480748120152827;
  // C02=0.004951807812618494;
  // C03=1.609826204700232e-05;
  // C04=-0.003525786382832272;
  // C05=-0.002031485727489795;
  // C06=0.022481259318399883;
  // C07=-0.02023245821862589;
  // C08=0.000620167958162515;
  // C09=-0.011268992409556708;
  // C10=0.001753056774105354;
  // C11=0.;

  // without meteo
  C01=-0.0016470407165829386;
  C02=0.004950436410548576;
  C03=1.570783055013337e-05;
  C04=0.0035206724168937997;
  C05=-0.0020331662360766334;
  C06=0.021250839505408223;
  C07=-0.01899882554163535;
  C08=0.0005521502126146447;
  C09=-0.010547389626308518;
  C10=0.0016328654381470434;
  C11=0.0;
  // with meteo
  C01=-0.0016500824226488623;
  C02=0.004969272570284432;
  C03=1.6715023996815055e-05;
  C04=0.00348939826206629;
  C05=-0.0020490207058165844;
  C06=0.03581416338052821;
  C07=-0.03362405006177056;
  C08=0.0012014399224878432;
  C09=-0.019551930281463563;
  C10=0.003329735842583767;
  C11=0.0;

  float az,alt;
  // currHrzPos_deg is IRAIT AltAz (N=0,E=90)
  // corrections were derived from EQ system (RA/Dec), and S=0 S, W=90
  az=(currHrzPos_deg->az +180.) *M_PI/180.; 
  alt=currHrzPos_deg->alt*M_PI/180.;

  float val_plus_poly;
  //Az
  //condon 1992
  //     C1   C2               C3               C4                          C5
  //val=(C1()+C2()*np.cos(alt)+C3()*np.sin(alt)+C4()*np.cos(az)*np.sin(alt)+C5()*np.sin(az)*np.sin(alt))/np.cos(alt)
  // we never go to pi/2.
  //                                 yes, minus!
  d_az=(C01+C02*cos(alt)+C03*sin(alt)-C04*cos(az)*sin(alt)+C05*sin(az)*sin(alt))/cos(alt);
  currHrzPos_deg->az +=d_az *180./M_PI;
  __pmDelta_enc[AZ]=d_az *180./M_PI * DEG_2_ENC;
  //Alt
  // plus_poly
  //C9() * (alt-C11()) + C09() * (alt-C11())**2 + C10() * (alt-C11())**3
  val_plus_poly=C08*(alt-C11) + C09*pow((alt-C11),2) + C10*pow((alt-C11),3);
  //condon 1992
  //   C6   C7                C4               C5
  //val=C6()+C7()*cos(alt)+C4()*sin(az)+C5()*np.cos(az)
  d_alt=C06+C07*cos(alt)+C04*sin(az)+C05*cos(az)+val_plus_poly;

  currHrzPos_deg->alt +=d_alt *180./M_PI;
  __pmDelta_enc[ALT]= d_alt *180./M_PI * DEG_2_ENC;
  //logStream(MESSAGE_ERROR) << "Irait::calculatePointingModel, d_az: " << d_az *180.*60./M_PI<< ", d_alt: "<< d_alt*180.*60./M_PI << " [arcmin]"<< sendLog;
   
}
//! \brief Get the target position
int Irait::setIRAITTargetPosition()
{
  //(mnt->REFRESH_TIME * 1.e6);

  double jd = ln_get_julian_from_sys();
 
  //yuri add
  //increase the precision calculating coord for the next iteration time (about 7 arcsec)

  jd = jd + (REFRESH_TIME * ONE_SEC_IN_JD);


  ln_hrz_posn currHrzPos_deg;
  // 2016-09-02, wildi ToDo, must go away, see teld.cpp
  //getTelTargetRaDec(&__tarEquPos_deg);
  getTargetAltAz(&currHrzPos_deg, (double)jd);
	  
  double ref;
  ref = 0.;
  // ToDo libnovas's formula might not be correct
  //ref = ln_get_refraction_adj (currHrzPos_deg.alt, 640., -50.);
  // libnova 0 = South, IRAIT 0 = North
  // here fmod(x, 360.) is necessary since AZ>180. (libnova) is not uncommon :-))

  if(apply_pointing_model->getValueBool())
    calculatePointingModel(&currHrzPos_deg);

  __tarRealPos_enc  = {(currHrzPos_deg.alt + ref) * DEG_2_ENC, fmod((currHrzPos_deg.az +180.),360.) * DEG_2_ENC};

  return 0;
}
//! \brief Get the mount - target distance
int Irait::calculateDistance()
{
    Lock cem(distCurr_en_mutex);
  if(isnan(encdr->steps(Encoder::ALT))|| isnan(encdr->steps(Encoder::AZ)))
    {
      logStream(MESSAGE_ERROR) << "Irait::calculateDistance: encdr->pos_enc[(ALT|AZ)]==NAN" << sendLog;
      return -1;
    }
  if(isnan( __tarLastPos_enc[ALT]) || isnan( __tarLastPos_enc[AZ]))
    {
      __tarLastPos_enc = __tarRealPos_enc;
      __iraitMotors[ALT]->spd_enc=NAN;
      __iraitMotors[AZ]->spd_enc=NAN;
      return 0 ;
    }
  std::vector<double> result = { (__tarRealPos_enc[ALT] - encdr->steps(Encoder::ALT)),
                 (__tarRealPos_enc[AZ] - encdr->steps(Encoder::AZ)) };
  

  //yuri add
  //check if direction of alt is changing (max_alt and min_alt)
  // if direction changes turn in the previous case... wait stop before send IP (see doc. galil [when moving IP must be in the same direction])
  bool temp_altIsgrowing = _altIsGrowing;
  if (result[ALT] > 0){
    _altIsGrowing = true;
  }else{
      _altIsGrowing = false;
  }
  if (temp_altIsgrowing != _altIsGrowing){
      __altDirectionNotChanged = false;
      logStream(MESSAGE_INFO) << "Irait::calculateDistance: Changed alt direction."<<sendLog;
  }else{
      __altDirectionNotChanged = true;
  }



  for (int ax : {ALT,AZ}) // yes! for both axis
  {
      if(result[ax] > HALF_ENC) // shortest path
          result[ax] -= MAX_ENC;
      else if(result[ax] <= -HALF_ENC)
          result[ax] += MAX_ENC;
  }



  //not used now
  if(_tiltmeter_enc) {// so far only this device makes errors
      if(__mntStatus == mnt_t::POINTING)
      {
          if(isnan(lastResult[ALT])) // done in parallel
          {
              lastResult = result; // may be a glitch if result is bad
          }
          // since encoders are error checked the threshold is counter productive
          // else if((fabs(result[ALT]) > UPPER_THRESHOLD_RESULT) ||
          //  	(fabs(result[AZ]) > UPPER_THRESHOLD_RESULT))
          //   {
          //     usleep( REFRESH_TIME * 1.e6 * .5) ;
          //     return 1;
          //   }
          else
              lastResult = result;
      }
   }
   __distCurr_enc = result;


   //yuri add
   //calculate new speeds and set them if minor of max speed "fast"


   if(__mntStatus == mnt_t::POINTING){
       vel_Alt = fabs(__distCurr_enc[ALT]) / (mnt->REFRESH_TIME * 1.e6);
       vel_Az = fabs(__distCurr_enc[AZ]) / (mnt->REFRESH_TIME * 1.e6);

       if (vel_Alt <__iraitMotors[ALT]->fast){
            __iraitMotors[ALT]->spd_enc=vel_Alt;
       }else{
           __iraitMotors[ALT]->spd_enc= __iraitMotors[ALT]->fast;
       }

       if (vel_Az <___iraitMotors[AZ]->fast){
            __iraitMotors[AZ]->spd_enc=vel_Az;
       }else{
           __iraitMotors[AZ]->spd_enc= __iraitMotors[AZ]->fast;
       }
   }



  if(isnan(__distInit_enc[ALT])) // done in pairs
      __distInit_enc = __distCurr_enc;

  __tarLastPos_enc = __tarRealPos_enc;
  return 0;
} 


int Irait::turnAxis()
{
    Lock cem(distCurr_en_mutex);

    //yuri add
    // if is in pointing and in the same direction

    if ((__mntStatus == mnt_t::POINTING) && (__altDirectionNotChanged)){

        for (int ax = ALT; ax <= AZ; ax++){

            if(isnan(__iraitMotors[ax]->spd_enc) || (isnan(__distCurr_enc[ax])))
                continue; // communicate only if necessary

            if(targetOutOfLimits(ax) == true)
            {
                stopMnt(true);
                logStream(MESSAGE_WARNING) << "Irait::turnAxis: Target outside limits: motion stopped" << sendLog;
                return OUT_OF_BOUNDS;
            }


            if(__iraitMotors[ax]->motorSpeed(__iraitMotors[ax]->spd_enc) == 0)
            {
                if(isnan(__distCurr_enc[ax]))
                {
                    logStream(MESSAGE_ERROR) << "Irait::turnAxis: Should never happen: mutex? >>>>>>> isnan (__distCurr_enc[ " << ax << "]), EXITING" << sendLog;
                    exit(1);
                }
                if(__iraitMotors[ax]->motorStep( __distCurr_enc[ax])==0)
                    __distLast_enc[ax] = __distCurr_enc[ax]; // AFTER all went succesfully, gell!
                else
                    return 1;
            }
            else
                return 1;

        }

    }else{

    //yuri add
    //old magement used while Moving and when alt change direction

        for (int ax = ALT; ax <= AZ; ax++) // send commands to motors ALT motor has priority
        {
            if(isnan(__iraitMotors[ax]->spd_enc) || (isnan(__distCurr_enc[ax])))
                continue; // communicate only if necessary

            if(__iraitMotors[ax]->motorTurning() == 0)
                continue; // do nothing until motion stopped

            if(targetOutOfLimits(ax) == true)
            {
                stopMnt(true);
                logStream(MESSAGE_WARNING) << "Irait::turnAxis: Target outside limits: motion stopped" << sendLog;
                return OUT_OF_BOUNDS;
            }
            while(true)
            {
                if(__iraitMotors[ax]->motorOn() == 0) // servo here (SH), gell
                    break; // a bit dangerous because the logic is misused.
            }
            if(__iraitMotors[ax]->motorSpeed(__iraitMotors[ax]->spd_enc) == 0)
            {
                if(isnan(__distCurr_enc[ax]))
                {
                    logStream(MESSAGE_ERROR) << "Irait::turnAxis: Should never happen: mutex? >>>>>>> isnan (__distCurr_enc[ " << ax << "]), EXITING" << sendLog;
                    exit(1);
                }
                if(__iraitMotors[ax]->motorStep( __distCurr_enc[ax])==0)
                    __distLast_enc[ax] = __distCurr_enc[ax]; // AFTER all went succesfully, gell!
                else
                    return 1;
            }
            else
                return 1;
        }
    }
    return 0;
}
bool Irait::runOver()
{
  double cd = sqrt(pow(__distCurr_enc[ALT], 2) + pow(__distCurr_enc[AZ], 2));

  if( cd < pointing_limit)
    return false;

  bool alt_sgn_chng = ((__distInit_enc[ALT] * __distLast_enc[ALT]) < 0);
  bool az_sgn_chng = ((__distInit_enc[AZ] * __distLast_enc[AZ]) < 0);

  if(az_sgn_chng && alt_sgn_chng)
    return true;

  return false;
}

void Irait::targetReached()
{
        Lock cem(distCurr_en_mutex);
    __iraitMotors[ALT]->spd_enc=__iraitMotors[ALT]->fast;
    __iraitMotors[AZ]->spd_enc=__iraitMotors[AZ]->fast;
    if(fabs(__distCurr_enc[ALT]) < POINT_ACCURACY_ENC[ALT])
    {
        if(!isnan(__iraitMotors[ALT]->spd_enc)) // only if ALT motors are on
        {
            if(motorAlt->motorStop() == 1)
                logStream(MESSAGE_WARNING) << "Irait::targetReached: Stop ALT motor FAILED."<<sendLog;
            else
                __iraitMotors[ALT]->spd_enc = NAN ;
        }
    }
    if(fabs(__distCurr_enc[AZ]) < POINT_ACCURACY_ENC[AZ])
    {
        if(!isnan(__iraitMotors[AZ]->spd_enc)) // only if AZ motors are on
        {
            if(motorAz->motorStop() == 1)
                logStream(MESSAGE_WARNING) << "Irait::targetReached: Stop AZ motor FAILED."<<sendLog;
            else
                __iraitMotors[AZ]->spd_enc = NAN ;
        }
    }
    if(isnan(__iraitMotors[ALT]->spd_enc) && isnan(__iraitMotors[AZ]->spd_enc))
    {
        stopMnt(false);
        if(point_after_move->getValueBool())
        {
            setMntStatus(mnt_t::POINTING);
            pthread_create (&pointid, NULL, thread_point, (void *) this);
        }
    }
    else if(runOver())
    {
        logStream(MESSAGE_WARNING) << "Irait::targetReached, target run over: "<<sqrt(pow(__distCurr_enc[ALT], 2) + pow(__distCurr_enc[AZ], 2)) * ENC_2_DEG *60. << ", limit: " << pointing_limit * ENC_2_DEG * 60. << " [min]" << sendLog;
        if(!ignore_motor_failure->getValueBool())
            stopMnt(false);
        else
            logStream(MESSAGE_WARNING) << "Irait::targetReached, ignored target run over" << sendLog;
    }
}

int Irait::pointMnt()
{

    {
        logStream(MESSAGE_ERROR) << "Irait::pointMnt: mount in state FAILED" << sendLog;
        return -1;
    }
    while(true) // ok, no escape in case of constant failure...
    {
        if(point_after_move->getValueBool())
            if(setIRAITTargetPosition()) // as of 2016-07-22 returns always 0
                return -1;

        if(setRTS2MntPosition())
        {
            // most likely:
            // 2015-09-30T16:21:39.257 CST rts2-teld-irait 8 Target ALT position Reached.
            // 2015-09-30T16:21:39.265 terminate called after throwing an instance of 'rts2core::ConnError'
            //  what():  connection error error calling select function: Bad file descriptor
            //  Aborted (core dumped)
            if(__mntStatus == mnt_t::POINTING)
            {
                logStream(MESSAGE_ERROR) << "Irait::pointMnt: POINTING FAILED encoder READ_ERROR" << sendLog;
                // wildi: this should be the state,
                // but since it happens so often, I try to ignore it!
                // setMntStatus(mnt_t::FAILED);
                logStream(MESSAGE_WARNING) << "Irait::pointMnt: ignoring state FAILED while pointing"  << sendLog;
                return -1;
            }
        }
        if(calculateDistance() == 0)
            break;
        else
            logStream(MESSAGE_WARNING) << "Irait::pointMnt: could not compute distance, retrying endlessly, status: "<< _mntStatus_str->getValue()<< sendLog;
    }
    if(__mntStatus == mnt_t::MOVING){
        targetReached(); // mount_t::MOVING -> mnt_t::POINTING
    }

    //yuri add
    // set max speed only if not in pointing mode
    if(__mntStatus != mnt_t::POINTING){
        __iraitMotors[ALT]->spd_enc=__iraitMotors[ALT]->fast;
        __iraitMotors[AZ]->spd_enc=__iraitMotors[AZ]->fast;
    }


    if(turnAxis())
        return -1;

    return 0;
}
// Check if target is outside the mount pointing limits
// NOTE: AZ motion has no limits so is always FALSE
bool Irait::targetOutOfLimits(const int& axis)
{
  // AZ axis is always false (never Out Of Limits)
  if(axis == ALT)
    if((__tarRealPos_enc[axis] > MNT_MAX_ENC[axis]) ||
	 (__tarRealPos_enc[axis] < MNT_MIN_ENC[axis]))
      {
	logStream(MESSAGE_WARNING) << "Target outside the mount ALT axis limits: " <<__tarRealPos_enc[axis] << sendLog;
	return true;
      }
  return false;
}

void Irait::stopMnt( bool stopPoint)
{
    {
        Lock cme(distCurr_en_mutex);
        __distCurr_enc   = {NAN,NAN};
    }
  __distLast_enc   = {NAN,NAN};
  __distInit_enc   = {NAN,NAN};
  __tarLastPos_enc = {NAN,NAN}; 
  __iraitMotors[ALT]->spd_enc = NAN;
  __iraitMotors[AZ]->spd_enc = NAN;
  if(stopPoint)
    {
      point_after_move->setValueBool(false);
      sendValueAll(point_after_move);
    }
  updateDisplay();
  if(_tiltmeter_enc) 
    lastResult = {NAN,NAN};
  // do that unconditionally: if e.g. Galil connection was the
  // cause for the state FAILED, we can not stop the threads.
  // As of 2015-07-20 Galil connection fails often.
  if(pointRun)
    pointRun = false;

  while(true)
    {
      try
	{
	  if(motorAlt->motorStop() == 1)
	    throw (std::string("Stopping motor FAILED."));
	  if(motorAlt->motorOff() == 1)
	    throw (std::string("Switch off motor FAILED."));
	  if(motorAz->motorStop() == 1)
	    throw (std::string("Stopping motor FAILED."));
	  if(motorAz->motorOff() == 1)
	    throw (std::string("Switch off motor FAILED."));
	  // reset the motors controller
	  // is useful? is "dangerous"(delete IP settings)? TO BE TESTED
	  //connectionGalil->command("RS");    
	  __iraitMotors[ALT]->spd_enc=NAN;
	  __iraitMotors[AZ]->spd_enc=NAN;
	  _mntSpeed_deg->setValueRaDec(NAN,NAN);
	  sendValueAll(_mntSpeed_deg);

	  setMntStatus(mnt_t::STOPPED);
	  /* may be the motors did not really stop yet (IP during forced decel)
	   * got ? instead of : response.  TC1 returned "16 IP wrong sign in pos move or IP during forced decel"
	   */
	  usleep( 1.e6) ;
	  break;
	}
      catch(...)
	{
	  logStream(MESSAGE_WARNING) << "Failed to stop the mount." << sendLog;
	  setMntStatus(mnt_t::FAILED);
	}
      // the mount must stop, this might be an endless loop!
      if(__mntStatus == mnt_t::FAILED)
	usleep( 1.e6) ;
      else
	break ;
    }
  logStream(MESSAGE_INFO) << "Irait::stopMnt: STOPPED mount." << sendLog;
}

static void* thread_point(void *arg)
{
  Irait *mnt = (Irait *) arg;
  mnt->pointRun = true; 
  while(mnt->pointRun)
    {
      mnt->pointMnt(); // here we can not do mutch if an error occurs, ignoring it
      usleep( mnt->REFRESH_TIME * 1.e6) ;
    }
  return NULL;
}

int main(int argc, char **argv)
{
	Irait device (argc, argv);
	return device.run ();
}
