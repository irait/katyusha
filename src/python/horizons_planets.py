#!/usr/bin/python
import sys
import numpy as np
from STARTUP import rcfile
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD#,#HorizonsEphemeridsDatabase,
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb

cmdline=CmdLineBase(sys.argv[0],['airless_refracted','object','date','time']
                    ,OptionParameter=[
                       ("-D","--dbHorizons",'dbHorizons',None,'path with horizons db, overrides init file')
                       ]
                    ,OptionFlag=[
                       ('-v','--verbose','Verbose','store_true','False','verbose output')
                       ,('-b','--TableBanner','TableBanner','store_true','False','banner of table')
                       ,('-a','--AstropyInterp','AstropyInterp','store_true','False','Astropy Interpolation')
                       ,('-e','--EphemInterp','EphemInterp','store_true','False','pyEphem Interpolation')
                       ]
                     ,Version='1.2 - 2016 dec 08 - 2016 dec 10 - 2016 dec 12 - By M.Maris'
                     ,Explanation="""

Example: 
  >./horizons_planets.py airless venus 2016-12-10 03:23 -S 20161208 -E 20161212 -v
  db/Horizons_20161208_20161212/observer_airless_venus.txt
  venus airless 2457732.64097 44.4713 20.3698055556 -21.834 20.3858472222 -21.7785555556 30.2744 58.4579
  
  >./horizons_planets.py airless venus ! ! -S 20161208 -E 20161212 -b
  Description of table  db/Horizons_20161208_20161212/observer_airless_venus.txt
  TargetName            Venus
  TargetCode            299
  CenterName            Earth
  CenterCode            399
  CenterGeodeticLong    123.319558
  CenterGeodeticLat     -75.099859
  CenterGeodeticAlt     3237.0
  CenterGeodeticAltUnit m
  TimeStart             A.D. 2016-Dec-08 00:00:00.0000 UT
  TimeStop              A.D. 2016-Dec-12 00:00:00.0000 UT
  TimeStep              1 minutes
  Refracted             NO
  RA_format             HMS
  Time_format           BOTH
  len                   5761
"""                       
                    )

if cmdline.Option.rcFile==None :
   RCFile=rcfile('katyusharc')
else :
   RCFile=rcfile(cmdline.Option.rcFile)

DBpath=RCFile['dbHorizons'] if cmdline.Option.dbHorizons == None else cmdline.Option.dbHorizons

#if len(sys.argv) < 1+2 : 
   #print "%s airless/refracted object "%sys.argv[0]
   #print
   #print "%s airless/refracted object date time"%sys.argv[0]
   #print 
   #print "object refraction jd SolarElongation J2000_DEC App_RA App_Dec App_Alt App_Azi" 
   #sys.exit()
  
airless_refracted=cmdline.argument['airless_refracted']
Object=cmdline.argument['object']
date=cmdline.argument['date']
time=cmdline.argument['time']
jd = ephemUTepoch2JD(date,time)

#Gets the table for the Sun
#HorizonsTableName=HorizonsEphemeridsDatabase(sys.argv[2],sys.argv[1])()
#HorizonsTableName=HorizonsEphemeridsDatabase(Object,airless_refracted,Start=cmdline.Option.Start,End=cmdline.Option.End)()

# gets metadata

#print cmdline.Option.DBpath+'/metadata.csv'

HOTdb=HorizonsObserverTablesDb(DBpath+'/metadata.csv')

# gets table name from metadata
HorizonsTableName=HOTdb.select_by_jd(Object,airless_refracted,jd)

if HorizonsTableName == None :
   print "No entry found for the selected body/atmosphere/epoch, install related horizons tables"
   sys.exit()
   
if cmdline.Option.Verbose :
   print HorizonsTableName
HTI=HorizonsTableObserver(HorizonsTableName)

if cmdline.Option.TableBanner :
   print "Description of table ",HorizonsTableName
   print HTI.banner()
   sys.exit()

print sys.argv[2],sys.argv[1],
print jd,
print np.interp(jd,HTI.UT_JD,HTI.SolarElongation),
print np.interp(jd,HTI.UT_JD,HTI.Astro_RA),
print np.interp(jd,HTI.UT_JD,HTI.Astro_Dec),
print np.interp(jd,HTI.UT_JD,HTI.App_RA),
print np.interp(jd,HTI.UT_JD,HTI.App_Dec),

if cmdline.Option.AstropyInterp :
   out=HTI.astropyInterpAltAzi(date,time)
   print out[3],
   print out[4],
elif cmdline.Option.EphemInterp :
   out=HTI.ephemInterpAltAzi(date,time)
   print out[3],
   print out[4],
else :
   out=HTI.linearInterpAltAz(date,time)
   print out[3],
   print out[4],

print
