#!/usr/bin/env python
 
import socket
import json
import argparse
import sys
import ast

import time
import pickle
import struct
import numpy as np

import MySQLdb
from datetime import datetime as dt

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val



def get_temp(r,k,o):
    i =  float(np.int16(r))
    return i/k+o


probedivfactor= 20.48
probeoffset =   [0,0,0,0,-53,0,0,0,  0,0,0,0,-51.09,-51.09,0,0,  -51.09,-51.09,-51.09,0,-51.09,-51.09,-51.09,-51.09,  0,0,0,0,0,0,0,0 ]


def updateDb(myPlcAddress, last_val,db):

	cursor = db.cursor()

	sql = "SELECT min_warning, max_warning, min_alarm, max_alarm, warning_active, alarm_active, alarm_send_message, ID, description, category FROM active_element WHERE plc_address = '%s' and plc_id = 1 and (status != 'DISABLED' or status IS NULL)" % (myPlcAddress)
	cursor.execute(sql)
	result = cursor.fetchone()

	if result:
		min_warning = result[0]
		max_warning = result[1]
		min_alarm   = result[2]
		max_alarm   = result[3]
		warning_active = result[4]
		alarm_active = result[5]
		alarm_send_message = result[6]
		ID = result[7]
		mydesc = result[8]
		mycategory = result[9] 

		insertEvent = False


		if ((min_warning is not None) and (max_warning is not None)): 

			if ((last_val < min_warning) or (last_val > max_warning)):
				if warning_active == 0:
					warning_active = 1
					insertEvent = True
			else:
				if warning_active == 1:
					warning_active = 0

		if ((min_alarm is not None) and (max_alarm is not None)): 

			if ((last_val < min_alarm) or (last_val > max_alarm)):
				if alarm_active == 0:
					alarm_active = 1
					insertEvent = True
			else:
				if alarm_active == 1:
					alarm_active = 0

		
		classEvent = ""
		if alarm_active:
			classEvent = "ALARM"
		elif warning_active:
			classEvent = "WARNING"
		else:
			classEvent = "OK"

		if insertEvent:
			#todo insert event
			myEvntDesc = "%s value %.1f, over threashold %s" % (mydesc, last_val, classEvent)

			sql = "INSERT INTO events (ev_date, ev_class, ev_category,  ev_name, ev_active_el_id) VALUES ('%s', '%s', '%s', '%s', %i)" % (dt.now().strftime('%Y-%m-%d %H:%M:%S'), classEvent, mycategory, myEvntDesc, ID) 	
			cursor.execute(sql)
			db.commit()
							


		sql = "UPDATE active_element SET el_value = %.1f, last_update = '%s', status = '%s', alarm_active = %i, warning_active = %i WHERE plc_address = '%s' and plc_id = 1 " % (last_val,  dt.now().strftime('%Y-%m-%d %H:%M:%S'), classEvent, alarm_active, warning_active, myPlcAddress)

		cursor.execute(sql)
		db.commit()

	else:
		#variable not present in db
		pass


def check(modbushost, modbusport, dbhost):

    t_ext=0
   
    BUFFER_SIZE = 1024
    db = MySQLdb.connect(dbhost,"irait_control","irait","irait_control")
    
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	#part 1 read probes (registers)

        s.connect((modbushost, modbusport))
        MESSAGE = '{"function":"R_REGISTERS","address":"%s","count":%d}' % (str(hex(0)),32)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])


	for i in range(0,32):
		
		if i == 8 or i == 9:
			last_val = get_temp(jobj['data'][i],1,0)
		else:
			last_val = get_temp(jobj['data'][i],probedivfactor,probeoffset[i])
		myPlcAddress = '0x%0*X' % (4,i)
		#print myPlcAddress, str(last_val), jobj['data'][i], probeoffset[i]
		updateDb(myPlcAddress, last_val, db)

	
	#part 2 read status (bits)
	
	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(4224)),32)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])


	for i in range(0,32):
		
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+4224)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	


	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(159)),35)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])


	for i in range(0,35):
		
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+159)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	#part 3 read ALARMS (bits)

	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(10593)),7)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])


	for i in range(0,7):
		
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+10593)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	#part 4 read COMMANDS mtm off (bits)

	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(8512)),8)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])
	for i in range(0,8):
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+8512)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	#part 5 read COMMANDS mtm on (bits)

	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(8528)),8)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])
	for i in range(0,8):
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+8528)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)


	#part 6 read COMMANDS desable (bits)

	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(8192)),31)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])
	for i in range(0,31):
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+8192)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	#part 7 read COMMANDS force (bits)

	MESSAGE = '{"function":"R_BITS","address":"%s","count":%d}' % (str(hex(8272)),31)
        s.send(MESSAGE)
        data = s.recv(BUFFER_SIZE)
        j = json.JSONDecoder()
        jobj = j.decode(data[0:-1])
        if jobj.has_key('error'):
            raise RuntimeError(jobj['error'])
	for i in range(0,31):
		last_val = jobj['data'][i]
		myPlcAddress = '0x%0*X' % (4,i+8272)
		#print myPlcAddress, str(last_val)
		updateDb(myPlcAddress, last_val,db)

	s.close()
	db.close()

    except Exception as e:
        print str(e)
        pass

    graphite_path='local.test.box0'

    try: # send data to graphite
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ts = time.time()    
        pdata = []

        #pdata.append((graphite_path+".t_ext",(ts,float(t_ext))))

        payload = pickle.dumps(pdata, protocol=2)
        header = struct.pack("!L", len(payload))
        message = header + payload

        s.connect(("localhost", 2004))
        n = s.send(message)
        s.close()

    except Exception as e:
        print str(e)
        pass

    

	
if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-H','--hostname',required=True,type=str)
    obj = ap.parse_args()

    TCP_PORT = 8000
    
    check(obj.hostname, TCP_PORT, "localhost")
    exit(0)
    
        

    

        

	



