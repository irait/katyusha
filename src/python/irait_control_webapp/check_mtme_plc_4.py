#!/usr/bin/env python
 
import socket
import json
import argparse
import sys
import ast


import time
import pickle
import struct
import numpy as np


import MySQLdb
from datetime import datetime as dt

blocks = [
    (0x1000,8,False,['3-PHASE SYSTEM VOLTAGE',
                     'PHASE VOLTAGE L1-N',
                     'PHASE VOLTAGE L2-N',
                     'PHASE VOLTAGE L3-N']),
    (0x1008,8,False,['LINE VOLTAGE L1-2',
                     'LINE VOLTAGE L2-3',
                     'LINE VOLTAGE L3-1',
                     '3-PHASE SYSTEM CURRENT']),
    (0x1010,6,False,['LINE CURRENT L1',
                     'LINE CURRENT L2',
                     'LINE CURRENT L3']),
    (0x1016,8,True,['3-PHASE SYS. POWER FACTOR',
                    'POWER FACTOR L1',
                    'POWER FACTOR L2',
                    'POWER FACTOR L3']),
    (0x101E,8,True,['3-PHASE SYSTEM COS(phi)',
                    'PHASE COS(phi)1',
                    'PHASE COS(phi)2',
                    'PHASE COS(phi)3']),
    (0x1026,8,False,['3-PHASE S. APPARENT POWER',
                      'APPARENT POWER L1',
                      'APPARENT POWER L2',
                      'APPARENT POWER L3']),
    (0x102E,8,False,['3-PHASE SYS. ACTIVE POWER',
                      'ACTIVE POWER L1',
                      'ACTIVE POWER L2',
                      'ACTIVE POWER L3']),
    (0x1036,8,False,['3-PHASE S. REACTIVE POWER',
                     'REACTIVE POWER L1',
                     'REACTIVE POWER L2',
                     'REACTIVE POWER L3']),
    (0x103E,4,False,['3-PHASE SYS. ACTIVE ENERGY',
                     '3-PHASE S. REACTIVE ENERGY']),
    (0x1046,4,False,['FREQUENCY',
                     'NEUTRAL CURRENT']),
    (0x1060,8,False,['MAX ISTANT. CURRENT L1',
                      'MAX ISTANT. CURRENT L2',
                      'MAX ISTANT. CURRENT L3',
                      'MAX ISTANT. 3-PHASE . ACTIVE POWER']),
    (0x1068,8,False,['MAX ISTANT. 3-PHASE . APPAR. POWER',
                      'MAX 15 min MEAN .CURRENT L1',
                      'MAX 15 min MEAN .CURRENT L2',
                      'MAX 15 min MEAN .CURRENT L3']),
    (0x1070,2,False,['MAX 15 min MEAN .3-PHASE ACTIVE POWER'])
]

values = {
    '3-PHASE SYSTEM VOLTAGE':('0x1000','V',0),
    'PHASE VOLTAGE L1-N':('0x1002','V',0),
    'PHASE VOLTAGE L2-N':('0x1004','V',0),
    'PHASE VOLTAGE L3-N':('0x1006','V',0),
    'LINE VOLTAGE L1-2':('0x1008','V',0),
    'LINE VOLTAGE L2-3':('0x100A','V',0),
    'LINE VOLTAGE L3-1':('0x100C','V',0),
    '3-PHASE SYSTEM CURRENT':('0x100E','mA',0),
    'LINE CURRENT L1':('0x1010','mA',0),
    'LINE CURRENT L2':('0x1012','mA',0),
    'LINE CURRENT L3':('0x1014','mA',0),
    '3-PHASE SYS. POWER FACTOR':('0x1016','-',0),
    'POWER FACTOR L1':('0x1018','-',0),
    'POWER FACTOR L2':('0x101A','-',0),
    'POWER FACTOR L3':('0x101C','-',0),
    '3-PHASE SYSTEM COS(phi)':('0x101E','-',0),
    'PHASE COS(phi)1':('0x1020','-',0),
    'PHASE COS(phi)2':('0x1022','-',0),
    'PHASE COS(phi)3':('0x1024','-',0),
    '3-PHASE S. APPARENT POWER':('0x1026','VA',0),
    'APPARENT POWER L1':('0x1028','VA',0),
    'APPARENT POWER L2':('0x102A','VA',0),
    'APPARENT POWER L3':('0x102C','VA',0),
    '3-PHASE SYS. ACTIVE POWER':('0x102E','W',0),
    'ACTIVE POWER L1':('0x1030','W',0),
    'ACTIVE POWER L2':('0x1032','W',0),
    'ACTIVE POWER L3':('0x1034','W',0),
    '3-PHASE S. REACTIVE POWER':('0x1036','VAR',0),
    'REACTIVE POWER L1':('0x1038','VAR',0),
    'REACTIVE POWER L2':('0x103A','VAR',0),
    'REACTIVE POWER L3':('0x103C','VAR',0),
    '3-PHASE SYS. ACTIVE ENERGY':('0x103E','100*Wh',0),
    '3-PHASE S. REACTIVE ENERGY':('0x1040','100*VARh',0),
    'FREQUENCY':('0x1046','mHz',0),
    'NEUTRAL CURRENT':('0x1048','mA',0),
    'MAX ISTANT. CURRENT L1':('0x1060','mA',0),
    'MAX ISTANT. CURRENT L2':('0x1062','mA',0),
    'MAX ISTANT. CURRENT L3':('0x1064','mA',0),
    'MAX ISTANT. 3-PHASE . ACTIVE POWER':('0x1066','W',0),
    'MAX ISTANT. 3-PHASE . APPAR. POWER':('0x1068','VA',0),
    'MAX 15 min MEAN .CURRENT L1':('0x106A','mA',0),
    'MAX 15 min MEAN .CURRENT L2':('0x106C','mA',0),
    'MAX 15 min MEAN .CURRENT L3':('0x106E','mA',0),
    'MAX 15 min MEAN .3-PHASE ACTIVE POWER':('0x1070','W',0)                     
    }

def get_signed(h,l):
    return np.int32((h<<16)+l)

def get_unsigned(h,l):
    return np.uint32((h<<16)+l)


def check(modbushost, modbusport, dbhost):
    BUFFER_SIZE = 1024
    db = MySQLdb.connect(dbhost,"irait_control","irait","irait_control")
    cursor = db.cursor()
    try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((modbushost, modbusport))
	
	for addr,count,is_sig,key_l in blocks:
	    MESSAGE = '{"function":"R_REGISTERS","address":"%s","count":%d}' % (str(hex(addr)),count)
	    s.send(MESSAGE)
	    data = s.recv(BUFFER_SIZE)

	    j = json.JSONDecoder()
	    jobj = j.decode(data[0:-1])
	    if jobj.has_key('error'):
	        raise RuntimeError(jobj['error'])
	    for idx in range(count/2):
	        h = jobj['data'][idx*2]
	        l = jobj['data'][idx*2+1]
	        tp = values[key_l[idx]]
	        if is_sig:
	            values[key_l[idx]] = (tp[0],get_signed(h,l))
	        else:
	            values[key_l[idx]] = (tp[0],get_unsigned(h,l))


		last_val = float(values[key_l[idx]][1])

		sql = "SELECT min_warning, max_warning, min_alarm, max_alarm, warning_active, alarm_active, alarm_send_message, ID, description, category FROM active_element WHERE plc_address = '%s' and plc_id = 4" % (values[key_l[idx]][0])
		cursor.execute(sql)
		result = cursor.fetchone()

		if result:

			min_warning = result[0]
			max_warning = result[1]
			min_alarm   = result[2]
			max_alarm   = result[3]
			warning_active = result[4]
			alarm_active = result[5]
			alarm_send_message = result[6]
			ID = result[7]
			mydesc = result[8]
			mycategory = result[9]

			insertEvent = False


			if ((min_warning is not None) and (max_warning is not None)): 

				if ((last_val < min_warning) or (last_val > max_warning)):
					if warning_active == 0:
						warning_active = 1
						insertEvent = True
				else:
					if warning_active == 1:
						warning_active = 0

			if ((min_alarm is not None) and (max_alarm is not None)): 

				if ((last_val < min_alarm) or (last_val > max_alarm)):
					if alarm_active == 0:
						alarm_active = 1
						insertEvent = True
				else:
					if alarm_active == 1:
						alarm_active = 0

			
			classEvent = ""
			if alarm_active:
				classEvent = "ALARM"
			elif warning_active:
				classEvent = "WARNING"
			else:
				classEvent = "OK"

			if insertEvent:
				#todo insert event
				myEvntDesc = "%s value %.1f, over threashold %s" % (mydesc, last_val, classEvent)

				sql = "INSERT INTO events (ev_date, ev_class, ev_category,  ev_name, ev_active_el_id) VALUES ('%s', '%s', '%s', '%s', %i)" % (dt.now().strftime('%Y-%m-%d %H:%M:%S'), classEvent, mycategory, myEvntDesc, ID) 	
				cursor.execute(sql)
				db.commit()
								


			#print key_l[idx], values[key_l[idx]][0],  values[key_l[idx]][1]
			sql = "UPDATE active_element SET el_value = %.1f, last_update = '%s', status = '%s', alarm_active = %i, warning_active = %i WHERE plc_address = '%s' and plc_id = 4 " % (last_val,  dt.now().strftime('%Y-%m-%d %H:%M:%S'), classEvent, alarm_active, warning_active, values[key_l[idx]][0])

			cursor.execute(sql)
			db.commit()

		else:
			#variable not present in db
			pass

	s.close()
	db.close()

    except Exception as e:
	print str(e)
	#exit(3)

    graphite_path='local.test.pwr'

    try: # send data to graphite
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	ts = time.time()    
	pdata = []
	
	for k in values.keys():
	    new_k = k.replace(' ','-').replace('.','_').replace('(','_').replace(')','_')
	    pdata.append((graphite_path+"."+new_k,(ts,float(values[k][1]))))
	
	payload = pickle.dumps(pdata, protocol=2)
	header = struct.pack("!L", len(payload))
	message = header + payload

	s.connect(("localhost", 2004))
	n = s.send(message)
	s.close()

    except Exception as e:
	print str(e)
	pass
	
    

        

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-H','--hostname',required=True,type=str)
    obj = ap.parse_args()
	
    

    TCP_PORT = 8008

    check(obj.hostname, TCP_PORT, "localhost")
    exit(0)
    

   
	    

    

        

	



