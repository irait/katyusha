import check_box0_plc_1 as ABB
import check_box5_plc_2 as box5
import check_box6_plc_3 as box6
import check_mtme_plc_4 as mtme
import check_box4_plc_5 as box4

import MySQLdb
from datetime import datetime as dt

import time
import os

myWebAppDb = "localhost"
myModbusServer = "10.10.18.19"


def pingIpDevices():
	db = MySQLdb.connect(myWebAppDb,"irait_control","irait","irait_control")
    	cursor = db.cursor()
	cursor2 = db.cursor()

	sql = "SELECT ID, IP_address, status FROM element WHERE IP_address IS NOT NULL"
	cursor.execute(sql)
	result = cursor.fetchall()

	
	for row in result:
		myid = int(row[0])
		myIp = str(row[1])
		oldStatus = str(row[2])

		newStatus = ""

		response = os.system("ping -c 1 " + myIp)

		if response == 0:
			newStatus = 'OK'
		else:
			newStatus = 'OFF'

		if newStatus != oldStatus:
			#insert event
			pass

		sql2 = "UPDATE element SET status = '%s', last_update = '%s' WHERE ID = %i" % (newStatus, dt.now().strftime('%Y-%m-%d %H:%M:%S'), myid)

		cursor2.execute(sql2)
		db.commit()
	
	db.close()

def defineBoxStatus():
	db = MySQLdb.connect(myWebAppDb,"irait_control","irait","irait_control")
    	cursor = db.cursor()

	sql = "SELECT ID FROM box WHERE enabled = 1"
	cursor.execute(sql)
	result = cursor.fetchall()

	box_list= []
	for row in result:
		myid = int(row[0])
		box_list.append(myid)

	
	for i in box_list:

		sql = "SELECT status, el_value FROM active_element WHERE ID in (SELECT active_element_id FROM active_element_box WHERE box_id = "+str(i)+") AND category = 'CLIMA' and el_type_logic = 'AI' and status != 'DISABLED'"
		cursor.execute(sql)
		result = cursor.fetchall()
		sumTemp=0.
		cnt=0.
		finalStatus = 1
		for row in result:
			myStatus = str(row[0])
			myVal = float(row[1])

			sumTemp += myVal
			cnt +=1.
			
			if myStatus == 'ALARM':
				finalStatus = 3
			elif myStatus == 'WARNING' and finalStatus == 1:
				finalStatus = 2
		if cnt > 0:
			finalTemp = sumTemp / cnt
		else:
			finalTemp = 0	

		sql = "UPDATE box SET status = %i, last_update = '%s', temperature = %.1f WHERE ID = %i " % (finalStatus, dt.now().strftime('%Y-%m-%d %H:%M:%S'), finalTemp, i)	

		cursor.execute(sql)
		db.commit()	
	
	db.close()

while True:
	ABB.check(myModbusServer, 8000, myWebAppDb)
	box6.check(myModbusServer, 8004, myWebAppDb)
	mtme.check(myModbusServer, 8008, myWebAppDb)
	box5.check(myModbusServer, 8012, myWebAppDb)
	box4.check(myModbusServer, 8016, myWebAppDb)
	defineBoxStatus()
	pingIpDevices()
	time.sleep(50)
