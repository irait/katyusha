import check_box0_plc_1 as ABB
import check_box5_plc_2 as box5
import check_box6_plc_3 as box6
import check_mtme_plc_4 as mtme

import MySQLdb
from datetime import datetime as dt

import time
import os
import socket
import json

myWebAppDb = "localhost"
myModbusServer = "10.10.18.19"


# { "function" : "W_REGISTER", "address" : "0x1093", "value": 5689} 
# { "function" : "W_BIT", "address" : "0x1093", "value": 1} 

def sendModbusCommand(port, write_type, plc_address, value):
	
	BUFFER_SIZE = 1024
	#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #s.connect((myModbusServer, port))

	MESSAGE = '{"function":"%s","address":"%s","value":%d}' % (write_type, plc_address, value)
	print "port ", port, "write type ", write_type, " plc_address ", plc_address, " value ", value	
	print "MESSAGE "+MESSAGE

        #s.send(MESSAGE)
        #data = s.recv(BUFFER_SIZE)
        #j = json.JSONDecoder()
        #jobj = j.decode(data[0:-1])
        #if jobj.has_key('error'):
        #    raise RuntimeError(jobj['error'])
	
	#return_val = jobj['value']
	#s.close()

	return_val = str(value)

	if (return_val == str(value)):
		return 1
	else:
		return 2
	

def checkCommands():
	db = MySQLdb.connect(myWebAppDb,"irait_control","irait","irait_control")
    	cursor1 = db.cursor()
	cursor2 = db.cursor()

	sql1 = "SELECT ID, plc_address, plc_id, new_value, active_element_id, name, old_value FROM commands WHERE done = 0"
	cursor1.execute(sql1)
	result = cursor1.fetchall()

	for row in result:
		myid = int(row[0])
		myplcaddress = str(row[1])
		myplcid      = int(row[2])
		myval        = float(row[3])
		myactive_el_id = int(row[4])
		mycmdname    = str(row[5])
		myoldval       = float(row[6])

		myPort=0

		if (myplcid == 1):
			myPort = 8000
			keyWrite = "W_BIT"
		elif (myplcid == 2):
			myPort = 8012
			keyWrite = "W_REGISTER"
		elif (myplcid == 3):
			myPort = 8004
			keyWrite = "W_REGISTER"
		
		if (myPort != 0):
			resp_done = sendModbusCommand(myPort, keyWrite, myplcaddress, myval)

			sql2 = "UPDATE commands SET done = %i, date_execution = '%s' WHERE ID = %i " % (resp_done, dt.now().strftime('%Y-%m-%d %H:%M:%S'), myid)	

			cursor2.execute(sql2)
			db.commit()
			
			#run frequent update
			i = 3
			while i > 0: 
				i -= 1
				time.sleep(1)
				if (myplcid == 1):
					ABB.check(myModbusServer, 8000, myWebAppDb)
				elif (myplcid == 2):
					box5.check(myModbusServer, 8012, myWebAppDb)
				elif (myplcid == 3):
					box6.check(myModbusServer, 8004, myWebAppDb)
				
				
			#insert event in db
			myCmdDesc = "%s From %.1f to %.1f" % (mycmdname, myoldval, myval)

			sql3 = "INSERT INTO events (ev_date, ev_class, ev_name, ev_active_el_id) VALUES ('%s', 'COMMAND', '%s', %i)" % (dt.now().strftime('%Y-%m-%d %H:%M:%S'), myCmdDesc, myactive_el_id) 	
			cursor2.execute(sql3)
			db.commit()		


			
	db.close()

while True:
	checkCommands()
	time.sleep(1)
