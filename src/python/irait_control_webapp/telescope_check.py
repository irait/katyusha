#!/usr/bin/python

import sys
import numpy as np
import ephem as ep
from TelescopeIRAIT import EPHEM as IRAIT
import rts2.scriptcomm
import time
import datetime as dt
import calendar

import pylab as plt
import pygxccd as gx

import MySQLdb

from PIL import Image
import PIL
import thread



def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')


def getAltAzi(fixedStar):
    IRAIT.date=ep.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)+180.0)%360.)



class G1_thread:
    def __init__(self):
            # init G1 camera for preview
	    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
	    self._cam = gx.GxCCD(3247)
	    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
		self.log('E','cannot connect to camera, exiting...')
		exit(1)

	    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
	    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
	    if(self._chip_d == -1 or self._chip_w == -1):
		self.log('E', 'got wrong parameters from camera, exiting')
		exit(1)


    def gx_exposure(self,exp_time,tries,g1_image_name):
	      image = None
	      e_time = None
	      chip_temp = None
	      for i in range(tries):
		  e_time = calendar.timegm(dt.datetime.now().utctimetuple())-28800

		  if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
		      self.log('W', 'bad start exposure')
		      continue

		  #time.sleep(.1+exp_time)
		  while not self._cam.image_ready():
		      time.sleep(.1)
		      
		  image = self._cam.read_image(self._chip_d,self._chip_w)
		  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
		  if self._cam.is_error():
		      self.log('W', 'ccd read')
		      continue
		  else:
		      break
		  self.log('E','cannot complete the exposure, exiting')
		  #exit(1)

      	      
	      im = np.array(image)
	      j = Image.fromarray((im).astype(np.uint8))
	      j = j.resize((458,348), PIL.Image.ANTIALIAS)
	      j.save("/var/www/html/g1_images/"+str(g1_image_name)+".png")


class Script (rts2.scriptcomm.Rts2Comm):
	
	def __init__(self):
	    rts2.scriptcomm.Rts2Comm.__init__(self)
	    self.g1_th = G1_thread()


	


	
	def run(self):
		
		db = MySQLdb.connect("10.10.18.16","irait_control","irait","irait_control")
		cursor = db.cursor()
   		
		last_g1_exp = calendar.timegm(dt.datetime.now().utctimetuple())-28800
		g1_image_name = 0

		while True:

			g1_exp = 0.3
			g1_delay = 4


			sqlSel = "SELECT ID, name, rts2_name, rts2_category, value FROM tel_el_write WHERE to_write = 1"
			cursor.execute(sqlSel)
			result = cursor.fetchall()

			newalt = None
			newazi = None
		 	newra  = None
			newdec = None

			for row in result:
				myID = row[0]
				name = row[1]
				rts2_name = row[2]
				rts2_category = row[3]				
				value = row[4]

				sqlupd = "UPDATE tel_el_write SET to_write = 0 WHERE ID ="+str(myID)
				cursor.execute(sqlupd)
				db.commit()

				if (rts2_category == 'C1'):
					
					sqlname= ""
					if (name == 'g1_exp_cmd'):
						sqlname ="g1_exp"
					else:
						sqlname= "g1_delay"

					sqlupd = "UPDATE tel_element SET value="+str(value)+" WHERE name = '"+sqlname+"' AND category = 'PREV_CAM'"
					cursor.execute(sqlupd)
					db.commit()
				
				elif (rts2_category == 'C0'):
					if (rts2_name == 'filter'):					
						self.setValue('filter', str(int(value)))
					else:
						self.setValue(rts2_name, str(value))

				elif (rts2_category == 'F0'):
					self.setValue(rts2_name, str(value), 'F0')

				elif (rts2_category == 'T0'):
					if (rts2_name == 'calcEQtoALT'):
						newra = str(value)
 					elif (rts2_name == 'calcEQtoAZI'):
						newdec = str(value)
					elif (rts2_name == 'TEL_[0]'):
						newalt = float(value)
					elif (rts2_name == 'TEL_[1]'):
						newazi = (float(value)+180.)%360.
					else:
						self.setValue(rts2_name, str(value), 'T0')

			

			if ((newra is not None) and (newdec is not None)):
				self.log("I", "calculate new point: %s %s" % (newra, newdec))
				mystar = None
				mystar = ep.FixedBody()
				mystar._ra = newra
				mystar._dec = newdec 

				alt, az = getAltAzi(mystar)
				if (alt > 4.5) and (alt < 76):
					self.log("I", "set new point: %.4f %.4f" % (alt, az))
					self.setValue('TEL_','{} {}'.format(alt, az),'T0')

			elif ((newalt is not None) and (newazi is not None)):
				self.setValue('point','false','T0')
				self.log("I", "set new point: %.4f %.4f" % (newalt, newazi))
				self.setValue('TEL_','{} {}'.format(newalt, newazi),'T0')
				time.sleep(4.)
				self.setValue('point','true','T0')
					

			sqlSel = "SELECT name, value FROM tel_element WHERE category = 'PREV_CAM'"
			cursor.execute(sqlSel)
			result = cursor.fetchall()

			for row in result:
				name = row[0]
				value = row[1]
				if (name == 'g1_exp'):
					g1_exp = float(value)
				if (name == 'g1_delay'):
					g1_delay = float(value)
				


			sql = ""
			alt_err= "00:00:00.00"
			az_err = "00:00:00.00"
			alt_speed = "00:00:00.00"
			az_speed = "00:00:00.00"


 			try:
			    alt_pos, az_pos = self.getValue('position.AA','T0').split()
			    alt_target, az_target = self.getValue('target.AA','T0').split()
			    alt_distance, az_distance = self.getValue('distance','T0').split()
                        except ValueError:
                            self.log("E", "ERROR: try to restart jmodbus, the encoder and rts2")
                            sys.exit(1)

			mount_status = self.getValue('status','T0')
			mount_point  = self.getValue('point','T0')
			mount_PM  = self.getValue('Apply_pointing_model','T0')

			alt_err,az_err=self.getValue('current_error','T0').split()
                        
			alt_speed,az_speed=self.getValue('speed','T0').split()

			pos_tar,min_off,max_off= self.getValue('FOC_TAR', 'F0').split()
			foc_target = float(pos_tar)
			pos_def,min_off,max_off= self.getValue('FOC_DEF', 'F0').split()
			foc_default = float(pos_def)
			pos_off_f,min_off,max_off= self.getValue('FOC_FILTEROFF', 'F0').split()
			foc_filter_off = float(pos_off_f)
        		foc_temp = float(self.getValue('FOC_TEMP', 'F0')) 
			foc_position = float(self.getValue('FOC_POS', 'F0'))
			foc_status = self.getValue('foc_status', 'F0')

			exposureg4, ming4,maxg4 = self.getValue('exposure', 'C0').split() 
			g4_exp = float(exposureg4)

			g4_filter = self.getValue('filter', 'C0')
			g4_shutter = self.getValue('SHUTTER', 'C0')
			

			g4_air_temp = float(self.getValue('CCD_AIR', 'C0'))
			g4_ccd_temp = float(self.getValue('CCD_TEMP', 'C0'))
			g4_ccd_set_s, mins, maxs = self.getValue('CCD_SET', 'C0').split()
			g4_ccd_set = float(g4_ccd_set_s)
			#g4_peltier = float(self.getValue('TEMPPWR', 'C0'))
			#g4_mode = self.getValue('RDOUTM', 'C0')

			g4_status = self.getState('C0')
			g4_exposure_end = self.getValue('exposure_end', 'C0')
			

			t_now = dt.datetime.now()
                        g4_infotime  = calendar.timegm(t_now.utctimetuple()) -28800

		 	#mount_status = 'POINTING'

                        #self.log("I", "%s" % g4_status)
			if ('POINTING' == mount_status):

			    # acquire G1 preview
			    if ((g4_infotime - last_g1_exp) > g1_delay):
				    last_g1_exp = calendar.timegm(dt.datetime.now().utctimetuple())-28800
		                    thread.start_new_thread(self.g1_th.gx_exposure, (g1_exp,1,g1_image_name))
			
				    if (g1_image_name < 4):
					g1_image_name += 1
				    else:
					g1_image_name = 0
			 
			
			sql = "UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (alt_pos, "alt_position")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (az_pos, "azi_position")
			cursor.execute(sql)			
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (alt_target, "alt_target")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (az_target, "azi_target")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (alt_distance, "alt_distance")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (az_distance, "azi_distance")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (alt_err, "alt_motor_error")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (az_err, "azi_motor_error")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (alt_speed, "alt_speed")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (az_speed, "azi_speed")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (mount_status, "mount_status")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (mount_point, "mount_point")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (mount_PM, "mount_PM")
			cursor.execute(sql)

			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_exp, "g4_exp")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_filter, "g4_filter")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_shutter, "g4_shutter")
			cursor.execute(sql)
			
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_air_temp, "g4_air_temp")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_ccd_temp, "g4_ccd_temp")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_ccd_set, "g4_ccd_set")
			cursor.execute(sql)
			#sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_peltier, "g4_peltier")
			#cursor.execute(sql)
			#sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_mode, "g4_mode")
			#cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_status, "g4_status")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_exposure_end, "g4_exposure_end")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (g4_infotime, "g4_infotime")
			cursor.execute(sql)

			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_position, "foc_position")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_target, "foc_target")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_default, "foc_default")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_filter_off, "foc_filter_off")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_temp, "foc_temp")
			cursor.execute(sql)
			sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % (foc_status, "foc_status")
			cursor.execute(sql)

			#sql = " UPDATE tel_element SET value = '%s' WHERE name = '%s'; " % ()	

                        
			time.sleep(.5)
			db.commit()    


                        #t_now = dt.datetime.now()
                        #tmstamp = calendar.timegm(t_now.utctimetuple())
                        #tmstamp += (t_now.microsecond * 0.000001)


			
			
			   	
    		

a = Script()
a.run()
