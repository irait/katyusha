import os
from itm_cli.catalog.sources import Source
from itm_cli.config.config import OBSERVATION_PATH


PATH = os.path.join(OBSERVATION_PATH, 'agb')


calibrator = {
    'source': Source(
        'kaus_australis',
         ('18:24:10.25', '-34:23:06.8'), 0.0, 0.0, 2000,
    ),
    'expositions': {
        'C': 0.03,
        'I': 5.0,
        'R': 3.0,
        'V': 2.0,
        'B': 4.0,
        'U': 9.0,
        'H': 9.0,
    }
}


targets = (
    Source('V401', ('18:28:53.9', '-14:29:18'), 0.0, 0.0, 2000),
    Source('V412', ('18:29:58.5', '-14:10:13'), 0.0, 0.0, 2000),
    Source('V418', ('18:30:28.7', '-14:21:35'), 0.0, 0.0, 2000),
    Source('GP', ('18:09:09.7', '-15:51:20'), 0.0, 0.0, 2000),
    Source('GR', ('18:09:24.0', '-15:19:31'), 0.0, 0.0, 2000),
    Source('GQ', ('18:09:18.2', '-14:38:08'), 0.0, 0.0, 2000),
    Source('FY', ('18:07:54.0', '-14:31:26'), 0.0, 0.0, 2000),
    Source('V405', ('18:29:22.7', '-15:07:58'), 0.0, 0.0, 2000),
    Source('V421', ('18:30:51.3', '-15:37:28'), 0.0, 0.0, 2000),
    Source('V424', ('18:31:36.1', '-15:26:54'), 0.0, 0.0, 2000),
    Source('V422', ('18:31:25.0', '-15:23:19'), 0.0, 0.0, 2000),
    Source('V425', ('18:31:41.7', '-15:12:12'), 0.0, 0.0, 2000),
)


def get_sources():
    sources = []
    for source in targets:
        sources.append(source.name)
    return tuple(sources)


def get_filters():
    return tuple(calibrator['expositions'].keys())
