from itm_cli.catalog.sources import Source
import ephem

jupiter = ephem.Jupiter()
mars = ephem.Mars()
mercury = ephem.Mercury()
moon = ephem.Moon()
neptune = ephem.Neptune()
saturn = ephem.Saturn()
sun = ephem.Sun()
uranus = ephem.Uranus()
venus = ephem.Venus()
