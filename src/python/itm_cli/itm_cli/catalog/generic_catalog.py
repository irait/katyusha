from itm_cli.catalog.sources import Source

canopus = Source('canopus', ('6:23:57.2', '-52:41:43.9'), 0.0, 0.0, 2000)
procyon = Source('procyon', ('07:39:17.2', '05:13:10.9'), 0.0, 0.0, 2000)
sirius = Source('sirius', ('06:45:08.1', '-16:43:21.5'), 0.0, 0.0, 2000)
kaus_australis = Source('kaus_australis', ('18:24:10.25', '-34:23:06.8'), 0.0, 0.0, 2000)
rigel = Source('rigel', ('05:14:32.3', '-08:12:05.9'), 0.0, 0.0, 2000)
mirzam = Source('mirzam', ('06:22:42.0', '-17:57:59.0'), 0.0, 0.0, 2000)
biham = Source('biham', ('22:10:12.2', '+06:11:53.6'), 0.0, 0.0, 2000)
biham = Source('biham', ('22:10:12.2', '+06:11:53.6'), 0.0, 0.0, 2000)
miaplacidus = Source('miaplacidus', ('+9:13:11.3', '-69:43:02.7'), 0.0, 0.0, 2000)
atria = Source('atria', ('+16:48:39.9', '-69:01:40.4'), 0.0, 0.0, 2000)
hip_43908 = Source('hip_43908', ('+8:56:39.2', '-85:39:48.4'), 0.0, 0.0, 2000)
ic_5197 = Source('ic_5197', ('+22:19:48.0', '-60:08:00.0'), 0.0, 0.0, 2000)
