import datetime
from math import radians

import numpy as np
import ephem
from astropy import units as u
from astropy.coordinates import SkyCoord, Angle


class ITMObserver(ephem.Observer):

    def __init__(self):
        ephem.Observer.__init__(self)
        self.lon = radians(123.299416)
        self.lat = radians(-75.099814)
        self.elevation = 3237
        self.pressure = 650
        now = datetime.datetime.utcnow()
        if 3 <= now.month <= 4:
            self.temp = -60
        elif 5 <= now.month <= 8:
            self.temp = -70
        else:
            self.temp = -40
        self.date = ephem.date(datetime.datetime.utcnow())


class Source(object):

    def __init__(self, name, coordinates, ra_motion=0, dec_motion=0, equinox=2000):
        """
        name: the source name
        coordinates: (ra, dec). For instance: ('6:23:57.2', '-52:41:43.9')
        dec_motion: seconds per year
        ra_motion: arcseconds per year
        equinox: an integer (2000 for J2000)
        """
        self.name = name
        ra, dec = coordinates
        # Compute the updated coordinates, taking in account the motion per year
        now = datetime.datetime.utcnow()
        years = now.year + now.month/12.0 - equinox 
        ura_motion_str = '0:0:%f' % abs(ra_motion * years)
        ra_motion_str = '-%s' % ura_motion_str if ra_motion <=0 else ura_motion_str 
        udec_motion_str = '0:0:%f' % abs(dec_motion * years)
        dec_motion_str = '-%s' % udec_motion_str if dec_motion <=0 else udec_motion_str 
        today_ra = Angle(ra, 'hour') + Angle(ra_motion_str, 'hour')
        today_dec = Angle(dec, 'deg') + Angle(ra_motion_str, 'deg')
        self.coordinates = SkyCoord(
            ra=today_ra.to_string(sep=':'),
            dec=today_dec.to_string(sep=':'),
            unit=(u.hourangle, u.deg)
        )
        self.observer = ITMObserver()

    def ra_dec(self, time_=None, azi=None, alt=None):
        self.observer.date = ephem.date(time_ or datetime.datetime.utcnow())
        if azi is None or alt is None:
            alt, azi = self.alt_az()
        return self.observer.radec_of(radians((azi+180)%360), radians(alt))

    def alt_az(self, alt_offset=0, az_offset=0, time_=None):
        body = ephem.FixedBody()
        body._ra = self.coordinates.ra.to_string(unit=u.hourangle, sep=':')
        body._dec = self.coordinates.dec.to_string(unit=u.deg, sep=':')
        self.observer.date = ephem.date(time_ or datetime.datetime.utcnow())
        body.compute(self.observer)
        alt = np.rad2deg(body.alt + radians(alt_offset))
        az = (np.rad2deg(body.az + radians(az_offset)) + 180.0) % 360
        return alt, az
