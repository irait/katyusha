# -*- coding: utf-8 -*-
"""
    itm_cli.cli
    ~~~~~~~~~~~

    A simple command line application to interact with ITM.

    :copyright: (c) 2018 by Marco Buttu.
"""
from __future__ import print_function

import subprocess
import shelve
import os
import sys
import cmd
from datetime import datetime
from itm_cli.config.config import (
    SHELVE_FILE, HISTORY_FILE, CMD_DIR, PM_DIR, CATALOG_FILE
)


class ArgumentType(object):

    def __init__(self, type_, required=False, default=None):
        self.type = type_
        self.required = required
        self.default = default


class ITMShell(cmd.Cmd):
    intro = "Welcome to the ITM command line interface.\n"
    prompt = 'ITM > '

    def __init__(self):
        cmd.Cmd.__init__(self)

    def precmd(self, line):
        """Save the line to the history file before parsing the command."""
        date = '{0:%Y/%b/%d %H:%M:%S}'.format(datetime.now())
        with open(HISTORY_FILE, 'a') as history:
            history.write(date + ' -> ' + line + '\n')
        return line

    def do_goto(self, line):
        """Take the source name as argument and go to the source.

        Example:

        > goto source=canopus

        The source argument can also take equatorial coordinates:

        > goto source=(6:23:57.2,-52:41:43.9)

        Pay attention to not put some spaces after the comma.
        You can also specify the altitude offset and the azimuth offset.
        For instance, in the following case we add to the Canopus coordinates
        a 0.02 altitude offset and a 0.03 azimuth offset:

        > goto source=canopus alt_offset=0.02 az_offset=0.03
        """
        command_name = 'goto'
        allowed_args = {
            'source': ArgumentType(str, required=True),
            'alt_offset': ArgumentType(float, required=False, default=0),
            'az_offset': ArgumentType(float, required=False, default=0),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_autofocus(self, line):
        """Perform the autofocus by calculating the optimal M2 position.

        Example:

        > autofocus exposition=0.25

        You can also specify some optional arguments:

        > autofocus exposition=0.25 step=0.2 central_position=3 distance=4 apply=yes

        The optional arguments are:

        * step: default=0.2 mm
        * central_position: default=2.2 mm
        * distance: default=3.0
        * shoots: default=1
        * filter: default='C'. Allowed values: 'C', 'I', 'R', 'V', 'B', 'U', 'H'
        * apply: default=no

        The filter value 'C' means Clear (no filter, all band). When `apply` is `yes`,
        the optimal M2 position will be set.
        """
        command_name = 'autofocus'
        allowed_args = {
            'exposition': ArgumentType(str, required=True),
            'step': ArgumentType(float, required=False, default=0.2),
            'central_position': ArgumentType(float, required=False, default=2.2),
            'distance': ArgumentType(float, required=False, default=3),
            'shoots': ArgumentType(int, required=False, default=1),
            'filter': ArgumentType(str, required=False, default='C'),
            'apply': ArgumentType(str, required=False, default='no'),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_set(self, line):
        """Set a value.

        Example:

        > set filter=R
        """
        command_name = 'set'
        allowed_args = {
            'filter': ArgumentType(str, required=False, default=''),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_map(self, line):
        """Make a map around a source.

        For example, in the following case the telescope will
        follow Canopus. `dimension=3` means that it will create a
        3x3 matrix centered in the source:

        > map exposition=10 source=canopus dimension=3

        It will shoot 9 images: one centered in the source,
        one on the left of the source, one on the right,
        one line above the source and one line below.  You can
        also choose the filter:

        > map exposition=10 source=canopus dimension=3 filter=I
        """
        command_name = 'map'
        allowed_args = {
            'exposition': ArgumentType(float, required=True),
            'dimension': ArgumentType(int, required=True),
            'source': ArgumentType(str, required=True),
            'filter': ArgumentType(str, required=False, default='C'),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_setup(self, line):
        """Perform a setup of the telescope: set the CCD temperature,
        set the default focus position.

        > setup
        """
        try:
            self.call_script('setup')
        except Exception, ex:
            print(ex)

    def do_stop(self, line):
        """Stop the mount.

        > stop
        """
        try:
            self.call_script('stop')
        except Exception, ex:
            print(ex)

    def do_track(self, line):
        """Track the source addressed by `goto`, in case the mount
        has been stopped.

        > track
        """
        try:
            self.call_script('track')
        except Exception, ex:
            print(ex)

    def do_park(self, line):
        """Park the telescope: stop the mount and warm the CCD up.

        > park
        """
        try:
            self.call_script('park')
        except Exception, ex:
            print(ex)

    def do_shoot(self, line):
        """Take the exposition (in seconds) and shoot an image:

        Example:

        > shoot exposition=2

        You can also specify the filter (default is R (red), the number
        of shoots (default is 1), the file_name (dafault is 'image', and the extension
        '.fits' will be appended automatically), and the directory where to save the
        images (default is $HOME/images): 

        > shoot exposition=2 filter=U n=3 file_name=foo dir=/home/irait/foo

        The allowed values for `filter` are: 'C', 'I', 'R', 'V', 'B', 'U', 'H'.
        The filter value 'C' means Clear (no filter, all band).
        There is also a `distance` parameter, and it is the distance in seconds
        between the shoots.  For instance, if you want a sleep time of 60 seconds
        between each shoot:

        > shoot exposition=2 distance=60
        """
        command_name = 'shoot'
        allowed_args = {
            'exposition': ArgumentType(float, required=True),
            'filter': ArgumentType(str, required=False, default='R'),
            'n': ArgumentType(int, required=False, default=1),
            'file_name': ArgumentType(str, required=False, default='image'),
            'dir': ArgumentType(str, required=False, default=''),
            'distance': ArgumentType(int, required=False, default=1),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_pmoffsets(self, line):
        """Take a directory containing some stars images and create a file with
        the input coordinates for TPoint, the program used to create the telescope
        pointing model.  Example:

        > pmoffsets

        The default directory is /home/irait/tpoint/shoots. If you want specify
        a differente dir:

        > pmoffsets dir=/foo/mydir

        The name of the file image will be the ID of the star in the output file.
        Therefore that name should be the same used in your TPoint catalog.
        """
        command_name = 'pmoffsets'
        allowed_args = {
            'dir': ArgumentType(str, required=False, default=PM_DIR),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_pmscan(self, line):
        """Take the exposition of the camera as argument and make a scan of the sky,
        in order to save the FITS file required to compute the pointing model offsets.

        Example:

        > pmscan exposition=0.1

        The optional argument `catalog` indicates a file of sources.  The default
        catalog is itmctg.dat.  The optional argument `starting_star` indicates
        the star in the catalog from which pmscan will start the observation, and the
        argument `ending_star` the one to stop.
        You can also specify the camera filter.  The default value is R (red):

        > pmscan catalog=FOO.DAT exposition=0.1 filter=B starting_star=10 ending_star=20

        The allowed values for `filter` are: 'C', 'I', 'R', 'V', 'B', 'U', 'H'.
        The filter value 'C' means Clear (no filter, all band).
        You can also do a map around the star, using the `star_map` optional argument:
   
        > pmscan exposition=1 star_map=3
        
        It will do a matrix 3x3 centered on the star.
        """
        command_name = 'pmscan'
        allowed_args = {
            'exposition': ArgumentType(str, required=True),
            'catalog': ArgumentType(str, required=False, default=CATALOG_FILE),
            'filter': ArgumentType(str, required=False, default='C'),
            'starting_star': ArgumentType(int, required=False, default=1),
            'ending_star': ArgumentType(int, required=False, default=1000),
            'star_map': ArgumentType(int, required=False, default=None),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_observe(self, line):
        """Run a schedule containing the observation to be performed.

        Example:

        > observe catalog=agb out_dir=2018_obs02
        
        The `focus_position` argument indicates the central position of F0
        for the autofocus algorithm. Default falue is 1.8.
        The `focus_exp_factor` is the factor the autofocus exposition (declared
        in the catalog) will be multiplied by. The default value is 1.
        An example using these arguments:

        > observe catalog=agb out_dir=2018_obs02 focus_position=2.2 focus_exp_factor=2

        You can also indicate when the script has to start and stop. For instance, if
        you want the observation to start from star V418 and to be interrupted after
        observing the star V425:

        > observe catalog=agb out_dir=2018_obs02 start_from=V418 stop_at=V425
        """
        command_name = 'observe'
        allowed_args = {
            'catalog': ArgumentType(str, required=True),
            'out_dir': ArgumentType(str, required=True),
            'focus_position': ArgumentType(float, required=False, default=1.8),
            'focus_exp_factor': ArgumentType(int, required=False, default=1),
            'start_from': ArgumentType(str, required=False, default='no'),
            'stop_at': ArgumentType(str, required=False, default='no'),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(args['catalog'])
        except Exception, ex:
            print(ex)

    def do_cleanfits(self, line):
        """Clean the FITS files applying the dark and flat images.

        Example:

        > cleanfits catalog=agb observation_dir=2018_obs02 
        
        The catalog is an optional argument with 'agb' as a default value.
        """
        command_name = 'cleanfits'
        allowed_args = {
            'catalog': ArgumentType(str, required=False, default='agb'),
            'observation_dir': ArgumentType(str, required=True),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name)
        except Exception, ex:
            print(ex)

    def do_rotate(self, line):
        """Rotate a group of FITS files in order to get a single image.

        Example:

        > rotate catalog=agb observation_dir=2018_obs02 
        
        The catalog is an optional argument with 'agb' as a default value.

        It is also possible to specity the stars to derotate and the filters:

        > rotate observation_dir=2018_obs02 stars=V418,GP filters=C,B

        To default it rotates all stars and filters.
        """
        command_name = 'rotate'
        allowed_args = {
            'catalog': ArgumentType(str, required=False, default='agb'),
            'observation_dir': ArgumentType(str, required=True),
            'stars': ArgumentType(str, required=False, default=''),
            'filters': ArgumentType(str, required=False, default=''),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name, rts2_exec=False)
        except Exception, ex:
            print(ex)

    def do_pmaz(self, line):
        """This command computes the azimuth parameters of the pointing model.

        Example:

            ITM > pmaz
            long double aa0 = -0.979736237408;
            long double aa1 = -0.00575685573437;
            long double aa3 = 2.08278756432e-06;
            long double aa4 = -1.43171360762e-08;
            long double aa5 = 3.36210872206e-11;
            long double aa6 = -2.65907339547e-14;
            long double acec = 0.00725602001729;
            long double ae5 = 1.11471813957e-10;
            long double aes = -0.865971596275;
            long double aes_delta_azr = -14.7659261475;
            long double aes_delta_elr = 1.39012652429;
        
        You have to copy these lines in the RTS2 file teled/irait.cpp,
        inside the applyGpoint() function, replacing the lines located
        above the definition of `delta_az`.
        """
        command_name = 'pmaz'
        allowed_args = {}
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name, rts2_exec=False)
        except Exception, ex:
            print(ex)

    def do_pmel(self, line):
        """This command computes the elevation parameters of the pointing model.

        Example:

            ITM > pmel
        
        You have to copy these lines in the RTS2 file teled/irait.cpp,
        inside the applyGpoint() function, replacing the lines located
        above the definition of `delta_el`.
        """
        command_name = 'pmel'
        allowed_args = {}
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name, rts2_exec=False)
        except Exception, ex:
            print(ex)

    def do_show(self, line):
        """Show the stars/filters that have been cleaned or rotated.

        Example:

        > show what=rotated catalog=agb observation_dir=2018_obs02 
        
        The argument 'what' is mandatory and could be 'rotated' or 'cleaned'.
        In case it is 'rotated' than the 'show' command will show all stars/filters
        already rotated, otherwise it will show the stars/filters already cleaned.
        The catalog is an optional argument with 'agb' as a default value.
        It is also possible to specity the stars and the filters to show:

        > show what=cleaned observation_dir=2018_obs02 stars=V418,GP filters=C,B

        To default it shows all stars and filters.
        
        The optional argument 'verbose' shows also the stars and filters not
        rotated and not cleaned.
        """
        command_name = 'show'
        allowed_args = {
            'what': ArgumentType(str, required=True),
            'catalog': ArgumentType(str, required=False, default='agb'),
            'observation_dir': ArgumentType(str, required=True),
            'stars': ArgumentType(str, required=False, default=''),
            'filters': ArgumentType(str, required=False, default=''),
            'verbose': ArgumentType(str, required=False, default='no'),
        }
        try:
            args = self.check_args(command_name, line, allowed_args)
            self.save_args(command_name, args)
            self.call_script(command_name, rts2_exec=False)
        except Exception, ex:
            print(ex)

    def check_args(self, command_name, line, allowed_args):
        """Return the arguments given to the command `command_name`.

        This method takes the line and the allowed arguments dictionary.
        It prints an error in case the line has a wrong number of arguments,
        otherwise it returns the arguments as a dictionary {name: value}."""
        method_name = 'do_%s' % command_name
        method = getattr(self.__class__, method_name)
        docstring_lines = method.__doc__.split('\n')
        docstring = '\n'.join([dline.strip(' ') for dline in docstring_lines])
        required_args = []
        for name, value in allowed_args.items():
            if value.required:
                required_args.append(name)
        args = {}
        tokens = line.split()
        for token in tokens:
            try:
                name, value = [item.strip() for item in token.split('=')]
            except ValueError:
                msg = "ERROR: specify arg_name=value.\nUsage: %s" % docstring
                raise ValueError(msg)

            if name not in allowed_args:
                msg = "ERROR: argument '%s' unknown.\nUsage: %s" % \
                      (name, docstring)
                raise ValueError(msg)
            else:
                type_ = allowed_args[name].type
                args[name] = type_(value)

        for arg in required_args:
            if arg not in args:
                msg = "ERROR: missing argument '%s'.\nUsage: %s" % \
                      (arg, docstring)
                raise ValueError(msg)

        for name, value in allowed_args.items():
            if not value.required and not name in args:
                args[name] = value.default
        return args

    def save_args(self, command_name, args):
        """Save the list of arguments to the shelve file."""
        db = shelve.open(SHELVE_FILE)
        db[command_name] = args
        db.close()

    def call_script(self, command_name, rts2_exec=True):
        """Call the script that has the same name of the command."""
        script = os.path.join(CMD_DIR, '%s.py' % command_name)
        try:
            if rts2_exec:
                subprocess.call(
                    'rts2-scriptexec -d C0 -s " exe %s"' % script,
                    shell=True
                )
            else:
                subprocess.call(
                    'python %s' % script,
                    shell=True
                )
        except KeyboardInterrupt:
            print

    def do_quit(self, line):
        """Quit the program."""
        print('\n')
        sys.exit(0)

    def emptyline(self):
        """Do nothing in case of empty line."""

    def do_EOF(self, line):
        """CTR-D to exit from the program."""
        self.do_quit(line)

    do_q = do_quit

    def default(self, line):
        """Manage the commands not allowed."""
        print("ERROR: command '%s' unknown" % line)

    def help_default(self):
        print(self.default.__doc__)


if __name__ == '__main__':
    itm_shell = ITMShell()
    itm_shell.cmdloop()
