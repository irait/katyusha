#!/usr/bin/python

import os
from datetime import datetime
from collections import namedtuple

from itm_cli.utils import (
    RTS2Wrapper, Camera, get_source, get_catalog, script_args
)
from itm_cli.commands.goto import GoTo
from itm_cli.commands.autofocus import Autofocus
from itm_cli.commands.park import Park
from itm_cli.commands.stop import Stop
from itm_cli.config.config import MIN_EL


Filter = namedtuple(
    'Filter',
    ['name', 'shoots', 'exposition']
)
filters = (
    Filter('B', shoots=20, exposition=120),
    Filter('V', shoots=20, exposition=60),
    Filter('R', shoots=20, exposition=60),
    Filter('I', shoots=20, exposition=60),
    Filter('C', shoots=15, exposition=60),
)


class ObserveAGB(RTS2Wrapper):
	
    def __init__(self, catalog, out_dir, focus_position, focus_exp_factor, start_from, stop_at):
        RTS2Wrapper.__init__(self)
        catalog_module = get_catalog('%s_catalog' % catalog)
        self.calibrator = catalog_module.calibrator
        self.targets = catalog_module.targets
        self.out_dir = os.path.join(catalog_module.PATH, out_dir)
        self.central_focus_position = focus_position
        self.focus_exp_factor = focus_exp_factor
        self.start_from = start_from
        self.stop_at = stop_at
        if not os.path.exists(self.out_dir):
            os.mkdir(self.out_dir)

    def run(self):
        self.log('I', 'Running the AGB script ...')
        calibrator_source = self.calibrator['source']
        starting_found = True if self.start_from == 'no' else False
        for target in self.targets:
            if target.name == self.start_from:
                starting_found = True 
            if not starting_found:
                continue
            self.log('I', 'Target to observe: %s' % target.name)
            now = str(datetime.now()).replace(' ', '_')
            target_dir = os.path.join(self.out_dir, target.name)
            if not os.path.exists(target_dir):
                os.mkdir(target_dir)
            for filter in filters:
                filter_dir = os.path.join(target_dir, filter.name)
                if not os.path.exists(filter_dir):
                    os.mkdir(filter_dir)
                files = [f for f in os.listdir(filter_dir) if not f.startswith('elab')]
                if len(files) < filter.shoots:
                    starting_filter_index = len(files)
                    rest = filter.shoots - len(files)
                    self.log('I', 'Remaining shoots for filter %s: %d' % (filter.name, rest))
                else:
                    self.log('I', 'Target already observed with filter %s' % filter.name)
                    continue
                calibrator_exposition=self.calibrator['expositions'][filter.name]
                self.log('I', 'Going to the calibrator %s.' % calibrator_source.name)
                goto_calibrator = GoTo(calibrator_source)
                goto_calibrator.run()
                self.log('I', 'Performing the autofocus for filter %s' % filter.name)
                autofocus_script = Autofocus(
                    exposition=calibrator_exposition*self.focus_exp_factor,
                    step=0.10,
                    central_position=self.central_focus_position,
                    distance=2.6,
                    shoots=1,
                    filter=filter.name,
                    apply='yes',
                )
                try:
                    autofocus_script.run()
                    goto_target = GoTo(target)
                    goto_target.run()
                except ValueError, ex:
                    self.log('I', str(ex))
                    continue

                if not os.path.exists(filter_dir):
                    os.mkdir(filter_dir)
                for i in range(starting_filter_index, filter.shoots):
                    el, az = [float(item) for item in self.getValue('position.AA', 'T0').split()]
                    if el < MIN_EL:
                        stop_script = Stop()
                        stop_script.run()
                        break
                    camera = Camera(self, filter_dir, False)
                    file_name = os.path.join(target_dir, filter_dir, 'G4_%s_%02d' % (filter.name, i))
                    fits_file = camera.shoot(filter.exposition, file_name)
            if target.name == self.stop_at:
                self.log('I', '%s is the last star to be observed.' % self.stop_at)
                break

        park_script = Park()
        park_script.run()


if __name__ == '__main__':
    kargs = script_args('observe')
    script = ObserveAGB(**kargs)
    script.run()
