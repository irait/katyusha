#! /usr/bin/python
from __future__ import division

import os
import sys
import time
import datetime as dt
from operator import itemgetter

import cv2 
import numpy as np
from astropy.io import fits

from itm_cli.utils import Camera, script_args, RTS2Wrapper
from itm_cli.config.config import AUTOFOCUS_DIR


class Autofocus(RTS2Wrapper):

    def __init__(self,
            exposition,
            step,
            central_position,
            distance,
            shoots,
            filter,
            apply):
        RTS2Wrapper.__init__(self)
        self.exposition = exposition
        self.step = step
        self.central_position = central_position
        self.distance = distance
        self.shoots = shoots  # Number of images per point
        self.filter = filter
        self.apply = apply
        self.camera = Camera(self, AUTOFOCUS_DIR)
        if filter != 'all' and filter not in self.camera.filters:
            raise ValueError('Filter %s not allowed' % filter)

    def shoot(self, file_name):
        self.camera.shoot(self.exposition, file_name)

    def focus_fit(self):
        DIR = self.camera.DESTINATION 
        fits_files = []
        for file in os.listdir(DIR):
            if file.endswith('.fits'):
                fits_files.append(os.path.join(DIR, file))
        fits_files.sort()
        HIGH_THRESHOLD = 20000
        LOWER_THRESHOLD = 10000
        PX_THRESHOLD = 150  # px
        max_value = 0 
        best_position = None
        for file in fits_files:
            self.log('I', 'Opening file FITS: %s' % file)
            try:
                hdulist = fits.open(file)
                focus_pos = hdulist[0].header['FOC_POS']
                image_data = hdulist[0].data
                if image_data.max() < HIGH_THRESHOLD:
                    self.log('I', 'Maximum value lower than threshold')
                    continue
                num_pixels = 0
                num_grey_pixels = 0
                total_counts = 0
                coordinates = []
                x, y = image_data.shape
                # We suppose the calibrator is almost centered, so we analyze
                # only the central part of the image
                imgray = image_data.astype(np.float32)
                imgray_blurred = cv2.GaussianBlur(imgray, (41, 41), 0)
                min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(imgray_blurred)
                x_max, y_max = max_loc
                radius = 200
                # radius px before and after the maximum
                x_start = x_max - radius if x_max - radius > 0 else 0
                x_end = x_max + radius if x_max + radius < x else x
                y_start = y_max - radius if y_max - radius > 0 else 0
                y_end = y_max + radius if y_max + radius < y else y
                for i in range(y_start, y_end):
                    for j in range(x_start, x_end):
                        counts = image_data[i, j]
                        if counts > HIGH_THRESHOLD:
                            coordinates.append((counts, (i, j)))
                            total_counts += counts
                            num_pixels += 1
                        elif LOWER_THRESHOLD < counts < HIGH_THRESHOLD:
                            num_grey_pixels += 1
                min_num_pixels = 10
                if num_pixels < min_num_pixels:
                    self.log('I', 'Number of pixels lower than %d' % min_num_pixels)
                    continue
                else:
                    coordinates.sort(key=itemgetter(0), reverse=True)
            finally:
                hdulist.close()
            peacks = coordinates[:1600]  # 40x40 px area
            # In case of no pixels?
            talt = tazi = tcounts = 0  # Total alt, azi and counts
            for _, pixel in peacks:
                alt, azi = pixel
                talt += alt
                tazi += azi
            alt_mean, azi_mean = talt/len(peacks), tazi/len(peacks)
            max_distance = 0
            for counts, pixel in peacks:
                alt, azi = pixel
                alt_distance = abs(alt - alt_mean)
                azi_distance = abs(azi - azi_mean)
                distance = max(alt_distance, azi_distance)
                if distance > PX_THRESHOLD:
                    continue
                elif distance > max_distance:
                    max_distance = distance
        
            if num_grey_pixels == 0:
                self.log('I', 'Skipping: no gray pixels')
                continue
            elif max_distance == 0:
                self.log('I', 'Skipping: max distance is 0')
                continue
            else:
                counts_mean = total_counts/num_pixels
                # On focus means maximum metrics
                metrics = num_pixels/(num_grey_pixels * max_distance**2) * counts_mean
                if metrics > max_value:
                    max_value = metrics
                    best_position = focus_pos
        
        if best_position:
            self.log('I', 'Best focus position: %.2f' % best_position)
        else:
            self.log('I', 'Can not perform the autofocus: star not found')
        return best_position

    def focsleep(self):
        self.log('I', 'Waiting for M2 to reach the target position')
        timeout = 180
        starting_time = dt.datetime.now()
        while True:
            try:
                current_focus_position = float(self.getValue('FOC_POS', 'F0'))
            except ValueError:
                self.log('E', 'ERROR: cannot get the focus position.')
                sys.exit(1)
            time.sleep(1)
            target_str = self.getValue('FOC_TAR', 'F0')
            target = float(target_str.split()[0])

            if abs(target - current_focus_position) < 0.02:
                break
            elif self.getValue('foc_status', 'F0') == 'STOP':
                self.setValue('FOC_TAR', str(target+0.01), 'F0')
            time.sleep(2)
            if (dt.datetime.now() - starting_time).seconds > timeout:
                self.log('E', 'ERROR: cannot reach the focus position: timeout reached')
                sys.exit(1)
        self.log('I', 'M2 current position: %.2f' % float(self.getValue('FOC_POS', 'F0')))
        self.log('I', 'Current filter: %s' % self.filter)

    def focusing(self):
        # Compute the positions
        positions = [self.central_position]
        step = self.step
        while step <= (self.distance/2.0 + 0.01):
            left_value = self.central_position - step
            right_value = self.central_position + step
            positions.append(left_value)
            positions.append(right_value)
            step += self.step
        positions.sort()
        self.log('I','Starting the focusing procedure...')
        time.sleep(2)
        # Take the images
        for position in positions:
            self.setValue('FOC_TAR', str(position), 'F0')
            self.focsleep()
            for i in range(self.shoots):
                self.log('I','Taking image n.%d for pos %s' % (i, position))
                file_name = 'position_%.2f_shoot_n%d' % (position, i)
                self.shoot(file_name)

        # Find the best focus position
        optimal_position = self.focus_fit()
        if optimal_position is None:
            raise ValueError('Position not set: no optimal value for filter %s' %self.filter)
        elif self.apply == 'yes':
            self.log('I', 'Setting the M2 position to %s' % optimal_position)
            self.setValue('FOC_TOFF', str(0), 'F0')  # Clear the offset
            self.focsleep()
            self.setValue('FOC_DEF', str(optimal_position), 'F0')
            self.focsleep()
            self.setValue('FOC_TAR', str(optimal_position), 'F0')
            self.focsleep()
            self.log('I', 'M2 position: %.2f' % float(self.getValue('FOC_POS', 'F0')))

    def run(self):
        self.log("I", "Setting the filter %s" % self.filter)
        self.setValue('filter', self.filter)
        time.sleep(7)
        self.setValue('FOC_TOFF', str(0), 'F0')  # Clear the offset
        time.sleep(7)
        self.focusing()


if __name__ == '__main__':
    kargs = script_args('autofocus')
    script = Autofocus(**kargs)
    script.run()
