#! /usr/bin/python
from __future__ import division

import datetime as dt
import sys
import os

import numpy as np
from astropy.io import fits

from itm_cli.utils import script_args, RTS2Wrapper
from itm_cli.config.config import OBSERVATION_PATH, FLAT_DARK_DIR


class CleanFits(RTS2Wrapper):

    def __init__(self, catalog, observation_dir):
        RTS2Wrapper.__init__(self)
        # For instance: catalog=agb, observation_dir=2018_obs02
        self.root_dir = os.path.join(OBSERVATION_PATH, catalog, observation_dir)

    def get_files(self):
        result = []  # [(star_name, filter, [files])]
        for star in os.listdir(self.root_dir):
            star_dir = os.path.join(self.root_dir, star)
            for filter in os.listdir(star_dir):
                filter_dir = os.path.join(star_dir, filter)
                filter_files = os.listdir(filter_dir)
                files = []
                for file in filter_files:
                    if not file.endswith('.fits'):
                        continue
                    else:
                        file_path = os.path.join(filter_dir, file)
                        files.append(file_path)
                files.sort()
                result.append((star, filter, files))
        result.sort()
        return result

    def update_file(self, file_path, log_file_name, star_name, filter):
        file_name = os.path.basename(file_path)
        ELARAW = 'ela' if 'elab' in file_name else 'raw'
        try:
            hdulist = fits.open(file_path)
            header = hdulist[0].header
            DATE = header['DATE']
            EXPOSURE = header['EXPOSURE']
            CCD_TYPE = header['CCD_TYPE']
            CCD_TEMP = header['CCD_TEMP']
            CCD_AIR = header['CCD_AIR']
            TELRA = header['TELRA']
            TELDEC = header['TELDEC']
        finally:
            hdulist.close()
        try:
            # Update the fits header
            hdulist = fits.open(file_path, mode='update')
            header = hdulist[0].header
            self.log('I', 'Updating file %s' % file_path)
            header['OBJ'] = (star_name, 'Name of the observed object')
            header['EXTNAME'] = ('LIGHT', '')
            header['UTC'] = (DATE, 'Date of creation UTC')
            header['TELESCOP'] = ('IRAIT', 'Name of the data acqusition telescope')
            header['INSTRUM'] = (CCD_TYPE, 'Camera model')
            header['ELARAW'] = (ELARAW, 'Elaborated or raw')
            header['REFDOC'] = ('', 'Documentation reference')
            header['FILTER'] = (filter, 'Filter for frame')
            header['VERSION'] = '1.0'
            header['CHIP_T'] = (CCD_TEMP, 'Chip CCD temperature (Celsius)')
            header['CAMERA_T'] = (CCD_AIR, 'Camera temperature (Celsius)')
            header['EXPOS'] = (EXPOSURE, 'Total Exposure time (sec)')
            header['RA'] = (TELRA, 'Right ascension of the target')
            header['DEC'] = (TELDEC, 'Declination of the target')
            log_file = open(log_file_name, 'a')
            log_file.write(file_path + '\n')
        finally:
            hdulist.close()
            log_file.close()

    def run(self):
        self.log('I', 'Staring the cleaning procedure ...')
        to_clean = []  # [(star_name, filter, [files]]
        for star_name, filter, files in self.get_files():
            files_to_clean = []
            for file in files:
                dir_name = os.path.dirname(file)
                file_name = os.path.basename(file)
                elab_path = os.path.join(dir_name, 'elab_%s' % file_name)
                if not file_name.startswith('elab_'):
                    if elab_path not in files:
                        # Elaborate that file
                        files_to_clean.append(file)
            files_to_clean.sort()
            to_clean.append((star_name, filter, files_to_clean))
            to_clean.sort()
                            
        for star_name, filter, files in to_clean:
            for file in files:
                try:
                    self.log('I', 'Cleaning file %s ...' % file)
                    hdulist = fits.open(file)
                    exposition = hdulist[0].header['EXPOSURE']
                    dark_file_name = 'mean30frame_DARK_exp_%d.fits' % int(exposition)
                    flat_file_name = 'mean30frame_FLAT_filter_%s.fits' % filter
                    try:
                        dark_file = fits.open(os.path.join(FLAT_DARK_DIR, dark_file_name))
                    except IOError:
                        dark_file_name = 'mean30frame_DARK_exp_%d.fits' % int(exposition + 10)
                        dark_file = fits.open(os.path.join(FLAT_DARK_DIR, dark_file_name))

                    flat_file = fits.open(os.path.join(FLAT_DARK_DIR, flat_file_name))
                    dark = dark_file[0].data
                    flat_raw = flat_file[0].data
                    array = []
                    for el in flat_raw:
                        for item in el:
                            array.append(item)
           
                    nparray = np.array(array)
                    nparray.sort()
                    fm = (nparray[4669568] + nparray[4669569]) / 2
                    flat = flat_raw / fm
                    file_data = hdulist[0].data
                    img = (file_data - dark) / flat
                    generic_hdulist = fits.HDUList()
                    generic_hdulist.append(fits.ImageHDU(data=img))
                    ihdu = len(generic_hdulist)-1
                    generic_hdulist[ihdu].header = hdulist[0].header
                    dir_name = os.path.dirname(file)
                    file_name = 'elab_%s' % os.path.basename(file)
                    file_path = os.path.join(dir_name, file_name)
                    generic_hdulist.writeto(file_path, clobber=True)
                    self.log('I', 'File %s saved on disk.' % file_path)
                finally:
                    hdulist.close()

        for star_name, filter, files in self.get_files():
            for file in files:
                dir_name = os.path.dirname(file)
                log_file_name = os.path.join(dir_name, 'updated.txt')
                already_updated = False
                open(log_file_name, 'a').close() # Create the file
                with open(log_file_name) as log_file:
                    for line in log_file:
                        if file in line:
                            self.log('I', 'File %s already updated' % file)
                            already_updated = True
                            break
                if not already_updated:
                    self.update_file(file, log_file_name, star_name, filter)


if __name__ == '__main__':
    kargs = script_args('cleanfits')
    script = CleanFits(**kargs)
    script.run()
