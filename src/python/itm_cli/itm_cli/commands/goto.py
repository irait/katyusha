#!/usr/bin/python
import time
import math
import datetime as dt

import ephem

from itm_cli.commands import stop, track, gotoephem
from itm_cli.catalog import sources
from itm_cli.config.config import SHELVE_FILE
from itm_cli.utils import script_args, RTS2Wrapper, get_source

from astropy.coordinates import Angle
from astropy import units as u


class GoTo(RTS2Wrapper):

    def __init__(self, source, alt_offset=0, az_offset=0):
        RTS2Wrapper.__init__(self)
        if isinstance(source, sources.Source):
            self.source = source
        elif '(' in source and ')' in source:
            # source sould be in the following format: (aa:bb:cc,dd:ee:ff)
            try:
                coordinates = source[1:-1].split(',')
                self.source = sources.Source('Unknown', coordinates, 0.0, 0.0, 2000)
            except ValueError:
                self.msg = 'Wrong RA-DEC coordinates: %s.' % source
                self.source = None
                return
        else:
            self.source = get_source(source)
        if self.source is None:
            self.msg = 'Can not find source %s.' % source

        self.alt_offset = alt_offset
        self.az_offset = az_offset

    def alt_az(self):
        return self.source.alt_az(self.alt_offset, self.az_offset)

    def track_source(self):
        self.set_tracking()
        self.setValue('TEL_', '{} {}'.format(*self.alt_az()), 'T0')

    def goto_source(self, wait=True):
        """Go to the source.

        If `wait=True`, then the method will block until the telescope
        points the source."""
        self.log('I', 'The goto script is running...')
        self.log('I', 'Offsets: (az: %.4f, alt: %.4f)' % (self.az_offset, self.alt_offset))
        target_alt, target_az = self.alt_az()
        if target_alt < 7 or target_alt > 75:
            self.log('W', 'Altitude out of range: %.2f' % target_alt)
            return
        else:
            # We want the telescope to reach the altitude position before
            # reaching the azimuth one.  That's because there is a bug
            # (maybe in RTS2), and if you reach the azimuth before the altitude,
            # then it is possible the movement does not stop in altitude and the
            # telescope stops only when it reaches the final-limit.  In that
            # case the Galil will have some problems and you will lose lot
            # of time trying to make things work properly as before.

            # Set the position and then stop immediately the mount.  In this
            # way the target position will be set in RTS2, and we can compute
            # the distance
            self.track_source()
            dummy_target = False
            time.sleep(2)
            self.stop_mount()
        time.sleep(2)
        starting_alt_delta_angle = None
        while wait:
            distance_str = self.getValue('distance', 'T0')
            alt_delta, az_delta = [float(item) for item in distance_str.split()]
            if math.isnan(alt_delta) or math.isnan(az_delta):
                self.log('E', 'The target position is not set')
                self.stop_mount()
                break
            alt_delta_angle = Angle(alt_delta, u.deg)
            az_delta_angle = Angle(az_delta, u.deg)
            if starting_alt_delta_angle is None:
                starting_alt_delta_angle = abs(alt_delta_angle.deg)
            # In case the alt_delta_angle is bigger than the az_delta_angle, add
            # the delta difference (moltiplied to a factor) to the az_target.
            # That because the speed is the same for the two axes, otherwise you
            # have to calculate the time each axis takes to reach the target, in
            # order to see if the target azimuth will be reached bofore the altitude.
            if abs(alt_delta_angle.deg) > abs(az_delta_angle.deg)*1.1:  # 1.1 is a safe factor
                delta_difference = abs(alt_delta_angle - az_delta_angle)
                target_alt, target_az = self.alt_az()
                target_az_angle = Angle(target_az, u.deg)
                if az_delta_angle.deg < 0:
                    dummy_target_az = (target_az_angle - delta_difference*1.5).deg % 360
                else:
                    dummy_target_az = (target_az_angle + delta_difference*1.5).deg % 360
                self.set_tracking()
                time.sleep(5)
                self.setValue('TEL_', '{} {}'.format(target_alt, dummy_target_az), 'T0')
                dummy_target = True
                time.sleep(5)
                continue  # Now it should not enter again in this 'if' block
            else:
                point = self.getValue('point', 'T0')  # 0 means false
                mount = self.getValue('Mount', 'T0')  # 1 means 'STOP'
                if point == '0' or mount == '1':
                    self.log('I', 'Mount or point disabled: enabling movement.')
                    self.set_tracking()
                    dummy_target = False
                    time.sleep(5)
                    continue

            if (abs(alt_delta_angle.deg)*1.1 < starting_alt_delta_angle/2.0) and dummy_target:
                # When we are going to the dummy_target, if the alt_delta_angle is half
                # the starting values, it means we moved for half distance both in
                # altitude and in azimuth.  Using the value 1.1, we are sure in azimuth
                # we moved more than half altitude stating distance, so we can not set
                # the real source position, because the azimuth delta will be bigger than
                # the altitude one
                self.track_source()
                dummy_target = False
                self.log('I', 'Going to the real target...')
                time.sleep(5)
                continue

            distance = math.sqrt(alt_delta_angle.degree**2 + az_delta_angle.degree**2)
            target = 'Dummy target' if dummy_target else 'Target'
            if distance < 0.1:
                if dummy_target:
                    self.log('I', '%s position almost reached - distance: %.4f' % (target, distance))
                    self.track_source()
                    dummy_target = False
                else:
                    self.log('I', 'Target %s reached.' % self.source.name)
                    time.sleep(2)
                    break
            else:
                self.log('I', 'Going to %s %s. Distance: %.4f' % \
                        (self.source.name, target.lower(), distance))

            time.sleep(15) if distance > 1 else time.sleep(5)

    def run(self):
        if self.source is None:
            self.log('E', 'ERROR: %s' % self.msg)
            return
        if isinstance(self.source, ephem.Body):
            script = gotoephem.GoToEphem(self.source, self.alt_offset, self.az_offset)
            script.run()
        else:
            self.goto_source()

    def stop_mount(self):
        script = stop.Stop()
        script.run()

    def set_tracking(self):
        script = track.Track()
        script.run()


if __name__ == '__main__':
    kargs = script_args('goto')
    script = GoTo(**kargs)
    script.run()
