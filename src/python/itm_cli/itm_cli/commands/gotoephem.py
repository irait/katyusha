#!/usr/bin/python
from __future__ import division

import time
import datetime as dt
from math import radians, isnan

import numpy as np
from itm_cli.catalog.sources import ITMObserver
from itm_cli.commands.track import Track
from itm_cli.utils import script_args, RTS2Wrapper


class GoToEphem(RTS2Wrapper):

    def __init__(self, source, alt_offset=0, az_offset=0):
        RTS2Wrapper.__init__(self)
        self.alt_offset = alt_offset
        self.az_offset = az_offset
        self.source = source

    def alt_az(self):
        observer = ITMObserver()
        observer.date = dt.datetime.utcnow()
        self.source.compute(observer)
        alt = np.rad2deg(self.source.alt + radians(self.alt_offset))
        az = (np.rad2deg(self.source.az + radians(self.az_offset)) + 180.0) % 360
        return alt, az

    def goto_source(self):
        target_alt, target_az = self.alt_az()
        if target_alt < 7 or target_alt > 75:
            self.log('W', 'Altitude out of range: %.2f' % target_alt)
            return
        else:
            self.setValue('TEL_', '{} {}'.format(target_alt, target_az), 'T0')

    def run(self):
        self.log('I', 'The gotoephem script is running...')
        self.log('I', 'Offsets: (az: %.4f, alt: %.4f)' % (self.az_offset, self.alt_offset))
        script = Track()
        script.run()
        time.sleep(2)
        self.goto_source()
        time.sleep(2)
        deltatime = 60  # Seconds
        while True:
            self.goto_source()
            time.sleep(2)
            distance_str = self.getValue('distance', 'T0')
            alt_delta, az_delta = [float(item) for item in distance_str.split()]
            if isnan(alt_delta) or isnan(az_delta):
                self.log('E', 'The target position is not set')
            else:
                self.log('I', 'Distance to %s: (az: %.4f, alt: %.4f)' % (
                         self.source.name, alt_delta, az_delta))
            time.sleep(deltatime)


if __name__ == '__main__':
    kargs = script_args('gotoephem')
    script = GoToEphem(**kargs)
    script.run()
