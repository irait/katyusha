#!/usr/bin/python

import os
import sys
import datetime
from math import radians, cos

from itm_cli.commands.goto import GoTo
from itm_cli.commands.stop import Stop
from itm_cli.catalog import sources
from itm_cli.catalog.sources import Source, ITMObserver
from itm_cli.utils import (
    Camera, script_args, RTS2Wrapper, is_already_observed
)

import ephem
from itm_cli.config.config import IMAGE_DIR, PM_DIR, MU, SIDE_PX
from astropy.io import fits


class Map(RTS2Wrapper):

    def __init__(
            self,
            exposition,
            source,
            filter='C',
            dimension=None):
        RTS2Wrapper.__init__(self)
        self.exposition = exposition
        self.source = getattr(sources, source)
        self.source_name = source
        self.camera = Camera(self, IMAGE_DIR)
        if filter != 'all' and filter not in self.camera.filters:
            raise ValueError('Filter %s not allowed' % filter)
        self.log("I", "Setting the filter %s" % filter)
        self.setValue('filter', filter)
        self.dimension = dimension

    def pointing_info(self):
        """Return the final telescope position and the pm corrections"""
        # The final position (row position + motion per year + pointing model)
        alt, azi = [float(item) for item in self.getValue('position.AA','T0').split()]
        # Maybe the coordinates are related to one second before
        obs_datetime = datetime.datetime.utcnow() - datetime.timedelta(seconds=1)
        # The offsets added by the poiting model
        alt_pm, azi_pm = [float(item) for item in self.getValue('pm_delta','T0').split()]
        true_azi = (azi + 180) % 360
        observer = ITMObserver()
        observer.date = ephem.date(obs_datetime)
        ra, dec = observer.radec_of(radians(azi), radians(alt))
        format_ = '%Y-%m-%dT%H:%M:%S'
        time_ = datetime.datetime.strftime(obs_datetime, format_)
        return str(ra), str(dec), alt, true_azi, alt_pm, azi_pm, time_

    def write_fits(self, name):
        # Shoot the image
        fits_file = self.camera.shoot(self.exposition, name)
        # Get the current position (with pointing model corrections), and also
        # the poiting model corrections and the related time
        ra, dec, alt, azi, alt_pm_offset, azi_pm_offset, time_ = self.pointing_info()
        # Update the fits header
        hdulist = fits.open(fits_file, mode='update')
        header = hdulist[0].header
        header['FNL_RA'] = (ra, 'The final RA value, with all corrections')
        header['FNL_DEC'] = (dec, 'The final DEC value, with all corrections')
        header['FNL_ALT'] = (alt, 'The final altitude value, with all corrections')
        header['FNL_AZI'] = (azi, 'The final azimuth value, with all corrections')
        header['FNL_TIME'] = (time_, 'The time relate to FNL_ALT and FNL_AZI')
        header['PM_ALT'] = (alt_pm_offset, 'The poiting model altitude correction')
        header['PM_AZI'] = (azi_pm_offset, 'The poiting model azimuth correction')
        hdulist.close()

    def run(self):
        self.log('I', 'The map script is running...')
        min_index = -(self.dimension-1) / 2
        max_index = +(self.dimension-1) / 2 + 1
        az_indexes = range(min_index, max_index) or [0]
        alt_indexes = range(-min_index, -max_index, -1) or [0]
        line = 0
        for alt_index in alt_indexes:
            line += 1
            column = 0
            for az_index in az_indexes:
                alt, az = self.source.alt_az()
                az_side, alt_side = SIDE_PX
                alt_offset = MU * alt_side * 0.7
                az_offset = MU *az_side * 0.7 / cos(radians(alt))
                goto_source = GoTo(
                    self.source,
                    alt_offset=alt_index*alt_offset, 
                    az_offset=az_index*az_offset
                )
                goto_source.run()
                column += 1
                file_name = '%s_%dx%d' % (self.source_name, line, column)
                self.write_fits(file_name) 

        stop_script = Stop()
        stop_script.run()


if __name__ == '__main__':
    kargs = script_args('map')
    script = Map(**kargs)
    script.run()
