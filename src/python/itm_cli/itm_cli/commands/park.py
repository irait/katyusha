#!/usr/bin/python
import time
from itm_cli.commands import stop
from itm_cli.utils import RTS2Wrapper
from itm_cli.config.config import PARK_POSITION


class Park(RTS2Wrapper):

    def run(self):
        self.setValue('TEL_', '{} {}'.format(*PARK_POSITION), 'T0')
        time.sleep(180)
        stop_mount = stop.Stop()
        stop_mount.run()
        self.setValue('CCD_SET', '0', 'C0')
        # TODO: Close the dome

if __name__ == '__main__':
    script = Park()
    script.run()
