from __future__ import print_function, division
import os

from numpy import loadtxt, sin, cos, radians, mean, pi
from astropy.io import fits
from lmfit import Model
from itm_cli.utils import script_args
from itm_cli.config.config import (
    PM_FILE_OFFSETS, PM_AZ_INIT, PM_DIR, PM_AZ_REPORT
)


def az_offset_func(
        coordinates,
        aa0, aa1, aa3, aa4, aa5, aa6,
        ae5,
        aes, aes_delta_azr, aes_delta_elr, acec):
    az, el = coordinates
    azr = radians(az)
    elr = radians(el)
    return aa6*az**6 + aa5*az**5 + aa4*az**4 + aa3*az**3 + aa1*az + aa0 + \
           ae5*el**5 + + aes*sin(azr + aes_delta_azr)*sin(elr + aes_delta_elr) + \
           acec*cos(elr)


class Pmaz(object):
 
    def __init__(self):
        self.report_warnings = ''

    def run(self):
        data = loadtxt(
            PM_FILE_OFFSETS,
            usecols=range(1, 5),
            unpack=True
        )
        azs, els, az_offsets, el_offsets = data
        az_offset_model = Model(az_offset_func)
        
        with open(PM_AZ_INIT) as init:
            for line in init:
                line = line.strip()
                if not line:
                    continue
                name, value = [item.strip() for item in line.split(':')]
                if name == 'initial_guess':
                    initial_guess = float(value)
                else:
                    az_offset_model.set_param_hint(name, value=eval(value))
        
        az_result = az_offset_model.fit(az_offsets, coordinates=(azs, els))
        az_params = az_result.best_values
        aa6 = az_params['aa6']
        aa5 = az_params['aa5']
        aa4 = az_params['aa4']
        aa3 = az_params['aa3']
        aa1 = az_params['aa1']
        aa0 = az_params['aa0']
        ae5 = az_params['ae5']
        aes = az_params['aes']
        acec = az_params['acec']
        aes_delta_azr = az_params['aes_delta_azr']
        aes_delta_elr = az_params['aes_delta_elr']
        
        fits_files = []
        for i in range(1, 200, 5):
            id = '%03d' % i
            fits_files.append('000%s.fits' % id)
        
        for file in fits_files:
            try:
                hdulist = fits.open(os.path.join(PM_DIR, file))
            except IOError, ex:
                self.report_warnings += str(ex) + '\n'
                continue
            az = hdulist[0].header['FNL_AZI']
            el = hdulist[0].header['FNL_ALT']
            az_offset = hdulist[0].header['PM_AZI']
            azr = radians(az)
            elr = radians(el)
        
        diff_mean = mean(abs(az_result.best_fit - az_offsets))
        max_diff = max(abs(az_result.best_fit - az_offsets))
        
        with open(PM_AZ_REPORT, 'w') as report:
            print('max: ', max_diff, file=report)
            print('mean: ', diff_mean, file=report)
            print('\n' + az_result.fit_report(), file=report)
            print('\nWARNINGS:\n' + self.report_warnings, file=report)
        
        for name in sorted(az_params.keys()):
            print('long double %s = %s;' % (name, az_params[name]))


if __name__ == '__main__':
    kargs = script_args('pmaz')
    script = Pmaz(**kargs)
    script.run()
