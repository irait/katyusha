from __future__ import print_function, division
import os

from numpy import loadtxt, sin, cos, radians, mean, pi
from astropy.io import fits
from lmfit import Model
from itm_cli.utils import script_args
from itm_cli.config.config import (
    PM_FILE_OFFSETS, PM_EL_INIT, PM_DIR, PM_EL_REPORT
)


def el_offset_func(
        coordinates,
        ea0, ea1, ea3, ea5, ea6):
    az, el = coordinates
    azr = radians(az)
    elr = radians(el)
    return ea6*az**6 + ea5*az**5 + ea3*az**3 + \
           ea1*az + ea0


class Pmel(object):
 
    def __init__(self):
        self.report_warnings = ''

    def run(self):
        data = loadtxt(
            PM_FILE_OFFSETS,
            usecols=range(1, 5),
            unpack=True
        )
        azs, els, az_offsets, el_offsets = data
        el_offset_model = Model(el_offset_func)
        
        with open(PM_EL_INIT) as init:
            for line in init:
                line = line.strip()
                if not line:
                    continue
                name, value = [item.strip() for item in line.split(':')]
                if name == 'initial_guess':
                    initial_guess = float(value)
                else:
                    el_offset_model.set_param_hint(name, value=eval(value))

        el_result = el_offset_model.fit(el_offsets, coordinates=(azs, els))
        el_params = el_result.best_values
        ea0 = el_params['ea0']
        ea1 = el_params['ea1']
        ea3 = el_params['ea3']
        ea5 = el_params['ea5']
        ea6 = el_params['ea6']
        
        fits_files = []
        for i in range(1, 200, 5):
            id = '%03d' % i
            fits_files.append('000%s.fits' % id)
        
        for file in fits_files:
            try:
                hdulist = fits.open(os.path.join(PM_DIR, file))
            except IOError, ex:
                self.report_warnings += str(ex) + '\n'
                continue
            az = hdulist[0].header['FNL_AZI']
            el = hdulist[0].header['FNL_ALT']
            el_offset = hdulist[0].header['PM_ALT']
            azr = radians(az)
            elr = radians(el)

        diff_mean = mean(abs(el_result.best_fit - el_offsets))
        max_diff = max(abs(el_result.best_fit - el_offsets))
        
        with open(PM_EL_REPORT, 'w') as report:
            print('max: ', max_diff, file=report)
            print('mean: ', diff_mean, file=report)
            print('\n' + el_result.fit_report(), file=report)
            print('\nWARNINGS:\n' + self.report_warnings, file=report)
        
        for name in sorted(el_params.keys()):
            print('long double %s = %s;' % (name, el_params[name]))


if __name__ == '__main__':
    kargs = script_args('pmel')
    script = Pmel(**kargs)
    script.run()
