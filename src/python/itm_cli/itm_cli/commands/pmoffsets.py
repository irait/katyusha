#!/usr/bin/python
"""This script takes a directory containing some starts images and
creates a file of offsets. The name of the file image
will be the ID of the star in the output file."""

import os
import datetime

from itm_cli.utils import (
    script_args, RTS2Wrapper, in_offset_file, get_star_info,
    in_file_offsets
)
from itm_cli.config.config import PM_DIR, PM_FILE_OFFSETS


class PMOffsets(RTS2Wrapper):

    def __init__(self, dir=PM_DIR):
        RTS2Wrapper.__init__(self)
        if not os.path.exists(dir):
            self.log("E", "Directory %s does not exist" % dir)
        else:
            self.dir = dir
        if not os.path.exists(PM_FILE_OFFSETS):
            open(PM_FILE_OFFSETS, 'a')

    def run(self):
        self.log('I', 'The pmoffsets script is running...')
        for file in sorted(os.listdir(self.dir)):
            if file.endswith('.fits'):
                if '_' in file:
                    continue  # Skip because it is a file of a map 
                star_id = file.rstrip('.fits')
                ra, dec, t = None, None, None
                full_path = os.path.join(self.dir, file)
                if not in_file_offsets(star_id):
                    self.log('I', '%s not in file of offsets' % star_id)
                    self.log('I', 'Getting the coordinates from %s' % file)
                    self.log('I', 'It can take a couple of minutes...')
                    ra, dec, t = get_star_info(full_path, self) # ra, dec, observed datetime
                if not all((ra, dec, t)):
                    self.log('I', 'Can not find star %s in %s' % (star_id, file))

if __name__ == '__main__':
    kargs = script_args('pmoffsets')
    script = PMOffsets(**kargs)
    script.run()
