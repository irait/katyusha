#!/usr/bin/python
"""This script makes a scan of the sky, in order to create a file of
input coordinates for TPoint, the program used to create the telescope
pointing model."""

import os
import sys
import datetime
from math import radians, cos
from collections import OrderedDict

from itm_cli.commands.goto import GoTo
from itm_cli.commands.stop import Stop
from itm_cli.commands.pmoffsets import PMOffsets
from itm_cli.catalog.sources import Source, ITMObserver
from itm_cli.utils import (
    Camera, script_args, RTS2Wrapper, is_already_observed
)

import ephem
from itm_cli.config.config import CATALOG_FILE, PM_DIR, MU, SIDE_PX
from astropy.io import fits


class PMScan(RTS2Wrapper):

    def __init__(
            self,
            catalog,
            exposition,
            filter='C',
            starting_star=1,
            ending_star=1000,
            star_map=None):
        RTS2Wrapper.__init__(self)
        self.stars = self.get_stars(catalog)
        self.exposition = exposition
        self.camera = Camera(self, PM_DIR, False)
        self.starting_star = starting_star
        self.ending_star = ending_star
        if filter != 'all' and filter not in self.camera.filters:
            raise ValueError('Filter %s not allowed' % filter)
        self.log("I", "Setting the filter %s" % filter)
        self.setValue('filter', filter)
        self.star_map = star_map

    def get_stars(self, catalog):
        stars = OrderedDict()
        try:
            catalog_file = open(CATALOG_FILE)
        except IOError:
            self.log('E', 'Can not find %s' % CATALOG_FILE)
            sys.exit(1)

        for line in catalog_file:
            line = line.strip()
            if not line or line.startswith('!') or 'END' in line:
                continue

            star_id, h, m, s, deg, arcm, arcs, ra_m, dec_m, eq = line.split()
            ra = ':'.join([h, m, s])  # hour, minutes, seconds
            dec = ':'.join([deg, arcm, arcs])  # degrees, arcminutes, arcseconds
            ra_motion = float(ra_m)  # seconds per year
            dec_motion = float(dec_m)  # arcseconds per year
            equinox = int(float(eq))  # '2000.0' -> 2000
            stars[star_id] = Source(star_id, (ra, dec), ra_motion, dec_motion, equinox)
        return stars

    def pointing_info(self):
        """Return the final telescope position and the pm corrections"""
        # The final position (row position + motion per year + pointing model)
        alt, azi = [float(item) for item in self.getValue('position.AA','T0').split()]
        # Maybe the coordinates are related to one second before
        obs_datetime = datetime.datetime.utcnow() - datetime.timedelta(seconds=1)
        # The offsets added by the poiting model
        alt_pm, azi_pm = [float(item) for item in self.getValue('pm_delta','T0').split()]
        true_azi = (azi + 180) % 360
        observer = ITMObserver()
        observer.date = ephem.date(obs_datetime)
        ra, dec = observer.radec_of(radians(azi), radians(alt))
        format_ = '%Y-%m-%dT%H:%M:%S'
        time_ = datetime.datetime.strftime(obs_datetime, format_)
        return str(ra), str(dec), alt, true_azi, alt_pm, azi_pm, time_

    def write_fits(self, star_id):
        # Shoot the image
        fits_file = self.camera.shoot(self.exposition, star_id)
        # Get the current position (with pointing model corrections), and also
        # the poiting model corrections and the related time
        ra, dec, alt, azi, alt_pm_offset, azi_pm_offset, time_ = self.pointing_info()
        # Update the fits header
        hdulist = fits.open(fits_file, mode='update')
        header = hdulist[0].header
        header['FNL_RA'] = (ra, 'The final RA value, with all corrections')
        header['FNL_DEC'] = (dec, 'The final DEC value, with all corrections')
        header['FNL_ALT'] = (alt, 'The final altitude value, with all corrections')
        header['FNL_AZI'] = (azi, 'The final azimuth value, with all corrections')
        header['FNL_TIME'] = (time_, 'The time relate to FNL_ALT and FNL_AZI')
        header['PM_ALT'] = (alt_pm_offset, 'The poiting model altitude correction')
        header['PM_AZI'] = (azi_pm_offset, 'The poiting model azimuth correction')
        hdulist.close()

    def run(self):
        self.log('I', 'The pmscan script is running...')
        # Set the camera temperature (by the setup command)
        # Set the focus to a fixed and good enough position (by the setup command)
        for star_id, star in self.stars.items():
            if not (self.starting_star <= int(star_id) <= self.ending_star):
                continue
            # If the star is already in the offsets file, skip (continue)
            if is_already_observed(star_id):
                self.log('I', 'Star %s already observed: skipping it.' % star_id)
                continue
            if self.star_map is None or self.star_map == 1:
                # Go to the star
                try:
                    goto_source = GoTo(star)
                    goto_source.run()
                    self.write_fits(star_id) 
                except ValueError, ex:
                    self.log('W', 'Skipping source %s: %s.' % (star_id, str(ex)))
                    stop_script = Stop()
                    stop_script.run()
                    continue
            else:
                min_index = -(self.star_map-1) / 2
                max_index = +(self.star_map-1) / 2 + 1
                az_indexes = range(min_index, max_index) or [0]
                alt_indexes = range(-min_index, -max_index, -1) or [0]
                line = 0
                for alt_index in alt_indexes:
                    line += 1
                    column = 0
                    for az_index in az_indexes:
                        alt, az = star.alt_az()
                        az_side, alt_side = SIDE_PX
                        alt_offset = MU * alt_side * 0.7
                        az_offset = MU *az_side * 0.7 / cos(radians(alt))
                        goto_source = GoTo(
                            star,
                            alt_offset=alt_index*alt_offset, 
                            az_offset=az_index*az_offset
                        )
                        goto_source.run()
                        column += 1
                        file_name = '%s_%dx%d' % (star_id, line, column)
                        self.write_fits(file_name) 

        stop_script = Stop()
        stop_script.run()
        # Analyze the shoots and write the offsets file
        pmoffsets = PMOffsets(PM_DIR)
        pmoffsets.run()


if __name__ == '__main__':
    kargs = script_args('pmscan')
    script = PMScan(**kargs)
    script.run()
