#! /usr/bin/python
from __future__ import division, print_function

import sys
import os

import alipy
from astropy.io import fits
import numpy as np

from itm_cli.utils import script_args, get_catalog
from itm_cli.config.config import OBSERVATION_PATH


class Rotate(object):

    def __init__(self, catalog, observation_dir, stars, filters):
        # For instance: catalog=agb, observation_dir=2018_obs02
        self.root_dir = os.path.join(OBSERVATION_PATH, catalog, observation_dir)
        catalog_module = get_catalog('%s_catalog' % catalog)
        self.filters = allowed_filters = catalog_module.get_filters()
        self.sources = allowed_sources = catalog_module.get_sources()
        if stars:
            for star in stars.split(','):
                if star not in allowed_sources:
                    print('ERROR: star %s not in %s' % (star, allowed_sources))
                    sys.exit(1)
            self.sources = stars.split(',')
        if filters:
            for filter in filters.split(','):
                if filter not in allowed_filters:
                    print('ERROR: filter %s not in %s' % (filter, allowed_filters))
                    sys.exit(1)
            self.filters = filters.split(',')

    def get_files(self):
        result = []  # [(star_name, filter, [files])]
        for star in os.listdir(self.root_dir):
            star_dir = os.path.join(self.root_dir, star)
            for filter in os.listdir(star_dir):
                filter_dir = os.path.join(star_dir, filter)
                final_fits = os.path.join(filter_dir, 'final.fits')
                if os.path.exists(final_fits):
                    continue
                filter_files = os.listdir(filter_dir)
                files = []
                for file in filter_files:
                    if not file.startswith('elab') or not file.endswith('.fits'):
                        continue
                    else:
                        file_path = os.path.join(filter_dir, file)
                        files.append(file_path)
                files.sort()
                result.append((star, filter, files))
        result.sort()
        return tuple(result)

    def derotate(self, images):
        ref_image, files = images[0], images[1:]
        ref = fits.open(ref_image)
        ref_header = ref[0].header
        hdulist = fits.HDUList()
        hdulist.append(fits.ImageHDU(data=None))
        ihdu= len(hdulist)-1
        for card in ref_header.cards:
            key, value, description = card
            if len(key) > 8:
                key = key[:8]
            hdulist[ihdu].header.set(key, value, description)
        
        try:
            identifications = alipy.ident.run(ref_image, files, visu=True, n=4)
        except RuntimeError:
            print('ERROR: cannot find objects in the reference image %s\n\n' % ref_image)
            return
 
        print('\nNumber of identifications: %s\n' % len(identifications))
        
        for i, id in enumerate(identifications): 
            if id.ok == True:
                info = "%15s : %20s, flux ratio %.2f" % (id.ukn.name, id.trans, id.medfluxratio)
                hdulist[ihdu].header.set('Img_%i ' % i, info)
            else:
                info = "%20s : no transformation found !" % (id.ukn.name) 
            print(info)
        
        outputshape = alipy.align.shape(ref_image)
        images_dir = os.path.dirname(ref_image)
        try:
            temp_dir = os.path.join(images_dir, 'alipy_out')
            if not os.path.exists(temp_dir):
                os.mkdir(temp_dir)
            
            affineremap_called = False
            for id in identifications:
                if id.ok == True:
                    print('Calling alipy.aligh.affinemap()...')
                    alipy.align.affineremap(
                        id.ukn.filepath,
                        id.trans,
                        outdir=temp_dir,
                        shape=outputshape,
                        makepng=True)
                    affineremap_called = True
            if not affineremap_called:
                print('Can not call alipy.aligh.affinemap(): id.ok NOT True\n\n')
                return
            
            final_data = np.zeros((3056,3056))
            for file in sorted(os.listdir(temp_dir)):
                if file.endswith('.fits'):
                    full_path = os.path.join(temp_dir, file)
                    myfile = fits.open(full_path)
                    data = myfile[0].data
                    print('Adding %s to final data' % file)
                    final_data += data
            
            hdulist[ihdu].data = final_data
            final_file = os.path.join(images_dir, 'final.fits')
            print('Writing %s' % final_file)
            hdulist.writeto(final_file, clobber=True)
        finally:
            hdulist.close()
            if os.path.exists(temp_dir):
                os.system('rm %s -r' % temp_dir)

    def run(self):
        print('Staring the rotating procedure ...')
        for star_name, filter, files in self.get_files():
            if star_name not in self.sources or filter not in self.filters:
                continue
            else:
                self.derotate(files)
        print('Rotating procedure done')


if __name__ == '__main__':
    kargs = script_args('rotate')
    script = Rotate(**kargs)
    script.run()
