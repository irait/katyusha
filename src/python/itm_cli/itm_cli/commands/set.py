#!/usr/bin/python
import time
from itm_cli.utils import RTS2Wrapper, Camera, script_args
from itm_cli.config.config import AUTOFOCUS_DIR


class Set(RTS2Wrapper):

    def __init__(self, filter=''):
        self.camera = Camera(self, AUTOFOCUS_DIR)
        self.filter = filter

    def run(self):
        if self.filter:
            if self.filter != 'all' and self.filter not in self.camera.filters:
                raise ValueError('Filter %s not allowed' % self.filter)
            else:
                self.setValue('filter', self.filter)


if __name__ == '__main__':
    kargs = script_args('set')
    script = Set(**kargs)
    script.run()
