#!/usr/bin/python
import time
from itm_cli.utils import RTS2Wrapper


class Setup(RTS2Wrapper):

    def run(self):
        # self.setValue('FOC_TAR', '2', 'F0')  # F2 position
        # time.sleep(2)
        self.setValue('CCD_SET', '-31', 'C0')  # CCD temperature
        time.sleep(3)

if __name__ == '__main__':
    script = Setup()
    script.run()
