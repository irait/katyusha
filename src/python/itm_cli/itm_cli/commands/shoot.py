#!/usr/bin/python

import os
import time

from itm_cli.utils import Camera, script_args, RTS2Wrapper
from itm_cli.config.config import IMAGE_DIR


class Shoot(RTS2Wrapper):

    def __init__(self, exposition, filter='R', n=1, file_name='image', distance=1, dir=''):
        RTS2Wrapper.__init__(self)
        self.exposition = exposition
        self.n = n  # Number of shoots
        self.file_name = file_name
        image_dir = dir or IMAGE_DIR
        if not os.path.exists(image_dir):
            os.mkdir(image_dir)
        self.camera = Camera(self, image_dir)
        if filter != 'all' and filter not in self.camera.filters:
            raise ValueError('Filter %s not allowed' % filter)
        self.filter = filter
        self.distance = distance if n > 1 else 0

    def run(self):
        self.log("I", "Setting the filter %s" % self.filter)
        self.setValue('filter', self.filter)
        for i in range(self.n):
            self.camera.shoot(self.exposition, '%s_%d' % (self.file_name, i))
            self.log("I", "Next shoot in %d seconds after the previous one." % self.distance)
            time.sleep(self.distance)


if __name__ == '__main__':
    kargs = script_args('shoot')
    script = Shoot(**kargs)
    script.run()
