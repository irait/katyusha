#! /usr/bin/python
from __future__ import division, print_function

import sys
import os

from itm_cli.utils import script_args, get_catalog
from itm_cli.config.config import OBSERVATION_PATH


class Show(object):

    def __init__(self, what, catalog, observation_dir, stars, filters, verbose):
        # For instance: catalog=agb, observation_dir=2018_obs02
        options = ('rotated', 'cleaned')
        if what not in options:
            print('ERROR: "what" should be one of %s' % (options,))
            sys.exit(1)
        else:
            self.what = what

        if verbose not in ('yes', 'no'):
            print('ERROR: "verbose" should be "yes" or "not"')
            sys.exit(1)
        else:
            self.what = what
        self.verbose = verbose
        self.root_dir = os.path.join(OBSERVATION_PATH, catalog, observation_dir)
        catalog_module = get_catalog('%s_catalog' % catalog)
        self.filters = allowed_filters = catalog_module.get_filters()
        self.sources = allowed_sources = catalog_module.get_sources()
        if stars:
            for star in stars.split(','):
                if star not in allowed_sources:
                    print('ERROR: star %s not in %s' % (star, allowed_sources))
                    sys.exit(1)
            self.sources = stars.split(',')
        if filters:
            for filter in filters.split(','):
                if filter not in allowed_filters:
                    print('ERROR: filter %s not in %s' % (filter, allowed_filters))
                    sys.exit(1)
            self.filters = filters.split(',')

    def get_result(self):
        cleaned = []
        no_cleaned = []
        rotated = []
        no_rotated = []
        for star in os.listdir(self.root_dir):
            star_dir = os.path.join(self.root_dir, star)
            for filter in os.listdir(star_dir):
                filter_dir = os.path.join(star_dir, filter)
                final_fits = os.path.join(filter_dir, 'final.fits')
                if os.path.exists(final_fits):
                    rotated.append('%s -> %s' % (star, filter))
                else:
                    no_rotated.append('%s -> %s' % (star, filter))
                filter_files = os.listdir(filter_dir)
                raw_files = 0
                ela_files = 0
                for file in filter_files:
                    if file.startswith('elab') and file.endswith('.fits'):
                        ela_files += 1
                    if not file.startswith('elab') and file.endswith('.fits'):
                        if not file.startswith('final'):
                            raw_files += 1
                if ela_files:
                    if ela_files != raw_files:
                        print('ERROR: in %s/%s there are %d raw and %d elaborated files'
                              % (star, filter, raw_files, ela_files))
                    else:
                        cleaned.append('%s -> %s' % (star, filter))
                else:
                    no_cleaned.append('%s -> %s' % (star, filter))
        result = cleaned if self.what == 'cleaned' else rotated
        no_result = no_cleaned if self.what == 'cleaned' else no_rotated
        result.sort()
        no_result.sort()
        return result, no_result

    def run(self):
        print('Starting the show %s procedure ...\n' % self.what)
        result, no_result = self.get_result()
        for item in result:
            star, filter = item.split(' -> ')
            if star in self.sources and filter in self.filters:
                print('  %s' % item)
        if self.verbose == 'yes':
            print('\nNot %s:' % self.what)
            for item in no_result:
                star, filter = item.split(' -> ')
                if star in self.sources and filter in self.filters:
                    print('  %s' % item)


if __name__ == '__main__':
    kargs = script_args('show')
    script = Show(**kargs)
    script.run()
