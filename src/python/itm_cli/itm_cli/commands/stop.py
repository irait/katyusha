#!/usr/bin/python
import time
from itm_cli.utils import RTS2Wrapper


class Stop(RTS2Wrapper):

    def run(self):
        # When the mount state is already in STOP, if you set a new
        # position the telescope will move.  At this point if you set
        # again the mount to stop, or 'point' to false, nothing heppens
        # and the telescope does not stop.  In that case, if you want
        # it to stop, you have to set the mount to Stand-by, and then
        # you set it to STOP and it will stop properly.
        self.setValue('Mount', 'Stand-by', 'T0')
        time.sleep(1)
        self.setValue('Mount', 'STOP', 'T0')
        time.sleep(2)
        self.setValue('point', 'false', 'T0')


if __name__ == '__main__':
    script = Stop()
    script.run()
