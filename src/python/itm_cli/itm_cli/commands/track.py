#!/usr/bin/python
import time
from itm_cli.utils import RTS2Wrapper


class Track(RTS2Wrapper):

    def run(self):
        self.setValue('Mount', 'Stand-by', 'T0')
        time.sleep(2)
        self.setValue('point', 'true', 'T0')
        time.sleep(1)


if __name__ == '__main__':
    script = Track()
    script.run()
