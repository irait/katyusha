import os
import itm_cli

PKG_PATH = os.path.dirname(os.path.abspath(itm_cli.__file__))


ITM_CLI_DIR = os.path.join(os.getenv('HOME'), '.itm_cli')
IMAGE_DIR = os.path.join(os.getenv('HOME'), 'images')
LOGFILE = os.path.join(ITM_CLI_DIR, 'logs', 'itm_cli.log')
SHELVE_FILE = os.path.join(ITM_CLI_DIR, 'shelve')
HISTORY_FILE = os.path.join(ITM_CLI_DIR, 'history')
AUTOFOCUS_DIR = os.path.join(os.getenv('HOME'), 'autofocus')
PM_DIR = os.path.join(os.getenv('HOME'), 'pmodel', 'shoots')
PM_CONFIG = os.path.join(os.getenv('HOME'), 'pmodel', 'config')

CMD_DIR =  os.path.join(PKG_PATH, 'commands')
CATALOG_DIR =  os.path.join(PKG_PATH, 'catalog')
CATALOG_FILE =  os.path.join(CATALOG_DIR, 'itmctg.dat')
SIDE_PX = (3056, 3056)  # azimuth px, elevation px
MU = 3.7383177570093454e-05 # degrees/px
CCD_ANGLE = 0.8203693227943754 # 47 degrees
PMLOG_FILE = os.path.join(PM_DIR, 'coordinates.txt')
PM_FILE_OFFSETS = os.path.join(PM_DIR, 'offsets.txt')
PM_AZ_INIT = os.path.join(PM_CONFIG, 'az_init.txt')
PM_EL_INIT = os.path.join(PM_CONFIG, 'el_init.txt')
PM_AZ_REPORT = os.path.join(PM_DIR, 'reportaz.txt')
PM_EL_REPORT = os.path.join(PM_DIR, 'reportel.txt')

OBSERVATION_PATH = os.path.join(os.getenv('HOME'), 'fits')
FLAT_DARK_DIR = os.path.join(os.getenv('HOME'), 'flat_dark')
MIN_EL = 6
PARK_POSITION = (40.0, 270.0)
