import os
import sys
import time
import shelve
import logging
import datetime
import importlib
from math import radians, cos, sin
from operator import itemgetter

import rts2.scriptcomm
from itm_cli.config.config import (
    SHELVE_FILE, LOGFILE, MU, CCD_ANGLE, PMLOG_FILE,
    PM_DIR, CATALOG_FILE, PM_FILE_OFFSETS, CATALOG_DIR
)
from itm_cli.catalog.sources import ITMObserver, Source

import ephem
from astropy.io import fits
from astropy.coordinates import Angle

logging.basicConfig(
    filename=LOGFILE,
    format='%(levelname)s:%(asctime)s: %(message)s',
    level=logging.NOTSET
)


class RTS2Wrapper(rts2.scriptcomm.Rts2Comm):

    def log(self, level, message):
        rts2.scriptcomm.Rts2Comm.log(self, level, message)
        levels = {'I': 'info', 'E': 'error', 'W': 'warning'}
        logging_level = levels.get(level, 'debug')  # Default is 'debug'
        logger = logging.getLogger() 
        level_method = getattr(logger, logging_level)
        level_method(message)


def script_args(script_name):
    db = shelve.open(SHELVE_FILE)
    return db[script_name]


class Camera(object):

    def __init__(self, cs_ref, destination_dir, add_date_subdir=True):
        self.cs_ref = cs_ref  # Reference to RTS2 control software
        if not os.path.exists(destination_dir):
            os.mkdir(destination_dir)
        if add_date_subdir:
            subdir = "{0:%Y_%m_%d_%H:%M}".format(datetime.datetime.now())
        else:
            subdir = ''
        self.DESTINATION = os.path.join(destination_dir, subdir)

    @property
    def filters(self):
        return ('I', 'R', 'V', 'B', 'U', 'H', 'C')

    def shoot(self, exposure, file_name):
        time.sleep(1)
        self.cs_ref.setValue('SHUTTER', 'LIGHT')
        self.cs_ref.log('I', 'Setting the exposure to %s' % exposure)
        self.cs_ref.setValue('exposure', exposure)
        self.cs_ref.log('I', 'Exposure set')
        if not os.path.exists(self.DESTINATION):
            os.mkdir(self.DESTINATION)
        path = os.path.join(self.DESTINATION, file_name + '.fits')
        self.cs_ref.log('I', 'Shooting the image %s' % path)
        image = self.cs_ref.exposure()
        self.cs_ref.rename(image, path)
        self.cs_ref.log('I', 'Image saved on disk')
        return path


def in_offset_file(star_id):
    """Return True if a star is in the offset file."""
    with open(PM_FILE_OFFSETS) as offsets:
        for line in offsets:
            line = line.strip()
            if line.startswith(star_id):
                return True
    return False


def in_shoots_dir(star_id):
    for file in os.listdir(PM_DIR):
        if star_id in file:
            return True
    return False


def is_already_observed(star_id):
    """Return True if a star is already observed."""
    if in_offset_file(star_id) or in_shoots_dir(star_id):
        return True
    else:
        return False


def in_file_offsets(star_id):
    for line in open(PM_FILE_OFFSETS):
        if line.startswith(star_id):
            return True
    return False


def get_coordinates(image_data):
    coordinates = []
    for i in range(1, 5):
        threshold = image_data.mean() + 1500/i
        for altitude, row in enumerate(image_data):
            for azimuth, counts in enumerate(row):
                if counts > threshold:
                    coordinates.append((counts, (altitude, azimuth)))
        coordinates.sort(key=itemgetter(0), reverse=True)
        if len(coordinates) > 100:
            break
    return coordinates
    

def get_j2000(star_id):
    coordinates = ()
    with open(CATALOG_FILE) as catalog:
        for line in catalog:
            if line.startswith(star_id):
                items = [item.strip() for item in line.split()]
                ra = ':'.join(items[1:4])
                dec = ':'.join(items[4:7])
                coordinates = (ra, dec)

    return coordinates
        

def get_star_info(fits_file, self):
    """Get information from a FITS file."""
    image_data = fits.getdata(fits_file)
    coordinates = get_coordinates(image_data)
    if not coordinates:
        return (None,) * 3
    else:
        peacks = coordinates[:900]  # 900px, like a 30x30 area

    def peacks_mean(peacks):
        talt = tazi = tcounts = 0  # Total alt, azi and counts
        for counts, pixel in peacks:
            alt, azi = pixel
            talt += alt
            tazi += azi
            tcounts += counts
        return talt/len(peacks), tazi/len(peacks), tcounts/len(peacks)

    alt_mean, azi_mean, counts_mean = peacks_mean(peacks)
    # If the mean of the peacks is not bigger than the image
    # mean more than 500 counts, then in all likelihood there is not star
    if counts_mean - 500 < image_data.mean():
        return (None,) * 3

    # If the distance from one px to the mean of peacks is bigger than
    # px_threshold, then the px is discarded (not added to filtered_peacks)
    px_threshold = 100  # px
    filtered_peacks = []
    for counts, pixel in peacks:
        alt, azi = pixel
        alt_distance = abs(alt - alt_mean)
        azi_distance = abs(azi - azi_mean)
        if alt_distance < px_threshold and azi_distance < px_threshold:
            filtered_peacks.append((counts, pixel))

    if not filtered_peacks:
        return (None,) * 3
    
    # Get the telescope information
    hdulist = fits.open(fits_file)
    obs_alt = hdulist[0].header['FNL_ALT']
    obs_azi = hdulist[0].header['FNL_AZI']
    obs_time = hdulist[0].header['FNL_TIME']
    format_ = '%Y-%m-%dT%H:%M:%S'  # year-month-dayThour/minute/second
    obs_datetime = datetime.datetime.strptime(obs_time, format_)
    star_alt, star_azi, star_counts = peacks_mean(filtered_peacks)
    # Compute the distance (in px) from the star to the center of the image
    alt_max_px, azi_max_px = image_data.shape
    alt_offset_px = alt_max_px/2 - star_alt
    azi_offset_px = azi_max_px/2 - star_azi
    alt_offset = -(alt_offset_px*cos(CCD_ANGLE) + azi_offset_px*sin(CCD_ANGLE)) * MU
    azi_offset = +(azi_offset_px*cos(CCD_ANGLE) - alt_offset_px*sin(CCD_ANGLE)) * MU
    alt = obs_alt + alt_offset
    azi = obs_azi + azi_offset/cos(radians(obs_alt))
    file_name = os.path.basename(fits_file) 
    star_id = file_name.rstrip('.fits')
    with open(PMLOG_FILE, 'a') as pmlog_file:
        pmlog_file.write('%s (azi, alt): (%s, %s)\n' % (star_id, star_azi, star_alt))

    if not in_file_offsets(star_id):
        coordinates = get_j2000(star_id)
        source = Source(star_id, coordinates)
        plain_alt, plain_azi = source.alt_az(0, 0, obs_datetime)
        azi_offset = azi - plain_azi
        # Immagine azi=1 and plain_azi=359
        if abs(azi_offset) > 350:
            if azi < 10:
                azi += 360
            elif plain_azi < 10:
                plain_azi += 360
            else:
                return (None,) * 3
            azi_offset = azi - plain_azi

        alt_offset = alt - plain_alt
        with open(PM_FILE_OFFSETS, 'a') as offsets:
            offsets.write('%s  %.4f %.4f %.4f %.4f\n' % (
                          star_id, plain_azi, plain_alt, azi_offset, alt_offset))

    observer = ITMObserver()
    observer.date = ephem.date(obs_datetime)
    ra, dec = observer.radec_of(radians((azi+180) % 360), radians(alt))
    return str(ra).replace(':', ' '), str(dec).replace(':', ' '), obs_datetime


def get_source(source_name):
    try:
        sys.path.insert(0, CATALOG_DIR) 
        catalogs = []
        for file in os.listdir(CATALOG_DIR):
            if file.endswith('_catalog.py'):
                catalogs.append(file.rstrip('.py'))
        for module in catalogs:
            catalog = importlib.import_module(module)
            if hasattr(catalog, 'targets'):
                targets = getattr(catalog, 'targets')
                for source in targets:
                    if source.name == source_name:
                        return source
            else:
                if hasattr(catalog, source_name):
                    return getattr(catalog, source_name)
    finally:
        sys.path.remove(CATALOG_DIR)


def get_catalog(catalog_name):
    try:
        sys.path.insert(0, CATALOG_DIR) 
        return importlib.import_module(catalog_name)
    finally:
        sys.path.remove(CATALOG_DIR)
