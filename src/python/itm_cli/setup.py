import os
import sys
from setuptools.command.install import install
from setuptools import setup, find_packages
from itm_cli.config.config import PM_CONFIG


class ITMInstall(install):

    def run(self):
        install.run(self)
        catalogpath = None
        for filepath in self.get_outputs():
            name = os.path.basename(filepath)
            abspath = os.path.abspath(filepath)
            if not name.startswith('__') and 'commands' in abspath:
                os.chmod(abspath, 0755)
            if not catalogpath and 'catalog' in abspath:
                catalogpath = os.path.dirname(abspath)
        with open(os.path.join('scripts', 'itm_cli')) as itm_cli:
            lines = itm_cli.readlines()
        lines.insert(0, '#! %s\n\n' % sys.executable)
        with open('/usr/local/bin/itm_cli', 'w') as itm_cli:
            for line in lines:
                itm_cli.write(line)

        if not catalogpath:
            print 'ERROR: no catalog has been provided'
            sys.exit(1)
        # Copy the *.DAT TPoing catalogs
        relative_catalog_dir = os.path.join('itm_cli', 'catalog')
        for file in os.listdir(relative_catalog_dir):
            if file.upper().endswith('.DAT'):
                with open(os.path.join(relative_catalog_dir, file)) as f:
                    lines = f.readlines()
                with open(os.path.join(catalogpath, file), 'w') as catalog:
                    catalog.writelines(lines)
        # Copy the configuration files
        configuration_dir = os.path.join('itm_cli', 'config')
        for file in os.listdir(configuration_dir):
            if file == 'az_init.txt' or file == 'el_init.txt':
                lines = []
                with open(os.path.join(configuration_dir, file)) as f:
                    lines = f.readlines()
                with open(os.path.join(PM_CONFIG, file), 'w') as f:
                    f.writelines(lines)


setup(
    name='itm_cli',
    author='Marco Buttu',
    version='0.1',
    description='ITM Command Line Interface',
    packages=find_packages(),
    scripts=['scripts/itm_cli'],
    license='GPL',
    platforms='all',
    install_requires=[
        'astropy',
        'numpy',
        'pyephem',
        'opencv',
    ],
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
    ],
    cmdclass={'install': ITMInstall},
)
