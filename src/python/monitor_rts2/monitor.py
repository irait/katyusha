#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import rts2.scriptcomm
import time
import datetime as dt
import calendar
import gc
import matplotlib.lines as mlines

fig = plt.figure()
plt.ion()


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

mylog = open("monitor_%s.csv"%(now_str()),"wt+")


class Script (rts2.scriptcomm.Rts2Comm):
	
	def __init__(self):
	    rts2.scriptcomm.Rts2Comm.__init__(self)
	


	def autoscale_y(self,ax,margin, last_x_sample):

		def get_bottom_top(line):

			yd = line.get_ydata()
			h = np.max(yd) - np.min(yd)
			bot = np.min(yd)-margin*h
			top = np.max(yd)+margin*h
			
			return bot,top

		lines = ax.get_lines()
		bot,top = np.inf, -np.inf

		for line in lines[:-(last_x_sample-1)]:
			line.remove()

		for line in lines[-last_x_sample:]:
			new_bot, new_top = get_bottom_top(line)
			if new_bot < bot: bot = new_bot
			if new_top > top: top = new_top

		delta = (top - bot)*margin
		if (delta == 0):
			delta = 0.1
		ax.set_ylim(bot-delta,top+delta)



	def run(self):
		#self.setValue('point','false','T0')
		alt_st,az_st=self.getValue('position.AA','T0').split()
		alt_tar,az_tar=self.getValue('target.AA','T0').split()
		alt_err,az_err=self.getValue('current_error','T0').split()
		alt_speed,az_speed=self.getValue('speed','T0').split()

		last_alt_t = float(alt_st)
		last_az_t= float(az_st)
		last_alt_tar = float(alt_tar)
		last_az_tar= float(az_tar)

		if (alt_err != "nan"):
			last_alt_err = float (alt_err) * 3600
		else:
			last_alt_err = 0.

		if (az_err != "nan"):
			last_az_err = float (az_err) * 3600
		else:
			last_az_err = 0.
		
		last_alt_speed = float(alt_speed)
		last_az_speed = float(az_speed)
	
		t_now = dt.datetime.utcnow()
		last_tmstamp = calendar.timegm(t_now.utctimetuple())
		last_tmstamp += (t_now.microsecond * 0.000001)
	
		queueAlt1 = []
		queueAlt2 = []	
		queueAz1 = []
		queueAz2 = []
		queueErr1 = []
		queueErr2 = []
		queueSpeed1 = []
		queueSpeed2 = []

		while True:
			alt_st,az_st=self.getValue('position.AA','T0').split()
			alt_tar,az_tar=self.getValue('target.AA','T0').split()
			alt_err,az_err=self.getValue('current_error','T0').split()
			alt_speed,az_speed=self.getValue('speed','T0').split()

	  		alt_t = float(alt_st)*3600
	   		az_t= float(az_st)*3600
			alt_tar = float(alt_tar)*3600
	   		az_tar= float(az_tar)*3600

			
			if (alt_err != "nan"):
				alt_err = float (alt_err) *3600
			else:
				alt_err = 0.

			if (az_err != "nan"):
				az_err = float (az_err) *3600
			else:
				az_err = 0.

			alt_speed = float(alt_speed) * 3600	
			az_speed = float(az_speed) * 3600

			t_now = dt.datetime.now()
			tmstamp = calendar.timegm(t_now.utctimetuple())
			tmstamp += (t_now.microsecond * 0.000001)

			#self.log('I', 'lasttime  %.4f last_alt %.6f now  %.4f alt %.6f ' % (last_tmstamp, last_alt_t, tmstamp, alt_t))
			
			mylog.write("%s,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n"%(t_now,alt_tar,az_tar, alt_t, az_t, alt_err,  az_err, alt_speed, az_speed ))
 			mylog.flush()

			myLine1 = plt.Line2D([last_tmstamp,tmstamp], [last_alt_tar, alt_tar], color='red', lw=3,  label='TARGET')
			myLine2 = plt.Line2D([last_tmstamp,tmstamp], [last_alt_t, alt_t] , color='blue', marker="o", label='REAL')

			myLine3 = plt.Line2D([last_tmstamp,tmstamp], [last_az_tar, az_tar] , color='red', lw=3,  label='TARGET')
			myLine4 = plt.Line2D([last_tmstamp,tmstamp], [last_az_t, az_t], color='blue', marker="o", label='REAL')		

			myLine5 = plt.Line2D([last_tmstamp,tmstamp], [last_alt_err, alt_err] , color='green', marker=".", label='ALT')
			myLine6 = plt.Line2D([last_tmstamp,tmstamp], [last_az_err, az_err] , color='blue', marker='.', label='AZ')
			
			myLine7 = plt.Line2D([last_tmstamp,tmstamp], [last_alt_speed, alt_speed] , color='green',marker=".", label='ALT')
			myLine8 = plt.Line2D([last_tmstamp,tmstamp], [last_az_speed, az_speed] , color='blue', marker=".", label='AZ')
			

			
	
			queueAlt1.append(myLine1)
			queueAlt2.append(myLine2)
			queueAz1.append(myLine3)
			queueAz2.append(myLine4)
			queueErr1.append(myLine5)
			queueErr2.append(myLine6)
			queueSpeed1.append(myLine7)
			queueSpeed2.append(myLine8)

			lastTime = 0.	 
			thisTime = int(tmstamp % 12)

			#self.log('I', 'tiem %.4f ' % (thisTime))

			if (thisTime == 0.) and (lastTime !=  int(tmstamp)): 

		
				lastTime = thisTime
				plt.subplot(321)
				plt.title("Alt taget vs. enc LONG TIME")
				axes = plt.gca()
			
				axes.add_line(plt.Line2D([last_tmstamp,tmstamp], [last_alt_t, alt_t] , color='red', lw=3))
				axes.autoscale()

				######################################################

				plt.subplot(322)
				plt.title("Alt taget vs. enc 10 SEC.")
				axes = plt.gca()
				axes.legend((myLine1, myLine2), ('target', 'real'), loc=3)
				axes.set_xticklabels(['-10','-8','-6','-4', '-2','0'])
				cnt = len(queueAlt1) + len(queueAlt2)
				for el in queueAlt1:
				 	axes.add_line(queueAlt1.pop())

				for el in queueAlt2:
				 	axes.add_line(queueAlt2.pop())

				axes.set_xlim([tmstamp-10.,tmstamp])	
				#axes.autoscale()
				self.autoscale_y(axes, 0.2,70)

				######################################################


				plt.subplot(323)
				plt.title("Azi taget vs. enc LONG TIME")
				axes = plt.gca()

				axes.add_line(plt.Line2D([last_tmstamp,tmstamp], [last_az_t, az_t] , color='red', lw=3))
				axes.autoscale()

				#######################################################

				plt.subplot(324)
				plt.title("Azi taget vs. enc 10 SEC")
				axes = plt.gca()
				axes.set_xticklabels(['-10','-8','-6','-4', '-2','0'])
		
				axes.legend((myLine3, myLine4), ('target', 'real'), loc=3)
				cnt = len(queueAz1) + len(queueAz2)
				for el in queueAz1:
				 	axes.add_line(queueAz1.pop())

				for el in queueAz2:
				 	axes.add_line(queueAz2.pop())

				axes.set_xlim([tmstamp-10.,tmstamp])	
				self.autoscale_y(axes, 0.2,cnt*2)

				#######################################################

				plt.subplot(325)
				plt.title("Ponting Error")
				
	
		
				axes = plt.gca()
				axes.legend((myLine5, myLine6), ('ALT', 'AZ'), loc=3)
				axes.set_xticklabels(['-10','-8','-6','-4', '-2','0'])
				cnt = len(queueErr1) + len(queueErr2)
				for el in queueErr1:
				 	axes.add_line(queueErr1.pop())

				for el in queueErr2:
				 	axes.add_line(queueErr2.pop())

				axes.set_xlim([tmstamp-10.,tmstamp])	
				self.autoscale_y(axes, 0.2,cnt*2)

				

				##############################################################

				plt.subplot(326)
				plt.title("Speed")
				axes = plt.gca()

				axes.legend((myLine7, myLine8), ('ALT', 'AZ'), loc=3)
				axes.set_xticklabels(['-10','-8','-6','-4', '-2','0'])
				cnt = len(queueSpeed1) + len(queueSpeed2)
				for el in queueSpeed1:
				 	axes.add_line(queueSpeed1.pop())

				for el in queueSpeed2:
				 	axes.add_line(queueSpeed2.pop())

				axes.set_xlim([tmstamp-10.,tmstamp])
				self.autoscale_y(axes, 0.2,cnt*2)

				###############################################################

				plt.pause(0.01)
				fig.clf()
				gc.collect()

			last_tmstamp = tmstamp
			last_alt_t = alt_t
			last_alt_tar = alt_tar
			last_az_t = az_t
			last_az_tar = az_tar
			last_alt_err = alt_err
			last_az_err = az_err
			last_alt_speed = alt_speed
			last_az_speed = az_speed

			time.sleep(0.4)
			   	
    		

a = Script()
a.run()
