#!/usr/bin/python

import numpy as np
import rts2.scriptcomm
import time
import datetime as dt
import calendar
import gc
import matplotlib.lines as mlines



def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

mylog = open("monitor_data_%s.csv"%(now_str()),"wt+")


class Script (rts2.scriptcomm.Rts2Comm):
	
	def __init__(self):
	    rts2.scriptcomm.Rts2Comm.__init__(self)
	

	def run(self):
	
		while True:
                        #self.log('I', 'status %s' %(self.getValue('status', 'T0')))

			if ('POINTING' != self.getValue('status','T0')):
				while 'POINTING' != self.getValue('status','T0'):
                			time.sleep(0.1)

				time.sleep(30)
                        else:

                            alt_err,az_err=self.getValue('current_error','T0').split()
                            alt_speed,az_speed=self.getValue('speed','T0').split()

			
                            if (alt_err != "nan"):
				alt_err = float (alt_err) *3600
                            else:
				alt_err = 0.

                            if (az_err != "nan"):
				az_err = float (az_err) *3600
                            else:
				az_err = 0.

                            alt_speed = float(alt_speed) * 3600	
                            az_speed = float(az_speed) * 3600

                            t_now = dt.datetime.now()
                            tmstamp = calendar.timegm(t_now.utctimetuple())
                            tmstamp += (t_now.microsecond * 0.000001)

                            #self.log('I', 'speed  %.4f ' % (alt_speed))
			

                            mylog.write("%s,%s,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n"%(t_now, tmstamp, alt_err,  az_err, alt_speed, az_speed, abs(alt_err), abs(az_err), (100*alt_err)/alt_speed, (100*az_err)/az_speed))
                            mylog.flush()

			
			time.sleep(2)
			   	
    		

a = Script()
a.run()
