
import os
import math
import scipy
import matplotlib
import pylab
import pyfits
import csv

import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import leastsq

myfile = "./pointing_inv.csv"
az_pos_csv = []
alt_corr_csv = []
az_corr_csv = []

canopo = "./canopo_pointing_inv.csv"
canopo_az_pos_csv = []
canopo_alt_corr_csv = []
canopo_az_corr_csv = []

sirio = "./sirio_pointing_inv.csv"
sirio_az_pos_csv = []
sirio_alt_corr_csv = []
sirio_az_corr_csv = []


### data for fit ##########

data = csv.reader(open(myfile))
data.next()

for row in data:
	az_pos_csv.append(float(row[0]))
	alt_corr_csv.append(float(row[1]))
	az_corr_csv.append(float(row[2]))

az_pos = scipy.array(az_pos_csv)
alt_corr = scipy.array(alt_corr_csv)
az_corr = scipy.array(az_corr_csv)


### data for test #########

test = csv.reader(open(canopo))
test.next()

for row in test:
	#print row[0]
	canopo_az_pos_csv.append(float(row[0]))
	canopo_alt_corr_csv.append(float(row[1]))
	canopo_az_corr_csv.append(float(row[2]))

canopo_az_pos = scipy.array(canopo_az_pos_csv)
canopo_alt_corr = scipy.array(canopo_alt_corr_csv)
canopo_az_corr = scipy.array(canopo_az_corr_csv)


test1 = csv.reader(open(sirio))
test1.next()

for row in test1:
	
	sirio_az_pos_csv.append(float(row[0]))
	sirio_alt_corr_csv.append(float(row[1]))
	sirio_az_corr_csv.append(float(row[2]))

sirio_az_pos = scipy.array(sirio_az_pos_csv)
sirio_alt_corr = scipy.array(sirio_alt_corr_csv)
sirio_az_corr = scipy.array(sirio_az_corr_csv)



### model creation

p_az = scipy.polyfit(az_pos, az_corr, 8)

yfit=scipy.polyval(p_az, az_pos)
yfit_test = scipy.polyval(p_az, canopo_az_pos)
yfit_test1 = scipy.polyval(p_az, sirio_az_pos)


#plt.plot(az_pos, yfit, 'r')
plt.plot(sirio_az_pos, yfit_test1, 'r')
#plt.plot(sirio_az_pos, sirio_az_corr_csv, 'g.')
plt.plot(canopo_az_pos, canopo_az_corr_csv, 'b.')

errors_model_az = abs(yfit - az_corr_csv)
errors_test_az = abs(yfit_test - canopo_az_corr_csv)

errors_test1_az = abs(yfit_test1 - sirio_az_corr_csv)


print "p_az: "+ str(p_az)

print "alnair (model base)"
print "min:",errors_model_az.min(),", max:", errors_model_az.max(), ", mean: ", errors_model_az.mean()
print "canopus (model test)"
print "min:",errors_test_az.min(),", max:", errors_test_az.max(), ", mean: ", errors_test_az.mean()
print "sirius (model test)"
print "min:",errors_test1_az.min(),", max:", errors_test1_az.max(), ", mean: ", errors_test1_az.mean()

plt.show()


p_alt = scipy.polyfit(az_pos, alt_corr, 5)
yfit=scipy.polyval(p_alt, az_pos)
yfit_test = scipy.polyval(p_alt, canopo_az_pos)
yfit_test1 = scipy.polyval(p_alt, sirio_az_pos)
plt.plot(az_pos, yfit, 'r')
#plt.plot(az_pos, alt_corr_csv, 'b.')
#plt.plot(sirio_az_pos, sirio_alt_corr_csv, 'g.')
plt.plot(canopo_az_pos, canopo_alt_corr_csv, 'b.')
print "p_alt: "+ str(p_alt)

errors_model = abs(yfit - alt_corr_csv)
errors_test = abs(yfit_test - canopo_alt_corr_csv)
errors_test1 = abs(yfit_test1 - sirio_alt_corr_csv)


print "alnair (model base)"
print "min:",errors_model.min(),", max:", errors_model.max(), ", mean: ", errors_model.mean()
print "canopus (model test)"
print "min:",errors_test.min(),", max:", errors_test.max(), ", mean: ", errors_test.mean()
print "siriusus (model test)"
print "min:",errors_test1.min(),", max:", errors_test1.max(), ", mean: ", errors_test1.mean()

plt.show()


complete_az_pos = scipy.array(az_pos_csv + canopo_az_pos_csv + sirio_az_pos_csv)
complete_alt_corr =  scipy.array(alt_corr_csv + canopo_alt_corr_csv + sirio_alt_corr_csv)
complete_az_corr = scipy.array( az_corr_csv + canopo_az_corr_csv + sirio_az_corr_csv)


p_alt = scipy.polyfit(complete_az_pos, complete_alt_corr, 5)
yfit=scipy.polyval(p_alt, complete_az_pos)

yfit_test = scipy.polyval(p_alt, canopo_az_pos)
yfit_test1 = scipy.polyval(p_alt, sirio_az_pos)
yfit_test2 = scipy.polyval(p_alt, az_pos)


errors_test = abs(yfit_test - canopo_alt_corr_csv)
errors_test1 = abs(yfit_test1 - sirio_alt_corr_csv)
errors_model = abs(yfit_test2 - alt_corr_csv)


print "complete model"



print "alnair (model test)"
print "min:",errors_model.min(),", max:", errors_model.max(), ", mean: ", errors_model.mean()
print "canopus (model test)"
print "min:",errors_test.min(),", max:", errors_test.max(), ", mean: ", errors_test.mean()
print "siriusus (model test)"
print "min:",errors_test1.min(),", max:", errors_test1.max(), ", mean: ", errors_test1.mean()



plt.plot(complete_az_pos, yfit, 'r')
plt.plot(az_pos, alt_corr_csv, 'k.')
plt.plot(sirio_az_pos, sirio_alt_corr_csv, 'g.')
plt.plot(canopo_az_pos, canopo_alt_corr_csv, 'b.')

plt.show()


#y = p[0]*x**5 + p[1]*x**4 + p[2]*x**3 + p[3]*x**2 + p[4]*x + p[5] 




