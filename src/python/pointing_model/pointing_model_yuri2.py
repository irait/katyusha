
import os
import math
import scipy
import matplotlib
import pylab
import pyfits
import csv

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.optimize import leastsq
import scipy.optimize as optimize

myfile = "./a_point_model_az.csv"
alt_pos_csv = []
az_pos_csv = []
alt_corr_csv = []
az_corr_csv = []

canopo = "./c_point_model_az.csv"
canopo_alt_pos_csv = []
canopo_az_pos_csv = []
canopo_alt_corr_csv = []
canopo_az_corr_csv = []

sirio = "./s_point_model_az.csv"
sirio_alt_pos_csv = []
sirio_az_pos_csv = []
sirio_alt_corr_csv = []
sirio_az_corr_csv = []


### data for fit alnair  ##########

data = csv.reader(open(myfile))
data.next()

for row in data:
	alt_pos_csv.append(float(row[0]))
	az_pos_csv.append(float(row[1]))
	alt_corr_csv.append(float(row[2]))
	az_corr_csv.append(float(row[3]))



### data for fit canopo #########

test = csv.reader(open(canopo))
test.next()

for row in test:
	#print row[1]
	canopo_alt_pos_csv.append(float(row[0]))
	canopo_az_pos_csv.append(float(row[1]))
	canopo_alt_corr_csv.append(float(row[2]))
	canopo_az_corr_csv.append(float(row[3]))

### data for fit sirio #########

test1 = csv.reader(open(sirio))
test1.next()

for row in test1:
	#print row[1]
	sirio_alt_pos_csv.append(float(row[0]))
	sirio_az_pos_csv.append(float(row[1]))
	sirio_alt_corr_csv.append(float(row[2]))
	sirio_az_corr_csv.append(float(row[3]))


complete_alt_pos = scipy.array(alt_pos_csv + canopo_alt_pos_csv + sirio_alt_pos_csv)
complete_az_pos = scipy.array(az_pos_csv + canopo_az_pos_csv + sirio_az_pos_csv)
complete_alt_corr =  scipy.array(alt_corr_csv + canopo_alt_corr_csv + sirio_alt_corr_csv)
complete_az_corr = scipy.array( az_corr_csv + canopo_az_corr_csv + sirio_az_corr_csv)

x = np.array([complete_alt_pos, complete_az_pos])

def overfit(data,a1,b1,c1,d1,e1,f1,g1, b0, a2,b2,c2, d2,e2,f2, f):
	return  a1*data[0]**7 +b1*data[0]**6+ c1*data[0]**5+  d1*data[0]**4+ e1*data[0]**3+ f1*data[0]**2 +g1*data[0] + b0*data[1]**7 +a2*data[1]**6+ b2*data[1]**5+  c2*data[1]**4+ d2*data[1]**3+ e2*data[1]**2 + f2*data[1] +f


def func(data,e1,f1,g1, b0, a2,b2,c2, d2,e2,f2):
    return  e1*data[0]**3+ f1*data[0]+  b0*data[1]**7 +a2*data[1]**6+ b2*data[1]**5+  c2*data[1]**4+ d2*data[1]**3+ e2*data[1]**2 + f2*data[1] + g1

def func_alt(data, a0, a1,b1,c1,d1,e1, f1, a2,b2,c2):
    return   a0*data[1]**6 +a1*data[1]**5 + b1*data[1]**4 + c1*data[1]**3+ d1*data[1]**2+ data[1]*e1+ f1 +a2*data[0]**3+ b2*data[0]**2+ data[0]*c2 


params, pcov = optimize.curve_fit(func, x, complete_az_corr)

params1, pcov1 = optimize.curve_fit(func_alt, x, complete_alt_corr)

print "az coeff:", params
print "alt coeff:", params1

def find_az(alt, az, params):
	
	(e1,f1,g1,b0,a2,b2,c2,d2,e2,f2) = params
	return    e1*alt**3+ f1*alt + g1 + b0*az**7 + a2*az**6+ b2*az**5+  c2*az**4+ d2*az**3+ e2*az**2 + f2*az


def find_alt(alt, az,params):
	(a0,a1,b1,c1,d1,e1, f1, a2,b2,c2) = params
	return  a0*az**6 +a1*az**5 + b1*az**4 + c1*az**3+ d1*az**2+ az*e1+ f1 +a2*alt**3+ b2*alt**2+ alt*c2 






test_alt_pos = []
test_az_pos = []
test_model = []

test_model_alt = []

for i in np.arange(10,80, 5):
	for y in np.arange(10,350, 5):
		test_alt_pos.append(i)
		test_az_pos.append(y)
		test_model.append(find_az(i, y, params) )
		test_model_alt.append(find_alt(i, y, params1) )




diff = []
diff_alt = []
for i in range(0,len(complete_alt_pos)-1):

	result = find_az(complete_alt_pos[i],complete_az_pos[i], params) 
	realdata = complete_az_corr[i]
	diff.append(abs(result -realdata))

	result = find_alt(complete_alt_pos[i],complete_az_pos[i], params1) 
	realdata = complete_alt_corr[i]
	diff_alt.append(abs(result -realdata))


diffnp = np.array(diff)
diffaltnp = np.array(diff_alt)


print "model azimuth"

print "max:" ,diffnp.max()
print "min:" ,diffnp.min()
print "mean:" ,diffnp.mean()

print "model alt"

print "max:" ,diffaltnp.max()
print "min:" ,diffaltnp.min()
print "mean:" ,diffaltnp.mean()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(complete_alt_pos, complete_az_pos, complete_az_corr, c='r', marker='o')
ax.scatter(test_alt_pos,test_az_pos,test_model, c='b', marker='o')

ax.set_xlabel('X - alt')
ax.set_ylabel('Y - az')
ax.set_zlabel('Z - corr (arcsec)')
plt.show()


fig1 = plt.figure()
ax = fig1.add_subplot(111, projection='3d')
ax.scatter(complete_alt_pos, complete_az_pos, complete_alt_corr, c='r', marker='o')
ax.scatter(test_alt_pos,test_az_pos,test_model_alt, c='b', marker='o')

ax.set_xlabel('X - alt')
ax.set_ylabel('Y - az')
ax.set_zlabel('Z - corr (arcsec)')
plt.show()






