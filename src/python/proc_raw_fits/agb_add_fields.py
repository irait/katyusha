from __future__ import print_function

import os
import sys
from astropy.io import fits

def update_agb_files(dir_path, filter):
    first, second = dir_path.split('_samples_')
    source_name = first.split('/')[-1]
    for file in os.listdir(dir_path):
        if not file.endswith('.fits'):
            continue
        if ('_%s_' % filter) not in file:
            print('Wrong filter %s for file %s' % (filter, file))
            sys.exit(0)

        abs_path = os.path.join(dir_path, file)
        ELARAW = 'ela' if 'elab' in file else 'raw'

        hdulist = fits.open(abs_path)
        header = hdulist[0].header
        DATE = header['DATE']
        EXPOSURE = header['EXPOSURE']
        CCD_TYPE = header['CCD_TYPE']
        CCD_TEMP = header['CCD_TEMP']
        CCD_AIR = header['CCD_AIR']
        TELRA = header['TELRA']
        TELDEC = header['TELDEC']
        hdulist.close()
        
        # Update the fits header
        hdulist = fits.open(abs_path, mode='update')
        header = hdulist[0].header
        print('Updating file %s' % file)
        header['OBJ'] = (source_name, 'Name of the observed object')
        header['EXTNAME'] = ('LIGHT', '')
        header['UTC'] = (DATE, 'Date of creation UTC')
        header['TELESCOP'] = ('IRAIT', 'Name of the data acqusition telescope')
        header['INSTRUM'] = (CCD_TYPE, 'Camera model')
        header['ELARAW'] = (ELARAW, 'Elaborated or raw')
        header['REFDOC'] = ('', 'Documentation reference')
        header['FILTER'] = (filter, 'Filter for frame')
        header['VERSION'] = '1.0'
        header['CHIP_T'] = (CCD_TEMP, 'Chip CCD temperature (Celsius)')
        header['CAMERA_T'] = (CCD_AIR, 'Camera temperature (Celsius)')
        header['EXPOS'] = (EXPOSURE, 'Total Exposure time (sec)')
        header['RA'] = (TELRA, 'Right ascension of the target')
        header['DEC'] = (TELDEC, 'Declination of the target')
        hdulist.close()

