#! /usr/bin/python
from __future__ import division

import datetime as dt
import sys
import os

import numpy as np
from astropy.io import fits

from operator import itemgetter
from itm_cli.utils import script_args, RTS2Wrapper
from itm_cli.config import OBSERVATION_PATH

from agb_add_fields import update_agb_files  # IMPORTANT




class Clean(RTS2Wrapper):

    def __init__(self, catalog, observation):
        # For instance: catalog=agb, observation=2018_obs02
        self.root_dir = os.path.join(OBSERVATION_PATH, catalog, observation)

    def run(self):
        to_clean = []  # [(star_name, filter, [files]]
        for star in os.listdir(self.root_dir):
            star_dir = os.listdir(os.path.join(self.root_dir, star))
            for filter in os.listdir(star_dir):
                filter_dir = os.path.join(star_dir, filter))
                files = os.listdir(filter_dir)
                files_to_clean = []
                for file in files:
                    file_path = os.path.join(filter_dir, file)
                    if not file.starts('elab_'):
                        if ('elab_%s' % file) not in files:
                            # Elaborate that file
                            files_to_clean.append(file_path)
                to_clean.append(star, filter, files_to_clean)
         # Testare il codice di sopra per stampare le immagini di cui fare il clean
                           
                            
            for file in to_elaborate
            darkfile = fits.open('mean30frame_DARK_exp_%s.fits' % exp)
            dark = darkfile[0].data


            dark = selDark(exp)
            flat = selFlat(filt)
            
            for f in filelist:
                if f.endswith('.fits'):
                    myfile = fits.open(''+folder+'/'+f)
                    data = myfile[0].data
                    img = data - dark
                    img = img / flat
                    hdulist=fits.HDUList()
                    hdulist.append(fits.ImageHDU(data=img))
                    ihdu= len(hdulist)-1
                    hdulist[ihdu].header = myfile[0].header
                    hdulist.writeto(''+folder+'/elab_'+f,clobber=True)

else:
    print "usage procRaw ./folderpath exp filter (es:python procRaw /home/irait/mystar 30 R)"


update_agb_files(folder, filt)




def calcMedian(a):
     b= []
     for el in a:
         for e in el:
             b.append(e)

     c = np.array(b)
     c.sort()
     d1 = c[4669568]
     d2 = c[4669569]
     ris = (d1 + d2) / 2
     print "median flat is: "+str(ris)
     return ris

def selDark(exp):
    darkfile = fits.open('mean30frame_DARK_exp_%s.fits' % exp)
    dark = darkfile[0].data
    return dark

def selFlat(filt):
    flatfile = fits.open('mean30frame_FLAT_filter_%s.fits' % filt)
    flatRaw = flatfile[0].data
    fm = calcMedian(flatRaw)
    flat = flatRaw / fm
    return flat

#------------------------------------
from __future__ import print_function

import os
import sys
from astropy.io import fits

def update_agb_files(dir_path, filter):
    first, second = dir_path.split('_samples_')
    source_name = first.split('/')[-1]
    for file in os.listdir(dir_path):
        if not file.endswith('.fits'):
            continue
        if ('_%s_' % filter) not in file:
            print('Wrong filter %s for file %s' % (filter, file))
            sys.exit(0)

        abs_path = os.path.join(dir_path, file)
        ELARAW = 'ela' if 'elab' in file else 'raw'

        hdulist = fits.open(abs_path)
        header = hdulist[0].header
        DATE = header['DATE']
        EXPOSURE = header['EXPOSURE']
        CCD_TYPE = header['CCD_TYPE']
        CCD_TEMP = header['CCD_TEMP']
        CCD_AIR = header['CCD_AIR']
        TELRA = header['TELRA']
        TELDEC = header['TELDEC']
        hdulist.close()
        
        # Update the fits header
        hdulist = fits.open(abs_path, mode='update')
        header = hdulist[0].header
        print('Updating file %s' % file)
        header['OBJ'] = (source_name, 'Name of the observed object')
        header['EXTNAME'] = ('LIGHT', '')
        header['UTC'] = (DATE, 'Date of creation UTC')
        header['TELESCOP'] = ('IRAIT', 'Name of the data acqusition telescope')
        header['INSTRUM'] = (CCD_TYPE, 'Camera model')
        header['ELARAW'] = (ELARAW, 'Elaborated or raw')
        header['REFDOC'] = ('', 'Documentation reference')
        header['FILTER'] = (filter, 'Filter for frame')
        header['VERSION'] = '1.0'
        header['CHIP_T'] = (CCD_TEMP, 'Chip CCD temperature (Celsius)')
        header['CAMERA_T'] = (CCD_AIR, 'Camera temperature (Celsius)')
        header['EXPOS'] = (EXPOSURE, 'Total Exposure time (sec)')
        header['RA'] = (TELRA, 'Right ascension of the target')
        header['DEC'] = (TELDEC, 'Declination of the target')
        hdulist.close()

