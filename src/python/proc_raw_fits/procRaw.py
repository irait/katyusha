import numpy as np
import pylab as plt

import datetime as dt
import time as tm
import pyfits
import ephem as ep
import sys
import os

from agb_add_fields import update_agb_files



#32353 #median calcuated for this flat file  CLEAR


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')


def calcMedian(a):

	b= []

	for el in a:
	    for e in el:
		b.append(e)

	c = np.array(b)
	c.sort()

	d1 = c[4669568]
	d2 = c[4669569]
	ris = (d1 + d2) / 2
		
	print "median flat is: "+str(ris)

	return ris

def selDark(exp):
	
	darkfile = pyfits.open('mean30frame_DARK_exp_%s.fits' % exp)
	dark = darkfile[0].data
	return dark

def selFlat(filt):
	flatfile = pyfits.open('mean30frame_FLAT_filter_%s.fits' % filt)
	flatRaw = flatfile[0].data
	fm = calcMedian(flatRaw)
	flat = flatRaw / fm
	return flat



if len(sys.argv) > 3 :
	folder=sys.argv[1]
	exp = str(sys.argv[2])
	filt = str(sys.argv[3])

	filelist = os.listdir(folder)
	filelist.sort()

	dark = selDark(exp)
	flat = selFlat(filt)

	for f in filelist:
		if f.endswith('.fits'):
			myfile = pyfits.open(''+folder+'/'+f)
			data = myfile[0].data


			img = data - dark
			img = img / flat

			hdulist=pyfits.HDUList()
			hdulist.append(pyfits.ImageHDU(data=img))
			ihdu= len(hdulist)-1
			hdulist[ihdu].header = myfile[0].header

			hdulist.writeto(''+folder+'/elab_'+f,clobber=True)

else:
	print "usage procRaw ./folderpath exp filter (es:python procRaw /home/irait/mystar 30 R)"


update_agb_files(folder, filt)
