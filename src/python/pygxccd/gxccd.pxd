cimport numpy as np

ctypedef np.uint8_t uint8_t
ctypedef np.uint16_t uint16_t
ctypedef np.uint32_t uint32_t
ctypedef np.int16_t int16_t

cdef extern from "gxccd.h":
    const char* GXETH_DEFAULT_IP
    const uint16_t GXETH_DEFAULT_PORT

    ctypedef void (*enum_callback_t)(int)

    struct camera:
       pass

    ctypedef camera camera_t

    void gxccd_configure(const char *ini_path)

    void gxccd_configure_eth(const char *ip, uint16_t port)
    
    void gxccd_enumerate_usb(enum_callback_t callback)

    void gxccd_enumerate_eth(enum_callback_t callback)

    camera_t *gxccd_initialize_usb(int camera_id)

    camera_t *gxccd_initialize_eth(int camera_id)

    void gxccd_release(camera_t *camera)

    cdef enum :
      GBP_CONNECTED
      GBP_SUB_FRAME
      GBP_READ_MODES
      GBP_SHUTTER
      GBP_COOLER
      GBP_FAN
      GBP_FILTERS
      GBP_GUIDE
      GBP_WINDOW_HEATING
      GBP_PREFLASH
      GBP_ASYMMETRIC_BINNING
      GBP_MICROMETER_FILTER_OFFSETS
      GBP_POWER_UTILIZATION
      GBP_GAIN
      GBP_CONFIGURED
      GBP_RGB
      GBP_CMY
      GBP_CMYG
      GBP_DEBAYER_X_ODD
      GBP_DEBAYER_Y_ODD
      GBP_INTERLACED

    int gxccd_get_boolean_parameter(camera_t *camera, int index, bint *value)

    cdef enum :
      GIP_CAMERA_ID
      GIP_CHIP_W
      GIP_CHIP_D
      GIP_PIXEL_W
      GIP_PIXEL_D
      GIP_MAX_BINNING_X
      GIP_MAX_BINNING_Y
      GIP_READ_MODES
      GIP_FILTERS
      GIP_MINIMAL_EXPOSURE
      GIP_MAXIMAL_EXPOSURE
      GIP_MAXIMAL_MOVE_TIME
      GIP_DEFAULT_READ_MODE
      GIP_PREVIEW_READ_MODE
      GIP_MAX_WINDOW_HEATING
      GIP_MAX_FAN
      GIP_FIRMWARE_MAJOR
      GIP_FIRMWARE_MINOR
      GIP_FIRMWARE_BUILD
      GIP_DRIVER_MAJOR
      GIP_DRIVER_MINOR
      GIP_DRIVER_BUILD
      GIP_FLASH_MAJOR
      GIP_FLASH_MINOR
      GIP_FLASH_BUILD

    int gxccd_get_integer_parameter(camera_t *camera, int index, int *value)

    cdef enum :
      GSP_CAMERA_DESCRIPTION
      GSP_MANUFACTURER
      GSP_CAMERA_SERIAL
      GSP_CHIP_DESCRIPTION

    int gxccd_get_string_parameter(camera_t *camera, int index, char *buf,size_t size)

    cdef enum :
      GV_CHIP_TEMPERATURE
      GV_HOT_TEMPERATURE
      GV_CAMERA_TEMPERATURE
      GV_ENVIRONMENT_TEMPERATURE
      GV_SUPPLY_VOLTAGE
      GV_POWER_UTILIZATION
      GV_ADC_GAIN

    int gxccd_get_value(camera_t *camera, int index, float *value)

    int gxccd_set_temperature(camera_t *camera, float temp)

    int gxccd_set_temperature_ramp(camera_t *camera, float temp_ramp)

    int gxccd_set_binning(camera_t *camera, int x, int y)

    int gxccd_set_preflash(camera_t *camera, double preflash_time,int clear_num)

    int gxccd_start_exposure(camera_t *camera, double exp_time, bint use_shutter, int x, int y, int w, int d)

    int gxccd_abort_exposure(camera_t *camera, bint download)

    int gxccd_image_ready(camera_t *camera, bint *ready)

    int gxccd_read_image(camera_t *camera, void *buf, size_t size)

    int gxccd_enumerate_read_modes(camera_t *camera, int index, char *buf, size_t size)

    int gxccd_set_read_mode(camera_t *camera, int mode)

    int gxccd_enumerate_filters(camera_t *camera, int index, char *buf, size_t size, uint32_t *color, int *offset)

    int gxccd_set_filter(camera_t *camera, int index)

    int gxccd_set_fan(camera_t *camera, uint8_t speed)

    int gxccd_set_window_heating(camera_t *camera, uint8_t heating)

    int gxccd_move_telescope(camera_t *camera, int16_t ra_duration_ms,int16_t dec_duration_ms)

    int gxccd_move_in_progress(camera_t *camera, bint *moving)

    void gxccd_get_last_error(camera_t *camera, char *buf, size_t size)

