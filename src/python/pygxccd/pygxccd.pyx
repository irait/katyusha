import numpy as np
cimport gxccd as gx
cimport numpy as np
import copy
from exceptions import Exception

GXETH_DEFAULT_IP = gx.GXETH_DEFAULT_IP          
GXETH_DEFAULT_PORT = gx.GXETH_DEFAULT_PORT

GBP_CONNECTED = gx.GBP_CONNECTED                                    # true if camera currently connected
GBP_SUB_FRAME = gx.GBP_SUB_FRAME                                    # true if camera supports sub-frame read
GBP_READ_MODES = gx.GBP_READ_MODES                                  # true if camera supports multiple read modes
GBP_SHUTTER = gx.GBP_SHUTTER                                        # true if camera is equipped with mechanical shutter
GBP_COOLER = gx.GBP_COOLER                                          # true if camera is equipped with active CCD cooler
GBP_FAN = gx.GBP_FAN                                                # true if camera fan can be controlled
GBP_FILTERS = gx.GBP_FILTERS                                        # true if camera controls filter wheel
GBP_GUIDE = gx.GBP_GUIDE                                            # true if camera is capable to guide the telescope mount
GBP_WINDOW_HEATING = gx.GBP_WINDOW_HEATING                          # true if camera can control the CCD window heating
GBP_PREFLASH = gx.GBP_PREFLASH                                      # true if camera can use CCD preflash
GBP_ASYMMETRIC_BINNING = gx.GBP_ASYMMETRIC_BINNING                  # true if camera horizontal and vertical binning can differ
GBP_MICROMETER_FILTER_OFFSETS = gx.GBP_MICROMETER_FILTER_OFFSETS    # true if filter focusing offsets are expressed in micrometers
GBP_POWER_UTILIZATION = gx.GBP_POWER_UTILIZATION                    # true if camera can return power utilization in get_value()
GBP_GAIN = gx.GBP_GAIN                                              # true if camera can return gain in get_value() 
GBP_CONFIGURED = gx.GBP_CONFIGURED                                  # true if camera is configured
GBP_RGB = gx.GBP_RGB                                                # true if camera has Bayer RGBG filters on CCD
GBP_CMY = gx.GBP_CMY                                                # true if camera has CMY filters on CCD
GBP_CMYG = gx.GBP_CMYG                                              # true if camera has CMYG filters on CCD
GBP_DEBAYER_X_ODD = gx.GBP_DEBAYER_X_ODD                            # true if camera Bayer masks starts on horizontal odd pixel
GBP_DEBAYER_Y_ODD = gx.GBP_DEBAYER_Y_ODD                            # true if camera Bayer masks starts on vertical odd pixel
GBP_INTERLACED = gx.GBP_INTERLACED                                  # true if CCD detector is interlaced (else progressive)


GIP_CAMERA_ID = gx.GIP_CAMERA_ID                        # Identifier of the current camera
GIP_CHIP_W = gx.GIP_CHIP_W                              # CCD detector width in pixels
GIP_CHIP_D = gx.GIP_CHIP_D                              # CCD detector depth in pixels
GIP_PIXEL_W = gx.GIP_PIXEL_W                            # CCD pixel width in nanometers
GIP_PIXEL_D = gx.GIP_PIXEL_D                            # CCD pixel depth in nanometers
GIP_MAX_BINNING_X = gx.GIP_MAX_BINNING_X                # Maximum binning in horizontal direction
GIP_MAX_BINNING_Y = gx.GIP_MAX_BINNING_Y                # Maximum binning in vertical direction
GIP_READ_MODES = gx.GIP_READ_MODES                      # Number of read modes offered by the camera
GIP_FILTERS = gx.GIP_FILTERS                            # Number of filters offered by the camera
GIP_MINIMAL_EXPOSURE = gx.GIP_MINIMAL_EXPOSURE          # Shortest exposure time in microseconds (µs)
GIP_MAXIMAL_EXPOSURE = gx.GIP_MAXIMAL_EXPOSURE          # Longest exposure time in milliseconds (ms)
GIP_MAXIMAL_MOVE_TIME = gx.GIP_MAXIMAL_MOVE_TIME        # Longest time to move the telescope in milliseconds (ms)
GIP_DEFAULT_READ_MODE = gx.GIP_DEFAULT_READ_MODE        # Read mode to be used as default
GIP_PREVIEW_READ_MODE = gx.GIP_PREVIEW_READ_MODE        # Read mode to be used for preview (fast read)
GIP_MAX_WINDOW_HEATING = gx.GIP_MAX_WINDOW_HEATING      # Maximal value for gxccd_set_window_heating()
GIP_MAX_FAN = gx.GIP_MAX_FAN                            # Maximal value for gxccd_set_fan()
GIP_FIRMWARE_MAJOR = gx.GIP_FIRMWARE_MAJOR              # Camera firmware version (optional)
GIP_FIRMWARE_MINOR = gx.GIP_FIRMWARE_MINOR              
GIP_FIRMWARE_BUILD = gx.GIP_FIRMWARE_BUILD              
GIP_DRIVER_MAJOR = gx.GIP_DRIVER_MAJOR                  # Driver version (optional)
GIP_DRIVER_MINOR = gx.GIP_DRIVER_MINOR                  
GIP_DRIVER_BUILD = gx.GIP_DRIVER_BUILD                  
GIP_FLASH_MAJOR = gx.GIP_FLASH_MAJOR                    # Flash version (optional)
GIP_FLASH_MINOR = gx.GIP_FLASH_MINOR                    
GIP_FLASH_BUILD = gx.GIP_FLASH_BUILD                    

GSP_CAMERA_DESCRIPTION = gx.GSP_CAMERA_DESCRIPTION         # Camera description
GSP_MANUFACTURER = gx.GSP_MANUFACTURER                     # Manufacturer name
GSP_CAMERA_SERIAL = gx.GSP_CAMERA_SERIAL                   # Camera serial number
GSP_CHIP_DESCRIPTION = gx.GSP_CHIP_DESCRIPTION             # Used CCD detector description

GV_CHIP_TEMPERATURE = gx.GV_CHIP_TEMPERATURE                 # Current temperature of the CCD detector in deg. Celsius
GV_HOT_TEMPERATURE = gx.GV_HOT_TEMPERATURE                   # Current temperature of the cooler hot side in deg. Celsius
GV_CAMERA_TEMPERATURE = gx.GV_CAMERA_TEMPERATURE             # Current temperature inside the camera in deg. Celsius
GV_ENVIRONMENT_TEMPERATURE = gx.GV_ENVIRONMENT_TEMPERATURE   # Current temperature of the environment air in deg. Celsius
GV_SUPPLY_VOLTAGE = gx.GV_SUPPLY_VOLTAGE                     # Current voltage of the camera power supply
GV_POWER_UTILIZATION = gx.GV_POWER_UTILIZATION               # Current utilization of the CCD cooler (rational number from 0.0 to 1.0)
GV_ADC_GAIN = gx.GV_ADC_GAIN                                 # Current gain of A/D converter in electrons/ADU

__id_list=[]

cdef void en_callback(int id):
     __id_list.append(id)

cpdef configure(const char *ini_path):
      '''Sets global path to your configuration .ini file.'''
      gx.gxccd_configure(ini_path)

cpdef configure_eth(const char *ip, gx.uint16_t port):
    '''Configures ip address and/or port of ethernet adapter.
       To configure port only, pass NULL or empty string in "ip".                                 
       To configure ip address only, pass 0 in "port". '''
    gx.gxccd_configure_eth(ip,port)


cpdef enumerate_usb():
      ''' Enumerates all cameras currently connected to your computer (_usb)
          You have to provide callback function, which is called for each connected                  
          camera and the camera identifier (camera ID) is passed as an argument to this              
          callback function.                                                                         
          If your application is designed to work with one camera only or the camera                 
          identifier is known, gxccd_enumerate_*() needs not to be called at all. '''
      global __id_list
      __id_list=[]
      gx.gxccd_enumerate_usb(en_callback)
      return copy.deepcopy(__id_list)

cpdef enumerate_eth():
      ''' Enumerates all cameras currently connected to ethernet adapter (_eth).
          You have to provide callback function, which is called for each connected
          camera and the camera identifier (camera ID) is passed as an argument to this
          callback function.
          If your application is designed to work with one camera only or the camera
          identifier is known, gxccd_enumerate_*() needs not to be called at all. '''
      global __id_list
      __id_list=[]
      gx.gxccd_enumerate_eth(en_callback)
      return copy.deepcopy(__id_list)


cdef char __error_buff[256]

cdef throw_gxError(gx.camera_t *camera):
    gx.gxccd_get_last_error(camera, __error_buff, 256)
    
cdef class GxCCD:
    ''' GxCCD(int id=-1, eth=True)
        "id" is camera indentifier (e.g. obtained by enumerate_*() function).
        If you have one camera connected, you can pass -1 as "id"
        "eth" is a flag to select ethernet or usb connection to the camera'''

    cdef gx.camera_t* _c_camera
    cdef int error

    def __cinit__(self, int id=-1, eth=True):
        if eth:
            self._c_camera = gx.gxccd_initialize_eth(id)
        else:
            self._c_camera = gx.gxccd_initialize_usb(id)
        self.error = 0
        
    def __dealloc__(self):
        if self._c_camera is not NULL:
            gx.gxccd_release(self._c_camera)
            
    def __split_str(self,s):
        return s.split('\x00')[0]

    def is_error(self):
        '''is_error() -> bool
           some methods set an error flag.
           In order to check the status of those methods use is_error()
           if the method returns True, you can use get_last_error() to
           get an error description string'''
        return self.error != 0

    def get_boolean_parameter(self,int index):
        '''get_boolean_parameter(int index) -> bool
           index must be one of GBP_* . If any error occours,
           the error flag is set and can be checked with is_error() method'''
        cdef bint val
        self.error = gx.gxccd_get_boolean_parameter(self._c_camera, index, &val)
        return val
    
    def get_integer_parameter(self,int index):
        '''get_integer_parameter(int index) -> int
           index must be one of GIP_* . If any error occours,
           the error flag is set and can be checked with is_error() method'''
        cdef int val
        self.error = gx.gxccd_get_integer_parameter(self._c_camera, index, &val)
        return val

    def get_string_parameter(self,int index):
        '''get_string_parameter(int index) -> str
           index must be one of GSP_* . If any error occours,
           the error flag is set and can be checked with is_error() method'''
        s = ' '*128
        self.error = gx.gxccd_get_string_parameter(self._c_camera, index, s, len(s))
        return self.__split_str(s)

    def get_value(self,int index):
        '''get_value(int index) -> float
           index must be one of GV_* . If any error occours,
           the error flag is set and can be checked with is_error() method'''
        cdef float val
        self.error = gx.gxccd_get_value(self._c_camera, index, &val)
        return val

    def set_temperature(self,float temp):
        '''set_temperature(float temp) -> 0 | -1
           Sets the required chip temperature.
           If the camera has no cooler, this function has no effect.
           "temp" is expressed in degrees Celsius.
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_set_temperature(self._c_camera, temp)
        return self.error
        
    def set_temperature_ramp(self,float temp_ramp):
        '''set_temperature_ramp(float temp_ramp) -> 0 | -1
           Sets the maximum speed with which the driver changes chip temperature.
           If the camera has no cooler, this function has no effect.
           "temp_ramp" is expressed in degrees Celsius per minute.
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_set_temperature_ramp(self._c_camera,temp_ramp)
        return self.error

    def set_binning(self, int x, int y):
        '''set_binning(self, int x, int y) -> 0 | -1
           Sets the required read binning. 
           If the camera does not support binning, this function has no effect.
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_set_binning(self._c_camera, x, y)
        return self.error

    def set_preflash(self, double preflash_time,int clear_num):
        '''set_preflash(double preflash_time,int clear_num) -> 0 | -1
           If the camera is equipped with preflash electronics, this function sets it.
           "preflash_time" defines time for which the preflash LED inside the camera is
           switched on. "clear_num" defines how many times the CCD has to be cleared
           after the preflash. Actual values of these parameters depends on
           the particular camera model (e.g. number and luminance of the LEDs used etc.).
           Gx series of CCD cameras typically need less than 1 second to completely
           saturate the CCD ("preflash_time"). Number of subsequent clears should be
           at last 2, but more than 4 or 5 clears is not useful, too.
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_set_preflash(self._c_camera, preflash_time, clear_num)
        return self.error

    def start_exposure(self, double exp_time, bint use_shutter, int x, int y, int w, int d):
        '''start_exposure(double exp_time, bool use_shutter, int x, int y, int w, int d) -> 0 | -1
           Starts new exposure.
           "exp_time" is the required exposure time in seconds. "use_shutter" parameter
           tells the driver the dark frame (without light) has to be acquired (false),
           or the shutter has to be opened and closed to acquire normal light image (true).
           Sub-frame coordinates are passed in "x", "y", "w" and "d".
           If the camera does not support sub-frame read, "x" and "y" must be 0 and "w"
           and "d" must be the chip pixel dimensions. 
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_start_exposure(self._c_camera, exp_time, use_shutter, x, y, w, d)
        return self.error

    def abort_exposure(self, bint download):
        '''abort_exposure(self, bool download) -> 0 | -1
           When the exposure already started by gxccd_start_exposure() call has to be
           terminated before the exposure time expires, this function has to be called.
           Parameter "download" indicates whether the image should be digitized, because
           the user will call gxccd_read_image() later or the image should be discarded.
           If any error occours, the error flag is set and -1 is returned'''
        self.error = gx.gxccd_abort_exposure(self._c_camera,download)
        return self.error

    def image_ready(self):
        '''image_ready() -> bool
           When the exposure already started by gxccd_start_exposure() call, parameter
           "ready" is false if the exposure is sill running. When the exposure finishes
           and it is possible to call gxccd_read_image(), parameter "ready" is true.
           It is recommended to count the exposure time in the application despite
           the fact the exact exposure time is determined by the camera/driver and to
           start calling of gxccd_image_ready() only upon the exposure time expiration.
           Starting to call gxccd_image_ready() in the infinite loop immediately after
           gxccd_start_exposure() and call it for the whole exposure time (and thus
           keeping at last one CPU core at 100% utilization) is a bad programming
           manner (politely expressed). If any error occours,
           the error flag is set and can be checked with is_error() method'''
        cdef bint ready
        self.error = gx.gxccd_image_ready(self._c_camera, &ready)
        return ready
    
    def read_image(self, d, w):
        '''read_image(int d, int w) -> nupy.ndarray([w,d],dtype=uint16_t)
           When image_ready() returns "ready" == true, it is possible to call read_image().
           The parameters "w" and "d" must be the same used to method start_exposure.
           If any error occours, the error flag is set and can be checked with is_error() method.'''
#        cdef np.ndarray[np.uint16_t, ndim=2] buff = np.zeros([x,y],dtype=np.NPY_UINT16)
        cdef np.ndarray[np.uint16_t, ndim=2] buff = np.zeros([d,w],dtype='H')
#        buff = np.zeros([w,d],dtype='H')
        cdef void* data = np.PyArray_DATA(buff)
        self.error = gx.gxccd_read_image(self._c_camera, data, w*d*2)
        return buff

    def enumerate_read_modes(self):
        '''enumerate_read_modes() -> [str]
           Enumerates all read modes provided by the camera.
           indices of the elements in the list are used in method set_read_mode'''
        cdef int ret = 0
        cdef int idx = 0
        modes = []
        while ret >= 0:
            s = ' '*128
            ret = gx.gxccd_enumerate_read_modes(self._c_camera, idx, s,len(s))
            if ret >=0:
                modes.append(self.__split_str(s))
            idx +=1
        return modes

    def set_read_mode(self, int mode):
        '''set_read_mode(index) -> 0 | -1
        Sets required read mode.
        "mode" is the "index" used in gxccd_enumerate_read_modes() call.
        If any error occours, the error flag is set and -1 is returned.'''
        self.error = gx.gxccd_set_read_mode(self._c_camera, mode)
        return self.error

    def enumerate_filters(self):
        '''enumerate_filters() -> [(index,filter name,color,offset)]
        Enumerates all read modes provided by the camera.
        first element of the touple are used in method set_filter.
        "color" element hints the RGB color (e.g. cyan color is 0x00ffff), which
        can be used to draw the filter name in the application.
        "offset" indicates the focuser shift when the particular filter is selected.
        Units of the "offset" can be micrometers or arbitrary focuser specific units
        (steps). If the units used are micrometers, driver returns true from
        get_boolean_parameter() with GBP_MICROMETER_FILTER_OFFSETS "index".'''
        cdef int ret = 0
        cdef int idx = 0
        cdef np.uint32_t color = 0
        cdef int offset = 0
        modes = []
        while ret >= 0:
            s = ' '*128
            ret = gx.gxccd_enumerate_filters(self._c_camera,idx,s,len(s),&color,&offset)
            if ret >=0:
                modes.append((idx,self.__split_str(s),color,offset))
            idx+=1
        return modes

    def set_filter(self, int index):
        '''set_filter(index) -> 0 | -1
           Sets the required filter.
           If the camera is not equipped with filter wheel, this function has no effect.
           If any error occours, the error flag is set and -1 is returned.'''
        self.error =  gx.gxccd_set_filter(self._c_camera, index)
        return self.error

    def set_fan(self, np.uint8_t speed):
        '''set_fan(index) -> 0 | -1
           If the camera is equipped with cooling fan and allows its control,
           this function sets the fan rotation speed.
           The maximum value of the "speed" parameter should be determined by
           gxccd_get_integer_parameter() call with GIP_MAX_FAN "index".
           If the particular camera supports only on/off switching, the maximum value
           should be 1 (fan on), while value 0 means fan off. 
           If any error occours, the error flag is set and -1 is returned.'''
        self.error= gx.gxccd_set_fan(self._c_camera, speed)
        return self.error

    def set_window_heating(self, np.uint8_t heating):
        '''set_window_heating(self, uint8_t heating) -> 0 | -1
           If the camera is equipped with CCD cold chamber front window heater
           and allows its control, this function sets heating intensity.
           The maximum value of the "heating" parameter should be determined by
           get_integer_parameter() call with GIP_MAX_WINDOW_HEATING "index".
           If the particular camera supports only on/off switching, the maximum value
           should be 1 (heating on), while value 0 means heating off.
           If any error occours, the error flag is set and -1 is returned.'''
        self.error = gx.gxccd_set_window_heating(self._c_camera, heating)
        return self.error

    def move_telescope(self, np.int16_t ra_duration_ms, np.int16_t dec_duration_ms):
        '''move_telescope(int16_t ra_duration_ms, int16_t dec_duration_ms) -> 0 | -1
           Instructs the camera to initiate telescope movement in the R.A. and/or Dec.
           axis for the defined period of time (in milliseconds).
           The sign of the parameters defines the movement direction in the respective
           coordinate. The maximum length is approx 32.7 seconds.
           If the camera is not equipped with autoguider port, this function has no effect. 
           If any error occours, the error flag is set and -1 is returned.'''
        self.error= gx.gxccd_move_telescope(self._c_camera, ra_duration_ms, dec_duration_ms)
        return self.error

    def move_in_progress(self):
        '''move_in_progress() -> bool
           Sets "moving" to true if the movement started with move_telescope()
           call is still in progress.If any error occours,                                                                                                                                                
           the error flag is set and can be checked with is_error() method'''
        cdef bint moving
        self.error = gx.gxccd_move_in_progress(self._c_camera, &moving)
        return moving

    def get_last_error(self):
        '''get_last_error() -> str
           If any call fails (returns -1), this function returns failure description.'''
        s = ' '*128
        gx.gxccd_get_last_error(self._c_camera, s, len(s))
        return self.__split_str(s)
