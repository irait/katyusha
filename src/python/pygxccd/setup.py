from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

#extensions = [ Extension("gxccd.pyx",libraries = ["-lgxccd","-lusb-1.0"])]

setup(
    ext_modules = cythonize([Extension("pygxccd",["pygxccd.pyx"],libraries = ["gxccd","usb-1.0"])])
)
