#!/usr/bin/python
import numpy as np
import ephem as ep
import datetime as dt

def getAltAzi():
    return (3.61852,115.049)

#print np.interp(jd,HTI.UT_JD,HTI.Astro_RA),
#print np.interp(jd,HTI.UT_JD,HTI.Astro_Dec),
#print np.interp(jd,HTI.UT_JD,HTI.App_Alt),
#print np.interp(jd,HTI.UT_JD,HTI.App_Azi),
#print np.interp(jd,HTI.UT_JD,HTI.SolarElongation)

import time
import rts2.scriptcomm

class Script (rts2.scriptcomm.Rts2Comm):
  """Move the mount randomly."""
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)

  def run(self):
    # self.setValue('ORI','15. -30.','T0')	
    # val1=self.getValue('ORI','T0')	
    # self.log('I','msg: {}'.format(val1))
    #alt_st=random.uniform(30,60)
    #az_st=random.uniform(0,360)
    
    alt_st,az_st=self.getValue('position.AA','T0').split()
    alt_t = float(alt_st)
    az_t= float(az_st)
    if alt_t < 25.:
      alt_t= 25.
    elif alt_t > 50.:
      alt_t = 50.

    self.setValue('exposure',0.0001)

    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 
    while True:
      self.setValue('point','false','T0')
      alt,az = getAltAzi()
      self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
      self.setValue('TEL_','{} {}'.format(alt,az),'T0')	
      time.sleep(5.)
      
      while 'STOPPED' != self.getValue('status','T0'):
          time.sleep(10)
      
      image = self.exposure()
      self.toArchive(image)
      #print image
      self.log('I', 'acquired %s' % image)
      

a = Script()
a.run()

