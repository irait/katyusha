#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import rts2.scriptcomm
import time
import datetime as dt
import calendar
import matplotlib.lines as mlines




def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')




class Script (rts2.scriptcomm.Rts2Comm):
	
	def __init__(self):
	    rts2.scriptcomm.Rts2Comm.__init__(self)
	

	def run(self):
		#self.setValue('point','false','T0')


		self.log('I','start acquire')
		self.setValue('filter', 'B')
		self.setValue('SHUTTER', 'LIGHT')

		self.setValue('exposure', 0.5)
           	image=self.exposure()

		self.log('I','got image '+image)

		renamed=self.rename(image, '/home/irait/images/%N/%c/%f')
		#plt.imshow(image)
		#plt.show()

		

a = Script()
a.run()
