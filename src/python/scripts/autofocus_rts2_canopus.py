#!/usr/bin/python

import string
import time as tm
import os
import math
import scipy
import matplotlib
import pylab
import pyfits

import numpy as np
import matplotlib.pyplot as plt

import rts2.scriptcomm
import datetime as dt




exp = 1.
# exp = 0.003
cam = None
boundRange = 8
learnMoveFactor = 0.2
exp_for_pos = 1
myFilter = 0 # none




class Script (rts2.scriptcomm.Rts2Comm):

    def __init__(self):

	rts2.scriptcomm.Rts2Comm.__init__(self)
	

    def shoot(self, name):
	    global exp

	    tm.sleep(1.)
	    self.log('I', "G4 start exp")

	    self.setValue('SHUTTER', 'LIGHT')
	    self.setValue('exposure', exp)
            image=self.exposure()

	    self.log('I','got image '+image)

	    renamed=self.rename(image, name+'.fits')



  
    def focus_fit(self, folder):

	filelist = os.listdir(folder)
	filelist.sort()

	FocPos=[]
	fwhm = []
	flux = []

	for f in filelist:
		if f.endswith('.fits'):

			data = f.split('_')
			pointLinear = data[1]
			trial = data[3]			
			pointShoot = data[4]
			temp = data[5].split('.f')[0]

			

                        # Call source extractor giving it the confiuration file
			exitos=os.system('sextractor -c /home/irait/sex/sex.cfg '+folder+f+' >/dev/null 2>&1')
			if exitos == 0:
				self.log('I', 'sextractor succesful')
			if exitos != 0:
				self.log('I', 'sextractor unsuccesful')    
		    
		   	file='/home/irait/sex/sex.cat'
		   	input = open(file,'r')
		    	star_infos = input.readlines()
			numobj = len(star_infos)

			self.log('I', 'found %i objects' % numobj)

			if numobj > 0:
				FocPos.append(float(pointShoot))
	
			    	tot_fwhm =0.0
				tot_flux =0.0

				for j in range(0, numobj):
					tot_fwhm += float(star_infos[j].split()[1])
			    		tot_flux += float(star_infos[j].split()[0])
				
			    	fwhm.append(tot_fwhm)
		
				flux.append((tot_flux/numobj)/1000)


			
				
			

	fway = scipy.array(fwhm)
	fluxay = scipy.array(flux)
	fpax = scipy.array(FocPos)

	fluxmaxPos = fluxay.argmax()
	fluxmax =fluxay.max()

	a = []
	for i in range(0,len(fway)):
	    a.append((FocPos[i],fway[i]))
	a.sort()
	lastX = a[0][0]
	b = []
	x= []

	xpos = []
	for i in range(0, len(a)):

		if a[i][0] == lastX:
			b.append(a[i][1])
		else:
			x.append(b)
			xpos.append(lastX)
			lastX = a[i][0]
			b = None
			b = []
			b.append(a[i][1])

	x.append(b)
	xpos.append(lastX)
	xx= np.array(x)

	mins = xx.min(1)
	maxs = xx.max(1)
	means= xx.mean(1)

	polycoeffs = scipy.polyfit(xpos, means, 2)
	yfit=scipy.polyval(polycoeffs, xpos)
    
	polycoeffsF = scipy.polyfit(fpax, fluxay, 2)
	yfitF=scipy.polyval(polycoeffsF, fpax)

	#polycoeffs3 = scipy.polyfit(xpos, means, 3)
	#yfit3=scipy.polyval(polycoeffs3, xpos)
	

	plt.plot(xpos, yfit, 'r')
	plt.plot(fpax, yfitF, 'b')
	plt.plot(fpax, fluxay, 'b.')
	#plt.plot(xpos, yfitFlux, 'g')
	
	#plt.plot(xpos, yfit3, 'g')

	plt.errorbar(xpos, means, [means-mins, maxs-means], fmt=".k", ecolor='gray', lw=1)

	if polycoeffs[0] > 0: 
		optfoc=float((-polycoeffs[1])/(2*polycoeffs[0]))
		minfway=polycoeffs[0]*optfoc*optfoc+polycoeffs[1]*optfoc+polycoeffs[2]
		
		if polycoeffsF[0] < 0:
			optfocF=float((-polycoeffsF[1])/(2*polycoeffsF[0]))

                        optfocFinal = (optfoc + optfocF) / 2

                else:
                    optfocFinal = optfoc

	else:
		lincoeff = scipy.polyfit(fpax, fway, 1)
		ylinfit = scipy.polyval(lincoeff, fpax)
		for i in range(0, len(ylinfit)):
     			if ylinfit[i] == min(ylinfit):
	     			optfoc=fpax[i]
				minfway=fway[i]
				
		optfocFinal = optfoc
		self.log("I",  "maybe is not the best focus, RUN AGAIN")


	strTitle= "T_%s, optFoc: %.4f, minfway: %.2f" % (temp,optfocFinal,minfway)
	plt.title(strTitle)     
	#os.system('mkdir /etc/rts2/bin/af/')   
	plt.savefig(folder+'all.png')
	plt.cla()

	self.log('I', " temperature: %s, optimal focus: %.6f, minfway: %.4f" % ( temp,optfocFinal,minfway))
	
	return optfocFinal

    def focsleep(self): 
        self.log('I','sleep while the focuser move')
        running = True
        i=0
        while running:
            curFocPos      =self.getValue('FOC_POS', 'F0')#Pos fuoco
            tm.sleep(1)
            curFocTar      =self.getValue('FOC_TAR', 'F0')#Pos
            curFocTar      =string.split(curFocTar)
            curFocTar      =curFocTar[0]

	    currTarFloat = round(float(curFocTar),3)
            
            if round(float(curFocPos),3) == currTarFloat:
                running = False # questo determina l'uscita dal loop

	    else:
		focState = self.getValue('foc_status', 'F0')
		if focState == "STOP":
			self.setValue('FOC_TAR', str(currTarFloat+0.01), 'F0')
     
            tm.sleep(2)
            i += 1
            if i > 15:
		self.log('W','Exit for time elapsed')
		running = False
                i=0

    def getFilterEnum(self, myFilter):
	if myFilter == 6:
		return 'C'
	elif myFilter == 5:
		return 'I'
	elif myFilter == 4:
		return 'R'
	elif myFilter == 3:
		return 'V'
	elif myFilter == 2:
		return 'B'
	elif myFilter == 1:
		return 'U'
	elif myFilter == 0:
		return 'H'

    def focusing(self, workingFolder):

	global exp_for_pos
	global learnMoveFactor
	global boundRange
	global myFilter        

        tm.sleep(2)
       
        
        self.log('I','start the focusing procedure')
        
        pos,min_off,max_off= self.getValue('FOC_DEF', 'F0').split()
	pos = float(pos)
        temperature = float(self.getValue('FOC_TEMP', 'F0')) 

	for y in range(-boundRange, boundRange+1):
		if (y > 0.):
			newPos = pos + (learnMoveFactor*(abs(y)**1)) #**1.5
                elif (y < 0.):
                        newPos = pos - (learnMoveFactor*(abs(y)**1)) #**1.5
		else:
			newPos = pos

            
		self.log('I','start to move the focuser. new pos %s' % str(newPos))

		self.setValue('FOC_TAR', str(newPos), 'F0')
		self.focsleep()
            
		## take image 
		i =0
		while i < exp_for_pos:
	      		i += 1
	      		self.log('I','got image n. %i for pos %s' % (i, str(newPos)))
			self.shoot('%slearn_%.4f_try_%d_%.4f_%.1f'%(workingFolder,pos,i, newPos, temperature))
            	

        
	#findBestFocus
	optfoc=self.focus_fit(workingFolder)

	if optfoc <= 0:
		optfoc = 111

	if (myFilter == 6):
             
		self.log('I','set new foc position '+str(optfoc))
		
		self.setValue('FOC_TOFF', str(0), 'F0') #Azzeramento degli offset
		self.focsleep()
		self.setValue('FOC_DEF', str(optfoc), 'F0')#Pos di default
		self.focsleep()
		#learnMoveFactor = 0.1
	
	self.log('I','End procedure')

    def run(self):

	global myFilter
	global exp

	self.log("I", "start filter %i" % myFilter)


	setFilt = self.getFilterEnum(myFilter)

	self.setValue('filter', setFilt)

	myFolder = '/home/irait/auto_focus/samples_filter_%i_%s' % (myFilter, str(dt.datetime.now()).replace(' ','_'))

	try:
		os.mkdir(myFolder)
		workingFolder = myFolder+"/"
	except:
		self.log("W", "cant' create folder " % (myFolder))


	self.focusing(workingFolder)
	tm.sleep(2)
 	



a = Script()
a.run()
   
