#!/usr/bin/python

import string
import time as tm
import os
import math
import scipy
import matplotlib
import pylab
import pyfits

import numpy as np
import matplotlib.pyplot as plt

import rts2.scriptcomm
import datetime as dt




exp = 2.8
cam = None
boundRange = 4
learnMoveFactor = 0.2
exp_for_pos = 3
myFilter = 6 # none




class Script (rts2.scriptcomm.Rts2Comm):

    def __init__(self):

	rts2.scriptcomm.Rts2Comm.__init__(self)
	

    def shoot(self, name):
	    global exp

	    tm.sleep(1.)
	    self.log('I', "G4 start exp")

	    self.setValue('SHUTTER', 'LIGHT')
	    self.setValue('exposure', exp)
            image=self.exposure()

	    self.log('I','got image '+image)

	    renamed=self.rename(image, name+'.fits')


    def focsleep(self): 
        self.log('I','sleep while the focuser move')
        running = True
        i=0
        while running:
            curFocPos      =self.getValue('FOC_POS', 'F0')#Pos fuoco
            tm.sleep(1)
            curFocTar      =self.getValue('FOC_TAR', 'F0')#Pos
            curFocTar      =string.split(curFocTar)
            curFocTar      =curFocTar[0]

	    currTarFloat = round(float(curFocTar),3)
            
            if round(float(curFocPos),3) == currTarFloat:
                running = False # questo determina l'uscita dal loop

	    else:
		focState = self.getValue('foc_status', 'F0')
		if focState == "STOP":
			self.setValue('FOC_TAR', str(currTarFloat+0.01), 'F0')
     
            tm.sleep(2)
            i += 1
            if i > 15:
		self.log('W','Exit for time elapsed')
		running = False
                i=0

    def getFilterEnum(self, myFilter):
	if myFilter == 6:
		return 'C', 0.0
	elif myFilter == 5:
		return 'I', 0.01
	elif myFilter == 4:
		return 'R', 0.3
	elif myFilter == 3:
		return 'V', 0.35
	elif myFilter == 2:
		return 'B', 0.3
	elif myFilter == 1:
		return 'U', 0.2 
	elif myFilter == 0:
		return 'H', 0.0


   

    def run(self):

	global myFilter
	global exp	

	myFolder = '/home/irait/fits/beta_pic/samples_%s' % (str(dt.datetime.now()).replace(' ','_'))

	try:
		os.mkdir(myFolder)
		workingFolder = myFolder+"/"
	except:
		self.log("W", "cant' create folder " % (myFolder))

	def_foc = 111.3
	i = 0
	

	while True:

		###  shoot I image
		myFilter = 5
		exp = 20.		
	
		self.log("I", "start filter %i" % myFilter)
		setFilt, focus_p = self.getFilterEnum(myFilter)
		self.setValue('filter', setFilt)

		tm.sleep(5)

		self.setValue('FOC_TAR', str(def_foc+focus_p), 'F0')
		self.focsleep()

		self.shoot('%sG4_%.2f_%s_%i'%(workingFolder,def_foc,setFilt,i))

		###  shoot B image
		myFilter = 2
		exp = 60.

		self.log("I", "start filter %i" % myFilter)
		setFilt, focus_p = self.getFilterEnum(myFilter)
		self.setValue('filter', setFilt)

		tm.sleep(5)

		self.setValue('FOC_TAR', str(def_foc+focus_p), 'F0')
		self.focsleep()

		self.shoot('%sG4_%.2f_%s_%i'%(workingFolder,def_foc,setFilt,i))

		 	
		i += 1


a = Script()
a.run()
   
