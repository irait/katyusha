#!/usr/bin/python

import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo
import cv2
import pylab as plt
import time
import rts2.scriptcomm
import sys

from datetime import datetime as dtd


from threading import Thread,Lock
import time as tm


def getAltAzi(fixedStar):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)+180.0)%360.)


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    
  
  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def getPMCorr(self):
    corr_alt_st, corr_az_st = self.getValue('pm_delta', 'T0').split()
    corr_alt = float(corr_alt_st)*3600.
    corr_az = float(corr_az_st)*3600.	
    return corr_alt, corr_az 	 

  def run(self):

    myMinute = dtd.now().minute 
    global exp
    global exp_g4
    alt_d = -0.
    azi_d = -0.
    

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    starList = []

    starList.append(['Pic Beta', 	'05:47:17.10', '-51:03:58.0'])
   
    G4pixel2deg = 0.15/ 3600.0
    G4pixel2asec = 0.15  # arcseconds per pixel
    surf = cv2.SURF(20)

    for i in range(0, len(starList)):

	starName = starList[i][0]
	starRA   = starList[i][1]
	starDEC  = starList[i][2]
	#startRAdeg =  starList[i][3]
	#startDECdeg = starList[i][4]

	self.log('I', 'Set position for %s' % starName)
	self.setValue('point','false','T0')

	mystar = None
	mystar = ep.FixedBody()
	mystar._ra = starRA
	mystar._dec = starDEC 

	alt, az = getAltAzi(mystar)

	if (alt < 4.5) or (alt > 76):
		self.log('I', 'Target out of range... alt: %4.3f' % alt)
	else:
		self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
	   	self.setValue('TEL_','{} {}'.format(alt,az),'T0')	


		time.sleep(5.)

	    	while 'STOPPED' != self.getValue('status','T0'):
			time.sleep(0.1)

		self.setValue('point','true','T0')


a = Script()
a.run()

