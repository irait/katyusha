#!/usr/bin/python
import os
import time
import datetime as dt

import numpy as np
import pygxccd as gx
import ephem
import rts2.scriptcomm
from astropy import units as u
from astropy.coordinates import SkyCoord

from TelescopeIRAIT import EPHEM as IRAIT

# TODO: Informazioni da mettere sul fits: posizione, ecc.

# Make a source catalog in a separate plain text file, and when you read it
# parse the ra-dec coordinates with `astropy.coordinates`.
# Because the catalog is not ready, use the following dictionary
SOURCES = {
    'Canopus': ('6:23:57.2', '-52:41:43.9'),  # (ra, dec)
}


class Source(object):

    def __init__(self, name):
        ra, dec = SOURCES[name]
        self.name = name
        self.coordinates = SkyCoord(ra=ra, dec=dec, unit=(u.hourangle, u.deg))

    def alt_az(self):
        body = ephem.FixedBody()
        body._ra = self.coordinates.ra.to_string(unit=u.hourangle, sep=':')
        body._dec = self.coordinates.dec.to_string(unit=u.deg, sep=':')
        IRAIT.date = ephem.date(dt.datetime.utcnow())
        body.compute(IRAIT)
        return (np.rad2deg(body.alt), (np.rad2deg(body.az)+180.0) % 360)


class Camera(object):

    cameras = {'G1': 3247, 'G4': 6103}

    def __init__(self, name='G4', IP='10.10.18.21'):
        if name not in Camera.cameras:
            raise ValueError('Camera %s not available' % name)
        self.name = gx.configure_eth(IP, gx.GXETH_DEFAULT_PORT)
        self.camera = gx.GxCCD(Camera.cameras[name])
        if not self.camera.get_boolean_parameter(gx.GBP_CONNECTED):
            raise RuntimeError('Cannot connect to the %s camera' % self.name)

        self.height = self.camera.get_integer_parameter(gx.GIP_CHIP_D)
        self.width = self.camera.get_integer_parameter(gx.GIP_CHIP_W)
        if self.height == -1 or self.width == -1:
            raise RuntimeError('Cannot get the CCD height and/or width')

    def shoot(self, rts2cs, exposition, file_name):
        time.sleep(1)
        rts2cs.setValue('SHUTTER', 'LIGHT')
        rts2cs.log('I', 'Setting the %s exposition to %s' % (self.name, exposition))
        rts2cs.setValue('exposure', exposition)
        rts2cs.log('I', 'Shooting image with the %s camera' % self.name)
        if not os.path.exists(rts2cs.DESTINATION):
            os.mkdir(rts2cs.DESTINATION)
        path = os.path.join(rts2cs.DESTINATION, file_name + '.fits')
        image = rts2cs.exposure()
        rts2cs.rename(image, path)
        rts2cs.log('I', 'Got image %s' % path)


class Map(rts2.scriptcomm.Rts2Comm):

    def __init__(self, source, dimension, distance, camera, exposition):
        """
        Arguments:

        * source: a Source object
        * dimension: for instance, dimension=3 means that the map will be
          a matrix of 3x3 pictures, with the source position in the center.
          dimension has to be odd.  If you give an even number, it will be
          incremented by one
        * distance: the angular distance (in degrees) between pictures.
          For instance, distance=7 means that each position in the map
          has a distance of 7 degrees from other positions
        * camera: a Camera object
        * lines: number of lines of the map
        """
        rts2.scriptcomm.Rts2Comm.__init__(self)
        self.log('I', 'OK 00')
        self.source = source
        self.log('I', 'OK source')
        self.dimension = dimension if dimension % 2 != 0 else dimension + 1
        self.distance = distance
        # self.camera = camera
        self.log('I', 'OK camera')
        self.exposition = exposition
        # Create the directory where to save the map
        MAPS_DIR = os.path.join(os.getenv('HOME'), 'maps')
        if not os.path.exists(MAPS_DIR):
            os.mkdir(MAPS_DIR)
        DIR_ID = "{0:}_{1:%Y_%m_%d_%H:%M}".format(
            self.source.name, dt.datetime.now())
        self.DESTINATION = os.path.join(MAPS_DIR, DIR_ID)
        self.log('I', 'Map created. Destination: %s' % self.DESTINATION)

    def telescope_position(self):
        alt_str, az_str = self.getValue('position.AA', 'T0').split()
        alt = float(alt_str)
        az = (float(az_str) + 180) % 360
        return alt, az

    def set_focus(self, position=1.8, timeout=30):
        self.log('I', 'Setting the M2 position to %s' % position)
        self.setValue('FOC_TAR', str(position), 'F0')
        reached = False
        starting_time = dt.datetime.now()
        while not reached:
            if (dt.datetime.now() - starting_time).seconds > timeout:
                raise RuntimeError('Cannot reach M2 position: timeout reached')
            current_pos = float(self.getValue('FOC_POS', 'F0'))
            if abs(current_pos - position) < 0.1:
                reached = True
            time.sleep(1)
        self.log('I', 'Current M2 position: %s' % current_pos)

    def goto_source(self, alt_offset=0, az_offset=0, wait=True):
        """alt_offset and az_offset are in arcmin."""
        alt, az = self.source.alt_az()
        target_alt = alt + alt_offset / 60.0  # Arcmin
        target_az = az + az_offset / 60.0  # Arcmin

        if target_alt < 4.5 or target_alt > 75:
            raise ValueError('Altitude out of range: %.2f' % target_alt)
        else:
            self.setValue('point', 'false', 'T0')
            self.setValue('TEL_', '{} {}'.format(target_alt, target_az), 'T0')
        time.sleep(3)
        while wait and 'STOPPED' != self.getValue('status', 'T0'):
            time.sleep(1)
        self.setValue('point', 'true', 'T0')
        if wait:
            time.sleep(10)

    def run(self):
        pass



Camera('G4')

# source_map = Map(
#     source=Source('Canopus'),
#     dimension=1,  # Matrix 3x3
#     distance=7,  # Arcmin
#     camera=Camera('G1'),
#     exposition=0.8  # Seconds
# )
# source_map.run()
