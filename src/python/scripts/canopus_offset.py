#!/usr/bin/python

import time
import datetime
import ephem
import numpy as np
import rts2.scriptcomm
from TelescopeIRAIT import EPHEM as IRAIT


class OffsetScript(rts2.scriptcomm.Rts2Comm):

    def __init__(self):
        rts2.scriptcomm.Rts2Comm.__init__(self)
    
    def run(self):
        canopus = ephem.FixedBody()
        canopus._ra, canopus._dec = ('06:23:57.17', '-52:41:43.9')
        IRAIT.date = ephem.date(datetime.datetime.utcnow())
        canopus.compute(IRAIT)
        alt = np.rad2deg(canopus.alt)
        az = (np.rad2deg(canopus.az) + 180.0) % 360
        alt += 0.05
        self.setValue('TEL_', '{} {}'.format(alt, az), 'T0')	


if __name__ == '__main__':
    script = OffsetScript()
    script.run()
