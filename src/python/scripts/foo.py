#!/usr/bin/python


import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo
import cv2
import pylab as plt
import string
import os

import random
import time
import rts2.scriptcomm
import sys
import argparse

# parser = argparse.ArgumentParser()
# parser.add_argument(
#     '-f', '--foo',
#     type=int,
#     required=True,
#     help='File da processare')
# args = parser.parse_args()

from datetime import datetime as dtd


from threading import Thread,Lock
import time as tm


__BASE_PATH__ = "/home/irait/fits/agb/"





exp = .01
exp_g4 = .02


def getAltAzi(fixedStar):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)+180.0)%360.)

Info={}
Info['Valid']='2018jan15'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Guide']='G1-2000 Moravian'
Info['Instrument']='G4-9000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='None'
Info['Version']='1.2'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    self.log('E', 'sys.argv: %s' %sys.argv)
    # self.log('E', 'args.foo: %s' %args.foo)
    exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
    if(self._chip_d == -1 or self._chip_w == -1):
        # TODO: print a log before exiting: self.log('E', 'Fooo')
        exit(1)


  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp

  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def getPMCorr(self):
    corr_alt_st, corr_az_st = self.getValue('pm_delta', 'T0').split()
    corr_alt = float(corr_alt_st)*3600.
    corr_az = float(corr_az_st)*3600.	
    return corr_alt, corr_az 	 


  def shoot(self, name):
	    global exp_g4

	    tm.sleep(1.)
	    self.log('I', "G4 start exp")

	    self.setValue('SHUTTER', 'LIGHT')
	    self.setValue('exposure', exp_g4)
            image=self.exposure()

	    self.log('I','got image '+image)

	    renamed=self.rename(image, name+'.fits')


  def focsleep(self): 
        self.log('I','sleep while the focuser move')
        running = True
        i=0
        while running:
            curFocPos      =self.getValue('FOC_POS', 'F0')#Pos fuoco
            tm.sleep(1)
            curFocTar      =self.getValue('FOC_TAR', 'F0')#Pos
            curFocTar      =string.split(curFocTar)
            curFocTar      =curFocTar[0]

	    currTarFloat = round(float(curFocTar),3)
            
            if round(float(curFocPos),3) == currTarFloat:
                running = False # questo determina l'uscita dal loop

	    else:
		focState = self.getValue('foc_status', 'F0')
		if focState == "STOP":
			self.setValue('FOC_TAR', str(currTarFloat+0.01), 'F0')
     
            tm.sleep(2)
            i += 1
            if i > 15:
		self.log('W','Exit for time elapsed')
		running = False
                i=0

  def getFilterEnum(self, myFilter):

	curFocPos   =float(self.getValue('FOC_POS', 'F0'))

	if myFilter == 6:
                # Add the computed offset to the position of this filter
                # Pay attention because in case of offset == 0 you need to add
                # a value, just because otherwise it does not advance with the expositions.
                # That is why there is a 0.01
		return 'C', curFocPos+0.01
	elif myFilter == 5:
		return 'I', curFocPos+0.3
	elif myFilter == 4:
		return 'R', curFocPos+0.01
	elif myFilter == 3:
		return 'V', curFocPos-0.01
	elif myFilter == 2:
		return 'B', curFocPos+0.01
	elif myFilter == 1:
		return 'U', curFocPos-0.1
	elif myFilter == 0:
		return 'H', curFocPos-0.2


   


  def run(self):

    myMinute = dtd.now().minute 
    global exp
    global exp_g4
 

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    starList = []
 
    #STAR FOR FOCUSING PROCEDURE
    # starList.append(['Kaus Australis',	'18:24:10.25', '-34:23:06.8'])
    # starList.append(['Canopus',	'6:23:57.2', '-52:41:43.9'])  # J2000
    starList.append(['Canopus',	'6:23:50.2', '-52:53:23.9'])  # basso a destra

    #TARGET LIST
    #starList.append(['V401', 	'18:28:53.9', '-14:29:18'])
    #starList.append(['V412', 	'18:29:58.5', '-14:10:13'])
    #starList.append(['V418', 	'18:30:28.7', '-14:21:35'])

    #starList.append(['GP', 	'18:09:09.7', '-15:51:20'])
    #starList.append(['GR', 	'18:09:24.0', '-15:19:31'])
    #starList.append(['GQ', 	'18:09:18.2', '-14:38:08'])
    #starList.append(['FY', 	'18:07:54.0', '-14:31:26'])

    #starList.append(['V405', 	'18:29:22.7', '-15:07:58'])
    #starList.append(['V421', 	'18:30:51.3', '-15:37:28'])
    #starList.append(['V424', 	'18:31:36.1', '-15:26:54'])
    # starList.append(['V422', 	'18:31:25.0', '-15:23:19'])
    # starList.append(['V425', 	'18:31:41.7', '-15:12:12'])



    G4pixel2deg = 0.15/ 3600.0
    G4pixel2asec = 0.15
    surf = cv2.SURF(20)

    for i in range(0, len(starList)):

	starName = starList[i][0]
	starRA   = starList[i][1]
	starDEC  = starList[i][2]

	self.log('I', 'Set position for %s' % starName)
	self.setValue('point','false','T0')

	mystar = None
	mystar = ep.FixedBody()
	mystar._ra = starRA
	mystar._dec = starDEC 

	alt, az = getAltAzi(mystar)

	if (alt < 4.5) or (alt > 76):
		self.log('I', 'Target out of range... alt: %4.3f' % alt)
	else:
		self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
	   	self.setValue('TEL_','{} {}'.format(alt,az),'T0')	

		time.sleep(5.)

	    	while 'STOPPED' != self.getValue('status','T0'):
			time.sleep(0.1)

		self.setValue('point','true','T0')
		time.sleep(30.)

		#image, e_time, chip_temp = self.gx_exposure(exp,5)
		#filename = "%sG1_%s_%s_%.3f.png"%(__BASE_PATH__,now_str(e_time),starName,exp)

		#alt_t, az_t = self.getPosTelAdj()
		#corr_alt, corr_az = self.getPMCorr() 

		#self.log('I', 'acquired %s' % filename)
		#plt.imshow(image)
		#plt.show()

                sys.exit(0)

		
		myFolder = '%s%s_samples_%s' % (__BASE_PATH__ ,starName,str(dt.datetime.now()).replace(' ','_'))

		try:
			os.mkdir(myFolder)
			workingFolder = myFolder+"/"
		except:
			self.log("W", "cant' create folder " % (myFolder))

	
		myFilter = 6
		while myFilter >= 2:

			self.log("I", "start filter %i" % myFilter)
		

			setFilt, focus_p = self.getFilterEnum(myFilter)
			self.setValue('filter', setFilt)

			tm.sleep(5)
		
		
			if myFilter == 1:
				exp_g4 = .1
				n_exp = 1
			
			elif myFilter == 2:
		                exp_g4 = .12
				n_exp = 1	
		
			elif myFilter == 6:
				exp_g4 = .06
				n_exp = 1
		        else:
				exp_g4 = .06
				n_exp = 1
			

			self.setValue('FOC_TAR', str(focus_p), 'F0')
			self.focsleep()

			i = n_exp
			while i > 0:
				self.shoot('%sG4_%s_%i'%(workingFolder,setFilt,i))
				i-=1

			tm.sleep(2)
	

			myFilter -= 1

   

a = Script()
a.run()

