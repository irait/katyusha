#!/usr/bin/python
import os
import time
import datetime as dt

import numpy as np
import ephem
import rts2.scriptcomm
from astropy import units as u
from astropy.coordinates import SkyCoord

from TelescopeIRAIT import EPHEM as IRAIT

# Make a source catalog in a separate plain text file, and when you read it
# parse the ra-dec coordinates with `astropy.coordinates`.
# Because the catalog is not ready, use the following dictionary
SOURCES = {
    # 'Canopus': ('6:23:57.2', '-52:52:43.9'),  # modified
    'Canopus': ('6:23:57.2', '-52:41:43.9'),  # (ra, dec)
}


class Source(object):

    def __init__(self, name):
        ra, dec = SOURCES[name]
        self.name = name
        self.coordinates = SkyCoord(ra=ra, dec=dec, unit=(u.hourangle, u.deg))

    def alt_az(self):
        body = ephem.FixedBody()
        body._ra = self.coordinates.ra.to_string(unit=u.hourangle, sep=':')
        body._dec = self.coordinates.dec.to_string(unit=u.deg, sep=':')
        IRAIT.date = ephem.date(dt.datetime.utcnow())
        body.compute(IRAIT)
        return (np.rad2deg(body.alt), (np.rad2deg(body.az)+180.0) % 360)


class GoTo(rts2.scriptcomm.Rts2Comm):

    def __init__(self, source):
        rts2.scriptcomm.Rts2Comm.__init__(self)
        self.source = source

    def goto_source(self, alt_offset=0, az_offset=0, wait=True):
        """alt_offset and az_offset are in arcmin."""
        alt, az = self.source.alt_az()
        target_alt = alt + alt_offset / 60.0  # Arcmin
        target_az = az + az_offset / 60.0  # Arcmin
        distance = self.getValue('distance', 'T0')
        self.log('I', 'distance: %s, type: %s' % (distance, type(distance)))
        import sys
        sys.exit(0)

        if target_alt < 4.5 or target_alt > 75:
            raise ValueError('Altitude out of range: %.2f' % target_alt)
        else:
            self.setValue('point', 'false', 'T0')
            self.setValue('TEL_', '{} {}'.format(target_alt, target_az), 'T0')
            self.log('I', 'Going to %s (%s, %s)' % (
                self.source.name, target_alt, target_az))
        time.sleep(3)
        while wait and self.getValue('status', 'T0') != 'STOPPED':
            time.sleep(1)
        if wait:
            self.setValue('point', 'true', 'T0')
            time.sleep(10)

    def run(self):
        self.goto_source()


script = GoTo(source=Source('Canopus'))
script.run()
