#!/usr/bin/python


import socket
import json
import numpy as np
import ephem as ep
import datetime as dt
import pygxccd as gx
import pyfits
import ephem
from TelescopeIRAIT import EPHEM as IRAIT
from HorizonsTables import HorizonsTableObserver,CmdLineBase,ephemUTepoch2JD                                           
from HorizonsTables.HorizonsTablesDb import HorizonsObserverTablesDb
import meteo_dome_c as Meteo
import cv2
import pylab as plt
import string
import os

__BASE_PATH__ = "/home/irait/fits/landolt/"





exp = 15.
exp_g4 = 20.


def getAltAzi(fixedStar):
    IRAIT.date=ephem.date(dt.datetime.utcnow())
    fixedStar.compute(IRAIT)
    return (np.rad2deg(fixedStar.alt),(np.rad2deg(fixedStar.az)+180.0)%360.)



class FrameToFits:
	def __init__(self,image, filename, alt, azi, exp_time_loc, chip_temp, exp):
		self._image = image
        	self._filename =  filename
		self._alt = alt
		self._azi = azi
		self._altCorr = None
		self._aziCorr = None
		self._tiltX = None
		self._tiltY = None
		self._fitsRef = None
		self._exp_time_loc = exp_time_loc
		self._chip_temp = chip_temp
		self._exp = exp 
		self._teoricAlt = None
		self._teoricAz = None

	def setCorrection(self, altCorr, aziCorr):
		self._altCorr = altCorr
		self._aziCorr = aziCorr

	def setTilt(self, tiltX, tiltY):
		self._tiltX = tiltX
		self._tiltY = tiltY

	def setReference(self, filename):
		self._fitsRef = filename
	
	def setTeoricCoord(self,altTeoric, azTeoric):
		self._teoricAlt = altTeoric
		self._teoricAz = azTeoric

class Tilt:
    BUFFER_SIZE = 1024

    def __init__(self,hostname,port):
        self.__s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.connect((hostname,port))

    def __del__(self):
        self.__s.close()

    def __get_tilt(self,i):
        if i > 32767:
            return -(65536 - i)
        else:
            return i

    def getTilt(self):
        MESSAGE = '{"function":"R_REGISTERS","address":"0x0008","count":2}'
        self.__s.send(MESSAGE)
        data = self.__s.recv(Tilt.BUFFER_SIZE)

        j = json.JSONDecoder()
        try:
            jobj = j.decode(data[0:-1])
        except:
            return None
        if jobj.has_key('error'):
            return None

        return(self.__get_tilt(jobj['data'][0]), self.__get_tilt(jobj['data'][1]))

import random
import time
import rts2.scriptcomm
import sys

from datetime import datetime as dtd


from threading import Thread,Lock
import time as tm


Info={}
Info['Valid']='2016dec10'
Info['Expire']='undef'
Info['Creator']=sys.argv[0]
Info['Telescope']='IRAIT/Guide Telescope'
Info['Guide']='G1-2000 Moravian'
Info['Instrument']='G4-9000 Moravian'
Info['Tiltmeter']='APPLIED GEOMECHANICS 711-2B'
Info['Filter']='None'
Info['Version']='1.2'


def now_str(d=dt.datetime.now()):
    return str(d).replace(' ','T')

class Script (rts2.scriptcomm.Rts2Comm):
	
  def __init__(self):
    rts2.scriptcomm.Rts2Comm.__init__(self)
    gx.configure_eth("10.10.18.21",gx.GXETH_DEFAULT_PORT)
    self._cam = gx.GxCCD(3247)
    if(not self._cam.get_boolean_parameter(gx.GBP_CONNECTED)):
        exit(1)

    self._chip_d = self._cam.get_integer_parameter(gx.GIP_CHIP_D)
    self._chip_w = self._cam.get_integer_parameter(gx.GIP_CHIP_W)
    if(self._chip_d == -1 or self._chip_w == -1):
        exit(1)

        
 

  def gx_exposure(self,exp_time,tries):
      filename = ""
      image = None
      e_time = None
      chip_temp = None
      for i in range(tries):
          e_time = dt.datetime.now()
          if 0 != self._cam.start_exposure(exp_time,False,0,0,self._chip_w,self._chip_d):
              self.log('W', 'bad start exposure')
              continue

          time.sleep(1+exp)
          while not self._cam.image_ready():
              time.sleep(.1)
              
          image = self._cam.read_image(self._chip_d,self._chip_w)
	  chip_temp = self._cam.get_value(gx.GV_CHIP_TEMPERATURE)
          if self._cam.is_error():
              self.log('W', 'ccd read')
              continue
          else:
              break
          self.log('E','cannot complete the exposure, exiting')
          exit(1)
      return image,e_time,chip_temp

  def getPosTelAdj(self): 
     alt_st,az_st=self.getValue('position.AA','T0').split()
     alt_t = float(alt_st)
     az_t= float(az_st)
     az_t += 180.
     az_t %= 360.
     return alt_t, az_t

  def getPMCorr(self):
    corr_alt_st, corr_az_st = self.getValue('pm_delta', 'T0').split()
    corr_alt = float(corr_alt_st)*3600.
    corr_az = float(corr_az_st)*3600.	
    return corr_alt, corr_az 	 


  def shoot(self, name):
	    global exp_g4

	    tm.sleep(1.)
	    self.log('I', "G4 start exp")

	    self.setValue('SHUTTER', 'LIGHT')
	    self.setValue('exposure', exp_g4)
            image=self.exposure()

	    self.log('I','got image '+image)

	    renamed=self.rename(image, name+'.fits')


  def focsleep(self): 
        self.log('I','sleep while the focuser move')
        running = True
        i=0
        while running:
            curFocPos      =self.getValue('FOC_POS', 'F0')#Pos fuoco
            tm.sleep(1)
            curFocTar      =self.getValue('FOC_TAR', 'F0')#Pos
            curFocTar      =string.split(curFocTar)
            curFocTar      =curFocTar[0]

	    currTarFloat = round(float(curFocTar),3)
            
            if round(float(curFocPos),3) == currTarFloat:
                running = False # questo determina l'uscita dal loop

	    else:
		focState = self.getValue('foc_status', 'F0')
		if focState == "STOP":
			self.setValue('FOC_TAR', str(currTarFloat+0.01), 'F0')
     
            tm.sleep(2)
            i += 1
            if i > 15:
		self.log('W','Exit for time elapsed')
		running = False
                i=0

  def getFilterEnum(self, myFilter):

	curFocPos   =float(self.getValue('FOC_POS', 'F0'))

	if myFilter == 6:
		return 'C', curFocPos+0.01
	elif myFilter == 5:
		return 'I', curFocPos+0.01
	elif myFilter == 4:
		return 'R', curFocPos+0.3
	elif myFilter == 3:
		return 'V', curFocPos-0.01
	elif myFilter == 2:
		return 'B', curFocPos+1.7
	elif myFilter == 1:
		return 'U', curFocPos-0.1
	elif myFilter == 0:
		return 'H', curFocPos-0.2


   


  def run(self):

    myMinute = dtd.now().minute 
    global exp
    global exp_g4
 

    #get current position
    alt_t, az_t = self.getPosTelAdj()
    az_t -= 180.
    self.log('I', 'position telescope: {0:5.1f},{1:5.1f}'.format(alt_t,az_t))
    az_t += 180. # libnova 

    starList = []
 
    #starList.append(['Ankaa',   	'00:26:17.54', '-42:18:27.7'])  #for focus
    #starList.append(['JL_163',	 	'00:10:33.22', '-50:15:24.37'])

    #starList.append(['Naos', 		'08:03:34.99', '-40:00:11.0'])  #for focus
    #starList.append(['LSS_982', 	'08:10:31.72', '-40:32:47.07'])

    #starList.append(['Hya Xi', 	'11:32:59.78', '-31:51:28.2'])  #for focus
    #starList.append(['WD_1056-384', 	'10:58:23.72', '-38:44:01.19'])

    #starList.append(['Vel Mu', 	'10:46:46.35', '-49:23:13.9'])  #for focus
    #starList.append(['WD_1053-484', 	'11:56:11.43', '-48:40:03.3'])

    #starList.append(['Ara Beta', 	'17:25:17.96', '-55:31:48.0'])  #for focus
    starList.append(['LSE_259', 	'16:53:54.57', '-56:01:54.76'])


    G4pixel2deg = 0.15/ 3600.0
    G4pixel2asec = 0.15
    surf = cv2.SURF(20)

    for i in range(0, len(starList)):

	starName = starList[i][0]
	starRA   = starList[i][1]
	starDEC  = starList[i][2]

	self.log('I', 'Set position for %s' % starName)
	self.setValue('point','false','T0')

	mystar = None
	mystar = ep.FixedBody()
	mystar._ra = starRA
	mystar._dec = starDEC 

	alt, az = getAltAzi(mystar)

	if (alt < 4.5) or (alt > 76):
		self.log('I', 'Target out of range... alt: %4.3f' % alt)
	else:
		self.log('I', 'Alt: {0:4.3f}, Az: {1:4.3f}'.format(alt,az))
	   	self.setValue('TEL_','{} {}'.format(alt,az),'T0')	

		time.sleep(5.)

	    	while 'STOPPED' != self.getValue('status','T0'):
			time.sleep(0.1)

		self.setValue('point','true','T0')
		time.sleep(20.)

		image, e_time, chip_temp = self.gx_exposure(exp,5)
		filename = "%sG1_%s_%s_%.3f.png"%(__BASE_PATH__,now_str(e_time),starName,exp)

		alt_t, az_t = self.getPosTelAdj()
		corr_alt, corr_az = self.getPMCorr() 

		self.log('I', 'acquired %s' % filename)
		plt.imshow(image)
		plt.show()

		
		myFolder = '%s%s_samples_%s' % (__BASE_PATH__ ,starName,str(dt.datetime.now()).replace(' ','_'))

		try:
			os.mkdir(myFolder)
			workingFolder = myFolder+"/"
		except:
			self.log("W", "cant' create folder " % (myFolder))

	
		myFilter = 5
		while myFilter >= 2:

			self.log("I", "start filter %i" % myFilter)
		

			setFilt, focus_p = self.getFilterEnum(myFilter)
			self.setValue('filter', setFilt)

			tm.sleep(5)
		
		
			if myFilter == 1:
				exp_g4 = 100.
				n_exp = 10.
			
			elif myFilter == 2:
		                exp_g4 = 120.
				n_exp = 30.	
		
			elif myFilter == 6:
				exp_g4 = 30.
				n_exp = 5.
		        else:
				exp_g4 = 60.
				n_exp = 30.
			

			self.setValue('FOC_TAR', str(focus_p), 'F0')
			self.focsleep()

			i = n_exp
			while i > 0:
				self.shoot('%sG4_%s_%i'%(workingFolder,setFilt,i))
				i-=1

			tm.sleep(2)
	

			myFilter -= 1
		 	




	
			
	    

	    

   

a = Script()
a.run()

