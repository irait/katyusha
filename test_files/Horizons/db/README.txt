This directory contains the database of Horizons tables

Tables are organized in directories named as:

   Horizons_<start-date>_<end-date>

where 

   <start-date> is the date at which the tables in the subdirectory start
   <end-date> is the date at which the tables in the subdirectory ends

<dates> have the format YYYYMMDD so as an example 20161122

The tables in the directories are named as 

   observer_<atmosphere>_<body>.txt

where 
   <atmosphere> = refracted or airless
   <body> = sun, mercury, venus, moon, mars, jupiter, saturn 

Metadata are stored in db/metadata.csv 

To generate metadata use 

   >python dbscan.py db db/metadata.csv

from outside (cd ..) this directory

The program dbscan.py skips all the files and all the directories which do not complain with the format 
described above.

So as an example: _Horizons_20161122_20161231, _Horizons_20161122_20161231_new or README.txt are skipped.

The program dbscan.py must be runned each time new tables or directories are installed or updated and / or removed.

